import os

from fastapi import FastAPI

app = FastAPI(
    title="QA Automation Debug Service",
)

is_active = os.environ["IS_ACTIVE"]


def health_check_status():
    if is_active == "true":
        return 200


@app.get("/automation/healthz", status_code=health_check_status())
def health_check():
    ...
