import json
import os
from concurrent.futures import ThreadPoolExecutor

from chloe.shared.toolbox_integration.toolbox_api_client import ToolboxIntegration
from automation_sdk.atomic_types import UserCredentials

toolbox = ToolboxIntegration(
    UserCredentials(user_name="amit.aizenkot", password="amit_az3354")
)


def build_test_data():
    path = os.getcwd() + "/mock_data"
    files_list = []

    for file in os.listdir(path):
        path_to_file = path + "/" + file
        with open(path_to_file, "r", encoding="utf-8") as f:
            files_list.append(json.load(f))

    return files_list


def trigger_spoofer():
    print("triggers the spoofer")
    return toolbox.trigger_inference_results_spoofer()


def collect_results(mock_data: list):
    print("collecting results")
    with ThreadPoolExecutor() as executor:
        res = executor.map(toolbox.collect_inference_results, mock_data)
        for r in res:
            print(r)


if __name__ == "__main__":
    mock_data = build_test_data()
    trigger_spoofer()
    collect_results(mock_data)
