Feature: Predict Embryo PN

  @testrail_C6272
  Scenario Outline: Predict 0 PN
    Given slide id <slide_id>, well number <well_number> and run <run> from TLI <tli>
    When Predicting the embryo's PN
    Then The prediction is 0, like the correct amount


    Examples:
      | slide_id    | well_number | run | tli   |
      | testslide10 | 15          | 146 | vitro |
      | testslide11 | 7           | 150 | vitro |
      | testslide12 | 7           | 150 | vitro |
      | testslide13 | 5           | 150 | vitro |
      | testslide14 | 5           | 150 | vitro |


  @testrail_C6289
  Scenario Outline: Predict 1 PN
    Given slide id <slide_id>, well number <well_number> and run <run> from TLI <tli>
    When Predicting the embryo's PN
    Then The prediction is 1, like the correct amount


    Examples:
      | slide_id   | well_number | run | tli   |
      | testslide1 | 3           | 150 | vitro |
      | testslide2 | 1           | 150 | vitro |
      | testslide3 | 12          | 150 | vitro |
      | testslide4 | 5           | 150 | vitro |


  @testrail_C6290
  Scenario Outline: Predict 2 PN
    Given slide id <slide_id>, well number <well_number> and run <run> from TLI <tli>
    When Predicting the embryo's PN
    Then The prediction is 2, like the correct amount


    Examples:
      | slide_id   | well_number | run | tli   |
      | testslide5 | 2           | 150 | vitro |
      | testslide6 | 2           | 150 | vitro |
      | testslide7 | 9           | 150 | vitro |
      | testslide8 | 7           | 150 | vitro |
      | testslide9 | 1           | 150 | vitro |


  @testrail_C6288
  Scenario Outline: Predict 3+ PN
    Given slide id <slide_id>, well number <well_number> and run <run> from TLI <tli>
    When Predicting the embryo's PN
    Then The prediction is 3+, like the correct amount


    Examples:
      | slide_id    | well_number | run | tli   |
      | testslide15 | 10          | 120 | vitro |
      | testslide16 | 6           | 150 | vitro |
      | testslide17 | 13          | 150 | vitro |
      | testslide18 | 12          | 150 | vitro |
      | testslide19 | 1           | 150 | vitro |