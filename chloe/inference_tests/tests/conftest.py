import os

import pytest

from automation_sdk.console_logging.error import Error
from automation_sdk.console_logging.test_end import TestEnd
from automation_sdk.console_logging.test_start import TestStart
from test.shared_packages.reporting.allure_reporter import AllureReporter
from test.shared_packages.reporting.testrail_reporter import TestRailReporter


def pytest_runtest_logstart(nodeid: str, location):
    """Indicates the start of a new test session"""
    TestStart.log(message=location[2])


@pytest.hookimpl
def pytest_bdd_after_scenario(request, feature, scenario):
    scenario_report = request.node.__scenario_report__.serialize()

    for step in range(len(scenario_report["steps"])):
        if scenario_report["steps"][step]["failed"]:
            Error.log(
                f"failed cucumber Step is :{scenario_report['steps'][step].get('name')}"
            )
            break


@pytest.hookimpl
def pytest_collection_modifyitems(config, items):
    TestRailReporter.detect_testrail_markers_within_gherkin(items)


@pytest.hookimpl
def pytest_runtest_logfinish(nodeid: str, location):
    """Indicates the end of the test session"""
    TestEnd.log(message=location[2])


@pytest.hookimpl
def pytest_sessionfinish(session, exitstatus):
    """Makes allure report and send it to allure-UI after every test-session finished"""
    allure_supposed_path = os.getcwd() + "/allure-report"
    if os.path.isdir(allure_supposed_path):
        project_id = "pn-count-automation"
        AllureReporter(allure_supposed_path, project_id).make_report()
