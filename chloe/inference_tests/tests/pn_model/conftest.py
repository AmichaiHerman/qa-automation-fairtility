import pytest
from pytest_bdd import given, parsers, when, then

from chloe.shared.toolbox_integration.dtos.inputs import PNResultsRequest
from chloe.shared.toolbox_integration.dtos.outputs import PNSuccess
from chloe.shared.toolbox_integration.toolbox_api_client import ToolboxIntegration
from automation_sdk.atomic_types import UserCredentials


@pytest.fixture
def toolbox():
    return ToolboxIntegration(
        UserCredentials(user_name="amit.aizenkot", password="amit_az3354")
    )


@pytest.fixture
@given(
    parsers.parse(
        "slide id {slide_id}, well number {well_number} and run {run} from TLI {tli}"
    )
)
def given_slide(slide_id, well_number, run, tli) -> PNResultsRequest:
    return PNResultsRequest(slide_id=slide_id, well_es_id=well_number, run=run)


@pytest.fixture
@when("Predicting the embryo's PN")
def actual_pn_prediction(toolbox, given_slide) -> PNSuccess:
    return toolbox.collect_inference_results(given_slide)


@then(parsers.parse("The prediction is {actual_pn_amount}, like the correct amount"))
def validate_pn_count(actual_pn_prediction, actual_pn_amount):
    actual_pn_as_string = str(actual_pn_prediction.pn_count.value)
    assert actual_pn_as_string == actual_pn_amount
