from pytest_bdd import scenario

feature_file = "../../features/pn_model.feature"


@scenario(feature_name=feature_file, scenario_name="Predict 0 PN")
def test_predicting_0_pn():
    pass


@scenario(feature_name=feature_file, scenario_name="Predict 1 PN")
def test_predicting_1_pn():
    pass


@scenario(feature_name=feature_file, scenario_name="Predict 2 PN")
def test_predicting_2_pn():
    pass


@scenario(feature_name=feature_file, scenario_name="Predict 3+ PN")
def test_predicting_3_or_more_pn():
    pass
