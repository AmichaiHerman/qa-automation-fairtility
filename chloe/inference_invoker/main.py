import json
import os
from concurrent.futures import ThreadPoolExecutor
from typing import Generator

from chloe.shared.toolbox_integration.toolbox_api_client import ToolboxIntegration
from automation_sdk.atomic_types import UserCredentials


toolbox = ToolboxIntegration(
    UserCredentials(user_name="amit.aizenkot", password="amit_az3354")
)


def build_test_data():
    path = os.getcwd() + "/mock_data"
    files_list = []

    for file in os.listdir(path):
        path_to_file = path + "/" + file
        with open(path_to_file, "r", encoding="utf-8") as f:
            files_list.append(json.load(f))

    with ThreadPoolExecutor() as executor:
        res = executor.map(toolbox.create_inference_mock_data, files_list)
        return res


def push_to_topic(mock_data_gen: Generator):
    payloads = []
    for data in mock_data_gen:
        payloads.append(data.text)

    print("pushing messages to topic")
    with ThreadPoolExecutor() as executor:
        res = executor.map(toolbox.push_inference_message_to_topic, payloads)
        for r in res:
            print(r.text)


if __name__ == "__main__":
    mock_data_gen = build_test_data()
    push_to_topic(mock_data_gen)
