from chloe.shared.types.request_data import RequestPayload


class PNResultsRequest(RequestPayload):
    slide_id: str
    well_es_id: str
    run: str
