from pydantic import BaseModel

from chloe.shared.types.abstract_response import HttpResponse


class GCPPushResponse(HttpResponse):
    topic: str
    message_id: str


class PnCount(BaseModel):
    value: int
    time: str


class PNSuccess(HttpResponse):
    pn_count: PnCount
