from abc import ABC, abstractmethod
from enum import Enum
from typing import List

from pydantic import BaseModel


class TLIEnum(Enum):
    VITRO_11_CLASSIC = "vitro_11_classic"  # VITRO TLI WITH 11 FOCALS STARTING FROM -75
    VITRO_11_ALTERNATIVE = (
        "vitro_11_alternative"  # VITRO TLI WITH 11 FOCALS STARTING FROM -5
    )
    VITRO_7 = "vitro_7"  # VITRO TLI WITH 7 FOCALS
    GERI = "geri"


class TLIDto(BaseModel):
    instrument_type: str
    focal_plane: List[int]


class VITRO11A(TLIDto):  # VITRO TLI WITH 11 FOCALS STARTING FROM -75
    instrument_type: str = "VITRO"
    focal_plane: List[int] = [-75, -60, -45, -30, -15, 0, 15, 30, 45, 60, 75]


class VITRO11B(TLIDto):  # VITRO TLI WITH 11 FOCALS STARTING FROM -5
    instrument_type: str = "VITRO"
    focal_plane: List[int] = [-5, -4, -3, -2, -1, 0, 1, 2, 3, 4, 5]


class VITRO7(TLIDto):  # VITRO TLI WITH 7 FOCALS
    instrument_type: str = "VITRO"
    focal_plane: str = [-45, -30, -15, 0, 15, 30, 45]


class GERI(TLIDto):
    instrument_type: str = "GERI"
    focal_plane: str = [0]


class VendorNotExistException(Exception):
    pass


def map_tli(tli_enum: TLIEnum) -> TLIDto:
    if tli_enum is TLIEnum.VITRO_7:
        return VITRO7()
    elif tli_enum is TLIEnum.VITRO_11_CLASSIC:
        return VITRO11A()
    elif tli_enum is TLIEnum.VITRO_11_ALTERNATIVE:
        return VITRO11B()
    elif tli_enum is TLIEnum.GERI:
        return GERI()
    else:
        raise VendorNotExistException(
            "The Vendor You provided is not exist or not supported."
        )
