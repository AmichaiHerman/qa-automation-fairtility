import requests

from typing import Dict
from chloe.shared.auth.basic_auth import BasicAuth
from test.shared_packages.clients.http import make_request
from chloe.shared.toolbox_integration.dtos.inputs import PNResultsRequest
from chloe.shared.toolbox_integration.dtos.outputs import PNSuccess
from automation_sdk.atomic_types import UserCredentials


# todo add http_requests plugin to all of the routes


class ResultsNotFoundException(Exception):
    pass


class ToolboxIntegration:
    """
    An interface for connecting with QA Toolbox, Fairitlity's QA Tools Software
    """

    def __init__(self, toolbox_credentials: UserCredentials):
        self.auth_mechanism = BasicAuth(toolbox_credentials)
        self.auth = self.auth_mechanism()
        self.base_url = "http://vitro-simulator/api/v1/"
        self.gcp_push_service_url = self.base_url + "gcpush/"
        self.inference_results_service_url = self.base_url + "inference-results/"
        self.mock_factory_service_url = self.base_url + "mock-factory/"

    def create_inference_mock_data(self, inference_mock_data_payload: Dict):
        """A method that generates mock pub/sub inference messaeg"""
        return requests.post(
            url=self.mock_factory_service_url + "mock-inference-message",
            json=inference_mock_data_payload,
            auth=self.auth,
        )

    def push_inference_message_to_topic(self, push_request_payload: Dict):
        """A method that pushes a message to a gcp pub/sub topic"""
        return requests.post(
            url=self.gcp_push_service_url + "push-to-topic",
            data=push_request_payload,
            auth=self.auth,
        )

    def push_inference_message_to_topic(self, push_request_payload: Dict):
        """A method that pushes a message to a gcp pub/sub topic"""
        return requests.post(
            url=self.gcp_push_service_url + "push-to-topic",
            data=push_request_payload,
            auth=self.auth,
        )

    def trigger_inference_results_spoofer(self):
        """A method that finds inference live results"""
        resp = requests.get(
            url=self.inference_results_service_url + "spoof-messages", auth=self.auth
        )

        assert resp.status_code == 201, f"status code is {resp.status_code}"

        return resp

    @make_request(action="Predict PN", dto=PNSuccess)
    def collect_inference_results(self, inference_collection_payload) -> PNSuccess:
        """A method that finds inference live results"""
        return requests.post(
            url=self.inference_results_service_url + "pn-live-results",
            json=self.__figure_inference_payload_type(inference_collection_payload),
            auth=self.auth,
        )

    def __figure_inference_payload_type(self, payload):
        if isinstance(payload, PNResultsRequest):
            return payload.dict()
        return payload
