from pydantic import BaseModel


class RequestData(BaseModel):
    """An interface for declaring abstract data types, such as query params, responses and responses"""

    pass


class QueryParams(RequestData):
    pass


class RequestPayload(RequestData):
    pass
