# README #

### The Repository Contains: ###
Fairtility's Complete Automation Framework.

1. UI-Tests: Written in pytest-bdd + playwright
2. API-Tests - Written in pytest-bdd

### In order to onboard, you must have: ###
1. Python Version 3.8 or above
2. Install the requirements.txt file
3. for UI Tests, run ```playwright install```

### To Build And Run The Dockerfiles used for Jenkins Pipelines: ###
1. Training/CI Mode (Tests Run in parallel, reporting to allure) 
```docker compose build --build-arg training_mode="on" <test_service>``` and
```docker compose up <test-service>```
2. Nightly Mode (Tests Run sequentially, reports to test-rail and allure)
```docker compose build --build-arg training_mode="on" <test_service>``` and
```docker compose up <test-service>```



