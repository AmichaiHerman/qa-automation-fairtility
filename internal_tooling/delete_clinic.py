import subprocess
import json
import base64
import os

from bson import ObjectId
from pymongo import MongoClient

product = os.getenv("PRODUCT")


def get_secret(secretname, product):
    if product == "oq":
        product_prefix = "_OQ_"
    elif product == "eq":
        product_prefix = "_"
    output = subprocess.run([
        "kubectl",
        "get", "secret",
        secretname,
        "-n", product,
        "-o", "json",
    ], stdout=subprocess.PIPE, check=True).stdout.decode("utf-8")
    secret_json = json.loads(output)
    encoded_conn_str = secret_json["data"][f"MONGODB{product_prefix}LAB_CONNECTOR_HOST"]
    conn_str = base64.b64decode(encoded_conn_str).decode("utf-8").rsplit('/', 1)[0]
    return conn_str


conn_str = get_secret("blue-mongodb-credentials", "eq")
print(conn_str)


def delete_tenant_connector_user(tenant_id: str, oq_mongo_connection_string: str, color: str) -> str:
    client = MongoClient(oq_mongo_connection_string)

    db_tenants = client[f"{color}-tenantsDB"]
    db_lab_connector = client[f"{color}-labConnectorDB"]
    db_users = client[f"{color}-usersDB"]

    collection_tenants = db_tenants['tenants']
    collection_connectors = db_lab_connector['connectors']
    collection_users = db_users['users']

    result_tenants = collection_tenants.delete_many({"_id": ObjectId(tenant_id)})
    print(list(collection_connectors.find({"tenantId": tenant_id})))
    result_connectors = collection_connectors.delete_many({"tenantId": tenant_id})
    result_users = collection_users.delete_many({"tenants._id": tenant_id})

    print(
        f"Deleted {result_tenants.deleted_count} documents from 'tenants' collection in '{color}-tenantsDB' database.")
    print(
        f"Deleted {result_connectors.deleted_count} documents from 'connectors' collection in '{color}-labConnectorDB' database.")
    print(
        f"Deleted {result_users.deleted_count} documents from 'users' collection in '{color}-usersDB' database.")

    client.close()

