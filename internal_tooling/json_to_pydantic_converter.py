from typing import Union, Dict, List
from pydantic import BaseModel
from test.shared_packages.chloe_backend.patient_data.types.responses.kpi_dashboard_response import (
    Model,
    KpiDashboardResponse,
)
from test.shared_packages.chloe_backend.patient_data.types.responses.settings import (
    KpiSettings,
)


def convert_response_to_pydantic(
    json_response: Union[Dict, List[Dict]], response_object: BaseModel
):
    if isinstance(json_response, list):
        return [response_object(**data) for data in json_response]

    elif isinstance(json_response, dict):
        return response_object(**json_response)
