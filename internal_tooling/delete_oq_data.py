import os
from typing import List

from bson import ObjectId
from pymongo import MongoClient


def delete_oq_data(tenant_id: str, oq_mongo_db_connection_string: str, color: str) -> str:
    client = MongoClient(oq_mongo_db_connection_string)

    db_patient_data = client[f'{color}-oq-patientsDataDB']
    db_lab_connector = client[f'{color}-oq-labConnectorDB']

    query = {
        "tenantId": tenant_id
    }

    oocytes_collection = db_patient_data['oocytes']
    result_oocytes = oocytes_collection.delete_many(query)

    collection_collection = db_patient_data['collections']
    result_collection = collection_collection.delete_many(query)

    treatments_collection = db_lab_connector['treatments']
    result_treatments = treatments_collection.delete_many(query)

    print(
        f"Deleted {result_oocytes.deleted_count} documents from 'oocytes' collection in 'blue-oq-patientDataDB' database.")
    print(
        f"Deleted {result_collection.deleted_count} documents from 'collection' collection in 'blue-oq-patientDataDB' database.")
    print(
        f"Deleted {result_treatments.deleted_count} documents from 'treatments' collection in 'blue-oq-labConnectorDB' database.")




