from datetime import datetime
import sqlite3
from typing import Any


def current_datetime_to_unix() -> int:
    current_datetime = datetime.now()
    return int(current_datetime.timestamp())


def update_data(db_file_src: str, db_file_dst: str,
                table_dst: str, column_dst: str, filter_columns_dst: list, filter_values_dst: list,
                table_src: str, column_src: str,  filter_columns_src: list, filter_values_src: list):
    
    # Connect to the SQLite database
    conn_src = sqlite3.connect(db_file_src)
    if db_file_src == db_file_dst:
        conn_dst = conn_src
    else:
        conn_dst = sqlite3.connect(db_file_dst)

    try:
        # Create a cursors
        cursor_src = conn_src.cursor()
        cursor_dst = conn_dst.cursor()

        # Build the WHERE clause for filtering based on multiple columns with exact matches
        where_clause_src = " AND ".join([f"{col} = ?" for col in filter_columns_src])

        # Build and execute the SQL SELECT statement to fetch the existing value
        select_query = f"SELECT {column_src} FROM {table_src} WHERE {where_clause_src}"
        cursor_src.execute(select_query, filter_values_src)
        existing_value = cursor_src.fetchone()[0]  # Assuming only one row is expected

        # Print the found value (you can replace this with your logic)
        print(f"Found value in {table_src} on column {column_src}, value is: {existing_value} ")

        # Modify the value as needed
        modified_value = modify_function(existing_value)
        print(f"Modified value is: {modified_value}")

        # Build the WHERE clause for filtering based on multiple columns with exact matches
        where_clause_dst = " AND ".join([f"{col} = ?" for col in filter_columns_dst])

        # Build and execute the SQL UPDATE statement in the update table
        update_query = f"UPDATE {table_dst} SET {column_dst} = ? WHERE {where_clause_dst}"
        cursor_dst.execute(update_query, (modified_value,) + tuple(filter_values_dst))

        # Commit the changes
        conn_dst.commit()

        print(f"Data updated successfully in {table_dst}.")
    except sqlite3.Error as e:
        print(f"Error: {e}")
    finally:
        # Close the connection
        if conn_src or conn_dst:
            conn_src.close()
            conn_dst.close()


def add_value_to_all_column_rows(value_to_add: Any, file_path: str, column_name: str, table_name: str) -> None:
    conn_dst = sqlite3.connect(file_path)

    try:
        cursor_dst = conn_dst.cursor()
        add_query = f"UPDATE {table_name} SET {column_name} =  {column_name} + ?;"
        cursor_dst.execute(add_query, (value_to_add,))
        conn_dst.commit()
    except sqlite3.Error as e:
        print(f"Error: {e}")
    finally:
        conn_dst.close()


# Example usage:
if __name__ == "__main__":
    # Specify the SQLite database file path
    db_file_src = r"C:\Users\Shachaf.Mijiritsky\Desktop\1 patient 1 slide - event on day 7.6 - Copy\api-welcome-package-v1.3b- (1)\api-welcome-package-v1.3\Test Data\test\2000\000\D2000.01.01_S0000_I000.pdb"

    db_file_dst = r"C:\Users\Shachaf.Mijiritsky\Desktop\api-welcome-package-v1.3b- (1)\api-welcome-package-v1.3\Test Data\test\MainDB.fdb"

    # Specify the details for the update operation
    table_dst = 'GENERAL'
    column_dst = 'val'
    filter_columns_dst = ['par', 'ID']
    filter_values_dst = ['Fertilization', 'D2000.01.01_S0000_I000']

    # Specify columns and values for the WHERE clause in the search table
    table_src = 'GENERAL'
    column_src = 'val'
    filter_columns_src = ['par', 'ID']
    filter_values_src = ['Fertilization', 'D2000.01.01_S0000_I000']


    # Define a function to modify the value
    def modify_function(value):
        # Modify this function based on your logic
        a = float(current_datetime_to_unix() - float(value))
        return a


    # Call the update_data function
    # update_data(db_file_src, db_file_dst, table_dst, column_dst, filter_columns_dst, filter_values_dst,
    #             table_src, column_src, filter_columns_src, filter_values_src)

    add_value_to_all_column_rows(1, db_file_src, "Time", "IMAGES")
