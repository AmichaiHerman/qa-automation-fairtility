from datetime import datetime, timedelta
from typing import Type


def current_datetime_in_excel_time() -> float:
    current_date = datetime.now()
    excel_epoch = datetime(1899, 12, 30)
    days_difference = (current_date - excel_epoch).total_seconds() / (24 * 3600)
    print(f"current date in excle time is:{days_difference}")
    return days_difference


def convert_excel_to_date(excel_serial_number: float) -> datetime:
    excel_epoch = datetime(1899, 12, 30)
    real_date = excel_epoch + timedelta(days=excel_serial_number)
    return real_date


def convert_date_to_excel(date: str) -> float:
    datetime_obj = convert_string_to_datetime(date)
    excel_epoch = datetime(1899, 12, 30)
    excel_time = (datetime_obj - excel_epoch).total_seconds() / (24 * 3600)
    return excel_time


def convert_string_to_datetime(date_str: str) -> datetime:
    date_format = '%Y-%m-%d %H:%M:%S.%f'
    date_time_obj = datetime.strptime(date_str, date_format)
    return date_time_obj


def add_num_hours_to_excel_time_number(num_hours: float, excel_num: float) -> float:
    date_str = convert_excel_to_date(excel_num)
    result_date = date_str + timedelta(hours=num_hours)
    result_excel_date = convert_date_to_excel(str(result_date))
    print(f"Result time is: {result_excel_date}")
    print(f"diff to add is: {result_excel_date-excel_num}")
    return result_excel_date


def hours_diff(bigger_excel_time: float, lesser_excel_time: float) -> float:
    diff = convert_excel_to_date(bigger_excel_time) - convert_excel_to_date(lesser_excel_time)
    diff_delta_hours = (diff.days * 24 + diff.seconds // 3600) - (diff.days * 24)

    print(f"diff is: {diff.days} days and {diff_delta_hours} hours")
    return diff.days


print(convert_excel_to_date(43466.630341794))
print(convert_excel_to_date(43466.6340573958))
