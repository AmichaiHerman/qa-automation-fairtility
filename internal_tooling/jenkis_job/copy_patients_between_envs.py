import os

from pymongo import MongoClient
from bson.objectid import ObjectId
from pprint import pprint


# UPDATE THE VIDEOS TO BE COPIED TO COPY ALL VIDEOS AND NOT ONLY 0
# UPDATE THE INSTRUMENTS COPY TO HANDLE MULTIPLE SLIDES PER INSTRUMENT


if os.getenv('SRC_PATIENT_DATA_DB_COLOR'):
    src_patient_data_db = f"{os.getenv('SRC_PATIENT_DATA_DB_COLOR')}-patientDataDB"
else:
    src_patient_data_db = f"patientDataDB"

print(f"src_patient_data_db is : {src_patient_data_db}")

dest_patient_data_db = f"{os.getenv('DST_PATIENT_DATA_DB_COLOR')}-patientDataDB"


image_urls = []
failed_image_urls = []
storage_paths = []

src_tenant = os.getenv('SRC_TENANT')
dest_tenant = os.getenv('DST_TENANT')
src_mongo_client = MongoClient(os.getenv('SRC_MONGO_SRV'))
dest_mongo_client = MongoClient(os.getenv("DST_MONGO_SRV"))

src_tenants_col = src_mongo_client['tenantsDB']['tenants']
dst_tenants_col = dest_mongo_client[f"{os.getenv('DST_PATIENT_DATA_DB_COLOR')}-tenantsDB"]['tenants']

result = src_tenants_col.find_one({"_id": ObjectId(src_tenant)})
if not result:
    print("error: failed to find source tenant: {src_tenant}")
    exit(1)

result = dst_tenants_col.find_one({"_id": ObjectId(dest_tenant)})
if not result:
    print("error: failed to find destination tenant: {dest_tenant}")
    exit(1)


def copy_patients():
    """
    """
    src_patients_col = src_mongo_client[src_patient_data_db]['patients']
    dest_patients_col = dest_mongo_client[dest_patient_data_db]['patients']
    # if I want to copy a specific patiet - specify it's obj ID - patients = list(src_patients_col.find({"tenantId":src_tenant, "_id": ObjectId('649d7c1ec26b0ee42800b5be')}))
    patients = list(src_patients_col.find({"tenantId": src_tenant}))
    for patient in patients:
        patient["tenantId"] = dest_tenant
        try:
           dest_patients_col.insert_one(patient)
        except Exception as e:
          print(f"error: failed to insert patients :{patients} reason: {str(e)}")
    return patients


def copy_slides(copied_patients):
    """
    """
    patient_ids = list(map(lambda p: str(p["_id"]),copied_patients))
    src_slides_col = src_mongo_client[src_patient_data_db]['slides']
    slides = list(src_slides_col.find({"tenantId":src_tenant, "patientId":{"$in":patient_ids}}))
    for slide in slides:
        slide['status'] = "IN_PROGRESS" #verify the slide is copied to the ongoing state and not to finished.
        slide["tenantId"] = dest_tenant
    dest_slides_col = dest_mongo_client[dest_patient_data_db]['slides']
    for slide in slides:
        try:
            dest_slides_col.insert_one(slide)

        except Exception as e:
            print('slide already exists', slide)
    # dest_slides_col.insert_many(slides)
    return slides


def copy_wells(copied_patients):
    """
    """
    patient_ids = list(map(lambda p: str(p["_id"]),copied_patients))
    src_wells_col = src_mongo_client[src_patient_data_db]['wells']
    wells = list(src_wells_col.find({"tenantId":src_tenant, "patientId":{"$in":patient_ids}}))
    for well in wells:
        well["tenantId"] = dest_tenant
    dest_wells_col = dest_mongo_client[dest_patient_data_db]['wells']
    dest_wells_col.insert_many(wells)
    return wells


def copy_instruments(copied_slides):
    """
    """
    instrument_ids = list(map(lambda s: ObjectId(s['instrumentId']), copied_slides))
    src_instruments_col = src_mongo_client[src_patient_data_db]['instruments']
    instruments = list(src_instruments_col.find({"tenantId": src_tenant, "_id": {"$in": instrument_ids}}))
    pprint(instruments)
    dest_instruments_col = dest_mongo_client[dest_patient_data_db]['instruments']
    for instrument in instruments:
        try:
            instrument["tenantId"] = dest_tenant
            dest_instruments_col.insert_one(instrument)
        except Exception as e:
            print(f"error: failed to insert instrument {str(instrument['_id'])}, :{str(e)} " )
    # dest_instruments_col.insert_many(instruments)


def copy_patients_between_envs():
    """
    This helper copies kidplus data of a tenant from 1 env to another.

    patientDataDB
        instruments
        patients
        slides
        wells - entities here contain env specific fields such as lastImageUrl
    """
    print("data copy started")

    copied_patients = copy_patients()
    copied_slides = copy_slides(copied_patients)
    copied_wells = copy_wells(copied_patients)
    copy_instruments(copied_slides)


copy_patients_between_envs()
