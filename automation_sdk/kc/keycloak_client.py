import os
from keycloak import KeycloakOpenID
from automation_sdk.atomic_types import UserCredentials
from automation_sdk.kc import keycloak_configs
from automation_sdk.kc.keycloak_response import KeycloakResponse


class KeycloakClient:
    def __init__(self, credentials: UserCredentials):
        self.__base_url = os.getenv("KEYCLOAK_BASE_URL")
        self.__creds = credentials
        self.__client_id_eq = keycloak_configs.KEYCLOAK_CLIENT_ID_EQ
        self.__client_id_oq = keycloak_configs.KEYCLOAK_CLIENT_ID_OQ
        self.__realm_name = keycloak_configs.KEYCLOAK_REALM_ID

    def create_header(self):
        return {"Authorization": "Bearer " + self.get_jwt()}

    def get_jwt(self) -> str:
        keycloak_instance = self.create_keycloak_instance()
        response_as_dict = self.fire_keycloak_token_request(keycloak_instance)
        return KeycloakResponse.from_dict(response_as_dict).access_token

    def fire_keycloak_token_request(self, instance: KeycloakOpenID) -> dict:
        return instance.token(
            username=self.__creds.user_name, password=self.__creds.password
        )

    def create_keycloak_instance(self) -> KeycloakOpenID:
        return KeycloakOpenID(
            server_url=self.__base_url,
            client_id=self.__client_id_eq,
            realm_name=self.__realm_name,
        )
