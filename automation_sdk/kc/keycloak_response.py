from dataclasses import dataclass, field
from dataclasses_json import dataclass_json, config


@dataclass_json
@dataclass
class KeycloakResponse:
    access_token: str
    expires_in: int
    refresh_expires_in: int
    refresh_token: str
    token_type: str
    not_before_policy: str = field(metadata=config(field_name="not-before-policy"))
    session_state: str
    scope: str
