import os
from enum import Enum


class VideoNotFoundException(Exception):
    ...


class VideoQualities(Enum):
    HIGH_QUALITY = "hq"
    LOW_QUALITY = "lq"


class VideosFetcher:
    def __init__(self, focal: int, root_dir: str):
        self._focal = focal
        self._root = root_dir
        self._video_files = os.listdir(self._root)

    def fetch_hq_video(self) -> str:
        return self.__fetch_video(VideoQualities.HIGH_QUALITY)

    def fetch_lq_video(self) -> str:
        return self.__fetch_video(VideoQualities.LOW_QUALITY)

    def __fetch_video(self, quality: VideoQualities) -> str:
        for file in self._video_files:
            focal_in_file = str(self._focal) in file
            quality_in_file = quality.value in file
            if focal_in_file and quality_in_file:
                return self._root + file
        raise VideoNotFoundException(
            f"{quality.value.upper()} Video For Focal {self._focal} not found"
        )
