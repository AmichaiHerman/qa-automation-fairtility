from typing import Any

import cv2


class VideoCompare:
    def __init__(self, video1_path, video2_path):
        self._video1 = self.__init_video(video1_path)
        self._video2 = self.__init_video(video2_path)

    def compare_frames_amount(self) -> bool:
        frame_count_hq = self.__count_frames(self._video1)
        frame_count_lq = self.__count_frames(self._video2)
        return frame_count_hq == frame_count_lq

    def asses_frame_inclusion(self) -> bool:
        while self._video1:
            success1, frame1 = self._video1.read()
            success2, frame2 = self._video2.read()

            if not success1 or not success2:
                break

            if frame1.tobytes() != frame2.tobytes():
                return False
        return True

    def __count_frames(self, video: Any) -> int:
        return int(video.get(cv2.CAP_PROP_FRAME_COUNT))

    def __init_video(self, path: str):
        return cv2.VideoCapture(path)
