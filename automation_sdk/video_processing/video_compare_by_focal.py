import os

from automation_sdk.video_processing.video_fetcher import VideosFetcher
from automation_sdk.video_processing.videos_compare import VideoCompare


class VideoComparePerFocal:
    """A Util That Compares 2 Videos"""

    def __init__(self, focal: int, root_dir: str):
        self._video_fetcher = VideosFetcher(focal, root_dir)
        self._hq_video_path = self._video_fetcher.fetch_hq_video()
        self._lq_video_path = self._video_fetcher.fetch_lq_video()
        self._videos_compare = VideoCompare(self._hq_video_path, self._lq_video_path)

    def compare_video_sizes(self) -> bool:
        hq_size = os.stat(self._hq_video_path).st_size
        lq_size = os.stat(self._lq_video_path).st_size
        return hq_size == lq_size

    def compare_frames_amount(self) -> bool:
        return self._videos_compare.compare_frames_amount()

    def asses_frame_inclusion(self) -> bool:
        return self._videos_compare.asses_frame_inclusion()
