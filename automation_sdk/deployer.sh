# PLS Delete The Package Current version from the GCP Artifact registry, prior to running the script


#delete previous version dist

echo "installing requirements"
pip3 install keyring
pip3 install keyrings.google-artifactregistry-auth


echo "removing old version distribution files/folders"

rm -r dist/
rm -r build/
rm -r automation_sdk/
rm -r automation_sdk.egg-info/


echo "building new wheel"

#build new wheel
python3 setup.py bdist_wheel


echo "deploying package to GCP Artifact Registry"

#deploy package to GCP Artifact Registry
twine upload --repository-url https://us-central1-python.pkg.dev/kidplusdev/automation-sdk/ --verbose dist/*

