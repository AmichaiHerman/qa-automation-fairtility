from typing import Dict, Any, Optional

import requests
from pydantic import BaseModel
from requests import Response

from automation_sdk.http_requests.request_util import set_payload


class HttpClient:
    """A Simple Http Client For Connecting Web Servers"""

    session = requests.session()

    @classmethod
    def get(cls, url: str, headers: Optional[Dict]) -> Response:
        """Fires A GET Request"""
        with cls.session as session:
            return session.get(url=url, headers=headers)

    @classmethod
    def post(
        cls,
        url: str,
        payload: Optional[BaseModel],
        headers: Dict,
        cut_pydantic_root: bool = False,
    ) -> Response:
        """Fires A POST Request"""
        with cls.session as session:
            return session.post(
                url=url,
                json=set_payload(payload, cut_pydantic_root=cut_pydantic_root),
                headers=headers,
            )

    @classmethod
    def patch(cls, url: str, payload: Any, headers: Dict) -> Response:
        """Fires A PATCH Request"""
        with cls.session as session:
            return session.patch(url=url, json=payload.dict(), headers=headers)

    @classmethod
    def delete(cls, url: str, headers: Optional[Dict]) -> Response:
        """Fires A DELETE Request"""
        with cls.session as session:
            return session.delete(url=url, headers=headers)
