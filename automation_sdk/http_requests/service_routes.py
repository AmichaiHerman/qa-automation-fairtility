import os
from abc import ABC
from requests import Response
from automation_sdk.auth.keycloak_auth import KeycloakAuth
from automation_sdk.atomic_types import UserCredentials
from typing import Dict


class ServiceRoutes(ABC):
    def __init__(self, creds: UserCredentials):
        self._auth = KeycloakAuth(credentials=creds)
        self._token: Dict = self._auth()
        self._base_url = os.getenv("CHLOE_BASE_URL")
        self._oq_base_url = os.getenv("CHLOE_OQ_BASE_URL")

    def check_health(self) -> Response:
        """Check the health of a service (smoke-tests) by peforming health-check"""
        ...



