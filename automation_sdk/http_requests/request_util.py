from typing import Any, Optional
from pydantic import ValidationError, BaseModel
from requests import Response

from automation_sdk.console_logging.debug import Debug

# Util For Handling Http Requests and responses


def set_payload(
    given_payload: Optional[BaseModel], cut_pydantic_root: bool = False
) -> Any:
    """Converts The Given Payload DTO to json-like object, or to nothing"""
    if given_payload is not None:
        if cut_pydantic_root:
            return given_payload.dict()["__root__"]
        else:
            return given_payload.dict()
    else:
        return given_payload


def make_request(action: str, dto: Optional[Any] = None):
    """
    A Wrapper Func for handling incoming http_requests requests

    :param action => the operation made, for logging
    :param dto => the required dto to deserialize the response to
    """

    def execution_wrapper(http_request):
        def handle(*args, **kwargs):
            Debug.log(f"Trying To {action}")
            response: Response = http_request(*args, **kwargs)
            if dto:
                return handle_deserialization(response, dto)
            return response
        return handle
    return execution_wrapper


# todo improve impl
def handle_deserialization(response: Response, dto: Optional[Any]) -> Any:
    response_json = response.json()
    if dto is not None:
        try:
            if isinstance(response_json, list):
                return [dto(**item) for item in response_json]
            return dto(**response_json)
        except ValidationError as e:  # Which Means Serialization Failed Because The Response Don't Match DTO Pattern
            Debug.log(f"serialization failed with error {e}")
            return e

    return response.json()
