from enum import Enum

from pydantic import BaseModel


class AtomicTypes(BaseModel):
    pass


class UserCredentials(AtomicTypes):
    user_name: str
    password: str


class ConnectorType(Enum):
    AGENT = "agent"
    LAB_CONNECTOR = "lab_connector"
