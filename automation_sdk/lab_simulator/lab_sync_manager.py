from automation_sdk.lab_simulator.connector_activation_service import (
    ConnectorActivationService,
)
from automation_sdk.lab_simulator.lab_simulator_client import LabSimulatorClient


class LabSyncManager:
    """A Module that performs lab-sync operations for the spec tests"""

    def __init__(self):
        self._lab_simulator_client = LabSimulatorClient()

    def sync(
        self, scenario, tenant_id, clear_tenant: bool, with_slides_sync: bool = False
    ) -> None:
        """Perform The Actual Syncing operation"""
        source = "lab-connector"
        self._lab_simulator_client.activate(source, scenario)
        self.__execute_cycle(source, tenant_id, clear_tenant, with_slides_sync)

    def stop_active_syncs(self) -> None:
        """Cleaning active syncs that being left open - used for test setup/teardown"""
        self._lab_simulator_client.deactivate_if_still_open()

    def __execute_cycle(
        self, source, tenant_id, clear_tenant: bool, with_slides_sync: bool = False
    ) -> None:
        """Call the actual activation service that manages the state of the connector on MongoDB"""
        if source == "lab-connector":
            cas = ConnectorActivationService(tenant_id)
            cas.run_cycle(clear_tenant, with_slides_sync)
        else:
            return
