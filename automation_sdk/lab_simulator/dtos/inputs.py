from pydantic import BaseModel


class ChosenScenario(BaseModel):
    source: str
    scenario: str


class ControlledActivationRequest(BaseModel):
    connection_string: str
    connector_id: str
    tenant_id: str
    reset_tenant: bool
    reset_connector: bool
