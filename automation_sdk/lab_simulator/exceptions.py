class ConnectorNotFoundException(Exception):
    """Exception raised when a connector is not found"""

    ...


class SimulatorCallException(Exception):
    """Exception raised when a simulator call fails for some reason"""

    ...
