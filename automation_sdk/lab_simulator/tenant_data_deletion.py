from automation_sdk.mongo.repos.agent_handler_repo import AgentHandlerRepo
from automation_sdk.mongo.repos.patient_data_repo import PatientDataRepo
from automation_sdk.console_logging.debug import Debug
from automation_sdk.mongo.repos.slide_revision_manager_repo import (
    SlideRevisionManagerRepo,
)


class TenantDataDeletionService:
    """A Service That is responsible for deleting tenant-data from MongoDB"""

    def __init__(self, tenant_id: str):
        self._tenant_id = tenant_id
        self._patient_data_repo = PatientDataRepo()
        self._slide_revision_manager_repo = SlideRevisionManagerRepo()
        self._agent_handler_repo = AgentHandlerRepo()

    def clear_data(self, hard_delete: bool = False) -> None:
        self._agent_handler_repo.delete_tenant_data(self._tenant_id)
        if hard_delete:
            self._patient_data_repo.delete_tenant_data(self._tenant_id)
            self._slide_revision_manager_repo.delete_tenant_data(self._tenant_id)
        Debug.log("Tenant Data Deleted")
