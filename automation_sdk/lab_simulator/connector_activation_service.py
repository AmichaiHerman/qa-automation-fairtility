from automation_sdk.lab_simulator.tenant_data_deletion import TenantDataDeletionService
from automation_sdk.mongo.repos.lab_connector_repo import LabConnectorRepo
from automation_sdk.console_logging.debug import Debug


class ConnectorActivationService:
    """A service that's responsible for controlling connector state"""

    def __init__(self, tenant_id: str) -> None:
        self._tenant_id = tenant_id
        self._lab_connector_repo = LabConnectorRepo()

    def run_cycle(
        self, clear_tenant: bool = True, with_slides_sync: bool = False
    ) -> None:
        self.__reset_connector()
        self.__clear_tenant_data() if clear_tenant else None
        self._lab_connector_repo.activate_connector(self._tenant_id)

        if with_slides_sync:
            self._lab_connector_repo.is_slides_sync_done(self._tenant_id)
        self._lab_connector_repo.is_patient_sync_done(self._tenant_id)
        self.__stop_connector()

    def __reset_connector(self) -> None:
        self._lab_connector_repo.reset_connector(self._tenant_id)

    def __clear_tenant_data(self) -> None:
        TenantDataDeletionService(self._tenant_id).clear_data()

    def __stop_connector(self) -> None:
        self._lab_connector_repo.deactivate_connector(self._tenant_id)
        Debug.log("Connector Stopped")
