import requests

from automation_sdk.lab_simulator.exceptions import SimulatorCallException
from automation_sdk.console_logging.debug import Debug


class LabSimulatorClient:
    """A Service that controls lab-simulator activation/deactivation"""

    def __init__(self):
        self._service_base_url = "http://10.0.16.20:3000/simulator/"

    def activate(self, source: str, scenario: str) -> None:
        """Activate the simulator for the chosen scenario and source"""
        payload = {"source": source, "scenario": scenario}
        Debug.log(f"Sending payload: {payload}")


        resp = requests.post(url=self._service_base_url + "activate", json=payload)

        # validate connection works properly
        if resp.status_code != 201:
            raise SimulatorCallException(f"Connection Refused {resp.text}")

        # Check the "message" field of the response
        if (
            resp.json()["message"]
            != f"server started successfully for scenario {scenario}"
        ):
            raise SimulatorCallException(
                f"Unexpected response from server: {resp.json()} while trying to activate it"
            )
        else:
            Debug.log(f"Simulator Activated with scenario {scenario}")

    def deactivate_if_still_open(self) -> None:
        """Checks if the server is on and turn it off"""
        if self.__deactivate():
            Debug.log("Leftover Simulator Is Down")
        else:
            Debug.log("No Leftover simulator remained open, continuing test")

    def __deactivate(self) -> bool:
        """Deactivates the simulator"""
        resp = requests.post(url=self._service_base_url + "deactivate")
        # validate connection works properly
        if resp.status_code != 200:
            raise SimulatorCallException(f"Connection Refused {resp.text}")
        # validate simulator activated
        if resp.json()["message"] == "server deactivated successfully":
            return True
        return False
