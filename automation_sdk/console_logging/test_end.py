from automation_sdk.console_logging.console_logger import (
    ConsoleLogger,
)
from automation_sdk.console_logging.colors import Colors


class TestEnd(ConsoleLogger):
    @classmethod
    def log(cls, message: str) -> None:
        color = Colors.OKBLUE.value
        print(f"{color} # Test End: {message}{color}")
