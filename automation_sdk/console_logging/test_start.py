from automation_sdk.console_logging.console_logger import (
    ConsoleLogger,
)
from automation_sdk.console_logging.colors import Colors


class TestStart(ConsoleLogger):
    @classmethod
    def log(cls, message: str) -> None:
        color = Colors.OKBLUE.value
        print(f"{color} # Test Start: {message}{color}")
