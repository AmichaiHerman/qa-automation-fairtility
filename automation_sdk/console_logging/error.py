from automation_sdk.console_logging.console_logger import (
    ConsoleLogger,
)
from automation_sdk.console_logging.colors import Colors


class Error(ConsoleLogger):
    @classmethod
    def log(cls, message: str) -> None:
        color = Colors.FAIL.value
        print(f"{color} # {message}{color}")
