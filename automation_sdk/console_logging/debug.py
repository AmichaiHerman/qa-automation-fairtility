from automation_sdk.console_logging.console_logger import (
    ConsoleLogger,
)
from automation_sdk.console_logging.colors import Colors


class Debug(ConsoleLogger):
    @classmethod
    def log(cls, message: str) -> None:
        color = Colors.OKGREEN.value
        print("\n" + f"ACTION:{color} {message}{color}")
