from enum import Enum


class Colors(Enum):
    OKBLUE = "\033[94m"
    OKGREEN = "\033[92m"
    LIGHTGREY = "\033[37m"
    FAIL = "\033[91m"
