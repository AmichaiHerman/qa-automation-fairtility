from abc import ABC, abstractmethod


class ConsoleLogger(ABC):
    """Defines an interface for console logging"""

    @abstractmethod
    def log(self, message: str) -> None:
        pass
