from typing import List

from automation_sdk.mongo.mongo_orm.agent_handler.patientdatas import Patientdatas
from automation_sdk.mongo.mongo_orm.agent_handler.settings import Settings
from automation_sdk.mongo.mongo_orm.agent_handler.wells import Wells
from automation_sdk.mongo.mongo_types.schemas.agent_handler.agent_handler_scheme import (
    AgentHandlerScheme,
)
from automation_sdk.mongo.repos.base_db import BaseRepo


class AgentHandlerRepo(BaseRepo):
    def __init__(self):
        self.__scheme = AgentHandlerScheme()

    def find_patients_data_by_tenant(self, tenant_id: str) -> List[Patientdatas]:
        return self.__scheme.patientdatas.find_all_documents.with_tenant_id(
            tenant_id
        ).serialize_to(Patientdatas)

    def find_wells_by_tenant(self, tenant_id: str) -> List[Wells]:
        return self.__scheme.wells.find_all_documents.with_tenant_id(
            tenant_id
        ).serialize_to(Wells)

    def find_single_well(self, tenant_id: str) -> Wells:
        return self.__scheme.wells.find_one_document.with_tenant_id(
            tenant_id
        ).serialize_to(Wells)

    def find_clinic_settings(self, tenant_id: str) -> Settings:
        return self.__scheme.settings.find_one_document.with_tenant_id(
            tenant_id
        ).serialize_to(Settings)

    def add_clinic_settings(self, settings: Settings) -> bool:
        return self.__scheme.settings.insert_one_document.with_objects(settings)

    def update_invoke_pipeline_every(self, tenant_id: str, number_of_runs: int) -> bool:
        return self.__scheme.settings.update_document.with_tenant_id(
            tenant_id
        ).with_new_values(
            invokePipeLineEveryNRun=number_of_runs,
        )

    def import_well_data(self, well_data: Wells) -> bool:
        return self.__scheme.wells.insert_one_document.with_objects(well_data)

    def update_well_runs(self, tenant_id: str, runs: List[int], times: List[int]):
        return self.__scheme.wells.update_document.with_tenant_id(
            tenant_id
        ).with_new_values(runs=runs, times=times)

    def delete_tenant_data(self, tenant_id: str) -> bool:
        col_list = [
            self.__scheme.wells,
            self.__scheme.patientdatas,
            self.__scheme.settings,
        ]
        delete_statuses = [
            col.delete_all_documents.with_tenant_id(tenant_id) for col in col_list
        ]
        return all(delete_statuses)
