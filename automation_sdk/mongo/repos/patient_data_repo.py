from datetime import datetime
from typing import List, Optional

from automation_sdk.mongo.mongo_orm.patient_data.instrument import Instrument
from automation_sdk.mongo.mongo_orm.patient_data.patient import Patients
from automation_sdk.mongo.mongo_orm.patient_data.slides import PDSlides
from automation_sdk.mongo.mongo_orm.patient_data.wells import PDWells
from automation_sdk.mongo.mongo_types.schemas.patient_data.patient_data_scheme import (
    PatientDataScheme, PatientDataSchemeProductionEu
)
from automation_sdk.mongo.repos.base_db import BaseRepo
from test.pn_model_tests.dtos.pn_model_dto import InferenceDataPNModelEqScore, PNWell


class PatientDataRepo(BaseRepo):
    """Component encapsulating patients data repo"""

    def __init__(self, is_proudction: bool = False):
        if is_proudction:
            self.__scheme = PatientDataSchemeProductionEu()
        else:
            self.__scheme = PatientDataScheme()
        self.__col_list = [
            self.__scheme.patients,
            self.__scheme.slides,
            self.__scheme.wells,
            self.__scheme.instruments
        ]

    def find_patients(self, tenant_id: str) -> List[Patients]:
        return self.__scheme.patients.find_all_documents.with_tenant_id(
            tenant_id
        ).serialize_to(Patients)

    def find_patient_by_patient_es_id_and_tenant(self, patient_es_id: str, tenant_id: str) -> Patients:
        return self.__scheme.patients.find_one_document.with_patient_es_id_and_tenant(
            patient_es_id,
            tenant_id
        ).serialize_to(Patients)

    def find_instruments(self, tenant_id: str) -> List[Instrument]:
        return self.__scheme.instruments.find_all_documents.with_tenant_id(
            tenant_id
        ).serialize_to(Instrument)

    def find_slides(self, tenant_id: str) -> List[PDSlides]:
        return self.__scheme.slides.find_all_documents.with_tenant_id(
            tenant_id
        ).serialize_to(PDSlides)

    def find_slides_by_treatment_id(self, treatment_id: str) -> List[PDSlides]:
        return self.__scheme.slides.find_all_documents.with_custom_query(
            custom_key="treatmentId", custom_value=treatment_id
        ).serialize_to(PDSlides)

    def find_slide_by_patient_id_and_tenant_id(self, patient_es_id: str, tenant_id: str) -> List[PDSlides]:
        return self.__scheme.slides.find_one_document.with_patient_id_and_tenant(
            patient_es_id, tenant_id
        ).serialize_to(PDSlides)

    def find_well_by_id(self, well_id: str) -> PDWells:
        return self.__scheme.wells.find_one_document.with_mongo_id(
            well_id
        ).serialize_to(PDWells)

    def find_pn_model_well_by_patient_id_and_es_id(self, patient_id: str, well_es_id: str) -> PDWells:
        return self.__scheme.wells.find_one_document.with_patient_id_and_well_es_id(
            patient_id,
            well_es_id
        ).serialize_to(PNWell)

    def find_wells(self, tenant_id: str) -> list[PDWells]:
        return self.__scheme.wells.find_all_documents.with_tenant_id(
            tenant_id
        ).serialize_to(PDWells)

    def find_pn_model_wells_by_tenant_id(self, tenant_id: str) -> list[PNWell]:
        return self.__scheme.wells.find_all_documents.with_tenant_id(
            tenant_id
        ).serialize_to(PNWell)

    def find_pn_model_wells_by_patient_id_and_well_es_id(self, patient_es_id: str, well_es_id) -> list[PNWell]:
        return self.__scheme.wells.find_all_documents.with_patient_es_id_and_well_es_id(
            patient_es_id,
        ).serialize_to(PNWell)

    def find_wells_by_treatment(self, treatment_id: str) -> List[PDWells]:
        return self.__scheme.wells.find_all_documents.with_custom_query(
            custom_key="treatmentId", custom_value=treatment_id
        ).serialize_to(PDWells)

    def add_patient(self, patient: Patients) -> bool:
        return self.__scheme.patients.insert_one_document.with_objects(patient)

    def add_many_patients(self, patients: List[Patients]) -> bool:
        return self.__scheme.patients.insert_multiple_documents.with_objects(patients)

    def import_well(self, doc: PDWells) -> bool:
        return self.__scheme.wells.insert_one_document.with_objects(doc)

    def delete_tenant_data(self, tenant_id: str) -> bool:
        delete_statuses = [
            col.delete_all_documents.with_tenant_id(tenant_id)
            for col in self.__col_list
        ]
        return all(delete_statuses)

    def update_slide_fertilization_date(
        self, es_id: str, fertilization_date: datetime
    ) -> bool:
        return self.__scheme.slides.update_document.with_custom_kv(
            key="esId", value=es_id
        ).with_new_values(fertilizationDate=fertilization_date)
