from abc import ABC, abstractmethod


class BaseRepo(ABC):
    @abstractmethod
    def delete_tenant_data(self, tenant_id: str) -> bool:
        """Deletes Tenant Data From Applicative Collections"""
        ...
