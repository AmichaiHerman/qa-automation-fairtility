from typing import List, Union

from automation_sdk.mongo.mongo_orm.qa_repo.embryo_gt_data import EmbryoGTData
from automation_sdk.mongo.mongo_types.schemas.qa.qa_scheme import QAScheme
from automation_sdk.mongo.repos.base_db import BaseRepo


class QARepo(BaseRepo):
    def __init__(self):
        self.__scheme = QAScheme()

    def import_embryo_gt_data(
        self,
        data: Union[EmbryoGTData, List[EmbryoGTData]],
        ignore_excpetions: bool = False,
    ) -> bool:
        return self.__scheme.embryoGT.insert_one_document.with_objects(
            data, ignore_exceptions=ignore_excpetions
        )

    def find_by_well_url(self, well_url: str) -> EmbryoGTData:
        return self.__scheme.embryoGT.find_one_document.with_custom_query(
            custom_key="well_url", custom_value=well_url
        ).serialize_to(EmbryoGTData)

    def delete_by_well_url(self, well_url: str) -> bool:
        return self.__scheme.embryoGT.delete_all_documents.with_custom_query(
            custom_key="well_url", custom_value=well_url
        )

    def delete_tenant_data(self, tenant_id: str):
        raise Exception("QA Repo Doesn't use tenant ID")
