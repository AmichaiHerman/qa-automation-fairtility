from automation_sdk.mongo.mongo_types.schemas.slide_revision_manager.srm_scheme import (
    SRMScheme,
)
from automation_sdk.mongo.repos.base_db import BaseRepo


class SlideRevisionManagerRepo(BaseRepo):
    """Component encapsulating slide revision manager repo"""

    def __init__(self):
        self.__scheme = SRMScheme()

    # todo add adding / finding logics

    def delete_tenant_data(self, tenant_id: str) -> bool:
        collections = [self.__scheme.slides, self.__scheme.wells]
        delete_statuses = [
            col.delete_all_documents.with_tenant_id(tenant_id) for col in collections
        ]
        return all(delete_statuses)
