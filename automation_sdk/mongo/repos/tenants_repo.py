import pytest


from automation_sdk.mongo.mongo_types.schemas.tenants.tenants_scheme import (
    TenantsScheme,
)
from automation_sdk.mongo.repos.base_db import BaseRepo
from test.shared_packages.chloe_backend.users.types.user_data_response import Tenant


class TenantsRepo(BaseRepo):
    def __init__(self):
        self.__scheme = TenantsScheme()

    def find_tenant(self, id: str) -> Tenant:
        return self.__scheme.tenants.find_one_document.with_mongo_id(id).serialize_to(
            Tenant
        )

    def delete_tenant(self, id: str) -> bool:
        return self.__scheme.tenants.delete_one_document.with_mongo_id(id)

    def change_vrepro_status(self, id: str, status: bool) -> bool:
        self.__scheme.tenants.update_document.with_mongo_id(id).with_new_values(
            settings_isVRepro=status
        )

    def check_vrepro_status(self, tenant_id: str) -> bool:
        result = self.find_tenant(tenant_id)
        return result.settings.isVRepro

    def delete_tenant_data(self, tenant_id: str) -> bool:
        return self.__scheme.tenants.delete_all_documents.with_mongo_id(tenant_id)
