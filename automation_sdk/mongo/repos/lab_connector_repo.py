from typing import List

from retry import retry

from automation_sdk.mongo.mongo_orm.lab_connector.connector import Connector
from automation_sdk.mongo.mongo_types.schemas.lab_connector.lab_connector_scheme import (
    LabConnectorScheme,
)
from automation_sdk.mongo.repos.base_db import BaseRepo


class LabConnectorRepo(BaseRepo):
    def __init__(self):
        self.__scheme = LabConnectorScheme()

    def find_connector(self, tenant_id: str) -> Connector:
        return self.__scheme.connectors.find_one_document.with_tenant_id(
            tenant_id
        ).serialize_to(Connector)

    def find_connectors(self, tenant_id: str) -> List[Connector]:
        return self.__scheme.connectors.find_all_documents.with_tenant_id(
            tenant_id
        ).serialize_to(Connector)

    def activate_connector(self, tenant_id: str) -> bool:
        return self.__scheme.connectors.update_document.with_tenant_id(
            tenant_id
        ).with_new_values(isActive=True)

    def deactivate_connector(self, tenant_id: str) -> bool:
        return self.__scheme.connectors.update_document.with_tenant_id(
            tenant_id
        ).with_new_values(isActive=False)

    def reset_connector(self, tenant_id: str) -> bool:
        return self.__scheme.connectors.update_document.with_tenant_id(
            tenant_id
        ).with_new_values(
            patientsSync_startRun=None,
            patientsSync_endRun=None,
            slidesSync_startRun=None,
            slidesSync_endRun=None,
            isRunning_status=False,
            isRunning_expireAt=None,
            isActive=False,
            runningBy=None,
        )

    @retry(ValueError, tries=8, delay=10)
    def is_patient_sync_done(self, tenant_id: str) -> bool:
        connector = self.find_connector(tenant_id)
        if connector.patientsSync.endRun is None:
            raise ValueError("Patients Sync is not done yet")
        else:
            return True

    @retry(ValueError, tries=4, delay=60)
    def is_slides_sync_done(self, tenant_id: str) -> bool:
        connector = self.find_connector(tenant_id)
        if connector.slidesSync.endRun is None:
            raise ValueError("Slides Sync is not done yet")
        else:
            return True

    def is_connector_running(self, tenant_id: str) -> bool:
        connector = self.find_connector(tenant_id)
        return connector.isRunning.status

    def is_connector_active(self, tenant_id: str) -> bool:
        connector = self.find_connector(tenant_id)
        return connector.isActive

    def delete_tenant_data(self, tenant_id: str) -> bool:
        return self.__scheme.connectors.delete_all_documents.with_tenant_id(tenant_id)
