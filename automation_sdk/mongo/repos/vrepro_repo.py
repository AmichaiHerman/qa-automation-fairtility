from automation_sdk.mongo.mongo_orm.vrepro.vrepro_settings import VreproSettings
from automation_sdk.mongo.mongo_types.schemas.vRepro.vrepro_scheme import VreproScheme
from automation_sdk.mongo.repos.base_db import BaseRepo


class VreproRepo(BaseRepo):
    """Component encapsulating vrepro mongo repo"""

    def __init__(self):
        self.__scheme = VreproScheme()

    def create_vrepro_settings(self, tenant_id: str) -> bool:
        return self.__scheme.vrepro_settings.insert_one_document.with_objects(
            VreproSettings(tenantId=tenant_id)
        )

    def search_vrepro_settings(self, tenant_id: str) -> VreproSettings:
        return self.__scheme.vrepro_settings.find_one_document.with_tenant_id(
            tenant_id
        ).serialize_to(VreproSettings)

    def delete_tenant_data(self, tenant_id: str) -> bool:
        return self.__scheme.job.delete_all_documents.with_tenant_id(tenant_id)

    def delete_settings(self, tenant_id: str) -> bool:
        return self.__scheme.vrepro_settings.delete_all_documents.with_tenant_id(
            tenant_id
        )
