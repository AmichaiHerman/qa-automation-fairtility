from typing import List, Union, Dict

from pydantic import BaseModel, parse_obj_as

from automation_sdk.mongo.mongo_types.reads.query_type import QueryType


class Serialize:
    def __init__(self, res: Dict, query_type: QueryType):
        self.__find_results = res
        self.__query_type = query_type

    def serialize_to(self, dto: BaseModel) -> Union[BaseModel, List[BaseModel]]:
        if self.__query_type == QueryType.FIND_ONE:
            # Manually handle _id field if it exists
            data = self.__find_results.copy()
            if '_id' in data:
                data['id'] = str(data.pop('_id'))
            return dto.parse_obj(data)
        else:
            # Manually handle _id field for each item if it exists
            return [dto.parse_obj({**item, 'id': str(item['_id'])} if '_id' in item else item) for item in
                    self.__find_results]

