from typing import Dict

from bson import ObjectId


def by_id(id: str) -> Dict:
    return {"_id": ObjectId(id)}


def by_tenant_id(tenant_id: str) -> Dict:
    return {"tenantId": tenant_id}


def by_patient_es_id_and_tenant_id(patient_es_id: str, tenant_id:str) -> Dict:
    return {"esId": patient_es_id, "tenantId": tenant_id}


def by_patient_id_and_well_es_id(patient_id: str, well_id: str) -> Dict:
    return {"patientId": patient_id, "esId": well_id}


def by_patient_id_and_tenant_id(patient_id: str, tenant_id: str) -> Dict:
    return {"patientId": patient_id, "tenantId": tenant_id}


def by_custom_kv(k: str, v: str) -> Dict:
    return {k: v}
