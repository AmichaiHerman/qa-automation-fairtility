from typing import Dict, List

from pymongo.collection import Collection
from pymongo.database import Database

from automation_sdk.mongo.mongo_types.reads.delete import Delete
from automation_sdk.mongo.mongo_types.reads.find import Find
from automation_sdk.mongo.mongo_types.reads.query_type import QueryType
from automation_sdk.mongo.mongo_types.writes.insert import Insert
from automation_sdk.mongo.mongo_types.writes.insert_amount import InsertAmount
from automation_sdk.mongo.mongo_types.writes.update import Update


class MongoCollection:
    def __init__(self, db: Database, name: str):
        self.__db = db
        self.__col: Collection = db[name]

    @property
    def find_one_document(self) -> Find:
        return Find(self.__col, query_type=QueryType.FIND_ONE)

    @property
    def find_all_documents(self) -> Find:
        return Find(self.__col, query_type=QueryType.FIND_MANY)

    @property
    def insert_one_document(self) -> Insert:
        return Insert(self.__col, insert_amount=InsertAmount.ONE_FILE)

    @property
    def insert_multiple_documents(self) -> Insert:
        return Insert(self.__col, insert_amount=InsertAmount.MULTIPLE_FILES)

    @property
    def delete_one_document(self) -> Delete:
        return Delete(self.__col, query_type=QueryType.FIND_ONE)

    @property
    def delete_all_documents(self) -> Delete:
        return Delete(self.__col, query_type=QueryType.FIND_MANY)

    @property
    def update_document(self) -> Update:
        return Update(self.__col)

    def __repr__(self):
        return f"Mongo Collection {self.__col.name} for db {self.__db.name}"
