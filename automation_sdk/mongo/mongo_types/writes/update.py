from typing import Any

from pydantic import BaseModel
from pymongo.collection import Collection

from automation_sdk.mongo.mongo_types.queries import by_tenant_id, by_id, by_custom_kv
from automation_sdk.mongo.mongo_types.writes.base_write import Write
from automation_sdk.mongo.mongo_types.writes.update_values import WithUpdatedData


class Update(Write):
    def __init__(self, col: Collection):
        super().__init__(col)

    def with_tenant_id(self, tenant_id: str) -> WithUpdatedData:
        return WithUpdatedData(self.col, by_tenant_id(tenant_id))

    def with_mongo_id(self, id: str) -> WithUpdatedData:
        return WithUpdatedData(self.col, by_id(id))

    def with_custom_kv(self, key: str, value: Any) -> WithUpdatedData:
        return WithUpdatedData(self.col, by_custom_kv(key, value))
