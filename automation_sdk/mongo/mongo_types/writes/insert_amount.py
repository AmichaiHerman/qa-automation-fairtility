from enum import Enum, auto


class InsertAmount(Enum):
    ONE_FILE = auto()
    MULTIPLE_FILES = auto()
