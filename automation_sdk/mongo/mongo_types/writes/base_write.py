from pymongo.collection import Collection

from automation_sdk.mongo.mongo_types.base_mongo_type import MongoType


class Write(MongoType):
    __slots__ = ["col"]

    def __init__(self, col: Collection):
        self.col = col
