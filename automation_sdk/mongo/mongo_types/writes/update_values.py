from pydantic import BaseModel
from pymongo.collection import Collection

from automation_sdk.mongo.mongo_types.actions_handler import handle_update
from automation_sdk.mongo.mongo_types.writes.base_write import Write


class WithUpdatedData(Write):
    def __init__(self, col: Collection, query: dict):
        super().__init__(col)
        self.query = query

    def with_new_values(self, **kwargs) -> bool:
        return handle_update(self.col.update_one, self.query, **kwargs)
