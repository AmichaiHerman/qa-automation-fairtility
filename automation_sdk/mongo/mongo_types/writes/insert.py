from typing import List, Union, Dict

from pydantic import BaseModel
from pymongo.collection import Collection

from automation_sdk.mongo.mongo_types.actions_handler import handle_write
from automation_sdk.mongo.mongo_types.exceptions import OperationNotSupported
from automation_sdk.mongo.mongo_types.writes.base_write import Write
from automation_sdk.mongo.mongo_types.writes.insert_amount import InsertAmount


class Insert(Write):
    def __init__(self, col: Collection, insert_amount: InsertAmount):
        super().__init__(col)
        self.__insert_amount = insert_amount

    def with_objects(
        self,
        doc_or_docs: Union[BaseModel, List[BaseModel]],
        ignore_exceptions: bool = False,
    ):
        if self.__insert_amount == InsertAmount.ONE_FILE:
            as_json = doc_or_docs.dict()
            return handle_write(
                ignore_exceptions=ignore_exceptions,
                action=self.col.insert_one,
                data=as_json,
            )
        elif self.__insert_amount == InsertAmount.MULTIPLE_FILES:
            as_jsons = [doc.dict() for doc in doc_or_docs]
            return handle_write(
                ignore_exceptions=ignore_exceptions,
                action=self.col.insert_many,
                data=as_jsons,
            )
