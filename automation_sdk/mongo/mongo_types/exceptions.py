class ResultsNotFoundException(Exception):
    ...


class QueryTypeNotSupported(Exception):
    ...


class InsertionAmountNotSupported(Exception):
    ...


class OperationNotSupported(Exception):
    ...
