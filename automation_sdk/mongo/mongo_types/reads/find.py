from typing import Any, List, Dict

from pymongo.collection import Collection

from automation_sdk.mongo.mongo_types.actions_handler import handle_read
from automation_sdk.mongo.mongo_types.exceptions import QueryTypeNotSupported
from automation_sdk.mongo.mongo_types.queries import *
from automation_sdk.mongo.mongo_types.reads.base_read import Read
from automation_sdk.mongo.mongo_types.reads.query_type import QueryType
from automation_sdk.mongo.mongo_types.serialize import Serialize


class Find(Read):
    def __init__(self, col: Collection, query_type: QueryType):
        super().__init__(col, query_type)

    def with_mongo_id(self, id: str) -> Serialize:
        return Serialize(self.__handle_mongo_id_query(id), self.query_type)

    def with_tenant_id(self, tenant_id: str):
        handle_res = self.__handle_tenant_id_query(tenant_id)
        return Serialize(handle_res, self.query_type)

    def with_patient_id_and_tenant(self, patient_es_id: str, tenant_id: str):
        handle_res = handle_read(action=self.col.find_one, query=by_patient_id_and_tenant_id(patient_es_id, tenant_id))
        return Serialize(handle_res, self.query_type)

    def with_patient_es_id_and_tenant(self, patient_es_id: str, tenant_id: str):
        handle_res = handle_read(action=self.col.find_one, query=by_patient_es_id_and_tenant_id(patient_es_id, tenant_id))
        return Serialize(handle_res, self.query_type)

    def with_patient_id_and_well_es_id(self, patient_id: str, well_id: str):
        handle_res = handle_read(action=self.col.find_one, query=by_patient_id_and_well_es_id(patient_id, well_id))
        return Serialize(handle_res, self.query_type)

    def with_custom_query(self, custom_key: str, custom_value: str) -> Serialize:
        return Serialize(
            self.__handle_custom_kv_query(custom_key, custom_value), self.query_type
        )

    def __handle_mongo_id_query(self, id: str) -> Any:
        if self.query_type == QueryType.FIND_ONE:
            return handle_read(action=self.col.find_one, query=by_id(id))
        elif self.query_type == QueryType.FIND_MANY:
            return handle_read(action=self.col.find, query=by_id(id))
        else:
            raise QueryTypeNotSupported(
                f"The Query Type Provided: {self.query_type} is Not Supported"
            )

    def __handle_tenant_id_query(self, tenant_id: str) -> Any:
        if self.query_type == QueryType.FIND_ONE:
            return handle_read(action=self.col.find_one, query=by_tenant_id(tenant_id))
        elif self.query_type == QueryType.FIND_MANY:
            return handle_read(action=self.col.find, query=by_tenant_id(tenant_id))
        else:
            raise QueryTypeNotSupported(
                f"The Query Type Provided: {self.query_type} is Not Supported"
            )

    def __handle_custom_kv_query(self, custom_key: str, custom_value: str) -> Any:
        if self.query_type == QueryType.FIND_ONE:
            return handle_read(
                action=self.col.find_one, query=by_custom_kv(custom_key, custom_value)
            )
        elif self.query_type == QueryType.FIND_MANY:
            return handle_read(
                action=self.col.find, query=by_custom_kv(custom_key, custom_value)
            )
        else:
            raise QueryTypeNotSupported(
                f"The Query Type Provided: {self.query_type} is Not Supported"
            )
