from typing import Any

from pymongo.collection import Collection
from automation_sdk.mongo.mongo_types.actions_handler import handle_delete
from automation_sdk.mongo.mongo_types.exceptions import QueryTypeNotSupported
from automation_sdk.mongo.mongo_types.queries import by_id, by_tenant_id, by_custom_kv
from automation_sdk.mongo.mongo_types.reads.base_read import Read
from automation_sdk.mongo.mongo_types.reads.query_type import QueryType


# todo mongo type should be ABC
class Delete(Read):
    def __init__(self, col: Collection, query_type: QueryType):
        super().__init__(col, query_type)

    def with_mongo_id(self, id: str) -> bool:
        return self.__handle_mongo_id_delete(id)

    def with_tenant_id(self, tenant_id: str) -> bool:
        return self.__handle_tenant_id_delete(tenant_id)

    def with_custom_query(self, custom_key: str, custom_value: str) -> bool:
        return self.__handle_custom_kv_delete(custom_key, custom_value)

    def __handle_mongo_id_delete(self, id: str) -> Any:
        if self.query_type == QueryType.FIND_ONE:
            return handle_delete(action=self.col.delete_one, query=by_id(id))
        elif self.query_type == QueryType.FIND_MANY:
            return handle_delete(action=self.col.delete_many, query=by_id(id))
        else:
            raise QueryTypeNotSupported(
                f"The Query Type Provided: {self.query_type} is Not Supported"
            )

    def __handle_tenant_id_delete(self, tenant_id: str) -> Any:
        if self.query_type == QueryType.FIND_ONE:
            return handle_delete(
                action=self.col.delete_one, query=by_tenant_id(tenant_id)
            )
        elif self.query_type == QueryType.FIND_MANY:
            return handle_delete(
                action=self.col.delete_many, query=by_tenant_id(tenant_id)
            )
        else:
            raise QueryTypeNotSupported(
                f"The Query Type Provided: {self.query_type} is Not Supported"
            )

    def __handle_custom_kv_delete(self, custom_key: str, custom_value: str) -> Any:
        if self.query_type == QueryType.FIND_ONE:
            return handle_delete(
                action=self.col.delete_one, query=by_custom_kv(custom_key, custom_value)
            )
        elif self.query_type == QueryType.FIND_MANY:
            return handle_delete(
                action=self.col.delete_many,
                query=by_custom_kv(custom_key, custom_value),
            )
        else:
            raise QueryTypeNotSupported(
                f"The Query Type Provided: {self.query_type} is Not Supported"
            )
