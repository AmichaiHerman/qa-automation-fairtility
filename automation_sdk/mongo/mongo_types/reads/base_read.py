from pymongo.collection import Collection

from automation_sdk.mongo.mongo_types.base_mongo_type import MongoType
from automation_sdk.mongo.mongo_types.reads.query_type import QueryType


class Read(MongoType):
    __slots__ = ["col", "query_type"]

    def __init__(self, col: Collection, query_type: QueryType):
        self.col = col
        self.query_type = query_type
