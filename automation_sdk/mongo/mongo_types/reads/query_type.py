from enum import Enum, auto


class QueryType(Enum):
    FIND_ONE = auto()
    FIND_MANY = auto()
