from typing import Callable, Any, Dict

from bson.json_util import dumps
from pymongo.errors import PyMongoError, DuplicateKeyError, BulkWriteError
from pymongo.results import _WriteResult, DeleteResult, UpdateResult

from automation_sdk.console_logging.debug import Debug
from automation_sdk.mongo.mongo_types.exceptions import ResultsNotFoundException


def handle_read(action: Callable, query: Dict) -> Any:
    res = None
    try:
        Debug.log("Executing Mongo Query:" + str((query)))
        res = action(query)
    except PyMongoError as e:
        raise e
    finally:
        if res is not None:
            return res
        else:
            raise ResultsNotFoundException("No results found for your query")


def handle_write(action: Callable, data: Any, ignore_exceptions: bool) -> Any:
    res = None
    print(type(data))
    try:
        Debug.log("Importing Test Documents:" + str(dumps(data)))
        res = action(data)
    except PyMongoError as e:
        if isinstance(e, DuplicateKeyError) or isinstance(e, BulkWriteError):
            if ignore_exceptions:
                return str(e)
            else:
                raise e
    return True if isinstance(res, _WriteResult) else False


def handle_delete(action: Callable, query: Dict) -> Any:
    try:
        Debug.log(f"Deleting Test Documents By {list(query.keys())[0]}")
        res = action(query)
    except PyMongoError as e:
        raise e
    return True if isinstance(res, DeleteResult) else False


def handle_update(action: Callable, query: Dict, **kwargs) -> Any:
    try:
        as_dict = _kwargs_to_dict(**kwargs)
        Debug.log(f"Updating The DB with {as_dict}")
        as_mongo_import_query = {"$set": as_dict}
        res = action(query, as_mongo_import_query)
    except PyMongoError as e:
        raise e
    return True if isinstance(res, UpdateResult) else False


def _kwargs_to_dict(**kwargs):
    my_dict = {}
    for key, value in kwargs.items():
        updated_key = key.replace("_", ".")
        my_dict[updated_key] = value
    return my_dict
