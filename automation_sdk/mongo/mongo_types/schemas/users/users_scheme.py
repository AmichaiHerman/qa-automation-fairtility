from automation_sdk.mongo.mongo_types.schemas.base_repo import MongoSchemeBuilder


class UsersScheme(MongoSchemeBuilder):
    def __init__(self):
        super().__init__(
            db_name="usersDB",
            collections=["users"],
        )
