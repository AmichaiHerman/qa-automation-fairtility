import os

from automation_sdk.mongo.mongo_types.schemas.base_repo import MongoSchemeBuilder


class TenantsScheme(MongoSchemeBuilder):
    """A Repo For Storing Tenants Data on mongo DB"""

    def __init__(self):
        super().__init__(
            db_name=os.getenv("TENANTS_COLLECTION"),
            collections=["tenants"],
        )
