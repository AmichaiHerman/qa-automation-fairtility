import os

from automation_sdk.mongo.mongo_types.schemas.base_repo import MongoSchemeBuilder


class VreproScheme(MongoSchemeBuilder):
    """Component encapsulating vrepro mongo repo"""

    def __init__(self):
        super().__init__(
            db_name=os.getenv("VREPRO_COLLECTION"),
            collections=["job", "vrepro-settings"],
        )
