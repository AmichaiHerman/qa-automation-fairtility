import os
from typing import List
import certifi

from pymongo import MongoClient

from automation_sdk.mongo.mongo_types.base_mongo_type import MongoType
from automation_sdk.mongo.mongo_types.collection import MongoCollection


class MongoSchemeBuilder(MongoType):
    """Component Responsible for creating reprs of then backend ms mongo schemes"""

    collections = {}

    def __init__(self, db_name: str, collections: List[str]):
        self.__client = MongoClient(
            os.getenv("MONGO_DB_CONNECTION_STRING_EQ"),
            tlsCAFile=certifi.where(),
            socketTimeoutMS=15000,
        )
        self.__db_name = self.__client[db_name]
        for collection in collections:
            self.collections[collection] = MongoCollection(
                db=self.__db_name, name=collection
            )

    def __getattr__(self, item: str):
        modified_item = item.replace("_", "-")
        if modified_item in self.collections:
            return self.collections[modified_item]

    def __dir__(self):
        return self.mapping.keys()


class MongoSchemeBuilderProductionEu(MongoType):
    """Component Responsible for creating reprs of then backend ms mongo schemes"""

    collections = {}

    def __init__(self, db_name: str, collections: List[str]):
        self.__client = MongoClient(
            os.getenv("MONGO_DB_CONNECTION_STRING_EQ_PRODUCTION_EU"),
            tlsCAFile=certifi.where(),
            socketTimeoutMS=60000,
        )
        self.__db_name = self.__client[db_name]
        for collection in collections:
            self.collections[collection] = MongoCollection(
                db=self.__db_name, name=collection
            )

    def __getattr__(self, item: str):
        modified_item = item.replace("_", "-")
        if modified_item in self.collections:
            return self.collections[modified_item]

    def __dir__(self):
        return self.mapping.keys()
