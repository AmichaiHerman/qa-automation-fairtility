import os
from automation_sdk.mongo.mongo_types.schemas.base_repo import MongoSchemeBuilder, MongoSchemeBuilderProductionEu


class PatientDataScheme(MongoSchemeBuilder):
    def __init__(self):
        super().__init__(
            db_name=os.getenv("PATIENT_DATA_COLLECTION"),
            collections=["patients", "instruments", "slides", "wells"],
        )


class PatientDataSchemeProductionEu(MongoSchemeBuilderProductionEu):
    def __init__(self):
        super().__init__(
            db_name=os.getenv("PATIENT_DATA_COLLECTION_NO_COLOR"),
            collections=["patients", "instruments", "slides", "wells"],
        )
