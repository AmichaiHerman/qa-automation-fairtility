from automation_sdk.mongo.mongo_types.collection import MongoCollection

class PatientDataScheme:
    patients: MongoCollection = None
    instruments: MongoCollection = None
    slides: MongoCollection = None
    wells: MongoCollection = None


class PatientDataSchemeProductionEu:
    patients: MongoCollection = None
    instruments: MongoCollection = None
    slides: MongoCollection = None
    wells: MongoCollection = None