from automation_sdk.mongo.mongo_types.schemas.base_repo import MongoSchemeBuilder


class NotesScheme(MongoSchemeBuilder):
    """A Repo For Storing QA Data on mongo DB"""

    def __init__(self):
        super().__init__(
            db_name="notesDB",
            collections=["notes"],
        )
