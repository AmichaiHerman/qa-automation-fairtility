import os

from automation_sdk.mongo.mongo_types.schemas.base_repo import MongoSchemeBuilder


class LabConnectorScheme(MongoSchemeBuilder):
    def __init__(self):
        super().__init__(
            db_name=os.getenv("LAB_CONNECTOR_COLLECTION"),
            collections=["connectors"],
        )
