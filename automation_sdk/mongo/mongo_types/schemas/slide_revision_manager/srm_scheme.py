import os

from automation_sdk.mongo.mongo_types.schemas.base_repo import MongoSchemeBuilder


class SRMScheme(MongoSchemeBuilder):
    def __init__(self):
        super().__init__(
            db_name=os.getenv("SRM_COLLECTION"),
            collections=["slides", "wells"],
        )
