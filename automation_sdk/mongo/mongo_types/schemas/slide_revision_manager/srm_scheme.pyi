from automation_sdk.mongo.mongo_types.collection import MongoCollection

class SRMScheme:
    slides: MongoCollection = None
    wells: MongoCollection = None
