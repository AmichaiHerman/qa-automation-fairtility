import os

from automation_sdk.mongo.mongo_types.schemas.base_repo import MongoSchemeBuilder


class AgentHandlerScheme(MongoSchemeBuilder):
    def __init__(self):
        super().__init__(
            db_name=os.getenv("AGENT_HANDLER_COLLECTION"),
            collections=["patientdatas", "wells", "settings"],
        )
