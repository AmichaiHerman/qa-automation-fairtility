from automation_sdk.mongo.mongo_types.collection import MongoCollection

class AgentHandlerScheme:
    patientdatas: MongoCollection = None
    wells: MongoCollection = None
    settings: MongoCollection = None
