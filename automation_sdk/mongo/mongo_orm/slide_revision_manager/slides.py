from __future__ import annotations

from pydantic import BaseModel, Field

from shared.mongo_client.mongo_objects.abstract_mongo_result import (
    SlideMetadataDBResponse,
)


class _Id(BaseModel):
    _oid: str = Field(..., alias="$oid")


class CreatedAt(BaseModel):
    _date: str = Field(..., alias="$date")


class UpdatedAt(BaseModel):
    _date: str = Field(..., alias="$date")


class Slides(SlideMetadataDBResponse):
    _id: _Id
    lastRevision: int
    version: int
    esId: str
    treatmentId: str
    tenantId: str
    externalId: str
    createdAt: CreatedAt
    updatedAt: UpdatedAt
    __v: int

    def slide_esid(self) -> str:
        return self.esId

    def num_runs(self) -> int:
        return self.lastRevision
