from __future__ import annotations

from pydantic import BaseModel, Field

from automation_sdk.mongo.mongo_orm.mongo_response import PatientDataResponse


class _Id(BaseModel):
    _oid: str = Field(..., alias="$oid")


class CreatedAt(BaseModel):
    _date: str = Field(..., alias="$date")


class UpdatedAt(BaseModel):
    _date: str = Field(..., alias="$date")


class Instrument(PatientDataResponse):
    _id: _Id
    instrumentSize: int
    esId: str
    tenantId: str
    createdAt: CreatedAt
    updatedAt: UpdatedAt
    __v: int

    @property
    def es_id(self) -> str:
        return self.esId
