from __future__ import annotations

from typing import List, Optional, Any

from bson import ObjectId
from pydantic import BaseModel, Field

from automation_sdk.mongo.mongo_orm.mongo_response import PatientDataResponse
from automation_sdk.mongo.mongo_orm.patient_data.instrument import _Id


class BirthDate(BaseModel):
    date: Any = Field(..., alias="$date")


class OocyteAge(BaseModel):
    months: int
    years: int


class Well(BaseModel):
    esId: str
    kidRank: int


class Slide(BaseModel):
    id: str
    wells: List[Well]

    @property
    def slide_mongo_id(self) -> str:
        return self.id


class ProducerTreatment(BaseModel):
    id: Any
    oocyteAge: Optional[OocyteAge]
    instrumentType: str
    startDate: Any
    slides: Optional[List[Slide]]

    @property
    def treatment_id(self) -> str:
        return self.id["$oid"]

    @property
    def start_date(self) -> str:
        return self.startDate["$date"]


class Treatment(BaseModel):
    startDate: Any


class Patients(PatientDataResponse):
    id: str
    tenantId: str
    esId: str
    identifier1: Optional[str]
    treatments: List[Treatment]
    producerTreatments: List[ProducerTreatment]

    @property
    def num_treatments(self) -> int:
        return len(self.treatments)

    @property
    def object_id(self) -> str:
        return self.id
