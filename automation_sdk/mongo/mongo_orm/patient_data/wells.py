# todo complete this class with more data, right now serializing only inference data + lastImageUrl

from __future__ import annotations

from typing import Any, List, Optional, Dict

from pydantic import BaseModel

from automation_sdk.mongo.mongo_orm.mongo_response import MongoResponse
from test.pn_model_tests.dtos.pn_model_dto import AiModels, Segmentation
from test.shared_packages.mk_events import MKEvents


class MkNotExistException(Exception):
    ...


class Confidence(BaseModel):
    blastScoreTP: float
    kidScoreTP: float
    kidScoreProb: float


class PnCount(BaseModel):
    value: int
    time: str


class Morphokinetic(BaseModel):
    systemName: str
    value: Optional[float]


class PredictMorphokinetic(BaseModel):
    systemName: str
    value: Optional[float]


class Duc(BaseModel):
    duc1: bool
    duc2: bool
    duc3: bool


class InferenceOverridingData(BaseModel):
    pnCount: Optional[PnCount]
    degeneratedOocyte: Optional[bool]


class InferenceData(BaseModel):
    blastScore: int
    blastScoreOverTime: Any
    kidScore: float
    kidScoreOverTime: Optional[Any]
    mkScore: float
    confidence: Confidence
    fragmentationTime: Any
    degeneratedOocyteTime: Any
    morphokinetics: Optional[List[Morphokinetic]]
    predictMorphokinetics: Optional[List[PredictMorphokinetic]]
    isValidT: bool
    duc: Duc
    pnCount: PnCount
    aiModels: AiModels
    confidencePnsClassification: Optional[str]
    mksPredictionUsed: bool
    xgboostUsed: bool
    segmentations: List[Segmentation]


class PDWells(MongoResponse):
    videoFramesTime: List[int]
    inferenceData: InferenceData
    inferenceOverridingData: Optional[InferenceOverridingData]
    lastImageUrl: str
    treatmentId: str

    @property
    def is_ICSI(self) -> bool:
        first_frame_in_hours = self.videoFramesTime[0] / (1000 * 60 * 60)
        return True if first_frame_in_hours < 10 else False

    @property
    def is_fertilization_failed(self) -> bool:
        return True if self.inferenceData.pnCount.value == 0 else False

    @property
    def is_DEG(self) -> bool:
        deg_time = self.inferenceData.degeneratedOocyteTime
        validation = deg_time != None and float(deg_time) < 34
        return validation

    @property
    def is_normally_fertilized_by_34h(self) -> bool:
        pn_count_obj = self.inferenceData.pnCount
        validation = pn_count_obj.value == 2 and float(pn_count_obj.time) < 34
        return True if validation else False

    @property
    def is_been_through_t4_in_day2(self) -> bool:
        morphokinetics = self.inferenceData.morphokinetics
        if morphokinetics is None:
            raise MkNotExistException("Morphokinetics not exist for the embryo")
        t4_validation = False
        t5_validation = False
        reached_t5 = False
        for mk in morphokinetics:
            if mk.systemName == "t4":
                t4_validation = mk.value <= 44
            if mk.systemName == "t5":
                reached_t5 = True
                t5_validation = mk.value >= 44

        if not reached_t5:
            return t4_validation
        else:
            return t4_validation & t5_validation

    @property
    def is_been_through_t8_in_day3(self) -> bool:
        morphokinetics = self.inferenceData.morphokinetics
        if morphokinetics is None:
            raise MkNotExistException("Morphokinetics not exist for the embryo")
        t8_validation = False
        t9_validation = False
        reached_t9 = False
        for mk in morphokinetics:
            if mk.systemName == "t8":
                t8_validation = mk.value <= 68
            if mk.systemName == "t9+":
                reached_t9 = True
                t9_validation = mk.value >= 68

        if not reached_t9:
            return t8_validation
        else:
            return t8_validation & t9_validation

    @property
    def is_been_in_tb_at_day5(self) -> bool:
        morphokinetics = self.inferenceData.morphokinetics
        if morphokinetics is None:
            raise MkNotExistException("Morphokinetics not exist for the embryo")
        for mk in morphokinetics:
            if mk.systemName == "tB":
                return mk.value <= 120

        return False

    @property
    def is_been_through_cleavage(self) -> bool:
        morphokinetics = self.inferenceData.morphokinetics
        if morphokinetics is None:
            raise MkNotExistException("Morphokinetics not exist for the embryo")
        for mk in morphokinetics:
            if mk.systemName == "t2":
                return mk.value <= 44

        return False

    @property
    def has_reached_blast(self) -> bool:
        morphokinetics = self.inferenceData.morphokinetics
        pn_count = self.inferenceData.pnCount.value
        if morphokinetics is None:
            raise MkNotExistException("Morphokinetics not exist for the embryo")
        for mk in morphokinetics:
            if mk.systemName == "tB":
                return pn_count == 2

        return False

    @property
    def is_with_1pn_on_34h(self) -> bool:
        pn_count_obj = self.inferenceData.pnCount
        validation = pn_count_obj.value == 1 and float(pn_count_obj.time) < 34
        return True if validation else False
