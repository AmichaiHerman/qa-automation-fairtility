from __future__ import annotations

from typing import List, Any, Optional

from pydantic import BaseModel, Field

from automation_sdk.mongo.mongo_orm.mongo_response import PatientDataResponse


class FertilizationDate(BaseModel):
    _date: str = Field(..., alias="$date")


class PDSlides(PatientDataResponse):
    id: str
    videoFramesTime: List[int]
    status: str
    esId: str
    tenantId: str
    patientId: str
    treatmentId: str
    fertilizationDate: Any
    slidePosition: int
    instrumentId: str
    instrumentType: str
    hoursSinceFertilization: Optional[float]
    __v: int

    # todo find solution for handling slides with this data
    # lastImageHour: Optional[int]
    # run: Optional[int]

    @property
    def slide_esid(self) -> str:
        return self.esId

    @property
    def slide_mongo_id(self) -> str:
        return self.id["$oid"]

    @property
    def object_id(self) -> str:
        return self.id
