from pydantic import BaseModel


class MongoResponse(BaseModel):
    ...


class PatientDataResponse(MongoResponse):
    ...


class AgentHandlerResponse(MongoResponse):
    ...
