from pydantic import BaseModel


class EmbryoGTData(BaseModel):
    well_url: str
    storage_path: str
    well_es_id: str
    vendor: str
    expected_pn: int
    actual_pn: int
    pn_identification_difficulty_level: str
    expected_duc: bool
    actual_duc: bool
    expected_deg: bool
    actual_deg: bool
    with_abnormalities: bool
