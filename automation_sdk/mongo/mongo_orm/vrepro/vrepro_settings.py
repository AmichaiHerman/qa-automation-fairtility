import os

from pydantic import BaseModel


class VreproSettings(BaseModel):
    apiBaseUrl: str = os.getenv("VREPRO_BASE_URL")
    apiKeybase64: str = "Z2tTQXhHY2c5aHduY1l3Mg=="
    clientId: str = "fairtility"
    tenantId: str
