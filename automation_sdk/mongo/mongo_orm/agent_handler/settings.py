from __future__ import annotations
from pydantic import BaseModel


class Settings(BaseModel):
    invokePipeLineEveryNRun: int
    pipelineDebounceInHours: float
    __v: int
    tenantId: str
