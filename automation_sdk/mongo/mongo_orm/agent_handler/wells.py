from __future__ import annotations

from typing import List, Any


from automation_sdk.mongo.mongo_orm.mongo_response import AgentHandlerResponse


class Wells(AgentHandlerResponse):
    focals: List[int]
    runs: Any
    times: List[int]
    esId: str
    tenantId: str
    slideId: str
    treatmentEsId: str
    slideEsId: str
    storagePath: str
    __v: int

    @property
    def num_runs(self) -> int:
        return len(self.runs)
