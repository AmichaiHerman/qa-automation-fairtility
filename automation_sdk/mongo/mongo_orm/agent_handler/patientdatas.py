from __future__ import annotations

from typing import List, Any, Optional

from pydantic import BaseModel, Field

from automation_sdk.mongo.mongo_orm.mongo_response import AgentHandlerResponse


class Slide(BaseModel):
    firstImageHour: Optional[int]
    wells: List[str]
    esId: Any
    status: str
    fertilizationDate: Optional[Any]
    instrumentEsId: str
    slidePosition: Optional[int]
    externalId: Optional[str]
    treatmentEsId: str

    @property
    def number_of_wells(self) -> int:
        return len(self.wells)

    @property
    def slide_mongo_id(self) -> str:
        return self.id["$oid"]


class CreatedAt(BaseModel):
    date: str = Field(..., alias="$date")


class UpdatedAt(BaseModel):
    date: str = Field(..., alias="$date")


class OocyteAge(BaseModel):
    months: int
    years: int


class Treatment(BaseModel):
    birthDate: Any
    oocyteAge: Optional[OocyteAge]
    dataDic: Optional[List]
    esId: str
    externalId: Optional[str]


class CreatedAt1(BaseModel):
    date: str = Field(..., alias="$date")


class UpdatedAt1(BaseModel):
    date: str = Field(..., alias="$date")


class Patientdatas(AgentHandlerResponse):
    dataDic: Optional[List]
    esId: str
    tenantId: str
    identifier1: Optional[str]
    externalId: Optional[str]
    slides: List[Slide]
    treatments: List[Treatment]

    @property
    def birthdates_list(self) -> List[Any]:
        return [treatment.birthDate for treatment in self.treatments]

    @property
    def num_slides(self) -> int:
        return len(self.slides)

    @property
    def num_treatments(self) -> int:
        return len(self.treatments)

    @property
    def is_active_patient(self) -> bool:
        slides_status_list = [slide.status for slide in self.slides]
        return True if "IN_PROGRESS" in slides_status_list else False
