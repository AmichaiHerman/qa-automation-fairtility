from __future__ import annotations

from typing import Optional

from pydantic import BaseModel, Field


class _Id(BaseModel):
    oid: str = Field(..., alias="$oid")


class _Id1(BaseModel):
    oid: str = Field(..., alias="$oid")


class KpiSetting(BaseModel):
    id: _Id1
    kpiName: str
    kpiType: str
    benchmark: int
    competency: int
    benchmarkCondition: str
    competencyCondition: str
    favourite: bool


class _Id2(BaseModel):
    oid: str = Field(..., alias="$oid")


class KpiAgeGroup(BaseModel):
    id: _Id2
    min: int
    max: Optional[int]
