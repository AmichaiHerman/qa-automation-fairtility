from __future__ import annotations

from typing import Any, Optional

from pydantic import BaseModel


class IsRunning(BaseModel):
    status: bool


class LabApi(BaseModel):
    batchSize: int
    url: str
    user: str
    password: str
    version: str


class PatientsSync(BaseModel):
    atHour: int
    startRun: Any
    endRun: Any


class SlidesSync(BaseModel):
    startRun: Any
    endRun: Any


class Connector(BaseModel):
    isRunning: IsRunning
    labApi: LabApi
    patientsSync: PatientsSync
    slidesSync: SlidesSync
    delayBetweenCycles: float
    labStorageSettings: Any
    isActive: bool
    runningBy: Optional[str]
    focals: Any
    name: str
    timeZone: str
    tenantId: str
    connectorType: str
