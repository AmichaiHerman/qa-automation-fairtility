from typing import List, TypeVar, Union

from bson.json_util import dumps
from pymongo.cursor import Cursor
from automation_sdk.mongo.mongo_orm.mongo_response import MongoResponse

T = TypeVar("T", bound=List[MongoResponse])


def serialize(dto: T, to_list: bool = True) -> Union[List[T], T]:
    def execution_wrapper(mongo_query):
        def handle(*args, **kwargs):
            query_results = mongo_query(*args, **kwargs)
            handled_response = handle_serialization(query_results, dto, to_list=to_list)
            return handled_response

        return handle

    return execution_wrapper


def handle_serialization(
    query_results: Cursor, dto: T, to_list: bool = True
) -> Union[List[T], T]:
    if to_list is False:
        return dto.parse_raw(dumps(query_results))
    else:
        return [dto.parse_raw(dumps(item)) for item in query_results]
