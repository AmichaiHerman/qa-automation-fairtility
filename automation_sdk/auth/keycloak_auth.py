from automation_sdk.atomic_types import UserCredentials
from automation_sdk.auth.base_auth import Auth
from automation_sdk.kc.keycloak_client import KeycloakClient


class KeycloakAuth(Auth):
    def __init__(self, credentials: UserCredentials):
        self.client: KeycloakClient = KeycloakClient(credentials=credentials)

    def __call__(self) -> dict:
        return self.client.create_header()
