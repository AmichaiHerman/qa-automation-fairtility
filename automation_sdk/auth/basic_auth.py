from requests.auth import HTTPBasicAuth

from automation_sdk.atomic_types import UserCredentials
from automation_sdk.auth.base_auth import Auth


class BasicAuth(Auth):
    def __init__(self, credentials: UserCredentials):
        self.credentials = credentials

    def __call__(self) -> HTTPBasicAuth:
        return HTTPBasicAuth(
            username=self.credentials.user_name, password=self.credentials.password
        )
