from google.cloud import logging


class LogsClient:
    def __init__(self):
        self.project_id = "kidplusdev"
        self.client = logging.Client(project=self.project_id)

    def check_error_logs_connector(self, connector_id):
        filter_str = f"httpRequest.requestUrl=\"/lab-sync/{connector_id}/sync\""
        entries = self.client.list_entries(filter_=filter_str, page_size=200)

        look_for_error_in_log_entries(entries)

    def check_error_logs_patient_data(self, env_color):
        filter_str = rf"""resource.type="k8s_container"
                        resource.labels.project_id="kidplusdev"
                        resource.labels.location="us-east1-b"
                        resource.labels.cluster_name="kidplus-staging"
                        resource.labels.namespace_name="kidplus"
                        labels.k8s-pod/app_kubernetes_io/instance="kidplus-app-{env_color}"
                        labels.k8s-pod/app_kubernetes_io/name="kidplus-app-{env_color}"
                        labels.k8s-pod/component="{env_color}-patient-data"""

        entries = self.client.list_entries(filter_=filter_str, page_size=200)
        look_for_error_in_log_entries(entries)

    def check_error_logs_slide_queue(self, env_color):
        filter_str = rf"resource.type=\"cloud_tasks_queue\" resource.labels.queue_id=\"{env_color}-slides-queue\""

        entries = self.client.list_entries(filter_=filter_str, page_size=200)
        look_for_error_in_log_entries(entries)

    def check_all_logs_for_error(self, env_color: str, connector_id: str):
        self.check_error_logs_slide_queue(env_color)
        self.check_error_logs_connector(connector_id)
        # self.check_error_logs_patient_data(env_color)

    
def look_for_error_in_log_entries(entries):
    for entry in entries:
        if entry.severity == "ERROR":
            print("-------------------------------------------")
            print(f"the log entry {entry.log_name} have Error and the payload is: {entry.payload}")

        # check nested logs
        if entry.resource.type == "cloud_logging_sink":
            for child_entry in entry.list_children():
                if child_entry.severity == "ERROR":
                    print(f"the child log entry {child_entry.log_name} "
                          f"have Error and the payload is: {child_entry.payload}")


a = LogsClient()
a.check_all_logs_for_error('yellow', "65b13ee1884d513f0e53fe2c")
