from google.cloud import storage

from automation_sdk.console_logging.debug import Debug


class StorageClient:
    """An interface for integration with GCS"""

    def __init__(self, bucket: str = "kidplus-bucket-dev"):
        self.client = storage.Client()
        self.bucket = self.client.get_bucket(bucket)

    def store(self, source_file_name: str, destination_blob_name: str) -> None:
        blob = self.bucket.blob(destination_blob_name)
        blob.upload_from_filename(source_file_name)

    def download_dir(self, blob_path: str) -> None:
        blob = self.bucket.blob(blob_path)
        blob.download_to_filename(blob)

    def is_folder_exists(self, folder_path) -> bool:
        blobs = self.bucket.list_blobs(prefix=folder_path + "/")
        return any(b.name.startswith(folder_path + "/") for b in blobs)

    def delete_folder(self, folder_path: str) -> None:
        blobs = self.bucket.list_blobs(prefix=folder_path)
        for blob in blobs:
            blob.delete()
        Debug.log(f"Folder {folder_path} Deleted")
