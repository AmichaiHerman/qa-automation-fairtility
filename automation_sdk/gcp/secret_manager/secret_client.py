import os

from google.cloud import secretmanager


class SecretClient:
    """A Client for contacting with GCP Secret Manager"""

    def __init__(self, project: str, env: str):
        self.project = project
        self.secret_detail = None
        self.env = env
        if project == "staging":
            self.secret_detail = (
                f"projects/981004217596/secrets/automation-{self.env}/versions/1"
            )
        elif project == "sandbox":
            self.secret_detail = (
                f"projects/103746648639/secrets/automation-{self.env}/versions/1"
            )

    def get_secret_data(self):
        client = secretmanager.SecretManagerServiceClient()
        response = client.access_secret_version(request={"name": self.secret_detail})
        data = response.payload.data.decode("UTF-8")
        as_dict = eval(data)
        for key, value in as_dict.items():
            os.environ[key] = value
