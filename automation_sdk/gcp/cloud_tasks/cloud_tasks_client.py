import json
from typing import Dict

from google.cloud import tasks_v2
from google.protobuf import timestamp_pb2
from pydantic import BaseModel

from automation_sdk.console_logging.debug import Debug
from automation_sdk.gcp.cloud_tasks.types.task_queue import TaskQueue


class CloudTasksClient:
    __PROJECT_NAME = "kidplusdev"
    __QUEUE_LOCATION = "us-east1"

    """ A Client for handling cloud tasks queues actions """

    def __init__(self):
        self.__client = tasks_v2.CloudTasksClient()

    def send_message_to_queue(self, queue_data: TaskQueue) -> None:
        """Fire Task to specific queue of a service API"""
        queue_path = self.__set_queue_path(queue_data.name)
        task = self.__build_task(queue_data.dest_api_url, queue_data.message)
        result = self.__client.create_task(parent=queue_path, task=task)
        Debug.log(f"Created task {result.name}")

    def purge_queue(self, queue_data: TaskQueue) -> None:
        """Gives ability to purge a queue"""
        queue_path = self.__set_queue_path(queue_data.name)
        purge_request = {"name": queue_path}
        self.__client.purge_queue(request=purge_request)
        Debug.log(f"Queue {queue_data.name} Purged")

    def __build_task(self, dest_api: str, payload: BaseModel) -> Dict:
        http_request = {
            "http_method": "POST",
            "url": dest_api,
            "headers": {
                "Content-Type": "application/json",
                "Authorization": "Bearer my-token",
            },
            "body": json.dumps(payload).encode("utf-8"),
        }

        task = {
            "http_request": http_request,
            "schedule_time": timestamp_pb2.Timestamp(seconds=0),
        }

        return task

    def __set_queue_path(self, queue_name: str) -> str:
        return self.__client.queue_path(
            self.__PROJECT_NAME, self.__QUEUE_LOCATION, queue_name
        )
