import os
from typing import List

from automation_sdk.gcp.cloud_tasks.types.task_queue import TaskQueue
from test.shared_packages.chloe_backend.imaging.types.requests_data import (
    CreateTimelapsePayload,
)


class VideoCropQueue(TaskQueue):
    @property
    def name(self) -> str:
        return "video-crop-queue"

    @property
    def dest_api_url(self) -> str:
        return os.getenv("CHLOE_BASE_URL") + "imaging-crop/video"

    @property
    def message(
        self,
        slide_id: str,
        well_es_id: str,
        run_number: int,
        last_image_hour: int,
        last_image_url: str,
        root_path: str,
        focals: List[int],
        images_format: str,
        instrument_type: str,
    ) -> CreateTimelapsePayload:
        return CreateTimelapsePayload(
            slideId=slide_id,
            wellEsId=well_es_id,
            runNumber=run_number,
            lastImageHour=last_image_hour,
            lastImageUrl=last_image_url,
            rootPath=root_path,
            focals=focals,
            imagesFormat=images_format,
            instrumentType=instrument_type,
        )
