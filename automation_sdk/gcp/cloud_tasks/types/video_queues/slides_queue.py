import os
from typing import List

from automation_sdk.gcp.cloud_tasks.types.task_queue import TaskQueue
from test.shared_packages.chloe_backend.slide_revision_manager.types.payloads.add_video_data_payload import (
    VideoDataPayload,
    Videos,
)


class SlidesQueue(TaskQueue):
    @property
    def name(self) -> str:
        return "slides-queue"

    @property
    def dest_api_url(self) -> str:
        return os.getenv("CHLOE_BASE_URL") + "imaging-crop/video"

    @property
    def message(
        self,
        es_id: str,
        run_number: int,
        last_image_url: str,
        last_image_hour: int,
        videos: Videos,
        video_frames_time: List[int],
    ) -> VideoDataPayload:
        return VideoDataPayload(
            esId=es_id,
            runNumber=run_number,
            lastImageHour=last_image_hour,
            lastImageUrl=last_image_url,
            videos=Videos,
            videoFramesTime=video_frames_time,
        )
