from abc import ABC, abstractmethod

from pydantic import BaseModel


class TaskQueue(ABC):
    @property
    @abstractmethod
    def name(self) -> str:
        ...

    @property
    @abstractmethod
    def dest_api_url(self) -> str:
        ...

    @property
    @abstractmethod
    def message(self, *args, **kwargs) -> BaseModel:
        ...
