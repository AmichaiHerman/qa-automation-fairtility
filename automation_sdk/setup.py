import os
from pathlib import Path
from shutil import rmtree, copytree

from setuptools import setup, find_packages

src_path = Path(os.environ["PWD"], "../../qa-automation-fairtility/automation_sdk")
dst_path = Path("./automation_sdk")

copytree(src_path, dst_path)

setup(
    name="automation-sdk",
    version="1.0.0",
    packages=find_packages(),
    install_requires=open("requirements.txt").read().splitlines(),
    package_dir={"": "."},
)

rmtree(dst_path)
