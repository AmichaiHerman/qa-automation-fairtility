import logging


def setup_logging():
    logging.basicConfig(
        level=logging.INFO,  # Set the desired log level (e.g., INFO, DEBUG, WARNING)
        format="%(asctime)s - %(levelname)s - %(message)s",
        filename="automation-logs.log",  # Specify the log file name
        filemode="w",  # Set the file mode (e.g., 'w' for write, 'a' for append)
    )
