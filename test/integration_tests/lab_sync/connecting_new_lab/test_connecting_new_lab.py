from typing import List

import pytest
from automation_sdk.console_logging.debug import Debug
from automation_sdk.lab_simulator.lab_sync_manager import LabSyncManager
from test.integration_tests.lab_sync.infra.dtos.patients_sync import (
    PatientsData,
    InstrumentsData,
)
from test.integration_tests.lab_sync.infra.dtos.slides_sync import SlidesSync
from test.integration_tests.lab_sync.infra.results_fetcher import (
    get_actual_results,
    get_expected_results,
)

TEST_CASES = {
    "patients_sync": [
        ("vitro-sanity-1", "636e5ff2bc0bd857a3d81c16"),
        ("vitro-sanity-2", "636e5ff2bc0bd857a3d81c16"),
        ("vitro-multi-slide-1", "636e5ff2bc0bd857a3d81c16"),
        ("vitro-multi-slide-2", "636e5ff2bc0bd857a3d81c16"),
        # ("geri-sanity-1", "63da8079c91eb52b5e425e12"),
        # ("geri-sanity-2", "63da8079c91eb52b5e425e12"),
        # ("geri-multi-slide-1", "63da8079c91eb52b5e425e12"),
        # ("geri-multi-slide-2", "63da8079c91eb52b5e425e12"),
    ],
    "slides_sync": [
        ("vitro-sanity-2", "636e5ff2bc0bd857a3d81c16"),
        # ("geri-sanity-1", "63da8079c91eb52b5e425e12"),
    ],
}


@pytest.mark.parametrize("scenario,tenant_id", TEST_CASES["patients_sync"])
def test_patients_sync(scenario: str, tenant_id: str, lab_sync: LabSyncManager) -> None:
    lab_sync.sync(scenario, tenant_id, clear_tenant=True, with_slides_sync=False)

    actual_results = get_actual_results(tenant_id, validate_slides_sync=False)
    expected_results = get_expected_results(scenario)
    validate_patients_data(
        actual_results.patients_sync.patients, expected_results.patients_sync.patients
    )
    validate_instruments_data(
        actual_results.patients_sync.instruments,
        expected_results.patients_sync.instruments,
        scenario
    )


@pytest.mark.parametrize("scenario,tenant_id", TEST_CASES["slides_sync"])
def test_slides_sync(scenario: str, tenant_id: str, lab_sync: LabSyncManager) -> None:
    lab_sync.sync(scenario, tenant_id, clear_tenant=True, with_slides_sync=True)
    Debug.log("Finished Lab Sync")
    actual_results = get_actual_results(tenant_id, validate_slides_sync=True)
    Debug.log("Finished gettign actial results")

    expected_results = get_expected_results(scenario)
    validate_patients_data(
        actual_results.patients_sync.patients, expected_results.patients_sync.patients
    )
    validate_instruments_data(
        actual_results.patients_sync.instruments,
        expected_results.patients_sync.instruments,
        scenario
    )
    validate_slides_sync(actual_results.slides_sync, expected_results.slides_sync)


def validate_patients_data(
    actual_patients_data: List[PatientsData], expected_patients_data: List[PatientsData]
) -> None:
    assert actual_patients_data == expected_patients_data


def validate_instruments_data(
    actual_instruments_data: InstrumentsData,
    expected_instruments_data: InstrumentsData,
    scenario: str
) -> None:
    if "geri" in scenario:
        assert (
            actual_instruments_data.geri_es_ids == expected_instruments_data.geri_es_ids
        )
    else:
        assert (
            actual_instruments_data.vitro_es_ids
            == expected_instruments_data.vitro_es_ids
        )


def validate_slides_sync(
    actual_slides_sync: SlidesSync, expected_slides_sync: SlidesSync
) -> None:
    """Checks data was uploaded successfully for slides-sync at agent-handler-db"""
    assert actual_slides_sync.wells == expected_slides_sync.wells
