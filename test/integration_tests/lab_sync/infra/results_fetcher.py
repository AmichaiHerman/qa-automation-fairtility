"""
A Helper Module that extracts the expected and actual results for the tests
"""
from test.integration_tests.lab_sync.expected_lab_sync_db import EXPECTED_LAB_SYNC
from test.integration_tests.lab_sync.infra.dtos.lab_sync_results import LabSyncResults
from test.integration_tests.lab_sync.infra.lab_sync_fetcher import LabSyncFetcher


def get_expected_results(scenario: str) -> LabSyncResults:
    """Fetches Expected lab-sync results for the tests to use"""
    return EXPECTED_LAB_SYNC[scenario]


def get_actual_results(tenant_id: str, validate_slides_sync: bool) -> LabSyncResults:
    """Fetches Actual lab-sync results for the tests to use"""
    return LabSyncFetcher(tenant_id).get_sync_results(
        with_slides_sync=validate_slides_sync
    )
