from typing import Optional

from pydantic import BaseModel

from test.integration_tests.lab_sync.infra.dtos.patients_sync import PatientsSync
from test.integration_tests.lab_sync.infra.dtos.slides_sync import SlidesSync


class LabSyncResults(BaseModel):
    patients_sync: Optional[PatientsSync]
    slides_sync: Optional[SlidesSync]
