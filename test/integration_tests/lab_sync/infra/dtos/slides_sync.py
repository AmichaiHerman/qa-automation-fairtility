from typing import List

from pydantic import BaseModel

from test.integration_tests.lab_sync.infra.dtos.abstract_sync_response import (
    AbstractSyncResponse,
)


class FetchedWell(BaseModel):
    focals: List[int]
    esId: str
    slideEsId: str
    is_storage_path_exists: bool


class SlidesSync(AbstractSyncResponse):
    wells: List[FetchedWell]
