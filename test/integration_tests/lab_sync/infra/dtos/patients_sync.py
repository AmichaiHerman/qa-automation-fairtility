from typing import Optional, List, Any

from pydantic import BaseModel

from test.integration_tests.lab_sync.infra.dtos.abstract_sync_response import (
    AbstractSyncResponse,
)


class SlidesData(BaseModel):
    es_id: Optional[str]
    status: Optional[str]
    number_of_wells: Optional[int]


class TreatmentsData(BaseModel):
    es_id: str
    start_date: Any
    slides: List[SlidesData]

    @property
    def number_of_slides(self) -> int:
        return len(self.slides)

    @property
    def slides_status(self) -> List[str]:
        status_list = []
        for slide in self.slides:
            status_list.append(slide.status)

        return status_list


class PatientsData(BaseModel):
    es_id: str
    identifier1: Optional[str]
    treatments: List[TreatmentsData]

    @property
    def patients_birthdate_list(self) -> List[Optional[Any]]:
        return [treatment.birthdate for treatment in self.treatments]


class InstrumentsData(BaseModel):
    es_ids: List[Any]

    @property
    def geri_es_ids(self) -> List[str]:
        as_strs = [str(i) for i in self.es_ids]
        return as_strs.sort()

    @property
    def vitro_es_ids(self) -> List[int]:
        return self.es_ids.sort()


class PatientsSync(AbstractSyncResponse):
    patients: List[PatientsData]
    instruments: InstrumentsData

    @property
    def number_of_patients(self) -> int:
        return len(self.patients)

    @property
    def number_of_slides_attached(self) -> int:
        total_slide_amount = 0
        patients_list = self.patients
        for patient in patients_list:
            for treatment in patient.treatments:
                total_slide_amount += treatment.number_of_slides

        return total_slide_amount

    @property
    def all_slide_statuses(self) -> List[str]:
        all_slides_statuses = []
        patients_list = self.patients
        for patient in patients_list:
            for treatment in patient.treatments:
                all_slides_statuses += treatment.slides_status

        return all_slides_statuses
