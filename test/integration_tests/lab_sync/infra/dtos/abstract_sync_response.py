from pydantic import BaseModel


class AbstractSyncResponse(BaseModel):
    ...
