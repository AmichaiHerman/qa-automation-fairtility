from test.integration_tests.lab_sync.infra.dtos.lab_sync_results import LabSyncResults
from test.integration_tests.lab_sync.infra.patients_sync_fetcher import (
    PatientsSyncFetcher,
)
from test.integration_tests.lab_sync.infra.slides_sync_fetcher import SlidesSyncFetcher


class LabSyncFetcher:
    def __init__(self, tenant_id: str):
        """Fetched Lab-Sync Data From Agent-Handler DB"""
        self.patient_sync_process = PatientsSyncFetcher(tenant_id)
        self.slide_sync_process = SlidesSyncFetcher(tenant_id)

    def get_sync_results(self, with_slides_sync: bool) -> LabSyncResults:
        """This method is performing the sync based on the testing scenario"""
        patients_data = self.patient_sync_process.fetch_patients_data()
        slides_data = (
            self.slide_sync_process.fetch_slides_data() if with_slides_sync else None
        )

        return LabSyncResults(patients_sync=patients_data, slides_sync=slides_data)
