from typing import List

from automation_sdk.mongo.mongo_orm.agent_handler.wells import Wells
from automation_sdk.mongo.repos.agent_handler_repo import AgentHandlerRepo
from test.integration_tests.lab_sync.infra.dtos.slides_sync import (
    SlidesSync,
    FetchedWell,
)


class SlidesSyncFetcher:
    def __init__(self, tenant_id: str):
        """Fetch the results related to the lab-connector slides-sync process"""
        self._agent_handler_repo = AgentHandlerRepo()
        self._tenant_id = tenant_id
        self._wells = self.__fetch_wells()

    def fetch_slides_data(self) -> SlidesSync:
        """Fetching Slides data from the wells"""
        fetched_wells = [
            FetchedWell(
                focals=well.focals,
                esId=well.esId,
                slideEsId=well.slideEsId,
                is_storage_path_exists=self.__check_for_storage_path(well),
            )
            for well in self._wells
        ]
        return SlidesSync(wells=fetched_wells)

    def filter_wells_by_slide_and_well_es_id(
        self, slide_es_id: str, well_es_id: str
    ) -> Wells:
        """Returns A Well With Some es_id from slide with some es_id"""
        wells_list = self.__fetch_wells()
        filtered_by_slide_es_id = list(
            filter(lambda well: well.slideEsId == slide_es_id, wells_list)
        )
        filtered_by_well_es_id = list(
            filter(lambda well: well.esId == well_es_id, filtered_by_slide_es_id)
        )
        return filtered_by_well_es_id[0]

    def __fetch_wells(self) -> List[Wells]:
        """Fetching Wells object from the agent handler repo"""
        return self._agent_handler_repo.find_wells_by_tenant(self._tenant_id)

    def __check_for_storage_path(self, well: Wells) -> bool:
        """Check if storage path exists for the specific well - which means it was uploaded to GCP storage"""
        return True if well.storagePath is not None else False
