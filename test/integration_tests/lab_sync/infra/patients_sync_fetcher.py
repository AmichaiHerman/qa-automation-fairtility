from typing import List, Any

from automation_sdk.mongo.mongo_orm.agent_handler.patientdatas import (
    Patientdatas,
    Slide,
    Treatment,
)
from automation_sdk.mongo.repos.agent_handler_repo import AgentHandlerRepo
from test.integration_tests.lab_sync.infra.dtos.patients_sync import (
    PatientsSync,
    PatientsData,
    TreatmentsData,
    SlidesData,
    InstrumentsData,
)


class DataNotFoundException(Exception):
    pass


class PatientsSyncFetcher:
    """Fetch the results related to the lab-connector patients-sync process"""

    def __init__(self, tenant_id: str) -> None:
        self._agent_handler_repo = AgentHandlerRepo()
        self._tenant_id = tenant_id
        self._patientdatas = self._agent_handler_repo.find_patients_data_by_tenant(
            self._tenant_id
        )

    def fetch_patients_data(self) -> PatientsSync:
        """Fetched Patient Sync Results"""
        patients_data = self.__get_patients_data()
        instruments_data = self.__get_instruments_data(self._patientdatas)
        return PatientsSync(patients=patients_data, instruments=instruments_data)

    def __get_patients_data(self) -> List[PatientsData]:
        """Fetches a list of PatientsData objects for the tenant."""
        return [
            PatientsData(
                es_id=record.esId,
                identifier1=record.identifier1,
                treatments=self.__get_treatments_data(record),
            )
            for record in self._patientdatas
        ]

    def __get_treatments_data(self, record: Patientdatas) -> List[TreatmentsData]:
        """Fetches a list of TreatmentsData objects for a Patientdatas record."""
        available_slides = self.__slides_list_per_record(record)
        return [
            TreatmentsData(
                es_id=treatment.esId,
                start_date=None,
                slides=self.__get_slides_data(available_slides),
            )
            for treatment in self.__treatment_list_per_record(record)
        ]

    def __get_slides_data(self, available_slides: List[Slide]) -> List[SlidesData]:
        """Fetches a list of SlidesData objects for a Patientdatas record."""
        return [
            SlidesData(
                es_id=slide.esId,
                status=slide.status,
                number_of_wells=slide.number_of_wells,
            )
            for slide in available_slides
        ]

    def __get_instruments_data(self, records: List[Patientdatas]) -> InstrumentsData:
        """Fetches a list of InstrumentsData objects for a Patientdatas record."""
        unique_instruments = {
            slide.instrumentEsId
            for record in records
            for slide in self.__slides_list_per_record(record)
        }

        return InstrumentsData(es_ids=unique_instruments)

    def __treatment_list_per_record(self, record: Patientdatas) -> List[Treatment]:
        """Returns a list of Treatment objects for a Patientdatas record."""
        return self.__fetch_items_per_record(record, "treatments")

    def __slides_list_per_record(self, record: Patientdatas) -> List[Slide]:
        """Returns a list of Slide objects for a Patientdatas record."""
        return self.__fetch_items_per_record(record, "slides")

    def __fetch_items_per_record(self, record: Patientdatas, attr: str) -> List[Any]:
        """Returns a list of objects for the specified attribute in a Patientdatas record (Treatments/Slides)"""
        items = getattr(record, attr)
        if not items:
            raise DataNotFoundException(
                f"No {attr} attached to {self._tenant_id} on the specific record"
            )

        return items
