import pytest

from automation_sdk.lab_simulator.lab_sync_manager import LabSyncManager


@pytest.fixture
def lab_sync() -> LabSyncManager:
    sync_manager = LabSyncManager()
    sync_manager.stop_active_syncs()
    yield sync_manager
    sync_manager.stop_active_syncs()
