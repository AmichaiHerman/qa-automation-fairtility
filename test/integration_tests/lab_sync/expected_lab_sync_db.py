"""
In Memory DB That is responsible for storing with expected results for the lab-sync tests
"""
from test.integration_tests.lab_sync.infra.dtos.lab_sync_results import LabSyncResults
from test.integration_tests.lab_sync.infra.dtos.patients_sync import (
    PatientsSync,
    PatientsData,
    TreatmentsData,
    SlidesData,
    InstrumentsData,
)
from test.integration_tests.lab_sync.infra.dtos.slides_sync import (
    FetchedWell,
    SlidesSync,
)

EXPECTED_LAB_SYNC = {
    "vitro-sanity-1": LabSyncResults(
        patients_sync=PatientsSync(
            patients=[
                PatientsData(
                    es_id="SCW-CK9300E38PZ_40095.5778277083",
                    identifier1=None,
                    treatments=[
                        TreatmentsData(
                            es_id="Algorithm",
                            start_date=None,
                            birthdate={"$date": "1985-02-01T00:00:00Z"},
                            slides=[
                                SlidesData(
                                    es_id="D2000.01.01_S0000_I000",
                                    status="IN_PROGRESS",
                                    number_of_wells=9,
                                )
                            ],
                        )
                    ],
                )
            ],
            instruments=InstrumentsData(es_ids=["123"]),
        ),
        slides_sync=None,
    ),
    "vitro-sanity-2": LabSyncResults(
        patients_sync=PatientsSync(
            patients=[
                PatientsData(
                    es_id="QA-AUTOMATION-DEMO-S00013",
                    identifier1=None,
                    treatments=[
                        TreatmentsData(
                            es_id="TIDX222",
                            start_date=None,
                            birthdate=None,
                            slides=[
                                SlidesData(
                                    es_id="D2018.11.06_S00013_I3169_P",
                                    status="IN_PROGRESS",
                                    number_of_wells=10,
                                )
                            ],
                        )
                    ],
                ),
                PatientsData(
                    es_id="QA-AUTOMATION-DEMO-S01225",
                    identifier1=None,
                    treatments=[
                        TreatmentsData(
                            es_id="TIDX001",
                            start_date=None,
                            birthdate=None,
                            slides=[
                                SlidesData(
                                    es_id="D2019.01.12_S01225_I0469_D",
                                    status="IN_PROGRESS",
                                    number_of_wells=8,
                                )
                            ],
                        )
                    ],
                ),
            ],
            instruments=InstrumentsData(es_ids=["469", "3169"]),
        ),
        slides_sync=SlidesSync(
            wells=[
                FetchedWell(
                    focals=[0, -3, -2, -1, 1, 2, 3],
                    esId="1",
                    slideEsId="D2018.11.06_S00013_I3169_P",
                    is_storage_path_exists=True,
                ),
                FetchedWell(
                    focals=[0, -3, -2, -1, 1, 2, 3],
                    esId="10",
                    slideEsId="D2018.11.06_S00013_I3169_P",
                    is_storage_path_exists=True,
                ),
                FetchedWell(
                    focals=[0, -3, -2, -1, 1, 2, 3],
                    esId="2",
                    slideEsId="D2018.11.06_S00013_I3169_P",
                    is_storage_path_exists=True,
                ),
                FetchedWell(
                    focals=[0, -3, -2, -1, 1, 2, 3],
                    esId="3",
                    slideEsId="D2018.11.06_S00013_I3169_P",
                    is_storage_path_exists=True,
                ),
                FetchedWell(
                    focals=[0, -3, -2, -1, 1, 2, 3],
                    esId="4",
                    slideEsId="D2018.11.06_S00013_I3169_P",
                    is_storage_path_exists=True,
                ),
                FetchedWell(
                    focals=[0, -3, -2, -1, 1, 2, 3],
                    esId="5",
                    slideEsId="D2018.11.06_S00013_I3169_P",
                    is_storage_path_exists=True,
                ),
                FetchedWell(
                    focals=[0, -3, -2, -1, 1, 2, 3],
                    esId="6",
                    slideEsId="D2018.11.06_S00013_I3169_P",
                    is_storage_path_exists=True,
                ),
                FetchedWell(
                    focals=[0, -3, -2, -1, 1, 2, 3],
                    esId="7",
                    slideEsId="D2018.11.06_S00013_I3169_P",
                    is_storage_path_exists=True,
                ),
                FetchedWell(
                    focals=[0, -3, -2, -1, 1, 2, 3],
                    esId="8",
                    slideEsId="D2018.11.06_S00013_I3169_P",
                    is_storage_path_exists=True,
                ),
                FetchedWell(
                    focals=[0, -3, -2, -1, 1, 2, 3],
                    esId="9",
                    slideEsId="D2018.11.06_S00013_I3169_P",
                    is_storage_path_exists=True,
                ),
                FetchedWell(
                    focals=[0, -3, -2, -1, 1, 2, 3],
                    esId="1",
                    slideEsId="D2019.01.12_S01225_I0469_D",
                    is_storage_path_exists=True,
                ),
                FetchedWell(
                    focals=[0, -3, -2, -1, 1, 2, 3],
                    esId="2",
                    slideEsId="D2019.01.12_S01225_I0469_D",
                    is_storage_path_exists=True,
                ),
                FetchedWell(
                    focals=[0, -3, -2, -1, 1, 2, 3],
                    esId="3",
                    slideEsId="D2019.01.12_S01225_I0469_D",
                    is_storage_path_exists=True,
                ),
                FetchedWell(
                    focals=[0, -3, -2, -1, 1, 2, 3],
                    esId="4",
                    slideEsId="D2019.01.12_S01225_I0469_D",
                    is_storage_path_exists=True,
                ),
                FetchedWell(
                    focals=[0, -3, -2, -1, 1, 2, 3],
                    esId="5",
                    slideEsId="D2019.01.12_S01225_I0469_D",
                    is_storage_path_exists=True,
                ),
                FetchedWell(
                    focals=[0, -3, -2, -1, 1, 2, 3],
                    esId="6",
                    slideEsId="D2019.01.12_S01225_I0469_D",
                    is_storage_path_exists=True,
                ),
                FetchedWell(
                    focals=[0, -3, -2, -1, 1, 2, 3],
                    esId="7",
                    slideEsId="D2019.01.12_S01225_I0469_D",
                    is_storage_path_exists=True,
                ),
                FetchedWell(
                    focals=[0, -3, -2, -1, 1, 2, 3],
                    esId="8",
                    slideEsId="D2019.01.12_S01225_I0469_D",
                    is_storage_path_exists=True,
                ),
            ]
        ),
    ),
    "vitro-multi-slide-1": LabSyncResults(
        patients_sync=PatientsSync(
            patients=[
                PatientsData(
                    es_id="QA-AUTOMATION-DEMO-S01225",
                    identifier1=None,
                    treatments=[
                        TreatmentsData(
                            es_id="TIDX222",
                            start_date=None,
                            birthdate=None,
                            slides=[
                                SlidesData(
                                    es_id="D2018.11.06_S00013_I3169_P",
                                    status="IN_PROGRESS",
                                    number_of_wells=10,
                                ),
                                SlidesData(
                                    es_id="D2019.01.12_S01225_I0469_D",
                                    status="IN_PROGRESS",
                                    number_of_wells=8,
                                ),
                            ],
                        )
                    ],
                )
            ],
            instruments=InstrumentsData(es_ids=["469", "3169"]),
        ),
        slides_sync=None,
    ),
    "vitro-multi-slide-2": LabSyncResults(
        patients_sync=PatientsSync(
            patients=[
                PatientsData(
                    es_id="QA-AUTOMATION-DEMO-S01225",
                    identifier1=None,
                    treatments=[
                        TreatmentsData(
                            es_id="TIDX222",
                            start_date=None,
                            birthdate=None,
                            slides=[
                                SlidesData(
                                    es_id="D2018.11.06_S00013_I3169_P",
                                    status="IN_PROGRESS",
                                    number_of_wells=10,
                                ),
                                SlidesData(
                                    es_id="D2019.01.12_S01225_I0469_D",
                                    status="IN_PROGRESS",
                                    number_of_wells=8,
                                ),
                            ],
                        )
                    ],
                )
            ],
            instruments=InstrumentsData(es_ids=["469"]),
        ),
        slides_sync=None,
    ),
    "vitro-move-slide-to-done": LabSyncResults(
        patients_sync=PatientsSync(
            patients=[
                PatientsData(
                    es_id="QA-AUTOMATION-DEMO-S01225",
                    identifier1=None,
                    treatments=[
                        TreatmentsData(
                            es_id="TIDX222",
                            start_date=None,
                            birthdate=None,
                            slides=[
                                SlidesData(
                                    es_id="D2019.01.12_S01225_I0469_D",
                                    status="IN_PROGRESS",
                                    number_of_wells=8,
                                ),
                                SlidesData(
                                    es_id="D2018.11.06_S00013_I3169_P",
                                    status="IN_PROGRESS",
                                    number_of_wells=10,
                                ),
                            ],
                        )
                    ],
                )
            ],
            instruments=InstrumentsData(es_ids=["469"]),
        ),
        slides_sync=None,
    ),
    "geri-sanity-1": LabSyncResults(
        patients_sync=PatientsSync(
            patients=[
                PatientsData(
                    es_id="65744",
                    identifier1=None,
                    treatments=[
                        TreatmentsData(
                            es_id="10023",
                            start_date=None,
                            birthdate={"$date": "1980-01-01T00:00:00Z"},
                            slides=[
                                SlidesData(
                                    es_id="6d8b04d1-9d82-11ed-9666-408d5cb61ff4",
                                    status="IN_PROGRESS",
                                    number_of_wells=10,
                                )
                            ],
                        )
                    ],
                )
            ],
            instruments=InstrumentsData(es_ids=["GERI00131"]),
        ),
        slides_sync=SlidesSync(
            wells=[
                FetchedWell(
                    focals=[-5, -4, -3, -2, -1, 0, 1, 2, 3, 4, 5],
                    esId="1",
                    slideEsId="6d8b04d1-9d82-11ed-9666-408d5cb61ff4",
                    is_storage_path_exists=True,
                ),
                FetchedWell(
                    focals=[-5, -4, -3, -2, -1, 0, 1, 2, 3, 4, 5],
                    esId="10",
                    slideEsId="6d8b04d1-9d82-11ed-9666-408d5cb61ff4",
                    is_storage_path_exists=True,
                ),
                FetchedWell(
                    focals=[-5, -4, -3, -2, -1, 0, 1, 2, 3, 4, 5],
                    esId="2",
                    slideEsId="6d8b04d1-9d82-11ed-9666-408d5cb61ff4",
                    is_storage_path_exists=True,
                ),
                FetchedWell(
                    focals=[-5, -4, -3, -2, -1, 0, 1, 2, 3, 4, 5],
                    esId="3",
                    slideEsId="6d8b04d1-9d82-11ed-9666-408d5cb61ff4",
                    is_storage_path_exists=True,
                ),
                FetchedWell(
                    focals=[-5, -4, -3, -2, -1, 0, 1, 2, 3, 4, 5],
                    esId="4",
                    slideEsId="6d8b04d1-9d82-11ed-9666-408d5cb61ff4",
                    is_storage_path_exists=True,
                ),
                FetchedWell(
                    focals=[-5, -4, -3, -2, -1, 0, 1, 2, 3, 4, 5],
                    esId="5",
                    slideEsId="6d8b04d1-9d82-11ed-9666-408d5cb61ff4",
                    is_storage_path_exists=True,
                ),
                FetchedWell(
                    focals=[-5, -4, -3, -2, -1, 0, 1, 2, 3, 4, 5],
                    esId="6",
                    slideEsId="6d8b04d1-9d82-11ed-9666-408d5cb61ff4",
                    is_storage_path_exists=True,
                ),
                FetchedWell(
                    focals=[-5, -4, -3, -2, -1, 0, 1, 2, 3, 4, 5],
                    esId="7",
                    slideEsId="6d8b04d1-9d82-11ed-9666-408d5cb61ff4",
                    is_storage_path_exists=True,
                ),
                FetchedWell(
                    focals=[-5, -4, -3, -2, -1, 0, 1, 2, 3, 4, 5],
                    esId="8",
                    slideEsId="6d8b04d1-9d82-11ed-9666-408d5cb61ff4",
                    is_storage_path_exists=True,
                ),
                FetchedWell(
                    focals=[-5, -4, -3, -2, -1, 0, 1, 2, 3, 4, 5],
                    esId="9",
                    slideEsId="6d8b04d1-9d82-11ed-9666-408d5cb61ff4",
                    is_storage_path_exists=True,
                ),
            ]
        ),
    ),
    "geri-sanity-2": LabSyncResults(
        patients_sync=PatientsSync(
            patients=[
                PatientsData(
                    es_id="63649",
                    identifier1=None,
                    treatments=[
                        TreatmentsData(
                            es_id="10723",
                            start_date=None,
                            birthdate={"$date": "1980-01-01T00:00:00Z"},
                            slides=[
                                SlidesData(
                                    es_id="45de3e9e-9e3d-11ed-827c-408d5cb61ff4",
                                    status="IN_PROGRESS",
                                    number_of_wells=9,
                                )
                            ],
                        )
                    ],
                ),
                PatientsData(
                    es_id="65744",
                    identifier1=None,
                    treatments=[
                        TreatmentsData(
                            es_id="10023",
                            start_date=None,
                            birthdate={"$date": "1980-01-01T00:00:00Z"},
                            slides=[
                                SlidesData(
                                    es_id="6d8b04d1-9d82-11ed-9666-408d5cb61ff4",
                                    status="IN_PROGRESS",
                                    number_of_wells=10,
                                )
                            ],
                        )
                    ],
                ),
            ],
            instruments=InstrumentsData(es_ids=["GERI00131"]),
        ),
        slides_sync=None,
    ),
    "geri-multi-slide-1": LabSyncResults(
        patients_sync=PatientsSync(
            patients=[
                PatientsData(
                    es_id="65744",
                    identifier1=None,
                    treatments=[
                        TreatmentsData(
                            es_id="10023",
                            start_date=None,
                            birthdate={"$date": "1980-01-01T00:00:00Z"},
                            slides=[
                                SlidesData(
                                    es_id="6d8b04d1-9d82-11ed-9666-408d5cb61ff4",
                                    status="IN_PROGRESS",
                                    number_of_wells=10,
                                ),
                                SlidesData(
                                    es_id="45de3e9e-9e3d-11ed-827c-408d5cb61ff4",
                                    status="IN_PROGRESS",
                                    number_of_wells=9,
                                ),
                            ],
                        )
                    ],
                )
            ],
            instruments=InstrumentsData(es_ids=["GERI000087", "GERI00131"]),
        ),
        slides_sync=None,
    ),
    "geri-multi-slide-2": LabSyncResults(
        patients_sync=PatientsSync(
            patients=[
                PatientsData(
                    es_id="65744",
                    identifier1=None,
                    treatments=[
                        TreatmentsData(
                            es_id="10023",
                            start_date=None,
                            birthdate={"$date": "1980-01-01T00:00:00Z"},
                            slides=[
                                SlidesData(
                                    es_id="6d8b04d1-9d82-11ed-9666-408d5cb61ff4",
                                    status="IN_PROGRESS",
                                    number_of_wells=10,
                                ),
                                SlidesData(
                                    es_id="45de3e9e-9e3d-11ed-827c-408d5cb61ff4",
                                    status="IN_PROGRESS",
                                    number_of_wells=9,
                                ),
                            ],
                        )
                    ],
                )
            ],
            instruments=InstrumentsData(es_ids=["GERI00131"]),
        ),
        slides_sync=None,
    ),
}
