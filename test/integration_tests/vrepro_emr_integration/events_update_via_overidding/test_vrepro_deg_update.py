import pytest

from test.integration_tests.vrepro_emr_integration.test_flow_runner import run_test_flow
from test.integration_tests.vrepro_emr_integration.vrepro_validations.assertion_funcs import (
    validate_deg_update,
)
from test.shared_packages.chloe_backend.patient_data.patient_data_routes import (
    PatientDataRoutes,
)
from test.shared_packages.chloe_backend.patient_data.types.requests_data.get_params import (
    GetWellQueryParams,
)
from test.shared_packages.chloe_backend.patient_data.types.requests_data.payloads import (
    DEG,
)
from test.shared_packages.test_data.test_users import VREPRO_UPDATE

TEST_USER = [
    (VREPRO_UPDATE, "63fcc9a03ac27a5f17b4450e", "update-DEG"),
]


@pytest.mark.parametrize("creds, tenant_id, scenario", TEST_USER)
@pytest.mark.skip("Until vrepro bugs fix!")
def test_vrepro_deg_update(creds, tenant_id, scenario, vrepro):
    # invoke change the pn value of some well
    patient_data = PatientDataRoutes(creds)
    patient_data.wells_route.activate_inference_overriding(
        params=GetWellQueryParams(well_id="63fcd4c57d8d0400e84903c4"),
        payload=DEG(degeneratedOocyte=True),
    )

    run_test_flow(creds, scenario, vrepro, validate_deg_update)
