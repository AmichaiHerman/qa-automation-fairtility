import pytest

from test.integration_tests.vrepro_emr_integration.test_flow_runner import run_test_flow
from test.integration_tests.vrepro_emr_integration.vrepro_validations.assertion_funcs import (
    validate_day1_import,
    validate_day2_import,
    validate_day3_import,
    validate_day4_import,
    validate_day5_import,
)
from test.shared_packages.test_data.test_users import (
    VREPRO_1,
    VREPRO_GERI,
    VREPRO_ADVANCED_DAYS_1,
    VREPRO_ADVANCED_DAYS_2,
)

DAY1_USERS = [
    (VREPRO_GERI, "63625ad74d08b24f8448a2fc", "day1-geri"),  # geri
    # (VREPRO_1, "63f4872aaf0fc24bd0922922", "day1-vitro"),  # vitro
]


@pytest.mark.parametrize("creds, tenant_id, scenario", DAY1_USERS)
@pytest.mark.skip("Until vrepro bugs fix!")
def test_day1_import(creds, tenant_id, scenario, vrepro):
    run_test_flow(creds, scenario, vrepro, validate_day1_import)


DAY2_USERS = [(VREPRO_ADVANCED_DAYS_1, "63fcf1153ac27af2b8b44516", "day2")]


@pytest.mark.parametrize("creds, tenant_id, scenario", DAY2_USERS)
@pytest.mark.skip("Until vrepro bugs fix!")
def test_day2_import(creds, tenant_id, scenario, vrepro):
    run_test_flow(creds, scenario, vrepro, validate_day2_import)


DAY3_TO_DAY5_USERS = [(VREPRO_ADVANCED_DAYS_2, "63fdfff0c258a3ec075c3ac8", "days3-5")]


@pytest.mark.parametrize("creds, tenant_id, scenario", DAY3_TO_DAY5_USERS)
@pytest.mark.skip("Until vrepro bugs fix!")
def test_day3_import(creds, tenant_id, scenario, vrepro):
    run_test_flow(creds, scenario, vrepro, validate_day3_import)


@pytest.mark.parametrize("creds, tenant_id, scenario", DAY3_TO_DAY5_USERS)
@pytest.mark.skip("Until vrepro bugs fix!")
def test_day4_import(creds, tenant_id, scenario, vrepro):
    run_test_flow(creds, scenario, vrepro, validate_day4_import)


@pytest.mark.parametrize("creds, tenant_id, scenario", DAY3_TO_DAY5_USERS)
@pytest.mark.skip("Until vrepro bugs fix!")
def test_day5_import(creds, tenant_id, scenario, vrepro):
    run_test_flow(creds, scenario, vrepro, validate_day5_import)
