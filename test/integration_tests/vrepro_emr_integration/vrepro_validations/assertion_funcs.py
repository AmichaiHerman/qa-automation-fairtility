from test.integration_tests.vrepro_emr_integration.embryo_monitoring_expected_results import (
    EmbryoMonitoringData,
)
from test.shared_packages.third_party_intgreations.vrepro.vrepro_embryo_response import (
    FivLabLinItem,
)

""" A Util for describing vrepro assertions for each test case """


def validate_deg_update(
    expected_actual_data: EmbryoMonitoringData, actual_embryo_data: FivLabLinItem
):
    assert expected_actual_data.lab_data.status == actual_embryo_data.estado_dia1_name


def validate_duc_update(
    expected_actual_data: EmbryoMonitoringData, actual_embryo_data: FivLabLinItem
):
    assert (
        expected_actual_data.lab_data.day2_duc == actual_embryo_data.pre2_div_dir_name
    )


def validate_pn_update(
    expected_actual_data: EmbryoMonitoringData, actual_embryo_data: FivLabLinItem
):
    assert expected_actual_data.lab_data.pn_count == actual_embryo_data.zi_pro_num


def validate_day1_import(
    expected_actual_data: EmbryoMonitoringData, actual_embryo_data: FivLabLinItem
):
    validate_pn_update(expected_actual_data, actual_embryo_data)
    assert expected_actual_data.lab_data.status == actual_embryo_data.estado_dia1_name


def validate_day2_import(
    expected_actual_data: EmbryoMonitoringData, actual_embryo_data: FivLabLinItem
):
    assert (
        expected_actual_data.lab_data.day2_cell_event == actual_embryo_data.pre2_bla_num
    )
    assert expected_actual_data.lab_data.day2_frag == actual_embryo_data.pre2_frag_name
    assert (
        expected_actual_data.lab_data.day2_duc == actual_embryo_data.pre2_div_dir_name
    )
    assert expected_actual_data.lab_data.status == actual_embryo_data.estado_dia1_name


def validate_day3_import(
    expected_actual_data: EmbryoMonitoringData, actual_embryo_data: FivLabLinItem
):
    assert (
        expected_actual_data.lab_data.day3_cell_event == actual_embryo_data.pre3_bla_num
    )
    assert expected_actual_data.lab_data.day3_frag == actual_embryo_data.pre3_frag_name
    assert (
        expected_actual_data.lab_data.day3_expansion_state
        == actual_embryo_data.pre3_mor_name
    )


def validate_day4_import(
    expected_actual_data: EmbryoMonitoringData, actual_embryo_data: FivLabLinItem
):
    assert (
        expected_actual_data.lab_data.day4_cell_event == actual_embryo_data.pre4_bla_num
    )
    assert (
        expected_actual_data.lab_data.day4_expansion_state
        == actual_embryo_data.pre4_gra_exp_name
    )
    assert (
        expected_actual_data.lab_data.medical_decision
        == actual_embryo_data.destino_name
    )


def validate_day5_import(
    expected_actual_data: EmbryoMonitoringData, actual_embryo_data: FivLabLinItem
):
    assert (
        expected_actual_data.lab_data.day5_expansion_state
        == actual_embryo_data.pre5_gra_exp_name
    )


def validate_data_limitations(
    expected_actual_data: EmbryoMonitoringData, actual_embryo_data: FivLabLinItem
):
    assert (
        expected_actual_data.lab_data.day2_cell_event == actual_embryo_data.pre2_bla_num
    )
    assert expected_actual_data.lab_data.day2_frag == actual_embryo_data.pre2_frag_name
    assert actual_embryo_data.pre3_bla_num == 0  # on day 3 it reached 4
    assert actual_embryo_data.pre3_frag_name == ""  # on day 3 it stays frag.
