from typing import List, Callable

from automation_sdk.console_logging.debug import Debug
from test.integration_tests.vrepro_emr_integration.embryo_monitoring_expected_results import (
    EmbryoMonitoringData,
)
from test.shared_packages.assertions.retried_assertions import assert_with_retry
from test.shared_packages.third_party_intgreations.vrepro.vrepro_intgreation import (
    VreproIntegration,
)


def validate_vRepro_integration_success(
    expected_embryos_data: List[EmbryoMonitoringData], assertions_function: Callable
) -> None:
    """A Wrapper function to assert flaky vRepro integration with retries"""
    for embryo_data in expected_embryos_data:
        embryo_id = embryo_data.embryo_id
        assert_with_retry(
            lambda: _compare(assertions_function, embryo_data, embryo_id),
            tries=15,
            delay=0.5,
        )


def _compare(
    assertions_function: Callable,
    expected_results: EmbryoMonitoringData,
    embryo_id: str,
):
    """Actual Comparison"""
    Debug.log(f"checking data import to vRepro for embryo {embryo_id}")
    actual_vRepro_response = (
        VreproIntegration().check_embryo_monitoring_data(embryo_id).fiv_lab_lin[0]
    )
    Debug.log(f"Actual vRepro response is  {actual_vRepro_response.json()}")
    Debug.log(
        f"Expected Results for the embryo {embryo_id} are:{expected_results.lab_data.json()}"
    )
    assertions_function(expected_results, actual_vRepro_response)
