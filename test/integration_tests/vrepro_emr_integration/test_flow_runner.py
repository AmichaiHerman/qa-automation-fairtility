from typing import Callable

from automation_sdk.atomic_types import UserCredentials
from automation_sdk.http_requests.status_codes import StatusCode
from test.integration_tests.vrepro_emr_integration.expected_payloads import (
    expected_payloads,
)
from test.integration_tests.vrepro_emr_integration.expected_results import (
    expected_results,
)
from test.integration_tests.vrepro_emr_integration.vrepro_validations.vrepro_assertions_handler import (
    validate_vRepro_integration_success,
)
from test.shared_packages.chloe_backend.vrepro.vrepro_routes import VreproRoutes


def run_test_flow(
    creds: UserCredentials,
    scenario: str,
    vRepro_service: VreproRoutes,
    assertion_func: Callable,
) -> None:
    resp = vRepro_service.export_patient_data(
        payload=expected_payloads()[creds.user_name]
    )
    print(resp.text)
    assert resp.status_code == StatusCode.CREATED.value
    expected_res = expected_results()[scenario]
    validate_vRepro_integration_success(expected_res, assertion_func)
