from typing import Optional

from pydantic import BaseModel


class LabData(BaseModel):
    pn_count: Optional[int] = 0
    status: Optional[str] = "No Fecundado"
    day2_duc: Optional[str] = ""
    day2_cell_event: Optional[int] = None
    day2_frag: Optional[str] = ""
    day3_expansion_state: Optional[str] = ""
    day3_cell_event: Optional[int] = None
    day3_frag: Optional[str] = ""
    day4_expansion_state: Optional[str] = ""
    day4_cell_event: Optional[int] = ""
    day5_expansion_state: Optional[str] = ""
    medical_decision: Optional[str] = ""


class EmbryoMonitoringData(BaseModel):
    lab_data: LabData
