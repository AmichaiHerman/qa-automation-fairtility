from typing import Dict

from test.shared_packages.chloe_backend.vrepro.types.sync_payload import (
    VreproSyncPayload,
    Patient,
    Instrument,
    Treatment,
    ReqSlide,
)


def expected_payloads() -> Dict[str, VreproSyncPayload]:
    # a function that maps the specific username to its specific payload
    return {
        "vrepro-update": VreproSyncPayload(
            patient=Patient(esId="0000545", id="63fcd2717d8d04e8fc4903bd"),
            tenantId="63fcc9a03ac27a5f17b4450e",
            instrument=Instrument(id="63fcd2717d8d04703b4903c2", esId="11"),
            treatment=Treatment(id="63fcd2717d8d04732a4903bf"),
            slide=ReqSlide(
                id="63fcd2717d8d04a5e94903c3",
                esId="S0000",
                wells=[
                    "63fcd4c57d8d0400e84903c4",
                    "63fcd4c57d8d04231b4903e1",
                    "63fcd4c57d8d040c8b4903fe",
                    "63fcd4c57d8d04ea2049041b",
                    "63fcd4c57d8d04b2b3490438",
                    "63fcd4c57d8d0478ff490455",
                    "63fcd4c57d8d04381d490472",
                    "63fcd4c57d8d0486f549048f",
                ],
            ),
        ),
        "vrepro-testings": VreproSyncPayload(
            patient=Patient(esId="0000545", id="63fc83985c9431cc58448542"),
            tenantId="63f4872aaf0fc24bd0922922",
            instrument=Instrument(id="63fc83995c94319b6a448549", esId="3169"),
            treatment=Treatment(id="63fc83995c9431ad01448545", esId="TIDX222"),
            slide=ReqSlide(
                id="63fc83995c94312d2a44854a",
                esId="S00013",
                wells=[
                    "63fc84eb5c9431688144863c",
                    "63fc84eb5c94310886448677",
                    "63fc84eb5c94311f44448695",
                    "63fc84eb5c943129414486b3",
                    "63fc84eb5c94313e624486d1",
                    "63fc84eb5c943126ed4486ef",
                    "63fc84eb5c9431016844870d",
                    "63fc84eb5c9431bd7b44872b",
                    "63fc84ec5c9431243d448749",
                    "63fc84eb5c943160c944865a",
                ],
            ),
        ),
        "crop-geri": VreproSyncPayload(
            patient=Patient(esId="0000545", id="63c42560666a5e3c8c56a2a1"),
            tenantId="63625ad74d08b24f8448a2fc",
            instrument=Instrument(id="63c42560666a5e141456a29f", esId="GERI00430"),
            treatment=Treatment(id="63c42561666a5e27d856a2a4", esId="fiv 223011"),
            slide=ReqSlide(
                id="63c42561666a5eff1e56a2a8",
                esId="273f75cf-90c9-11ed-8a05-c400ad4629c0",
                wells=[
                    "63c425fc666a5e3f8e56a3ea",
                    "63c425fc666a5eda5456a40f",
                    "63c425fc666a5ee61256a434",
                    "63c425fc666a5e681f56a459",
                    "63c425fc666a5ef2f556a47e",
                    "63c425fc666a5e6d7656a4a3",
                    "63c425fc666a5e162456a4c8",
                    "63c425fc666a5ef55756a4ed",
                    "63c425fc666a5e933456a512",
                ],
            ),
        ),
        "vrepro-day-three": VreproSyncPayload(
            patient=Patient(esId="0000545", id="63fcf18b7d8d042d624904b3"),
            tenantId="63fcf1153ac27af2b8b44516",
            instrument=Instrument(id="63fcf18b7d8d0427264904b8", esId="11"),
            treatment=Treatment(id="63fcf18b7d8d0446e64904b5", esId="Algorithm"),
            slide=ReqSlide(
                id="63fcf18b7d8d0417714904b9",
                esId="S0000",
                wells=[
                    "63fcfcf67d8d0406ad490625",
                    "63fcfcf67d8d04fc84490644",
                    "63fcfcf77d8d0444ef490661",
                    "63fcfcf77d8d0490fc49067e",
                    "63fcfcf77d8d0489a349069b",
                    "63fcfcf77d8d0462234906b8",
                    "63fcfcf77d8d04354e4906d5",
                    "63fcfcf77d8d04f32e4906f2",
                ],
            ),
        ),
        "vrepro-advanced-days": VreproSyncPayload(
            patient=Patient(esId="0000545", id="63da5351a50d7e7af5848832"),
            tenantId="63fdfff0c258a3ec075c3ac8",
            instrument=Instrument(id="63da5350a50d7e7af584882f", esId="GERI00031"),
            treatment=Treatment(id="63da5351a50d7e7af5848831", esId="8623"),
            slide=ReqSlide(
                id="63da5351a50d7e7af5848830",
                esId="afa74c00-9b27-11ed-994e-408d5c143b9f",
                wells=[
                    "63da5355a50d7e7af5848876",
                    "63da5355a50d7e7af584887b",
                    "63da5354a50d7e7af5848872",
                    "63da5355a50d7e7af584887c",
                    "63da5355a50d7e7af5848873",
                    "63da5355a50d7e7af5848874",
                    "63da5355a50d7e7af584887d",
                    "63da5355a50d7e7af5848875",
                    "63da5355a50d7e7af584887e",
                    "63da5355a50d7e7af5848877",
                    "63da5354a50d7e7af5848870",
                    "63da5355a50d7e7af5848878",
                    "63da5354a50d7e7af5848871",
                    "63da5355a50d7e7af5848879",
                    "63da5355a50d7e7af584887a",
                ],
            ),
        ),
    }
