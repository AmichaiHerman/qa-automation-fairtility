from typing import Dict, List

from test.integration_tests.vrepro_emr_integration.embryo_monitoring_expected_results import (
    EmbryoMonitoringData,
    LabData,
)


def expected_results() -> Dict[str, List[EmbryoMonitoringData]]:
    # expected results func that maps the scenario with it's expected embryo data
    return {
        "day1-geri": [
            EmbryoMonitoringData(
                embryo_id="957", lab_data=LabData(pn_count=0, status="No Fecundado")
            )
        ],
        "day1-vitro": [
            EmbryoMonitoringData(
                embryo_id="955", lab_data=LabData(pn_count=2, status="Fecundado")
            )
        ],
        "day2": [
            EmbryoMonitoringData(
                embryo_id="958",
                lab_data=LabData(
                    pn_count=2,
                    status="Fecundado",
                    day2_duc="Sí",
                    day2_cell_event=3,
                    day2_frag="26-35%",
                ),
            ),
            EmbryoMonitoringData(
                embryo_id="961",
                lab_data=LabData(
                    pn_count=1,
                    status="Fec. Anómalo ",
                    day2_duc="",
                    day2_cell_event=4,
                    day2_frag="",
                ),
            ),
            EmbryoMonitoringData(
                embryo_id="962",
                lab_data=LabData(
                    pn_count=3,
                    status="Fec. Anómalo ",
                    day2_duc="Sí",
                    day2_cell_event=6,
                    day2_frag="26-35%",
                ),
            ),
            EmbryoMonitoringData(
                embryo_id="959",
                lab_data=LabData(
                    pn_count=3,
                    status="No Fecundado",
                    day2_duc="Sí",
                    day2_cell_event=5,
                    day2_frag="",
                ),
            ),
        ],
        "days3-5": [
            EmbryoMonitoringData(
                embryo_id="955",
                lab_data=LabData(
                    day3_frag="",
                    day3_cell_event=9,
                    day4_cell_event=0,
                    day4_expansion_state="Blastocisto temprano",
                    medical_decision="Transferido",
                ),
            ),
            EmbryoMonitoringData(
                embryo_id="956",
                lab_data=LabData(
                    day3_frag="",
                    day3_cell_event=9,
                    day4_cell_event=0,
                    day4_expansion_state="Morula",
                    medical_decision="Criopreservado",
                ),
            ),
            EmbryoMonitoringData(
                embryo_id="958",
                lab_data=LabData(
                    day3_frag="26-35%",
                    day3_cell_event=0,
                    day4_cell_event=0,
                    day4_expansion_state="Blastocisto temprano",
                    medical_decision="Descartado",
                ),
            ),
            EmbryoMonitoringData(
                embryo_id="959",
                lab_data=LabData(
                    day3_cell_event=9,
                    day4_cell_event=9,
                    day4_expansion_state="Morula",
                    day5_expansion_state="Blastocisto",
                    medical_decision="Criopreservado",
                ),
            ),
        ],
        "update-PN": [
            EmbryoMonitoringData(
                embryo_id="955", lab_data=LabData(pn_count=2, status="Fecundado")
            )
        ],
        "update-DUC": [
            EmbryoMonitoringData(embryo_id="955", lab_data=LabData(day2_duc="Sí"))
        ],
        "update-DEG": [
            EmbryoMonitoringData(embryo_id="955", lab_data=LabData(status="Degenerado"))
        ],
    }
