#!/bin/bash

# Check if a test suite name was provided as an argument
#!/bin/bash

# fetch secrets
if [ -z "$1" ]; then
  echo "Error: Please provide env for testing"
  exit 1
  fi

ENV=$1

echo "Trying to pull secrets"

SECRET=$(gcloud secrets versions access latest --secret="automation-$ENV")

echo "Trying to create env variables"

SECRET=$(echo $SECRET | jq -r 'to_entries[] | .key + "=" + .value')

for line in $SECRET; do
  export "$line"
  done

# Run the tests for the service
python3 -m pytest -s -v --alluredir=results --allure-project="integration-nightly" --reruns 2 --reruns-delay 1