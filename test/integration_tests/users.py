from test.shared_packages.atomic_types import UserCredentials

DATA_IMPORT1 = UserCredentials(user_name="bulk-import3", password="Bulk123!")
DATA_IMPORT2 = UserCredentials(user_name="bulk-import4", password="E+sKMk?H&1")
DATA_IMPORT3 = UserCredentials(user_name="bulk-import5", password="WidP~YD$1v")
DATA_IMPORT4 = UserCredentials(user_name="bulk-import6", password="ng,D9s)5}G")
