# Fairtility's Integration Testing Framework #

This package contains the complete integration testing framework, designed to test the main processes inside
fairtility's backend.

## Coverage ##

Right Now, The Framework Contains Coverage to these processes:

```lab-sync``` - A process that's responsible for uploading clinic data (patient data and slides) to CHLOE. 
### note - Geri Data gets updated with geri-update-data job on jenkins. code is in lab simulator repo. job is in https://jenkins.backend.fairtility.com/job/QA-Automation/job/Automation-Jobs/job/geri-records-sync-job/

* Services Covered In ths spec: ```lab-connector``` -> ```agent-handler```

```images upload``` - A process that's responsible for uploading images (patient data and slides) to CHLOE.

* Services Covered In ths spec: ```lab-connector``` -> ```Google Cloud Storage```

```data import``` - A process that's responsible for moving clinic data (patient data and slides) inside CHLOE services,
until te UI.

* Services Covered In ths spec: ```agent-handler``` -> ```slide-revision-manager``` -> ```patient-data```

```vRepro``` - Our integration with the vRepro EMR Server ,
until te UI.

* Services Covered In ths spec: ```vrepro (chloe)``` -> ```vrepro EMR test server```

## Usage ##

to run the tests locally, please make sure that:

* Dependencies Installed. use ```pip install -r requirements.txt```
* VPN is turned on (Test is Connecting to mongo DB and GCP)
* Gcloud CLI is installed. for more details please visit https://cloud.google.com/sdk/docs/install


* To Run All The Tests Together, use ```pytest```


