from test.integration_tests.tenants import BULK_IMPORT_1, BULK_IMPORT_2
from test.integration_tests.users import DATA_IMPORT1, DATA_IMPORT2
from test.shared_packages.atomic_types import Vendor

SINGLE_TLI = [
    (DATA_IMPORT1, BULK_IMPORT_1, Vendor.VITRO),
    (DATA_IMPORT1, BULK_IMPORT_1, Vendor.GERI),
    (DATA_IMPORT1, BULK_IMPORT_1, Vendor.MIRI),
]

MULTI_TLI = [
    (DATA_IMPORT2, BULK_IMPORT_2, Vendor.VITRO, Vendor.VITRO),
    (DATA_IMPORT2, BULK_IMPORT_2, Vendor.VITRO, Vendor.GERI),
    (DATA_IMPORT2, BULK_IMPORT_2, Vendor.VITRO, Vendor.MIRI),
    (DATA_IMPORT2, BULK_IMPORT_2, Vendor.GERI, Vendor.GERI),
    (DATA_IMPORT2, BULK_IMPORT_2, Vendor.GERI, Vendor.MIRI),
    (DATA_IMPORT2, BULK_IMPORT_2, Vendor.MIRI, Vendor.MIRI),
]
