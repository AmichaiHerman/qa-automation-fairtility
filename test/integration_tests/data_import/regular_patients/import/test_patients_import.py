import pytest

from test.integration_tests.data_import.regular_patients.testing_cases import (
    SINGLE_TLI,
    MULTI_TLI,
)
from test.integration_tests.data_import.utils.bulk_import_payloads import (
    regular_bulk_import_payload,
)


@pytest.mark.parametrize("creds, tenant_id, vendor", SINGLE_TLI)
def test_patients_import(
    creds,
    tenant_id,
    vendor,
    agent_handler,
    agent_handler_db,
    patient_data_db,
):
    patient_metadata = regular_bulk_import_payload(vendor)
    agent_handler.bulk_import_patients_from_agent(patient_metadata)
    patients_in_agent_handler = agent_handler_db.find_patients_data_by_tenant(tenant_id)
    patients_in_patients_data = patient_data_db.find_patients(tenant_id)
    assert len(list(patients_in_patients_data)) == len(patients_in_agent_handler)


@pytest.mark.parametrize("creds, tenant_id, vendor1, vendor2", MULTI_TLI)
def test_multi_tli_patients_import(
    creds,
    tenant_id,
    vendor1,
    vendor2,
    agent_handler,
    agent_handler_db,
    patient_data_db,
):
    payload1 = regular_bulk_import_payload(vendor1)
    payload2 = regular_bulk_import_payload(vendor2)
    agent_handler.bulk_import_patients_from_agent(payload1)
    agent_handler.bulk_import_patients_from_agent(payload2)
    patients_in_agent_handler = agent_handler_db.find_patients_data_by_tenant(tenant_id)
    patients_in_patients_data = patient_data_db.find_patients(tenant_id)
    assert len(list(patients_in_patients_data)) == len(patients_in_agent_handler)
