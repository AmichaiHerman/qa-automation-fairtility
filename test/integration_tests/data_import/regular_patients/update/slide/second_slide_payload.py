from test.shared_packages.chloe_backend.agent_handler.types.bulk_import_payload import (
    Treatment,
    Slide,
    OocyteAge,
)


def second_slide_payload() -> Slide:
    return Slide(
        esId="D2018.11.06_S00013_I3169_P",
        wells=[
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9",
            "10",
        ],
        fertilizationDate="2018-11-05T15:45:00.000Z",
        firstImageHour=59940000,
        slidePosition=-1,
        instrumentEsId="1234",
        slideDescriptionId=None,
    )
