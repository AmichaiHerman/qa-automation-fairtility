from typing import List

import pytest

from test.integration_tests.data_import.regular_patients.testing_cases import SINGLE_TLI
from test.integration_tests.data_import.regular_patients.update.slide.second_slide_payload import (
    second_slide_payload,
)
from test.integration_tests.data_import.utils.bulk_import_payloads import (
    one_patient_bulk_import_payload,
)
from test.shared_packages.chloe_backend.agent_handler.types.bulk_import_payload import (
    Treatment,
    Slide,
)


def add_slide_to_existing_treatment_data(
    treatments_list: List[Treatment], slide_to_add: Slide
) -> None:
    return treatments_list[0].slides.append(slide_to_add)


@pytest.mark.parametrize("creds, tenant_id, vendor", SINGLE_TLI)
def test_adding_slide_to_existing_treratment(
    creds,
    tenant_id,
    vendor,
    agent_handler,
    agent_handler_db,
    patient_data_db,
):
    patient_payload = one_patient_bulk_import_payload(vendor)
    agent_handler.bulk_import_patients_from_agent(patient_payload)

    patients_in_agent_handler = agent_handler_db.find_patients_data_by_tenant(tenant_id)
    assert (
        patients_in_agent_handler[0].num_slides == 1
    )  # make sure there is one slide right now

    add_slide_to_existing_treatment_data(
        patient_payload.__root__[0].treatments, second_slide_payload()
    )
    agent_handler.bulk_import_patients_from_agent(patient_payload)

    patients_in_agent_handler = agent_handler_db.find_patients_data_by_tenant(tenant_id)

    assert (
        patients_in_agent_handler[0].num_slides == 2
    )  # make sure there is one slide right now

    slides_in_patient_data = patient_data_db.find_slides(tenant_id)

    assert len(slides_in_patient_data) == 2
