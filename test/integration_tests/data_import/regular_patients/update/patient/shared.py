from typing import List

from test.shared_packages.chloe_backend.agent_handler.types.bulk_import_payload import (
    BulkImport,
    BulkImportPayload,
)


def remove_patient(data: List[BulkImport]) -> BulkImportPayload:
    """Remove Patient to a current bulk-import payload"""
    return BulkImportPayload(__root__=[data.pop()])
