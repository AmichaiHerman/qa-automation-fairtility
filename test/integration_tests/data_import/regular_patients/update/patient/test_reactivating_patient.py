from typing import List

import pytest

from test.integration_tests.data_import.regular_patients.testing_cases import SINGLE_TLI
from test.integration_tests.data_import.regular_patients.update.patient.shared import (
    remove_patient,
)
from test.integration_tests.data_import.utils.bulk_import_payloads import (
    regular_bulk_import_payload,
)


@pytest.mark.parametrize("creds, tenant_id, vendor", SINGLE_TLI)
def test_reactivating_patient(
    creds,
    tenant_id,
    vendor,
    agent_handler,
    agent_handler_db,
    patient_data_db,
):
    two_patients_payload = regular_bulk_import_payload(vendor)
    agent_handler.bulk_import_patients_from_agent(two_patients_payload)

    # remove a patient
    payload_with_removed_patient = remove_patient(two_patients_payload.__root__)
    agent_handler.bulk_import_patients_from_agent(payload_with_removed_patient)

    patients_in_agent_handler = agent_handler_db.find_patients_data_by_tenant(tenant_id)

    # validate the deactived patient wasn't deleted from chloe

    assert len(patients_in_agent_handler) == 2

    # validate one of the patients is not active anymore on agent-handler

    patient_active_status_list = [
        patient.is_active_patient for patient in patients_in_agent_handler
    ]
    assert (False in patient_active_status_list) is True

    # validate one of the slides (part of the inactive patient) is not active anymore on agent-handler

    slides_in_patients_data = patient_data_db.find_slides(tenant_id)
    slides_status = [slide.status for slide in slides_in_patients_data]

    assert ("INACTIVE" in slides_status) is True

    # reactivate the patient by sending again the first request

    agent_handler.bulk_import_patients_from_agent(regular_bulk_import_payload(vendor))

    # check that all the slides related to the patient are active again in agent-handler and patient data

    slide_status_in_agent_handler = [
        patient.is_active_patient for patient in patients_in_agent_handler
    ]
    assert (False in slide_status_in_agent_handler) is True

    slides_in_patients_data = patient_data_db.find_slides(tenant_id)
    slides_status_in_patient_data = [slide.status for slide in slides_in_patients_data]

    assert ("INACTIVE" not in slides_status_in_patient_data) is True
