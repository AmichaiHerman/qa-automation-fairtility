import pytest

from test.integration_tests.data_import.regular_patients.testing_cases import SINGLE_TLI
from test.integration_tests.data_import.utils.bulk_import_payloads import (
    regular_bulk_import_payload,
    one_patient_bulk_import_payload,
)


@pytest.mark.parametrize("creds, tenant_id, vendor", SINGLE_TLI)
def test_adding_patient(
    creds,
    tenant_id,
    vendor,
    agent_handler,
    agent_handler_db,
    patient_data_db,
):
    first_patient_payload = one_patient_bulk_import_payload(vendor)
    agent_handler.bulk_import_patients_from_agent(first_patient_payload)

    patients_in_agent_handler = agent_handler_db.find_patients_data_by_tenant(tenant_id)
    patients_in_patients_data = patient_data_db.find_patients(tenant_id)
    assert len(list(patients_in_patients_data)) == len(patients_in_agent_handler) == 1

    second_patient_update_payload = regular_bulk_import_payload(vendor)
    agent_handler.bulk_import_patients_from_agent(second_patient_update_payload)

    patients_in_agent_handler = agent_handler_db.find_patients_data_by_tenant(tenant_id)
    patients_in_patients_data = patient_data_db.find_patients(tenant_id)
    assert len(list(patients_in_patients_data)) == len(patients_in_agent_handler) == 2
