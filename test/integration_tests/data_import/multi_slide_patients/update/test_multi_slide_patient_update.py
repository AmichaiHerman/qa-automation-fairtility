from typing import List

import pytest

from automation_sdk.http_requests.status_codes import StatusCode
from test.integration_tests.data_import.multi_slide_patients.multi_slide_scenarios import (
    START_DATES_INSIDE_10_DAYS_RULE,
    START_DATES_OUTSIDE_10_DAYS_RULE,
    SAME_INSTRUMENT_LIST,
    DIFFERENT_INSTRUMENT_LIST,
)
from test.integration_tests.data_import.multi_slide_patients.testing_cases import (
    MS_TEST_CASES,
)
from test.integration_tests.data_import.multi_slide_patients.update.second_treatment_payload import (
    second_treatment_payload,
)
from test.integration_tests.data_import.utils.bulk_import_payloads import (
    one_patient_bulk_import_payload,
)


def set_treatment_start_dates(apply_10_day_rule: bool) -> List[str]:
    return (
        START_DATES_INSIDE_10_DAYS_RULE
        if apply_10_day_rule
        else START_DATES_OUTSIDE_10_DAYS_RULE
    )


def set_instrument_es_ids(slides_are_on_same_instrument: bool) -> List[str]:
    return (
        SAME_INSTRUMENT_LIST
        if slides_are_on_same_instrument
        else DIFFERENT_INSTRUMENT_LIST
    )


# this test covers treatment addon as well.
@pytest.mark.parametrize("creds, tenant_id, vendor, scenario", MS_TEST_CASES)
def test_multi_slide_patients_update(
    creds,
    tenant_id,
    vendor,
    scenario,
    agent_handler,
    agent_handler_db,
    patient_data_db,
):
    treatments_start_dates = set_treatment_start_dates(
        apply_10_day_rule=scenario.is_applicable_to_10_day_rule
    )
    instrument_es_ids = set_instrument_es_ids(
        slides_are_on_same_instrument=scenario.is_slides_on_same_instrument
    )
    patient_payload = one_patient_bulk_import_payload(
        vendor, treatments_start_dates[0], instrument_es_ids[0]
    )
    resp = agent_handler.bulk_import_patients_from_agent(patient_payload)
    assert resp.status_code == StatusCode.CREATED.value

    second_treatment = second_treatment_payload(
        treatments_start_dates[1], instrument_es_ids[1]
    )
    patient_payload.__root__[0].treatments.append(
        second_treatment
    )  # add the second treatment
    resp = agent_handler.bulk_import_patients_from_agent(patient_payload)
    assert resp.status_code == StatusCode.CREATED.value

    # validate agent_handler data
    patients_in_agent_handler = agent_handler_db.find_patients_data_by_tenant(tenant_id)

    assert len(patients_in_agent_handler) == 1
    assert patients_in_agent_handler[0].num_treatments == 2
    assert patients_in_agent_handler[0].num_slides == 2

    # validate patient data import applies the 10 day rule

    patients_in_patients_data = patient_data_db.find_patients(tenant_id)

    assert len(patients_in_patients_data) == 1
    if scenario.is_applicable_to_10_day_rule:
        assert patients_in_patients_data[0].num_treatments == 1
    else:
        assert patients_in_patients_data[0].num_treatments == 2
