from test.shared_packages.chloe_backend.agent_handler.types.bulk_import_payload import (
    Treatment,
    Slide,
    OocyteAge,
)


def second_treatment_payload(
    treatment_start_date: str, instrument_es_id: str
) -> Treatment:
    return Treatment(
        slides=[
            Slide(
                esId="D2018.11.06_S00013_I3169_P",
                wells=[
                    "1",
                    "2",
                    "3",
                    "4",
                    "5",
                    "6",
                    "7",
                    "8",
                    "9",
                    "10",
                ],
                fertilizationDate="2018-11-05T15:45:00.000Z",
                firstImageHour=59940000,
                slidePosition=-1,
                instrumentEsId=instrument_es_id,
                slideDescriptionId=None,
            )
        ],
        esId="TIDX222",
        created=treatment_start_date,
        oocyteAge=OocyteAge(years=51, months=6),
    )
