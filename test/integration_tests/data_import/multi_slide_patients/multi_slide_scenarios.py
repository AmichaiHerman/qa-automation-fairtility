"""In Memory Storage for MS Scenarios"""

from typing import List

from pydantic import BaseModel


class MultiSlideScenario(BaseModel):
    instrument_list: List[str]
    treatment_start_dates: List[str]

    @property
    def is_applicable_to_10_day_rule(self) -> bool:
        as_set = set(self.treatment_start_dates)

        return len(as_set) == 1

    @property
    def is_slides_on_same_instrument(self) -> bool:
        as_set = set(self.instrument_list)
        return len(as_set) == 1


SAME_INSTRUMENT_LIST = ["1234", "1234"]
DIFFERENT_INSTRUMENT_LIST = ["1234", "5678"]
START_DATES_INSIDE_10_DAYS_RULE = [
    "2019-09-23T13:01:00.000Z",
    "2019-09-23T13:01:00.000Z",
]
START_DATES_OUTSIDE_10_DAYS_RULE = [
    "2019-09-23T13:01:00.000Z",
    "2020-09-23T13:02:00.000Z",
]

SCENARIO_1 = MultiSlideScenario(
    instrument_list=DIFFERENT_INSTRUMENT_LIST,
    treatment_start_dates=START_DATES_INSIDE_10_DAYS_RULE,
)

SCENARIO_2 = MultiSlideScenario(
    instrument_list=DIFFERENT_INSTRUMENT_LIST,
    treatment_start_dates=START_DATES_OUTSIDE_10_DAYS_RULE,
)

SCENARIO_3 = MultiSlideScenario(
    instrument_list=SAME_INSTRUMENT_LIST,
    treatment_start_dates=START_DATES_INSIDE_10_DAYS_RULE,
)

SCENARIO_4 = MultiSlideScenario(
    instrument_list=SAME_INSTRUMENT_LIST,
    treatment_start_dates=START_DATES_OUTSIDE_10_DAYS_RULE,
)
