import pytest

from automation_sdk.http_requests.status_codes import StatusCode
from test.integration_tests.data_import.multi_slide_patients.testing_cases import (
    MS_TEST_CASES,
)
from test.integration_tests.data_import.utils.bulk_import_payloads import (
    multi_slide_bulk_import_payload,
)


@pytest.mark.parametrize("creds, tenant_id, vendor, scenario", MS_TEST_CASES)
def test_multi_slide_patients_import_in_one_request(
    creds,
    tenant_id,
    vendor,
    scenario,
    agent_handler,
    agent_handler_db,
    patient_data_db,
):
    payload = multi_slide_bulk_import_payload(vendor, scenario)
    resp = agent_handler.bulk_import_patients_from_agent(payload)

    assert resp.status_code == StatusCode.CREATED.value

    # validate agent_handler data
    patients_in_agent_handler = agent_handler_db.find_patients_data_by_tenant(tenant_id)

    assert len(patients_in_agent_handler) == 1
    assert patients_in_agent_handler[0].num_treatments == 2
    assert patients_in_agent_handler[0].num_slides == 2

    # validate patient data import applies the 10 day rule

    patients_in_patients_data = patient_data_db.find_patients(tenant_id)

    assert len(patients_in_patients_data) == 1
    if scenario.is_applicable_to_10_day_rule:
        assert patients_in_patients_data[0].num_treatments == 1
    else:
        assert patients_in_patients_data[0].num_treatments == 2
