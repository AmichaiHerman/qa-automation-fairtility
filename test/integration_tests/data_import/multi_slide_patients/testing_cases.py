from typing import Tuple, List

from test.integration_tests.data_import.multi_slide_patients.multi_slide_scenarios import (
    SCENARIO_1,
    SCENARIO_2,
    SCENARIO_3,
    SCENARIO_4,
)
from test.integration_tests.tenants import BULK_IMPORT_1
from test.integration_tests.users import DATA_IMPORT1
from test.shared_packages.atomic_types import Vendor


# testing cases builder util


def build_test_cases(vendor: Vendor) -> List[Tuple]:
    return [
        (DATA_IMPORT1, BULK_IMPORT_1, vendor, SCENARIO_1),
        (DATA_IMPORT1, BULK_IMPORT_1, vendor, SCENARIO_2),
        (DATA_IMPORT1, BULK_IMPORT_1, vendor, SCENARIO_3),
        (DATA_IMPORT1, BULK_IMPORT_1, vendor, SCENARIO_4),
    ]


MS_TEST_CASES = (
    build_test_cases(Vendor.VITRO)
    + build_test_cases(Vendor.GERI)
    + build_test_cases(Vendor.MIRI)
)
