from test.integration_tests.data_import.multi_slide_patients.multi_slide_scenarios import (
    MultiSlideScenario,
)
from test.shared_packages.atomic_types import Vendor
from test.shared_packages.chloe_backend.agent_handler.types.bulk_import_payload import (
    BulkImportPayload,
    BulkImport,
    Treatment,
    Slide,
    OocyteAge,
)


def one_patient_bulk_import_payload(
    vendor: Vendor,
    treatment_start_date: str = "2019-01-11T13:40:00.000Z",
    instrument_es_id: str = "449",
) -> BulkImportPayload:
    return BulkImportPayload(
        __root__=[
            BulkImport(
                esId="test-1",
                identifier1="0000547",
                birthDate=None,
                treatments=[
                    Treatment(
                        slides=[
                            Slide(
                                esId="D2019.01.12_S01225_I0469_D",
                                wells=["1", "2", "3", "4", "5", "6", "7", "8"],
                                fertilizationDate="2019-01-11T13:40:00.000Z",
                                firstImageHour=70452000,
                                slidePosition=-1,
                                instrumentEsId=instrument_es_id,
                                slideDescriptionId=None,
                            )
                        ],
                        esId="TIDX001",
                        created=treatment_start_date,
                        oocyteAge=OocyteAge(years=51, months=8),
                    )
                ],
                instrumentType=vendor.value,
            )
        ]
    )


def regular_bulk_import_payload(vendor: Vendor) -> BulkImportPayload:
    return BulkImportPayload(
        __root__=[
            BulkImport(
                esId="test-1",
                identifier1="0000547",
                birthDate=None,
                treatments=[
                    Treatment(
                        slides=[
                            Slide(
                                esId="D2019.01.12_S01225_I0469_D",
                                wells=["1", "2", "3", "4", "5", "6", "7", "8"],
                                fertilizationDate="2019-01-11T13:40:00.000Z",
                                firstImageHour=70452000,
                                slidePosition=-1,
                                instrumentEsId="469",
                                slideDescriptionId=None,
                            )
                        ],
                        esId="TIDX001",
                        created="2019-01-11T13:40:00.000Z",
                        oocyteAge=OocyteAge(years=51, months=8),
                    )
                ],
                instrumentType=vendor.value,
            ),
            BulkImport(
                esId="test-2",
                identifier1="0000545",
                birthDate="1967-05-02T00:00:00.000Z",
                treatments=[
                    Treatment(
                        slides=[
                            Slide(
                                esId="D2018.11.06_S00013_I3169_P",
                                wells=[
                                    "1",
                                    "2",
                                    "3",
                                    "4",
                                    "5",
                                    "6",
                                    "7",
                                    "8",
                                    "9",
                                    "10",
                                ],
                                fertilizationDate="2018-11-05T15:45:00.000Z",
                                firstImageHour=59940000,
                                slidePosition=-1,
                                instrumentEsId="3169",
                                slideDescriptionId=None,
                            )
                        ],
                        esId="TIDX222",
                        created="2018-11-05T15:45:00.000Z",
                        oocyteAge=OocyteAge(years=51, months=6),
                    )
                ],
                instrumentType=vendor.value,
            ),
        ]
    )


def multi_slide_bulk_import_payload(
    vendor: Vendor, scenario: MultiSlideScenario
) -> BulkImportPayload:
    instrument_list = scenario.instrument_list
    treatment_creation_dates = scenario.treatment_start_dates
    return BulkImportPayload(
        __root__=[
            BulkImport(
                esId="esid-1",
                identifier1="id1",
                birthDate="1985-02-01T00:00:00.000Z",
                treatments=[
                    Treatment(
                        slides=[
                            Slide(
                                esId="D2000.01.01_S0035_I000",
                                wells=["1", "2", "3", "4", "5", "6", "7", "8", "9"],
                                fertilizationDate="2009-09-23T13:00:00.000Z",
                                firstImageHour=13068003,
                                slidePosition=5,
                                instrumentEsId=instrument_list[0],
                                slideDescriptionId=None,
                            )
                        ],
                        esId="Algorithms",
                        created=treatment_creation_dates[0],
                        oocyteAge=OocyteAge(years=1999, months=2),
                    ),
                    Treatment(
                        slides=[
                            Slide(
                                esId="D2000.01.01_S0033_I000",
                                wells=["1", "2", "3", "4", "5", "6", "7", "8", "9"],
                                fertilizationDate="2009-09-23T13:00:00.000Z",
                                firstImageHour=13068000,
                                slidePosition=8,
                                instrumentEsId=instrument_list[1],
                                slideDescriptionId=None,
                            )
                        ],
                        esId="Algorithm",
                        created=treatment_creation_dates[1],
                        oocyteAge=OocyteAge(years=1999, months=2),
                    ),
                ],
                instrumentType=vendor.value,
            )
        ]
    )
