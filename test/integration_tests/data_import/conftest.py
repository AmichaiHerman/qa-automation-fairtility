import pytest

from automation_sdk.mongo.repos.patient_data_repo import PatientDataRepo


@pytest.fixture
def patient_data_db(tenant_id: str):
    repo = PatientDataRepo()
    yield repo
    repo.delete_tenant_data(tenant_id)
