import os

import pytest
from dotenv import load_dotenv

from automation_sdk.console_logging.debug import Debug
from automation_sdk.console_logging.test_end import TestEnd
from automation_sdk.console_logging.test_start import TestStart

pytest_plugins = [
    "test.shared_packages.plugins.service_connection.shared_services",
    "test.shared_packages.plugins.shared_dbs",
    "pytest_logger",
]


@pytest.hookimpl
def pytest_addoption(parser):
    parser.addoption("--env", action="store", default="stage")
    parser.addoption("--allure-project", action="store", default="default")


@pytest.hookimpl
def pytest_configure(config):
    path_to_configs = os.path.dirname(os.path.abspath(__file__))
    os.environ["GOOGLE_APPLICATION_CREDENTIALS"] = path_to_configs + "/key.json"
    selected_env = config.getoption("--env")
    Debug.log(f"Running On {selected_env.upper()} env")
    if selected_env == "stage":
        path_to_configs = path_to_configs + "/stage_config.env"
    else:
        path_to_configs = path_to_configs + "/ci_config.env"

    load_dotenv(path_to_configs)


def pytest_runtest_logstart(nodeid: str, location):
    """Indicates the start of a new test session"""
    TestStart.log(message=location[2])


@pytest.hookimpl
def pytest_runtest_logfinish(nodeid: str, location):
    """Indicates the end of the test session"""
    TestEnd.log(message=location[2])


# @pytest.hookimpl
# def pytest_sessionfinish(session, exitstatus):
#     """Makes allure report and send it to allure-UI after every test-session finished"""
#     allure_project_id = session.config.getoption("--allure-project")
#     if allure_project_id != "default":
#         allure_path = os.getcwd() + "/results"
#         AllureReporter(allure_path, allure_project_id).make_report()
