import pytest

from automation_sdk.mongo.repos.patient_data_repo import PatientDataRepo


@pytest.fixture
def setup_patient_data_repo():
    repo = PatientDataRepo()
    return repo


@pytest.fixture
def setup_patient_data_production_eu_repo():
    repo = PatientDataRepo(is_proudction=True)
    return repo
