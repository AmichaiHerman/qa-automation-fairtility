from typing import Optional, Any

from test.pn_model_tests.dtos.pn_model_dto import InferenceDataPNModelEqScore


def validate_pn_model_is_eq(expected_pn_model: dict, db_pn_model: InferenceDataPNModelEqScore):

    result = False
    expected_pn_model_obj = InferenceDataPNModelEqScore(**expected_pn_model)

    expected_pn_model_dict = expected_pn_model_obj.dict(exclude={'aiModels', 'segmentations', 'kidScoreOverTime', 'kidScore'})
    db_pn_model_dit = db_pn_model.dict(exclude={'aiModels', 'segmentations', 'kidScoreOverTime', 'kidScore'})

    # Compare segmentations
    diffs = sum(
        sum(1 for s1, s2 in zip(expected_pn_model_obj.segmentations, db_pn_model.segmentations) if s1 != s2)
        for s1, s2 in zip(expected_pn_model_obj.segmentations, db_pn_model.segmentations)
        for list1, list2 in zip(s1.pn, s2.pn)
    )

    # Comparing ai model
    for ai_model1 in expected_pn_model_obj.aiModels:
        for ai_model2 in db_pn_model.aiModels:
            if ai_model1 == ai_model2:
                result = True

    if not result:
        print(f"spotted difference in the ai model")

    # Comparing everything except aiModels and segmentations
    if expected_pn_model_dict != db_pn_model_dit:
        result = False
        differing_fields = [field for field, value in expected_pn_model_dict.items() if
                            value != db_pn_model_dit.get(field)]
        if differing_fields:
            print(f"The difference was in {', '.join(differing_fields)}")
        else:
            print("Some fields are different, but specific differences were not identified.")

    return result, diffs


def validate_eq_score_over_time_is_eq(expected_eq_score_over_time: dict[Any], db_eq_score_over_time: dict[Any],
                                      hours_since_fert_limit: int):
    first_difference_key = next((key for key, value in expected_eq_score_over_time.items()
                                 if db_eq_score_over_time.get(key) != value), None)

    # Validating first difference is after the "hours since fertilization" limit
    if first_difference_key >= hours_since_fert_limit:
        return True

    # There are differences before the hour limit
    differences = sum(1 for key, value in expected_eq_score_over_time.items()
                      if db_eq_score_over_time.get(key) != value)
    result = differences > 0
    print(f'Number of differences: {differences}')
    print(f'First differing key: {first_difference_key}')
    return result
