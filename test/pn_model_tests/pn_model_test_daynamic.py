import pytest
from test.pn_model_tests.pn_model_functions import *
from test.pn_model_tests.pn_model_test_scenarios import *
from test.pn_model_tests.pn_model_validations import *


@pytest.mark.parametrize("tenant_id, patient_es_id, ai_pn_model_json, well_number", ES_PLUS_AND_ES_WELLS_TEST_CASE)
def test_pn_model_esd_es_plus(
        tenant_id: str, patient_es_id: str, ai_pn_model_json: dict, well_number: int, setup_patient_data_repo
):
    result_pn_model_and_eq_score_inference_data = get_well_pn_model_and_eq_score_data_by_patient_es_id_and_well_es_id(
        patient_es_id, tenant_id, well_number, setup_patient_data_repo)
    result_bool, segmentation_diff_num = validate_pn_model_is_eq(
        ai_pn_model_json, result_pn_model_and_eq_score_inference_data
    )
    print(f"For patient: {patient_es_id}: Number of differences in segmentations:", segmentation_diff_num)
    assert result_bool


@pytest.mark.parametrize("tenant_id, patient_es_id, ai_pn_model_json, well_number", MIRI_WELLS_TEST_CASE)
def test_pn_model_miri(
        tenant_id: str, patient_es_id: str, ai_pn_model_json: dict, well_number: int, setup_patient_data_repo
):
    result_pn_model_and_eq_score_inference_data = get_well_pn_model_and_eq_score_data_by_patient_es_id_and_well_es_id(
        patient_es_id, tenant_id, well_number, setup_patient_data_repo)
    result_bool, segmentation_diff_num = validate_pn_model_is_eq(
        ai_pn_model_json, result_pn_model_and_eq_score_inference_data
    )
    print(f"For patient: {patient_es_id}: Number of differences in segmentations:", segmentation_diff_num)
    assert result_bool


@pytest.mark.parametrize("tenant_id, patient_es_id, ai_pn_model_json, well_number", GERI_PN_WELLS_TEST_CASE)
def test_pn_model_geri(
        tenant_id: str, patient_es_id: str, ai_pn_model_json: dict, well_number: int, setup_patient_data_repo
):
    result_pn_model_and_eq_score_inference_data = get_well_pn_model_and_eq_score_data_by_patient_es_id_and_well_es_id(
        patient_es_id, tenant_id, well_number, setup_patient_data_repo)

    result_bool, segmentation_diff_num = validate_pn_model_is_eq(
        ai_pn_model_json, result_pn_model_and_eq_score_inference_data
    )

    print(f"For patient: {patient_es_id}: Number of differences in segmentations:", segmentation_diff_num)

    assert result_bool


@pytest.mark.parametrize("tenant_id, tenant_id_expected, patient_es_id, well_number",
                         GERI_EQ_WELLS_TEST_CASE)
def test_eq_score_geri(
        tenant_id: str, tenant_id_expected: str, patient_es_id: str, well_number: int,
        setup_patient_data_repo, setup_patient_data_production_eu_repo
):
    result_pn_model_and_eq_score_inference_data = get_well_pn_model_and_eq_score_data_by_patient_es_id_and_well_es_id(
        patient_es_id, tenant_id, well_number, setup_patient_data_repo)
    expected_pn_model_and_eq_score_inference_data = get_well_pn_model_and_eq_score_data_by_patient_es_id_and_well_es_id(
        patient_es_id, tenant_id_expected, well_number, setup_patient_data_production_eu_repo)
    hours_since_fert = get_hours_since_fertilization(patient_es_id, tenant_id, setup_patient_data_repo)
    hours_since_fert_natural_odd = make_natural_odd(hours_since_fert)
    result = validate_eq_score_over_time_is_eq(expected_pn_model_and_eq_score_inference_data.kidScoreOverTime,
                                               result_pn_model_and_eq_score_inference_data.kidScoreOverTime,
                                               hours_since_fert_natural_odd)
    assert result


@pytest.mark.parametrize("tenant_id, patient_es_id, ai_pn_model_json, well_number", ES_PLUS_AND_ES_WELLS_TEST_CASE)
def test_eq_score_esd_es_plus(
        tenant_id: str, patient_es_id: str, ai_pn_model_json: dict, well_number: int, setup_patient_data_repo
):
    result_pn_model_and_eq_score_inference_data = get_well_pn_model_and_eq_score_data_by_patient_es_id_and_well_es_id(
        patient_es_id, tenant_id, well_number, setup_patient_data_repo)
    assert result_pn_model_and_eq_score_inference_data.kidScore == ai_pn_model_json['kidScore']


@pytest.mark.parametrize("tenant_id, patient_es_id, ai_pn_model_json, well_number", MIRI_WELLS_TEST_CASE)
def test_eq_score_miri(
        tenant_id: str, patient_es_id: str, ai_pn_model_json: dict, well_number: int, setup_patient_data_repo
):
    result_pn_model_and_eq_score_inference_data = get_well_pn_model_and_eq_score_data_by_patient_es_id_and_well_es_id(
        patient_es_id, tenant_id, well_number, setup_patient_data_repo)
    assert result_pn_model_and_eq_score_inference_data.kidScore == ai_pn_model_json['kidScore']
