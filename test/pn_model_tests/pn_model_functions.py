from test.pn_model_tests.dtos.pn_model_dto import InferenceDataPNModelEqScore


def get_pn_model_and_eq_score_data_by_tenant_id(
        tenant_id: str, setup_patient_data_repo, well_number: int = 0,
) -> InferenceDataPNModelEqScore:
    wells = setup_patient_data_repo.find_pn_model_wells_by_tenant_id(tenant_id)
    print(f"the well from db is: {wells[well_number].inferenceData}")
    return wells[well_number].inferenceData


def get_well_pn_model_and_eq_score_data_by_patient_es_id_and_well_es_id(
        patient_es_id: str, tenant_id: str, well_number: int, patient_data_repo
) -> InferenceDataPNModelEqScore:
    patient = patient_data_repo.find_patient_by_patient_es_id_and_tenant(patient_es_id, tenant_id)
    well = patient_data_repo.find_pn_model_well_by_patient_id_and_es_id(patient.id, well_number)
    print(f"the well from Tenant: {tenant_id} is: {well.inferenceData}")
    return well.inferenceData


def get_hours_since_fertilization(patient_es_id: str, tenant_id: str, patient_data_repo) -> str:
    patient = patient_data_repo.find_patient_by_patient_es_id_and_tenant(patient_es_id, tenant_id)
    slide = patient_data_repo.find_slide_by_patient_id_and_tenant_id(
        patient.id, tenant_id)
    return slide.hoursSinceFertilization


def make_natural_odd(number) -> int:
    natural_part = int(number)
    odd_part = natural_part if natural_part % 2 == 1 else natural_part - 1
    return odd_part
