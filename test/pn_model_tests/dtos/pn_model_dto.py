from typing import List, Optional, Any

from pydantic import BaseModel, validator

from automation_sdk.mongo.mongo_orm.mongo_response import MongoResponse


class Segmentation(BaseModel):
    frame: int
    pn: List[List[List[float]]]


class AIPnModel(BaseModel):
    pn_count: int
    pn_count_time: str
    segmentations: List[Segmentation]
    mks_prediction_used: bool
    confidence_pns_classification: str
    xgboost_used: bool
    model_id: str


class PnCount(BaseModel):
    value: int
    time: str

    @validator('time')
    def validate_pn_count_time(cls, value):
        if '.' in value:
            integer_part, decimal_part = value.split('.')
            one_digit_decimal = decimal_part[0] if decimal_part else '0'
            return f'{integer_part}.{one_digit_decimal}'
        return value


class AiModels(BaseModel):
    pnsMiri: str = None
    pnsEsdEsplus: str = None
    npsGeri: str = None
    eqScore: str = None


class InferenceDataPNModelEqScore(BaseModel):
    pnCount: PnCount
    aiModels: Optional[AiModels]
    confidencePnsClassification: Optional[str]
    mksPredictionUsed: Optional[bool]
    xgboostUsed: Optional[bool]
    segmentations: List[Segmentation]
    kidScoreOverTime: Optional[Any]
    kidScore: Optional[float]


class PNWell(MongoResponse):
    inferenceData: InferenceDataPNModelEqScore
