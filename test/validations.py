from typing import Any

from automation_sdk.http_requests.status_codes import StatusCode
from pydantic import BaseModel
from pydantic import ValidationError


def is_status_code(res_status_code: int, status_code_to_validate: StatusCode) -> bool:
    return res_status_code == status_code_to_validate


def is_int_eq(resp_int: int, assertion_int: int) -> bool:
    return resp_int == assertion_int


def validate_int_is_not_eq(resp_int: int, assertion_int: int) -> None:
    assert resp_int != assertion_int


def is_str_eq(resp_str: str, assertion_str: str) -> bool:
    return resp_str == assertion_str


def validate_str_is_not_eq(resp_str: str, assertion_str: str) -> None:
    assert resp_str != assertion_str


def is_str_starts_with(input_str: str, expected_str: str) -> bool:
    return input_str.startswith(expected_str)


def validate_str_ends_with(input_str: str, expected_str: str) -> None:
    assert input_str.endswith(expected_str)


def is_jsons_eq(input_json: Any, expected_json: Any) -> bool:
    return input_json == expected_json


def check_list_equality(resp_lst: list[BaseModel], exp_lst: list[BaseModel]) -> bool:
    if len(resp_lst) != len(exp_lst):
        return False

    for i in range(len(resp_lst)):
        if not resp_lst[i] == exp_lst[i]:
            return False
    return True


def is_bool_equal(input_bool: bool, expected_bool: bool) -> bool:
    return input_bool == expected_bool


def is_obj_in_list(input_obj: Any, input_list: list) -> bool:
    return input_obj in input_list


def is_deserialization_succeeded(resp_deserialization: any):
    return not isinstance(resp_deserialization, ValidationError)
