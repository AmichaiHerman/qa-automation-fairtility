import os

from dotenv import load_dotenv

load_dotenv()

base_url = os.getenv('CHLOE_BASE_URL')
valid_username = os.getenv('KEYCLOAK_ADMIN_USER')
valid_password = os.getenv('KEYCLOAK_ADMIN_PASSWORD')
