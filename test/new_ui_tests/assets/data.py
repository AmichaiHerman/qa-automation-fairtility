from pydantic import BaseModel


class UserInfo(BaseModel):
    name: str
    lase_name: str
    email: str
    clinic_name: str
    city: str


user_settings_fields = [UserInfo(name="name", lase_name="last name", email="email@gmail.com", clinic_name="clinic", city="TLV")]
