from playwright.sync_api import Page


class WellPage:
    def __init__(self, page: Page):
        self.page = page
        _elements = WellPageElements()
        self.eq_score = self.page.locator(_elements.eq_score).first
        self.blast_score = self.page.locator(_elements.blast_score).first
        self.note_field = self.page.locator(_elements.note_field)
        self.note_add_button = self.page.locator(_elements.note_add_button)
        self.new_note_note = self.page.locator(_elements.note_list).first
        self.note_list = self.page.locator(_elements.note_list)
        self.confirmation_dialog = self.page.locator(_elements.delete_note_confirmation)
        self.confirmation_dialog_ok_button = self.confirmation_dialog.get_by_text("OK")
        self.well_view_video = self.page.locator(_elements.well_view_video)
        self.embryo_id = self.page.locator(_elements.embryo_id).first
        self.prev_next_buttons = self.page.locator(_elements.prev_next_buttons)
        self.prev_button = self.prev_next_buttons.get_by_title("Previous Well").first
        self.well_status_button = self.page.locator(_elements.well_status_dropdown).first
        self.view_event_images_button = self.page.locator(_elements.view_event_images_button).nth(1)
        self.event_images_window = self.page.locator(_elements.event_images_window)
        self.video_seek_modes_list = self.page.locator(_elements.video_seek_modes_list).all()
        self.video_seek_mode_dropdown_button = self.page.locator(_elements.video_seek_mode_dropdown_button)
        self.skip_by_list = self.page.locator(_elements.skip_by_list).all()
        self.pn_score_button = self.page.locator(_elements.pn_score_button).nth(1)

    def get_note_by_text(self, text: str) -> list:
        final_list = []
        notes = self.note_list.locator(".note-container").all()
        for element in notes:
            if element.locator(".message").text_content() == text:
                final_list.append(element)
        return final_list

    def remove_notes_by_text(self, text: str):
        elements = self.get_note_by_text(text)
        for x in elements:
            x.locator(".delete-button").click()
            self.confirmation_dialog_ok_button.click()

    def choose_well_status(self, status: str):
        self.page.locator(f'//span[contains(text(), "{status}")]').first.click()

    def getting_well_status_color(self):
        element = self.page.locator('.well-status-overlay')
        class_attribute_value = element.get_attribute('class')
        return class_attribute_value

    def choose_seek_mode(self, seek_mode: str):
        seek_mode_list = self.video_seek_modes_list
        for mode in seek_mode_list:
            if seek_mode.lower() == "event":
                mode.locator('a:has-text("Events")').click()
            elif seek_mode.lower() == "istanbul":
                mode.locator('a:has-text("Istanbul")').click()
            elif seek_mode.lower() == "day":
                mode.locator('a:has-text("Day")').click()
            elif seek_mode.lower() == "time":
                mode.locator('a:has-text("Time")').click()
            else:
                raise ValueError(f"Invalid seek mode: {seek_mode}")


class WellPageElements:
    eq_score = ".kid-score"
    blast_score = ".blast-score"
    note_field = "#new-note"
    note_add_button = ".add-note-button"
    note_list = ".notes-list"
    notes_container = ".note-container"
    delete_note_confirmation = ".modal-content"
    well_view_video = ".well-view-video"
    previous_well_button = ''
    well_left_panel = ".focused-well-left-panel"
    embryo_id = ".embryo-id-value"
    prev_next_buttons = ".prev-next-well-buttons"
    NA_status_button = "item-status .NA"
    well_status_dropdown = ".dropdown-toggle-no-caret"
    view_event_images_button = ".btn.overly-button.btn-secondary"
    event_images_window = "#images_overlay___BV_modal_content_"
    # # Change seek mode
    video_seek_mode_dropdown_button = '#__BVID__597'
    video_seek_modes_list = '.dropdown-menu.show[aria-labelledby="__BVID__1966__BV_toggle_"]'
    skip_by_dropdown = "#__BVID__2207"
    skip_by_list = '.dropdown-menu.show[aria-labelledby="__BVID__2207__BV_toggle_"]'
    # # Change PN score
    pn_score_button = "div.pn-duc-deg-fix-panel"
    # # Brightness Slider
    brightness_slider = ".d-inline-block brightness-slider"
