from playwright.sync_api import Page

from test.new_ui_tests.pages.utils import ViewMode


class PatientPage:
    def __init__(self, page: Page):
        self.page = page
        self.header = self.page.locator(".moving")
        self.oocyte_and_patient_age_btn = self.header.locator(".oocyte-age")
        self.fertilization_time = self.header.locator(".fertilization-time")
        self.treatment_status = self.header.locator(".treatment-status")
        self.treatment_start_date = self.header.locator(".treatment-start-date")
        self.medical_decision_summary = self.header.locator(".medical-decision-summary")
        self.items_count = self.header.locator(".items-count")
        self.view_selector = self.page.locator(".treatment-view-mode-select")
        self.view_selector_slide_view = self.view_selector.get_by_title("Slide View")
        self.view_selector_table_view = self.view_selector.get_by_title("Table View")
        self.view_selector_istanbul_view = self.view_selector.get_by_title("Istanbul View")
        self.table = self.page.get_by_role("table")
        self.table_body = self.table.locator("tbody")
        self.table_patient_wells = self.table_body.locator("tr")
        self.table_header = self.table.locator("thead").locator("tr")

    def test_the_new_elements_you_can_remove_it_after_you_check_them(self):
        self.header.hover()
        self.oocyte_and_patient_age_btn.hover()
        self.fertilization_time.hover()
        self.treatment_status.hover()
        self.treatment_start_date.hover()
        self.medical_decision_summary.hover()
        self.items_count.hover()

    def shift_to_table_view(self):
        self.view_selector_table_view.click()

    def is_view_selected(self, view: ViewMode):
        # is_aria-pressed =
        selector_view = self.view_selector.get_by_title(view.value)
        return selector_view.get_attribute("aria-pressed")

    def change_view(self, view: ViewMode) -> bool:
        selector_view = self.view_selector.get_by_title(view.value)
        selector_view.click()
        # print(f'${view.value}, is aria-pressed - ${selector_view.get_attribute("aria-pressed")}')
        return bool(selector_view.get_attribute("aria-pressed"))

    def is_gpta_values_are_correct(self, item_lst: list) -> bool:
        gpta = self.get_patient_well_gpta_element(0)
        gpta.click()
        self.page.wait_for_timeout(5000)
        li = gpta.locator("li").all_text_contents()
        print(f"list in position 4 - ${li[4]} --- ")
        for item in item_lst:
            if item not in li:
                return False
        return True

    def is_istanbul_table_header_contains(self, item_lst: list) -> bool:
        headers = self.get_patient_well_table_header()
        for item in item_lst:
            if item not in headers:
                return False
        return True

    def get_patient_well_gpta_element(self, row_index):
        return self.table_patient_wells.all()[row_index].locator(".pgta-cell")

    def get_patient_well_table_header(self):
        return self.table_header.all()[0].text_content()
