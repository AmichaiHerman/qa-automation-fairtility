import os

from playwright.sync_api import Page
from test.shared_packages.atomic_types import UserCredentials


class LoginPage:

    def __init__(self, page: Page):
        self.page = page
        _elements = LoginElements()
        self.username_field = self.page.locator(_elements.username_field)
        self.password_field = self.page.locator(_elements.password_field)
        self.login_button = self.page.locator(_elements.login_button)
        self.remember_me_checkbox = self.page.locator(_elements.remember_me_checkbox)
        self.forget_password_button = self.page.locator(_elements.forget_password_div).locator('//a')
        self.forget_password_message = self.page.locator(_elements.forget_password_message)
        self.forget_password_back_to_login = self.page.locator(_elements.forget_password_back_to_login).locator("//a")
        self.forget_password_submit_button = (self.page.locator(_elements.forget_password_submit_button)
                                              .locator("//input"))
        self.reset_password_success_message = self.page.locator(_elements.reset_password_success_message)
        self.reset_password_fail_message = self.page.locator(_elements.reset_password_fail_message)
        self.wrong_credentials_message = self.page.locator(_elements.wrong_credentials_message)

    def navigate_to_login_page(self):
        self.page.goto(os.getenv('BASE_URL'))

    def fill_login_fields(self, username: str, password: str):
        self.username_field.fill(username)
        self.password_field.fill(password)

    def perform_valid_login(self, creds: UserCredentials):
        self.fill_login_fields(creds.user_name, creds.password)
        self.login_button.click()

    def perform_invalid_username_login(self, creds: UserCredentials):
        self.fill_login_fields(creds.user_name, creds.password)
        self.login_button.click()

    def perform_invalid_password_login(self, creds: UserCredentials):
        self.fill_login_fields(creds.user_name, creds.password)
        self.login_button.click()

    def perform_remember_me_login(self, creds: UserCredentials):
        self.fill_login_fields(creds.user_name, creds.password)
        self.remember_me_checkbox.click()
        self.login_button.click()

    def is_login_page_display(self):
        return self.username_field.is_enabled() and self.password_field.is_enabled()

    def go_to_forget_password_page(self) -> bool:
        self.forget_password_button.click()
        return self.forget_password_message.is_visible()

    def return_to_login_page_from_forget_password(self) -> bool:
        self.go_to_forget_password_page()
        self.forget_password_back_to_login.click()
        return self.is_login_page_display()

    def submit_forget_password_form(self) -> bool:
        self.go_to_forget_password_page()
        self.username_field.fill("some_name")
        self.forget_password_submit_button.click()
        self.page.wait_for_timeout(500)
        return ((self.is_login_page_display() and self.reset_password_success_message.is_visible()) or
                self.reset_password_fail_message.is_visible())


class LoginElements:
    username_field = "#username"
    password_field = "#password"
    remember_me_checkbox = "#rememberMe"
    forget_password_div = ".forgot-password-button"
    login_button = "#kc-login"
    wrong_credentials_message = ".kc-feedback-text"
    forget_password_message = "#kc-info-wrapper"
    forget_password_back_to_login = "#kc-form-options"
    forget_password_submit_button = "#kc-form-buttons"
    reset_password_success_message = ".alert-success"
    reset_password_fail_message = ".kc-error-message"
