from enum import Enum


class FilterByDayOptions(Enum):
    DAY0 = 'D 0'
    DAY1 = 'D 1'
    DAY2 = 'D 2'
    DAY3 = 'D 3'
    DAY4 = 'D 4'
    DAY5 = 'D 5'
    DAY6 = 'D 6'
    DAY7 = 'D 7'


class ViewMode(Enum):
    SLIDE = "Slide View"
    TABLE = "Table View"
    ISTANBUL = "Istanbul View"


def clean_string(string):
    return string.replace("\n", "").replace(" ", "").strip()
