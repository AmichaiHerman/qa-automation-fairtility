import re

from playwright.sync_api import Page


from test.new_ui_tests.assets.data import UserInfo
from .utils import FilterByDayOptions, clean_string


class HomePage:

    def __init__(self, page: Page):
        self.page = page
        _elements = HomePageElements()
        self.user_manu = self.page.locator(_elements.user_manu)
        self.user_menu_dropdown = self.user_manu.locator(_elements.user_menu_dropdown)
        self.user_avatar = self.user_manu.locator("//img")
        self.sign_out = self.user_menu_dropdown.get_by_text("Sign Out")
        self.user_manu_about = self.user_menu_dropdown.get_by_text("About")
        self.user_manu_terms_of_use = self.user_menu_dropdown.get_by_text("Terms Of Use")
        self.user_manu_help_center = self.user_menu_dropdown.get_by_text("Help Center")
        self.user_manu_settings = self.user_menu_dropdown.get_by_text("Settings")
        self.help_center_go_back_btn = self.page.locator(_elements.help_center_go_back_btn)
        self.help_center_tutorials_container = self.page.locator(_elements.help_center_tutorials_container)
        self.help_center_user_manuals_container = self.page.locator(_elements.help_center_user_manuals_container)
        self.dialog_model = self.page.locator(_elements.help_center_dialog_model)
        self.help_center_dialog_video = self.dialog_model.locator("//video")
        self.help_center_dialog_close_btn = self.dialog_model.get_by_text("Close")
        self.user_settings_profile_image_input = self.dialog_model.locator(_elements.user_settings_profile_image)
        self.user_settings_remove_profile_image = (self.dialog_model.locator(_elements.user_settings_profile_image)
                                                   .locator("svg"))
        self.user_settings_first_name = self.dialog_model.locator(".first-name").locator("input")
        self.user_settings_last_name = self.dialog_model.locator(".last-name").locator("input")
        self.user_settings_email = self.dialog_model.locator(".email").locator("input")
        self.user_settings_clinic_name = self.dialog_model.locator(".clinic-name").locator("input")
        self.user_settings_city = self.dialog_model.locator(".city").locator("input")
        self.user_settings_save_btn = self.dialog_model.get_by_text("Save")

        # modal_dialog it's a general dialog for all user menu popup options
        self.modal_dialog = self.page.locator(_elements.modal_dialog)
        self.about_title = self.modal_dialog.locator("//header").get_by_text("About")
        self.terms_of_use_title = self.modal_dialog.locator("//header").get_by_text("Terms Of Use")
        self.terms_of_use_close_btn = self.modal_dialog.locator("//footer").get_by_text("Close")
        self.about_window_close_btn = self.modal_dialog.locator(_elements.about_window_close_btn)
        self.search_input = self.page.get_by_placeholder(_elements.search_placeholder).first
        self.view_menu_button = self.page.locator(_elements.home_page_view_menu)
        self.view_menu_elements = self.page.locator(_elements.home_page_view_menu_text_element).locator("//a").first
        self.view_nemu_selected = self.view_menu_elements
        self.view_menu_finish = self.page.locator(_elements.home_page_view_menu).get_by_text('Finished')
        self.share_button = self.page.locator(_elements.share_button_link)
        self.tutorial_button_link = self.page.locator(_elements.tutorial_button_link)
        self.tutorial_video_title = self.modal_dialog.locator(_elements.tutorial_video_title)
        self.tutorial_video_source = self.modal_dialog.locator("//source")
        self.filter_by_day_button = self.page.locator(_elements.filter_by_day_button)
        self.filter_by_day_dropdown = self.page.locator(_elements.filter_by_day_dropdown)
        self.slides = self.page.locator(_elements.slides).all()
        self.slide = self.page.locator(_elements.well)

    def is_home_page_display(self):
        return self.view_menu_button.is_enabled()

    def navigate_to_well_page(self):
        return self.slide.click()

    def logout(self):
        self.user_manu.click()
        self.sign_out.click()

    def change_view_menu_from_ongoing_to_finish(self):
        self.view_menu_button.click()
        self.view_menu_finish.click()

    def get_view_manu_status(self):
        pattern = r'^(.*?\n.*?\n)'
        matches = re.search(pattern, self.view_nemu_selected.text_content())
        text = matches.group(1).strip()
        return text

    def is_tutorial_video_open_the_correct_video_and_title(self) -> bool:
        self.tutorial_button_link.click()
        title = self.tutorial_video_title.text_content().strip() == "Home page"
        video_source = self.tutorial_video_source.get_attribute("src").split('/')[-1] == "home-page-tutorial.mp4"
        return title and video_source

    def filter_slides_by_day(self, day_option: list[FilterByDayOptions]):
        self.filter_by_day_button.click()
        for day in day_option:
            self.get_day_option(day).click()

    def get_day_option(self, day: FilterByDayOptions):
        """ param day : filter by day options (day 1-7) """
        return self.filter_by_day_dropdown.get_by_text(day.value)

    def is_filter_by_day_options_are_correct(self):
        self.filter_by_day_button.click()
        filter_options = self.filter_by_day_dropdown.locator('//label').all()
        enum_filter_options = list(map(lambda o: clean_string(o.value), FilterByDayOptions))
        for option in filter_options:
            if clean_string(option.locator("//span").all()[0].text_content()) not in enum_filter_options:
                return False
        return True

    def is_filter_by_day_dropdown_options_checked(self, options: list[FilterByDayOptions]) -> bool:
        verified_checked_options = []
        # the map is to get the value (7) from the string ("D 7")
        options_value = list(map(lambda o: o.value[-1], options))
        filter_options = self.filter_by_day_dropdown.locator('//input').all()
        for option in filter_options:
            if option.is_checked() and option.input_value() in options_value:
                verified_checked_options.append(option.input_value())
        return len(verified_checked_options) == len(options)

    def is_filter_by_day(self, filter_options) -> bool:
        match_filter_options = list(map(lambda o: o.value.replace(" ", ""), filter_options))
        for slide in self.get_all_slides():
            slide_day_and_hour = slide.get_by_title("Time Since Fertilization").text_content()
            # remove all unnecessary white spaces
            day = clean_string(slide_day_and_hour.split('|')[0])
            if day not in match_filter_options:
                return False
        return True

    def open_about_window(self):
        self.user_manu.click()
        self.user_manu_about.click()

    def open_terms_of_use_window(self):
        self.user_manu.click()
        self.user_manu_terms_of_use.click()

    def open_help_center_window(self):
        self.user_manu.click()
        self.user_manu_help_center.click()

    def open_user_settings_window(self):
        self.user_manu.click()
        self.user_manu_settings.click()

    def open_patient_page(self, instrument_index, slide_index):
        instrument = self.get_instrument(instrument_index).instrument
        slide = self.get_instrument_slides(instrument, slide_index).locator(".slide-card-header")
        slide.click()

    def open_well_page(self, instrument_index, slide_index, well_index):
        instrument = self.get_instrument(instrument_index).instrument
        slide = self.get_instrument_slides(instrument, slide_index).locator(".slide-card-body")
        well = slide.locator(".common-card-score-metric").all()[well_index]
        well.click()

    def is_play_all_help_center_tutorial_videos(self):
        help_canter_tutorials_video_cards = (self.help_center_tutorials_container.locator(".help-center-video-preview")
                                             .all())
        for video in help_canter_tutorials_video_cards:
            video.click()
            if not self.dialog_model.is_visible() and self.help_center_dialog_video.is_visible():
                return False
            self.help_center_dialog_close_btn.click()
        return True

    def is_help_center_user_manual_download_pdf_file(self):
        help_canter_user_manual_cards = self.help_center_user_manuals_container.locator(".help-center-manual").all()
        for manual in help_canter_user_manual_cards:
            # start waiting for the pdf file
            with self.page.expect_popup() as popup_info:
                download_btn = manual.locator(".download-button")
                download_btn.click()
            downloaded_file_is_pdf = popup_info.value.url.split('.')[-1] == "pdf"
            if not downloaded_file_is_pdf:
                return False
        return True

    def change_profile_image(self, path):
        self.user_settings_remove_profile_image.click()
        self.user_settings_profile_image_input.click()
        self.user_settings_profile_image_input.locator('input').set_input_files(path)
        self.user_settings_save_btn.click()

    def get_user_settings_inputs(self):
        user_info = UserInfo(
            name=self.user_settings_first_name.input_value(),
            lase_name=self.user_settings_last_name.input_value(),
            email=self.user_settings_email.input_value(),
            clinic_name=self.user_settings_clinic_name.input_value(),
            city=self.user_settings_city.input_value()
        )
        return user_info

    def change_user_settings_inputs(self, inputs: UserInfo):
        self.user_settings_first_name.fill(inputs.name)
        self.user_settings_last_name.fill(inputs.lase_name)
        self.user_settings_email.fill(inputs.email)
        self.user_settings_clinic_name.fill(inputs.clinic_name)
        # TODO: there is a bug with the city field (wait for it)
        # self.user_settings_city.fill(inputs.city)

    def get_instrument(self, index=0):
        return self.Instrument(self.page, index)

    def get_all_instrument(self):
        return self.Instrument(self.page).instrument_containers

    def get_all_slides(self):
        slides = []
        instruments = self.get_all_instrument()
        for instrument in instruments:
            instrument_slides = instrument.locator(".card-body").all()
            if instrument_slides:
                slides += instrument_slides
        return slides

    @staticmethod
    def get_instrument_slides(instrument, index=0):
        return instrument.locator(".card-body").all()[index]

    class Instrument:
        def __init__(self, page: Page, index=0):
            _elements = InstrumentElements()
            page.wait_for_selector(_elements.instrument_containers)
            self.instrument_containers = page.locator(_elements.instrument_containers).all()
            self.instrument = self.instrument_containers[index]
            self.title = self.instrument.locator(_elements.instrument_title_parent)
            self.edit_name_button = self.title.locator(_elements.edit_name_button)
            self.edit_name_dialog = self.instrument.locator(_elements.edit_name_dialog)
            self.edit_name_dialog_input = self.edit_name_dialog.locator(_elements.instrument_change_name_field)
            self.edit_name_dialog_save_button = self.edit_name_dialog.locator(_elements.instrument_change_save_button)

        def change_name(self, name):
            self.edit_name_button.click()
            self.edit_name_dialog_input.fill(name)
            self.edit_name_dialog_save_button.click()

        def get_instrument_name(self):
            match = re.search(r'\((.*?)\)', self.title.text_content())
            text_inside_parentheses = match.group(1)
            return text_inside_parentheses

    class Slides:
        def __init__(self, instrument, index=0):
            self.instrument_slide_cards = instrument.locator(".card-body").all()
            self.slide = self.instrument_slide_cards[index]
            self.slide_day_and_hour = self.slide.get_by_title("Time Since Fertilization")

        def get_slide_frozen_day(self):
            return clean_string(self.slide_day_and_hour.text_content().split('|')[0])

        def get_slide_frozen_hour(self):
            return clean_string(self.slide_day_and_hour.text_content().split('|')[1])


class HomePageElements:
    user_manu = ".user-menu "
    user_menu_dropdown = '.dropdown-menu'
    modal_dialog = ".modal-dialog"
    help_center_tab = ".help-center-tab"
    help_center_go_back_btn = ".back-button"
    help_center_content = ".help-center-center-content"
    help_center_tutorials_container = "#help_center_tutorials"
    tutorial_video_title = ".text-bold"
    help_center_user_manuals_container = "#help_center_manuals"
    help_center_dialog_model = ".modal-content"
    user_settings_profile_image = ".croppa-container"
    about_window_close_btn = ".close"

    filter_by_day_button = '#filter-dropdown-button'
    filter_by_day_dropdown = "#filter-dropdown-content"
    search_placeholder = "search"
    home_page_view_menu = ".home-page-view-menu"
    home_page_view_menu_text_element = "#home-page-view-dropdown"
    share_button_link = '.share-button'
    tutorial_button_link = '.tutorial-nav'
    slides = ".card-body"
    well = ".common-card-score-metric"


class InstrumentElements:
    instrument_containers = ".slide-cards-container"
    instrument_title_parent = ".instrument-id"
    instrument_slide_content = ".slide-card-content"
    instrument_change_name_field = ".instrument-input"
    instrument_change_save_button = '.btn-submit'
    edit_name_dialog = '.dialog-form'
    edit_name_button = ".btn"
