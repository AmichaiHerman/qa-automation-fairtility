from .home_page import HomePage
from .login_page import LoginPage
from .patient_page import PatientPage
from .well_page import WellPage


class Chloe:
    def __init__(self, page):
        self.login = LoginPage(page)
        self.home_page = HomePage(page)
        self.patient_page = PatientPage(page)
        self.well_page = WellPage(page)
