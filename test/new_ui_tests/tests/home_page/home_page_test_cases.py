from test.new_ui_tests.assets.data import user_settings_fields
from test.new_ui_tests.pages.utils import FilterByDayOptions
from test.shared_packages.test_data.test_users import *

filter_by_day_test_cases = [(PATIENT_DATA_VITRO, [FilterByDayOptions.DAY1]),
                            (PATIENT_DATA_VITRO, [FilterByDayOptions.DAY2]),
                            (PATIENT_DATA_VITRO, [FilterByDayOptions.DAY7]),
                            (PATIENT_DATA_VITRO, [FilterByDayOptions.DAY5])]

IMAGES_PATHS = [(PATIENT_DATA_VITRO, os.path.join('../assets/images/white.jpg')),
                (PATIENT_DATA_VITRO, os.path.join('../assets/images/original_photo.png'))]

VALID_CREDS_TEST_CASE = [PATIENT_DATA_VITRO]
VIEW_MODES = {
    "ISTANBUL": "Istanbul View",
    "TABLE": "Table View",
    "SLIDE": "Slide View"
}
USER_SETTINGS_INPUTS = user_settings_fields
