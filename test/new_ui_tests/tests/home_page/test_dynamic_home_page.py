import random

import pytest

from .home_page_test_cases import *


@pytest.mark.parametrize('creds', VALID_CREDS_TEST_CASE)
def test_sign_out(creds, pages):
    pages.home_page.logout()
    assert pages.login.is_login_page_display()


@pytest.mark.parametrize('creds', VALID_CREDS_TEST_CASE)
def test_change_instrument_name(creds, pages):
    new_name = f'name{int(random.randint(1, 100))}'
    first_instrument = pages.home_page.get_instrument(0)
    first_instrument.change_name(new_name)
    pages.home_page.page.wait_for_timeout(500)
    assert first_instrument.get_instrument_name() == new_name


@pytest.mark.parametrize('creds, options', filter_by_day_test_cases)
def test_filter_by_day(creds, pages, options):
    page = pages.home_page
    page.filter_slides_by_day(options)
    page.page.wait_for_timeout(500)
    assert page.is_filter_by_day_dropdown_options_checked(options) and page.is_filter_by_day(options)


@pytest.mark.parametrize('creds', VALID_CREDS_TEST_CASE)
def test_filter_by_day_options_are_correct(creds, pages):
    assert pages.home_page.is_filter_by_day_options_are_correct()


@pytest.mark.parametrize('creds', VALID_CREDS_TEST_CASE)
def test_dropdown(creds, pages):
    page = pages.home_page
    page.user_manu.click()
    assert page.user_menu_dropdown.is_visible()
    page.user_manu.click()
    assert not page.user_menu_dropdown.is_visible()


@pytest.mark.parametrize('creds', VALID_CREDS_TEST_CASE)
def test_view_menu_is_change_to_finished(creds, pages):
    pages.home_page.change_view_menu_from_ongoing_to_finish()
    pages.home_page.page.wait_for_timeout(1000)
    assert pages.home_page.get_view_manu_status() == "Finished"


@pytest.mark.parametrize('creds', VALID_CREDS_TEST_CASE)
def test_tutorial_video_link(creds, pages):
    assert pages.home_page.is_tutorial_video_open_the_correct_video_and_title()


@pytest.mark.parametrize('creds', VALID_CREDS_TEST_CASE)
def test_about_window(creds, pages):
    pages.home_page.open_about_window()
    assert pages.home_page.modal_dialog.is_visible()
    assert pages.home_page.about_title.text_content() == "About"


@pytest.mark.parametrize('creds', VALID_CREDS_TEST_CASE)
def test_about_window_closing_button(creds, pages):
    pages.home_page.open_about_window()
    assert pages.home_page.modal_dialog.is_visible()
    pages.home_page.about_window_close_btn.click()
    pages.home_page.page.wait_for_timeout(500)
    assert not pages.home_page.modal_dialog.is_visible()


@pytest.mark.parametrize('creds', VALID_CREDS_TEST_CASE)
def test_terms_of_use(creds, pages):
    pages.home_page.open_terms_of_use_window()
    assert pages.home_page.modal_dialog.is_visible()
    assert pages.home_page.terms_of_use_title.text_content() == "Terms Of Use"


@pytest.mark.parametrize('creds', VALID_CREDS_TEST_CASE)
def test_terms_of_use_window_closing_button(creds, pages):
    pages.home_page.open_terms_of_use_window()
    assert pages.home_page.modal_dialog.is_visible()
    pages.home_page.terms_of_use_close_btn.click()
    pages.home_page.page.wait_for_timeout(500)
    assert not pages.home_page.modal_dialog.is_visible()


@pytest.mark.parametrize('creds', VALID_CREDS_TEST_CASE)
def test_help_center(creds, pages):
    pages.home_page.open_help_center_window()
    assert pages.home_page.help_center_tutorials_btn.is_visible()


@pytest.mark.parametrize('creds', VALID_CREDS_TEST_CASE)
def test_help_center_go_back_button(creds, pages):
    pages.home_page.open_help_center_window()
    assert pages.home_page.help_center_tutorials_btn.is_visible()
    pages.home_page.help_center_go_back_btn.click()
    assert pages.home_page.is_home_page_display()


@pytest.mark.parametrize('creds', VALID_CREDS_TEST_CASE)
def test_help_canter_tutorials_video(creds, pages):
    pages.home_page.open_help_center_window()
    assert pages.home_page.is_play_all_help_center_tutorial_videos()


@pytest.mark.parametrize('creds', VALID_CREDS_TEST_CASE)
def test_help_center_user_manual_downloads(creds, pages):
    pages.home_page.open_help_center_window()
    assert pages.home_page.is_help_center_user_manual_download_pdf_file()


@pytest.mark.parametrize('creds, path', IMAGES_PATHS)
def test_user_settings_change_profile_image(creds, pages, path):
    user_avatar = pages.home_page.user_avatar.get_attribute("src")
    pages.home_page.open_user_settings_window()
    pages.home_page.change_profile_image(path)
    pages.home_page.page.wait_for_timeout(3000)
    new_user_avatar = pages.home_page.user_avatar.get_attribute("src")
    assert user_avatar != new_user_avatar


@pytest.mark.parametrize('creds', VALID_CREDS_TEST_CASE)
@pytest.mark.parametrize('inputs', USER_SETTINGS_INPUTS)
def test_user_settings_inputs(creds, inputs, pages):
    pages.home_page.open_user_settings_window()
    old_settings_inputs = pages.home_page.get_user_settings_inputs()
    pages.home_page.change_user_settings_inputs(inputs)
    pages.home_page.user_settings_save_btn.click()
    pages.home_page.open_user_settings_window()
    new_setting_inputs = pages.home_page.get_user_settings_inputs()
    assert new_setting_inputs == inputs
    pages.home_page.change_user_settings_inputs(old_settings_inputs)
    pages.home_page.user_settings_save_btn.click()
