import pytest
from test.new_ui_tests import ui_elements
from test.new_ui_tests.tests.login_page.login_page_test_cases import *


@pytest.mark.login_test
@pytest.mark.parametrize("creds", VALID_LOGIN_TEST_CASE)
def test_valid_login(pages_without_login, creds):
    pages_without_login.login.perform_valid_login(creds)
    assert pages_without_login.home_page.user_manu and pages_without_login.home_page.view_menu_button


@pytest.mark.login_test
@pytest.mark.parametrize("creds", INVALID_LOGIN_USERNAME_TEST_CASE)
def test_invalid_username_login(pages_without_login, creds):
    pages_without_login.login.perform_invalid_username_login(creds)
    assert (pages_without_login.login.wrong_credentials_message.text_content() ==
            "Invalid username or password.")


@pytest.mark.parametrize("creds", INVALID_LOGIN_PASSWORD_TEST_CASE)
def test_invalid_password_login(pages_without_login, creds):
    pages_without_login.login.perform_invalid_password_login(creds)
    assert (pages_without_login.login.wrong_credentials_message.text_content() ==
            "Invalid username or password.")


@pytest.mark.parametrize("creds", VALID_LOGIN_TEST_CASE)
def test_remember_me_(pages_without_login, creds):
    page = pages_without_login.login
    page.perform_remember_me_login(creds)
    pages_without_login.home_page.logout()
    assert page.username_field.text_content() == ui_elements.valid_username
    assert page.password_field.text_content() == ui_elements.valid_password


def test_forget_password(pages_without_login):
    assert pages_without_login.login.go_to_forget_password_page()


def test_forget_password_is_return_to_login_page(pages_without_login):
    assert pages_without_login.login.return_to_login_page_from_forget_password()


def test_forget_password_form(pages_without_login):
    assert pages_without_login.login.submit_forget_password_form()
