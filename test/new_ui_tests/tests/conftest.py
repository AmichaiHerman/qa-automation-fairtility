import os
import pytest
from playwright.sync_api import sync_playwright
from automation_sdk.atomic_types import UserCredentials
from test.new_ui_tests.pages.chloe import Chloe


@pytest.fixture()
def playwright_setup(request):
    with sync_playwright() as p:
        browser = p.chromium.launch(headless=False, args=["--start-maximized"])
        context = browser.new_context(no_viewport=True)
        page = context.new_page()
        context.tracing.start(screenshots=True, snapshots=True, sources=True)

        yield page

        test_name = request.session.items[0].name
        # In win will use '\\' and mac '/'
        try:
            folder_name = str(request.path).split('\\')[-2]
        except IndexError:
            folder_name = str(request.path).split('/')[-2]
        # if the test is field
        if request.session.testsfailed:
            file_name = test_name + ".zip"
            path = os.path.join(f'../../trace/{folder_name}', file_name)
            output_file = os.path.normpath(path)
            context.tracing.stop(path=output_file)

        page.close()
        context.close()
        browser.close()


@pytest.fixture()
def pages_without_login(playwright_setup):
    pages = Chloe(playwright_setup)
    pages.login.navigate_to_login_page()
    return pages


@pytest.fixture()
def pages(pages_without_login, creds: UserCredentials):
    pages = pages_without_login
    pages.login.perform_valid_login(creds)
    return pages

