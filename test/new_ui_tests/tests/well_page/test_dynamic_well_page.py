import time
import pytest
from test.new_ui_tests.tests.home_page.home_page_test_cases import VALID_CREDS_TEST_CASE
from test.new_ui_tests.tests.well_page.well_page_test_cases import WELL_STATUSES


@pytest.mark.wellUI
@pytest.mark.parametrize('creds', VALID_CREDS_TEST_CASE)
@pytest.mark.xfail("With headless=True not working! NTF")
def test_add_note(creds, pages):
    pages.home_page.open_well_page(0, 0, 0)
    time.sleep(3)
    pages.well_page.note_field.fill("lolipop")
    pages.well_page.note_add_button.click()
    time.sleep(3)
    assert pages.well_page.new_note_note.is_visible()
    pages.well_page.remove_notes_by_text("lolipop")


@pytest.mark.WellUI
@pytest.mark.parametrize('creds', VALID_CREDS_TEST_CASE)
def test_enter_well_page_from_well_image(creds, pages):
    pages.home_page.open_well_page(0, 0, 0)
    pages.well_page.well_view_video.is_visible()


@pytest.mark.WellUI
@pytest.mark.parametrize('creds', VALID_CREDS_TEST_CASE)
def test_move_between_notes(creds, pages):
    pages.home_page.open_well_page(0, 0, 0)
    my_embryo_id = pages.well_page.embryo_id.text_content()
    print(my_embryo_id)
    pages.well_page.prev_button.click()
    time.sleep(3)
    assert int(pages.well_page.embryo_id.text_content()) == int(my_embryo_id) - 1


@pytest.mark.WellUI
@pytest.mark.parametrize('creds', VALID_CREDS_TEST_CASE)
def test_change_well_status(creds, pages):
    in_well_page = True
    for well_status, expected_color in WELL_STATUSES.items():
        if in_well_page:
            pages.home_page.open_well_page(0, 0, 0)
            in_well_page = False
        pages.well_page.well_status_button.click()
        pages.well_page.choose_well_status(well_status)
        time.sleep(3)
        if expected_color is not None:
            assert expected_color == pages.well_page.getting_well_status_color().split()[1]
        else:
            assert len(pages.well_page.getting_well_status_color().split()) < 2


@pytest.mark.WellUI
@pytest.mark.parametrize('creds', VALID_CREDS_TEST_CASE)
def test_event_image_window_open(creds, pages):
    pages.home_page.open_well_page(0, 0, 0)
    pages.well_page.view_event_images_button.click()
    assert pages.well_page.event_images_window.is_visible()


