from test.shared_packages.test_data.test_users import PATIENT_DATA_VITRO

VALID_CREDS_TEST_CASE = [PATIENT_DATA_VITRO]

WELL_STATUSES = {
        "Freeze": "blue",
        "Thawed": "purple",
        "Transfer": "green",
        "Discard": "red",
        "Pending": "yellow",
        "N/A": None
    }
