import pytest
from test.new_ui_tests.pages.utils import ViewMode
from test.new_ui_tests.tests.home_page.home_page_test_cases import *


@pytest.mark.patientUI
@pytest.mark.parametrize('creds', VALID_CREDS_TEST_CASE)
def test_patient_page_is_display(creds, pages):
    assert not pages.patient_page.view_selector.is_visible()
    pages.home_page.open_patient_page(0, 0)
    pages.patient_page.page.wait_for_selector(".treatment-view-mode-select")
    assert pages.patient_page.view_selector.is_visible()


@pytest.mark.patientUI
@pytest.mark.parametrize('creds', VALID_CREDS_TEST_CASE)
@pytest.mark.parametrize('view_mode', [ViewMode.TABLE, ViewMode.ISTANBUL, ViewMode.SLIDE])
def test_change_view(creds, pages, view_mode):
    for view_mode, locator in VIEW_MODES.items():
        pages.home_page.open_patient_page(1, 1)
        pages.patient_page.page.wait_for_timeout(500)
        assert pages.patient_page.change_view(locator)


@pytest.mark.patientUI
@pytest.mark.parametrize('creds', VALID_CREDS_TEST_CASE)
def test_change_to_table_view(creds, pages):
    pages.home_page.open_patient_page(1, 1)
    pages.patient_page.page.wait_for_timeout(500)
    assert pages.patient_page.change_view(ViewMode.TABLE)


@pytest.mark.patientUI
@pytest.mark.parametrize('creds', VALID_CREDS_TEST_CASE)
def test_change_to_istanbul_view(creds, pages):
    pages.home_page.open_patient_page(1, 1)
    pages.patient_page.page.wait_for_timeout(500)
    assert pages.patient_page.change_view(ViewMode.ISTANBUL)


@pytest.mark.patientUI
@pytest.mark.parametrize('creds', VALID_CREDS_TEST_CASE)
def test_change_back_to_slide_view(creds, pages):
    # assert not pages.patient_page.view_selector_table_view.is_visible()
    pages.home_page.open_patient_page(1, 1)
    pages.patient_page.change_view(ViewMode.TABLE)
    assert pages.patient_page.change_view(ViewMode.SLIDE)


@pytest.mark.patientUI
@pytest.mark.parametrize('creds', VALID_CREDS_TEST_CASE)
def test_check_pgt_a_items_in_table_view(creds, pages):
    pages.home_page.open_patient_page(1, 1)
    pages.patient_page.change_view(ViewMode.TABLE)
    assert pages.patient_page.is_gpta_values_are_correct(['Euploid (D7)', 'Aneuploid (D7)', 'Mosaic (D7)'])


@pytest.mark.patientUI
@pytest.mark.parametrize('creds', VALID_CREDS_TEST_CASE)
def test_check_pgt_a_items_in_istanbul_view(creds, pages):
    pages.home_page.open_patient_page(1, 1)
    pages.patient_page.change_view(ViewMode.ISTANBUL)
    assert pages.patient_page.is_gpta_values_are_correct(['Euploid (D7)', 'Aneuploid (D7)', 'Mosaic (D7)'])


@pytest.mark.patientUI
@pytest.mark.parametrize('creds', VALID_CREDS_TEST_CASE)
def test_check_table_header(creds, pages):
    pages.home_page.open_patient_page(1, 1)
    patient_page = pages.patient_page
    patient_page.change_view(ViewMode.ISTANBUL)
    assert patient_page.is_istanbul_table_header_contains(["Day 6", "Day 7"])
