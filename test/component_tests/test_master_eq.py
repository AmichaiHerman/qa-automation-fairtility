# test_master.py
import pytest

if __name__ == "__main__":
    # Use the -k option to run tests matching the naming pattern 'test_dynamic_*.py'
    pytest.main(["--pyargs", "test", "-k", "test_dynamic_"])
