import sys
import argparse

import pytest

from automation_sdk.gcp.secret_manager.secret_client import SecretClient


def fetch_secrets(env: str) -> None:
    secret_client = SecretClient("sandbox", env)
    secret_client.get_secret_data()


def invoke_ci_cycle() -> None:
    arg_parser = argparse.ArgumentParser()
    arg_parser.add_argument("--env", required=True)
    arg_parser.add_argument("--services", required=True)
    args = arg_parser.parse_args()
    updated_params = args.services.replace("-", "_").replace(",", " or ")
    fetch_secrets(env=args.env)
    exit_status = pytest.main(["-m", f"{updated_params}", "-s", "-v"])
    return exit_status


def main():
    exit_status = invoke_ci_cycle()
    print(exit_status)
    sys.exit(exit_status)


if __name__ == "__main__":
    main()
