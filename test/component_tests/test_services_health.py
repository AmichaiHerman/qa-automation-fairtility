import pytest

from automation_sdk.http_requests.status_codes import StatusCode
from test.shared_packages.test_data.test_users import ADMIN_CREDS

CREDS = [ADMIN_CREDS]

# todo import inside CI


@pytest.mark.parametrize("creds", CREDS)
@pytest.mark.lab_connector
def test_lab_connector_health(creds, lab_connector):
    resp = lab_connector.check_health()
    assert resp.status_code == StatusCode.OK.value


@pytest.mark.parametrize("creds", CREDS)
@pytest.mark.agent_handler
def test_agent_handler_health(creds, agent_handler):
    resp = agent_handler.check_health()
    assert resp.status_code == StatusCode.OK.value


@pytest.mark.parametrize("creds", CREDS)
@pytest.mark.imaging
def test_imaging_health(creds, imaging):
    resp = imaging.check_health()
    assert resp.status_code == StatusCode.OK.value


@pytest.mark.parametrize("creds", CREDS)
@pytest.mark.imaging_crop
def test_imaging_crop_health(creds, imaging_crop):
    resp = imaging_crop.check_health()
    assert resp.status_code == StatusCode.OK.value


@pytest.mark.parametrize("creds", CREDS)
@pytest.mark.notes
def test_notes_health(creds, notes):
    resp = notes.check_health()
    assert resp.status_code == StatusCode.OK.value


@pytest.mark.parametrize("creds", CREDS)
@pytest.mark.patient_data
def test_patient_data_health(creds, patient_data):
    resp = patient_data.check_health()
    assert resp.status_code == StatusCode.OK.value


@pytest.mark.parametrize("creds", CREDS)
@pytest.mark.slide_revision_manager
def test_slide_revision_manager_health(creds, slide_revision_manager):
    resp = slide_revision_manager.check_health()
    assert resp.status_code == StatusCode.OK.value


@pytest.mark.parametrize("creds", CREDS)
@pytest.mark.vrepro
def test_vrepro_health(creds, vrepro):
    resp = vrepro.check_health()
    assert resp.status_code == StatusCode.OK.value


@pytest.mark.parametrize("creds", CREDS)
@pytest.mark.users
def test_users_health(creds, users):
    resp = users.check_health()
    assert resp.status_code == StatusCode.OK.value


@pytest.mark.parametrize("creds", CREDS)
@pytest.mark.tenants
def test_tenants_health(creds, users):
    resp = users.check_health()
    assert resp.status_code == StatusCode.OK.value
