from requests import Response

from automation_sdk.http_requests.status_codes import StatusCode
from test.shared_packages.chloe_backend.patient_data.types.responses.Kpi_age_groups_resp import (
    KpiAgeGroupsAdded,
)


def validate_age_groups_min_max(resp_min, resp_max, expected_min, expected_max: int):
    assert resp_min == expected_min
    assert resp_max == expected_max


def validate_age_group_not_exists(resp_to_assert: KpiAgeGroupsAdded, age_group_id: int):
    assert not any(item.age_group_id == age_group_id for item in resp_to_assert)


def validate_tenant_created(tenants_db, tenant_id: str):
    assert tenants_db.find_tenant(tenant_id) is not None


def validate_kpi_settings_data(resp, expected_kpi_settings_data):
    assert resp == expected_kpi_settings_data
