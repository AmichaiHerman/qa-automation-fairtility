import logging

import pytest

from automation_sdk.mongo.repos.tenants_repo import TenantsRepo
from test.shared_packages.chloe_backend.tenants.tenants_routes import TenantsRoutes
from test.shared_packages.markers_helper.markers_helper import get_marker

logger = logging.getLogger(__name__)


@pytest.fixture
def tenants_db(request) -> TenantsRepo:
    repo = TenantsRepo()
    yield repo
    tenant_id_param = get_marker(request, "tenant_id_param")
    repo.delete_tenant_data(tenant_id_param)


@pytest.fixture
def kpi_age_group_teardown(request, tenants: TenantsRoutes):
    yield
    logger.info("starting teardown kpi_age_group_setter")
    age_group_id = get_marker(request, "age_group_id")
    tenants.delete_kpi_age_group(age_group_id)
