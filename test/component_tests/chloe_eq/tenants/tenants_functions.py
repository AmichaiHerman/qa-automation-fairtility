from typing import Union, List

from requests import Response

from test.shared_packages.chloe_backend.patient_data.types.requests_data.payloads import (
    AddAgeGroup,
)
from test.shared_packages.chloe_backend.patient_data.types.responses.Kpi_age_groups_resp import (
    KpiAgeGroupsAdded,
)
from test.shared_packages.chloe_backend.patient_data.types.responses.settings import (
    SettingsModel,
    KpiSettings,
)
from test.shared_packages.chloe_backend.tenants.tenants_routes import TenantsRoutes
from test.shared_packages.chloe_backend.tenants.types.tenant_response import (
    TenantsResponse,
)
from test.shared_packages.chloe_backend.tenants.types.tenants_payload import (
    TenantsPayload,
)
from test.shared_packages.markers_helper.markers_helper import set_marker


def add_kpi_age_group(
    tenants, request, cred_min: int, cred_max: int
) -> KpiAgeGroupsAdded:
    resp: KpiAgeGroupsAdded = tenants.post_kpi_age_group(
        payload=AddAgeGroup(min=cred_min, max=cred_max)
    )
    age_group_id = resp.age_group_id
    set_marker(request, marker_name="age_group_id", marker_value=age_group_id)
    return resp


def delete_kpi_age_group(
    tenants, cred_min: int, cred_max: int
) -> (KpiAgeGroupsAdded, int):
    resp: KpiAgeGroupsAdded = tenants.post_kpi_age_group(
        payload=AddAgeGroup(min=cred_min, max=cred_max)
    )
    age_group_id = resp.age_group_id
    tenants.delete_kpi_age_group(resp.age_group_id)
    resp_to_assert = tenants.get_kpi_age_groups()
    return resp_to_assert, age_group_id


def tenant_creation(
    request, tenants: TenantsRoutes, tenant_settings_input_data: TenantsPayload
):
    resp: TenantsResponse = tenants.create_tenant(tenant_settings_input_data)
    tenant_id_param = resp.tenant_id
    set_marker(request, marker_name="tenant_id_param", marker_value=tenant_id_param)
    return tenant_id_param


def tenant_creation_security(
    tenants: TenantsRoutes, tenant_settings_input_data: TenantsPayload
) -> Union[Response, SettingsModel]:
    resp = tenants.create_tenant(tenant_settings_input_data)
    return resp


def tenant_update(
    tenants: TenantsRoutes, tenant_id: str, tenant_settings_input_data: TenantsPayload
):
    resp: TenantsResponse = tenants.update_tenant(tenant_id, tenant_settings_input_data)
    return resp


def get_kpi_settings(tenants: TenantsRoutes) -> Union[Response, List[KpiSettings]]:
    resp = tenants.get_kpi_settings()
    return resp
