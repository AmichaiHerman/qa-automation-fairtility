import pytest

from test.shared_packages.chloe_backend.tenants.types.tenants_payload import (
    TenantsPayload,
    TenantPayloadSettings,
)


@pytest.fixture
def tenant_settings_input_data() -> TenantsPayload:
    return TenantsPayload(name="test-tenant", settings=TenantPayloadSettings())
