from automation_sdk.http_requests.status_codes import StatusCode
from automation_sdk.mongo.repos.tenants_repo import TenantsRepo
from test.component_tests.chloe_eq.tenants import tenants_functions
from test.component_tests.chloe_eq.tenants import tenants_validations
from test.component_tests.chloe_eq.tenants.tenants_cases import *
from test.shared_packages.atomic_types import UserCredentials
from test.shared_packages.chloe_backend.tenants.tenants_routes import TenantsRoutes
from test.component_tests.chloe_eq.tenants.input_data_tenants import *
from test.validations import is_status_code


@pytest.mark.tenants
@pytest.mark.parametrize("creds, mini, maxi", TEST_CASES_ADD_KPI_AGE_GROUP)
def test_add_kpi_age_group(request, tenants: TenantsRoutes, kpi_age_group_teardown, creds: UserCredentials, mini: int,
                           maxi: int):
    resp = tenants_functions.add_kpi_age_group(tenants, request, mini, maxi)
    tenants_validations.validate_age_groups_min_max(resp.min, resp.max, mini, maxi)


@pytest.mark.tenants
@pytest.mark.parametrize("creds , mini, maxi", TEST_CASES_DELETE_KPI_AGE_GROUP)
def test_delete_kpi_age_group(tenants: TenantsRoutes, creds: UserCredentials, mini: int, maxi: int):
    resp_to_assert, age_group_id = tenants_functions.delete_kpi_age_group(tenants, mini, maxi)
    tenants_validations.validate_age_group_not_exists(resp_to_assert, age_group_id)


@pytest.mark.tenants
@pytest.mark.parametrize("creds", TEST_CASES_CREATE_TENANTS)
def test_tenant_creation(request, creds: UserCredentials, tenants_db: TenantsRepo, tenants: TenantsRoutes,
                         tenant_settings_input_data):
    tenant_id = tenants_functions.tenant_creation(request, tenants, tenant_settings_input_data)
    tenants_validations.validate_tenant_created(tenants_db, tenant_id)


@pytest.mark.tenants
@pytest.mark.parametrize("creds", TEST_CASES_CREATE_TENANTS_SECURITY)
def test_tenant_creation_security(creds: UserCredentials, tenants: TenantsRoutes, tenant_settings_input_data):
    resp = tenants_functions.tenant_creation_security(tenants, tenant_settings_input_data)
    assert is_status_code(resp.status_code, StatusCode.FORBIDDEN.value)


@pytest.mark.tenants
@pytest.mark.parametrize("creds, tenant_id", TEST_CASES_UPDATE_TENANTS)
def test_tenant_update(creds: UserCredentials, tenant_id: str, tenants_db: TenantsRepo, tenants: TenantsRoutes,
                       tenant_settings_input_data):
    tenants_functions.tenant_update(tenants, tenant_id, tenant_settings_input_data)
    #TODO: ADD VALIDATION FOR UPDATED TENANT


@pytest.mark.tenants
@pytest.mark.parametrize("creds", TEST_CASES_GET_KPI_SETTINGS)
def test_get_kpi_settings(creds: UserCredentials, tenants: TenantsRoutes, expected_kpi_settings_data):
    resp = tenants.get_kpi_settings()
    tenants_validations.validate_kpi_settings_data(resp, expected_kpi_settings_data[creds.user_name])
