from test.shared_packages.test_data.test_users import (
    ADMIN_CREDS,
    LAB_CONNECTOR_1,
    PATIENT_DATA_VITRO2,
)

# test_tenant_creation
TEST_CASES_CREATE_TENANTS = [(ADMIN_CREDS)]

TEST_CASES_UPDATE_TENANTS = [(ADMIN_CREDS, "blablabla")]

# test_tenant_creation_security
TEST_CASES_CREATE_TENANTS_SECURITY = [(LAB_CONNECTOR_1)]

# test_add_kpi_age_group
TEST_CASES_ADD_KPI_AGE_GROUP = [(PATIENT_DATA_VITRO2, 20, 30)]

# test_delete_kpi_age_group
TEST_CASES_DELETE_KPI_AGE_GROUP = [(PATIENT_DATA_VITRO2, 20, 30)]

# test_get_kpi_settings
TEST_CASES_GET_KPI_SETTINGS = [(PATIENT_DATA_VITRO2)]
