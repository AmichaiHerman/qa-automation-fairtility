import pytest

from automation_sdk.gcp.storage.storage_client import StorageClient
from automation_sdk.http_requests.status_codes import StatusCode

from test.shared_packages.test_data.test_users import ADMIN_CREDS
from test.component_tests.chloe_eq.imaging.timelapse_creation.test_requests import (
    timelapse_request,
)

TEST_CASES = [(ADMIN_CREDS, timelapse_request(run_number=1, vendor_type="VITRO"))]


@pytest.fixture(scope="session")
def storage_client():
    client = StorageClient()
    return client


@pytest.mark.imaging
@pytest.mark.parametrize("creds, timelapse_creation_payload", TEST_CASES)
def test_timelapse_overriding(
    imaging, creds, timelapse_creation_payload, storage_client
):
    # create first video
    resp = imaging.create_timelapse(timelapse_creation_payload)
    assert resp.status_code == StatusCode.CREATED.value
    video_folder_path = f"{timelapse_creation_payload.rootPath}/videos/{timelapse_creation_payload.runNumber}"
    is_videos_folder_exists = storage_client.is_folder_exists(video_folder_path)
    assert is_videos_folder_exists is True

    # create second video
    resp = imaging.create_timelapse(timelapse_creation_payload)
    assert resp.status_code == StatusCode.CREATED.value
    video_folder_path = f"{timelapse_creation_payload.rootPath}/videos/{timelapse_creation_payload.runNumber}"
    is_videos_folder_exists = storage_client.is_folder_exists(video_folder_path)
    assert is_videos_folder_exists is True
