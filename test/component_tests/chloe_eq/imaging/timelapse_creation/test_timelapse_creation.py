import pytest
from automation_sdk.http_requests.status_codes import StatusCode
from test.shared_packages.payload_builders.sample_imaging_payload_by_vendor import (
    miri_timelapse_request,
    geri_timelapse_request,
    vitro_timelapse_request,
)
from test.shared_packages.test_data.test_users import ADMIN_CREDS

VALID_TEST_CASES = [
    (ADMIN_CREDS, vitro_timelapse_request(run_number=1)),
    (ADMIN_CREDS, vitro_timelapse_request(run_number=10)),
    (ADMIN_CREDS, vitro_timelapse_request(run_number=100)),
    (ADMIN_CREDS, vitro_timelapse_request(run_number=197)),
]
UNSUPPORTED_VENDOR_TEST_CASES = [
    (ADMIN_CREDS, miri_timelapse_request(run_number=1)),
    (ADMIN_CREDS, geri_timelapse_request(run_number=1)),
]
NEGATIVE_RUN_TEST_CASES = [(ADMIN_CREDS, vitro_timelapse_request(run_number=-1))]
FOLLOWING_RUNS_TEST_CASES = [(ADMIN_CREDS, vitro_timelapse_request(run_number=100))]


@pytest.mark.imaging
@pytest.mark.parametrize("creds, timelapse_creation_payload", VALID_TEST_CASES)
def test_timelapse_creation(imaging, creds, timelapse_creation_payload, storage_client):
    print(timelapse_creation_payload)
    resp = imaging.create_timelapse(timelapse_creation_payload)
    assert resp.status_code == StatusCode.CREATED.value
    video_folder_path = f"{timelapse_creation_payload.rootPath}/videos/{timelapse_creation_payload.runNumber}"
    is_videos_folder_exists = storage_client.is_folder_exists(video_folder_path)
    assert is_videos_folder_exists is True


@pytest.mark.imaging
@pytest.mark.parametrize("creds, timelapse_creation_payload", VALID_TEST_CASES)
def test_supported_image_file_timelapse_request_png(
    imaging, creds, timelapse_creation_payload, storage_client
):
    resp = imaging.create_timelapse(timelapse_creation_payload)
    resp.imagesFormat = "jpg"
    assert resp.status_code == StatusCode.CREATED.value
    video_folder_path = f"{timelapse_creation_payload.rootPath}/videos/{timelapse_creation_payload.runNumber}"
    is_videos_folder_exists = storage_client.is_folder_exists(video_folder_path)
    assert is_videos_folder_exists


@pytest.mark.imaging
@pytest.mark.parametrize("creds, timelapse_creation_payload", VALID_TEST_CASES)
def test_missing_focal_in_timelapse_request(
    imaging, creds, timelapse_creation_payload, storage_client
):
    timelapse_creation_payload.focals = [timelapse_creation_payload.focals.pop()]
    resp = imaging.create_timelapse(timelapse_creation_payload)
    assert resp.status_code == StatusCode.CREATED.value
    video_folder_path = f"{timelapse_creation_payload.rootPath}/videos/{timelapse_creation_payload.runNumber}"
    is_videos_folder_exists = storage_client.is_folder_exists(video_folder_path)
    assert is_videos_folder_exists is True


@pytest.mark.imaging
@pytest.mark.parametrize("creds, timelapse_creation_payload", FOLLOWING_RUNS_TEST_CASES)
def test_following_run_same_tenant_timelapse_request(
    imaging, creds, timelapse_creation_payload, storage_client
):
    imaging.create_timelapse(timelapse_creation_payload)
    timelapse_creation_payload.runNumber = timelapse_creation_payload.runNumber + 1
    resp = imaging.create_timelapse(timelapse_creation_payload)
    assert resp.status_code == StatusCode.CREATED.value
    video_folder_path = f"{timelapse_creation_payload.rootPath}/videos/{timelapse_creation_payload.runNumber}"
    is_videos_folder_exists = storage_client.is_folder_exists(video_folder_path)
    assert is_videos_folder_exists is True


#  ↓↓↓↓ Negative Tests ↓↓↓↓
@pytest.mark.skip("Until Bug Fix")
@pytest.mark.imaging
@pytest.mark.parametrize("creds, timelapse_request", UNSUPPORTED_VENDOR_TEST_CASES)
def test_geri_pass_imaging(imaging, creds, timelapse_request):
    resp = imaging.create_timelapse(timelapse_request)
    assert resp.status_code != StatusCode.CREATED.value


@pytest.mark.imaging
@pytest.mark.skip("Until Bug Fix KID")
@pytest.mark.parametrize("creds, timelapse_creation_payload", NEGATIVE_RUN_TEST_CASES)
def test_negative_run_in_timelapse_request(imaging, creds, timelapse_creation_payload):
    resp = imaging.create_timelapse(timelapse_creation_payload)
    assert resp.status_code == StatusCode.SERVER_ERROR.value


@pytest.mark.imaging
@pytest.mark.parametrize("creds, timelapse_creation_payload", VALID_TEST_CASES)
def test_unsupported_image_file_timelapse_request_png(
    imaging, creds, timelapse_creation_payload, storage_client
):
    timelapse_creation_payload.imagesFormat = "png"
    resp = imaging.create_timelapse(timelapse_creation_payload)
    assert resp.status_code == StatusCode.SERVER_ERROR.value
