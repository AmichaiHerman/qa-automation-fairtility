# Imaging Service Tests #

This package contains component tests for the Imaging service.

## Coverage

The tests are validating the functionality of these API endpoints:

```POST /create-timelapse```: Allows tenants to create video from his stored images.


