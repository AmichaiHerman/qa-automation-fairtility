import os

import pytest

from test.shared_packages.chloe_backend.imaging.types.requests_data import (
    CreateTimelapsePayload,
)


@pytest.fixture
def desired_payload() -> CreateTimelapsePayload:
    return CreateTimelapsePayload(
        slideId="639731c63c6366290902225f",
        wellEsId="3",
        runNumber=1,
        lastImageHour=1801000,
        lastImageUrl="https://storage.googleapis.com/kidplus-bucket-dev/639731c6d9558c4efa60cc08_3%2Fimages%2F0%2F1801000.jpeg",
        rootPath="639731c6d9558c4efa60cc08_3",
        instrumentType="VITRO",
        focals=[-5, -4, -3, -2, -1, 1, 2, 3, 4, 5, 0],
        imagesFormat="jpeg",
    )


@pytest.fixture(scope="session")
def local_videos_dir() -> str:
    local_videos_dir = (
        os.getcwd() + "/chloe_eq/imaging/video_quality/test_videos/"
    )  # running from component_tests folder
    # if running locally -> local_videos_dir = os.getcwd() + "/test_videos/"
    yield local_videos_dir
    # remove video dir from storage after each tests
    os.system(
        "gsutil -m rm -r gs://kidplus-bucket-dev/639731c6d9558c4efa60cc08_3/videos"
    )
