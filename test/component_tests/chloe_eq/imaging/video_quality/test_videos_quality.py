import os
import pytest

from automation_sdk.http_requests.status_codes import StatusCode
from automation_sdk.video_processing.video_compare_by_focal import VideoComparePerFocal
from test.component_tests.chloe_eq.imaging.video_quality.test_cases import BASE_SCENARIO


# todo make this test available on CI.


@pytest.mark.parametrize("creds, focal", BASE_SCENARIO)
def test_videos_quality_diff(creds, focal, desired_payload, imaging, local_videos_dir):
    """
    This Test Validates That the Imaging Service Was able To Create
     High Quality and Low Quality Video For each focal.
    """
    resp = imaging.create_timelapse(desired_payload)
    assert resp.status_code == StatusCode.CREATED
    storage_path = desired_payload.rootPath
    download_videos(storage_path, focal)
    video_compare = VideoComparePerFocal(focal, local_videos_dir)
    assert video_compare.compare_video_sizes() is False
    assert video_compare.compare_frames_amount()
    assert video_compare.asses_frame_inclusion() is False


def download_videos(root_path: str, focal: int):
    base_path = f"gs://kidplus-bucket-dev/{root_path}/videos/1/"
    paths_to_download = [base_path + f"{focal}_hq.mp4", base_path + f"{focal}_lq.mp4"]
    for path in paths_to_download:
        os.system(f"gsutil cp -r {path} test_videos")
