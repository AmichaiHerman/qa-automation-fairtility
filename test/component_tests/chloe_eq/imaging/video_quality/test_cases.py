from test.shared_packages.test_data.test_users import IMAGING_1

BASE_SCENARIO = [
    (IMAGING_1, -5),
    (IMAGING_1, -4),
    (IMAGING_1, -3),
    (IMAGING_1, -2),
    (IMAGING_1, -1),
    (IMAGING_1, 0),
    (IMAGING_1, 1),
    (IMAGING_1, 2),
    (IMAGING_1, 3),
    (IMAGING_1, 4),
    (IMAGING_1, 5),
]
