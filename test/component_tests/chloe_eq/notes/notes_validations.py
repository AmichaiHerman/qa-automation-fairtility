from typing import Union
from requests import Response
from test.shared_packages.chloe_backend.patient_data.types.requests_data.payloads import (
    PostNote,
)


def validate_get_note(resp: Union[Response, PostNote], expected_notes_data: PostNote):
    assert (expected_notes_data in resp) is True
