from typing import Dict, List
import pytest
from test.shared_packages.chloe_backend.patient_data.types.requests_data.payloads import (
    PostNote,
)


@pytest.fixture
def expected_data_notes() -> Dict[str, PostNote]:
    return {
        "vitamin-c": PostNote(
            authorId="63e126eb520ef4432912bc7a",
            content="lolipop",
            entityName="well",
            relatedDocId="630dd24a44d91c16371fa717",
            tenantId="63e126ea142baca0a43f78d6",
        )
    }
