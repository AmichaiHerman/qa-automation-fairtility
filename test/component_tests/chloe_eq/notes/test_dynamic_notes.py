from typing import Any, Dict

import pytest
from automation_sdk.http_requests.status_codes import StatusCode
from test.component_tests.chloe_eq.notes.notes_functions import (
    delete_note,
    delete_note_security,
    get_notes,
    post_note,
    post_note_security,
)
from test.component_tests.chloe_eq.notes.notes_validations import validate_get_note
from test.component_tests.chloe_eq.notes.test_cases_notes import *
from test.component_tests.chloe_eq.notes.expected_data_notes import expected_data_notes
from test.shared_packages.chloe_backend.patient_data.types.requests_data.payloads import PostNote
from test.validations import is_status_code


@pytest.mark.parametrize("creds, well_id", TEST_CASES_DELETE_NOTE)
def test_delete_note(creds, notes, well_id: str):
    resp = delete_note(well_id, notes)
    assert is_status_code(resp.status_code, StatusCode.OK.value)


@pytest.mark.parametrize("creds, well_id_not_in_tenant", TEST_CASES_DELETE_NOTE_SECURITY)
def test_delete_note_security(creds, well_id_not_in_tenant, notes):
    resp = delete_note_security(well_id_not_in_tenant, notes)
    assert is_status_code(resp.status_code, StatusCode.SERVER_ERROR.value)


@pytest.mark.parametrize("creds, well_id", TEST_CASES_GET_NOTE)
def test_get_notes(creds, well_id: str, notes, expected_data_notes: Dict[str, PostNote]):
    resp = get_notes(well_id, notes)
    validate_get_note(resp, expected_data_notes["vitamin-c"])


@pytest.mark.parametrize("creds", TEST_CASES_POST)
def test_post_note(creds, notes):
    resp = post_note(notes)
    assert is_status_code(resp.status_code, StatusCode.CREATED.value)


@pytest.mark.parametrize("creds", TEST_CASES_POST)
def test_post_note_security(creds, notes):
    resp = post_note_security(notes)
    assert is_status_code(resp.status_code, StatusCode.BAD_REQUEST.value)
