from typing import Union
from requests import Response
from test.shared_packages.chloe_backend.notes.notes_routes import NotesRoutes
from test.shared_packages.chloe_backend.patient_data.types.requests_data.payloads import (
    PostNote,
)


def delete_note(well_id: str, notes: NotesRoutes) -> Union[Response, PostNote]:
    notes.post_note(
        payload=PostNote(
            authorId="63e126eb520ef4432912bc7a",
            content="lolipop",
            entityName="well",
            relatedDocId="63e1296c243ec97e3e5df728",  # well_id
            tenantId="63e126ea142baca0a43f78d6",
        )
    )
    resp = notes.delete_note(well_id)
    return resp


def delete_note_security(
    well_id_not_in_tenant: str, notes: NotesRoutes
) -> Union[Response, PostNote]:
    notes.post_note(
        payload=PostNote(
            authorId="63e126eb520ef4432912bc7a",
            content="lolipop",
            entityName="well",
            relatedDocId="63e1296c243ec97e3e5df728",  # well_id
            tenantId="63e126ea142baca0a43f78d6",
        )
    )
    resp = notes.delete_note(well_id_not_in_tenant)
    return resp


def get_notes(well_id: str, notes: NotesRoutes) -> Union[Response, PostNote]:
    resp = notes.get_notes(well_id)
    return resp


def post_note(notes: NotesRoutes) -> Response:
    resp = notes.post_note(
        payload=PostNote(
            authorId="63e126eb520ef4432912bc7a",
            content="lolipop",
            entityName="well",
            relatedDocId="630dd24a44d91c16371fa717",  # well_id
            tenantId="63e126ea142baca0a43f78d6",
        )
    )
    return resp


def post_note_security(notes: NotesRoutes) -> Response:
    resp = notes.post_note(
        payload=PostNote(
            authorId="63e126eb520ef4432912bc7",
            content="lolipop",
            entityName="well",
            relatedDocId="630dd24a44d91c16371fa717",  # well_id
            tenantId="63e126ea142baca0a43f78d6",
        )
    )
    return resp
