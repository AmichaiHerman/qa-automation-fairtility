from typing import Dict

import pytest

from automation_sdk.http_requests.status_codes import StatusCode
from test.shared_packages.payload_builders.sample_imaging_payload_by_vendor import (
    miri_timelapse_request,
    geri_timelapse_request,
    vitro_timelapse_request,
)
from test.shared_packages.test_data.test_users import ADMIN_CREDS

VALID_TEST_CASES = [
    (ADMIN_CREDS, miri_timelapse_request(run_number=1)),
    (ADMIN_CREDS, miri_timelapse_request(run_number=4)),
    (ADMIN_CREDS, miri_timelapse_request(run_number=10)),
    (ADMIN_CREDS, miri_timelapse_request(run_number=30)),
    (ADMIN_CREDS, miri_timelapse_request(run_number=60)),  # final run
    (ADMIN_CREDS, geri_timelapse_request(run_number=1)),
    (ADMIN_CREDS, geri_timelapse_request(run_number=4)),
    (ADMIN_CREDS, geri_timelapse_request(run_number=10)),
    (
        ADMIN_CREDS,
        geri_timelapse_request(run_number=100),
    ),  # large run number, geri images are heavy.
]

UNSUPPORTED_VENDOR_TEST_CASES = [(ADMIN_CREDS, vitro_timelapse_request(run_number=1))]
NEGATIVE_RUN_TEST_CASES = [
    (ADMIN_CREDS, miri_timelapse_request(run_number=-1))
]  # (ADMIN_CREDS, vitro_timelapse_request(run_number=-1))]


@pytest.mark.imaging
@pytest.mark.parametrize("creds, timelapse_creation_payload", VALID_TEST_CASES)
def test_timelapse_creation(
    imaging_crop, creds, timelapse_creation_payload, storage_client
):
    resp = imaging_crop.create_timelapse(timelapse_creation_payload)
    assert resp.status_code == StatusCode.CREATED.value
    video_folder_path = f"{timelapse_creation_payload.rootPath}/videos/{timelapse_creation_payload.runNumber}"
    cropped_images_folder_path = (
        f"{timelapse_creation_payload.rootPath}/cropped_images/0"
    )
    is_videos_folder_exists = storage_client.is_folder_exists(video_folder_path)
    is_cropped_images_folder_exists = storage_client.is_folder_exists(
        cropped_images_folder_path
    )
    assert is_videos_folder_exists is True
    assert is_cropped_images_folder_exists is True


#  ↓↓↓↓ Negative ↓↓↓↓
@pytest.mark.imaging_crop
@pytest.mark.parametrize(
    "creds, timelapse_creation_payload", UNSUPPORTED_VENDOR_TEST_CASES
)
def test_timelapse_creation_with_vitro(imaging_crop, creds, timelapse_creation_payload):
    resp = imaging_crop.create_timelapse(timelapse_creation_payload)
    assert resp.status_code != StatusCode.CREATED.value


@pytest.mark.skip("Until Bug Fix")
@pytest.mark.imaging_crop
@pytest.mark.parametrize("creds, timelapse_creation_payload", VALID_TEST_CASES)
def test_missing_focal_in_timelapse_request(
    imaging_crop, creds, timelapse_creation_payload
):
    timelapse_creation_payload.focals = [timelapse_creation_payload.focals.pop()]
    resp = imaging_crop.create_timelapse(timelapse_creation_payload)
    assert resp.status_code == StatusCode.SERVER_ERROR.value


@pytest.mark.skip("Until Bug Fix")
@pytest.mark.imaging_crop
@pytest.mark.parametrize("creds, timelapse_creation_payload", NEGATIVE_RUN_TEST_CASES)
def test_negative_run_in_timelapse_request(
    imaging_crop, creds, timelapse_creation_payload
):
    resp = imaging_crop.create_timelapse(timelapse_creation_payload)
    assert resp.status_code == StatusCode.SERVER_ERROR.value


@pytest.mark.skip("Until Bug Fix")
@pytest.mark.imaging_crop
@pytest.mark.parametrize("creds, timelapse_creation_payload", VALID_TEST_CASES)
def test_unsupported_image_file_timelapse_request(
    imaging_crop, creds, timelapse_creation_payload
):
    timelapse_creation_payload.imagesFormat = "png"
    resp = imaging_crop.create_timelapse(timelapse_creation_payload)
    assert resp.status_code == StatusCode.SERVER_ERROR.value


# ↑↑↑↑ Negative Tests ↑↑↑↑
