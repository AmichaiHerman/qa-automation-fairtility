import pytest

from automation_sdk.console_logging.debug import Debug
from automation_sdk.gcp.cloud_tasks.cloud_tasks_client import CloudTasksClient
from automation_sdk.gcp.cloud_tasks.types.video_queues.slides_queue import SlidesQueue
from automation_sdk.gcp.storage.storage_client import StorageClient
from test.shared_packages.chloe_backend.imaging.types.requests_data import (
    CreateTimelapsePayload,
)


@pytest.fixture(scope="function")
def storage_client(timelapse_creation_payload: CreateTimelapsePayload):
    # setup
    storage_client = StorageClient()
    yield storage_client
    # teardown
    Debug.log("Deleting Test Data")
    storage_client.delete_folder(
        f"{timelapse_creation_payload.rootPath}/videos/{timelapse_creation_payload.runNumber}"
    )
    queue_client = CloudTasksClient()
    slides_queue = SlidesQueue()
    Debug.log("Cleaning Slides Queue")
    queue_client.purge_queue(slides_queue)
