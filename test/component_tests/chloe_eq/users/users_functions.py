from requests import Response
import secrets
from typing import Optional

from test.component_tests.chloe_eq.users.conftest import UserTypes
from test.shared_packages.chloe_backend.users.types.user_settings import UserSettings
from test.shared_packages.chloe_backend.users.users_routes import UsersRoutes


def chloe_user_creation(user_type: UserTypes, users: UsersRoutes) -> Response:
    user_settings = create_user_settings(user_type)
    resp = users.create_user(user_settings)
    return resp


def chloe_user_creation_with_existed_name(
    user_type: UserTypes, users: UsersRoutes
) -> Response:
    user_settings = create_user_settings(user_type, use_name="agent-handler-test-5")
    resp = users.create_user(user_settings)
    return resp


def chloe_user_creation_with_existed_mail(
    user_type: UserTypes, users: UsersRoutes
) -> Response:
    user_settings = create_user_settings(user_type, use_mail="_LKMWaHq8Kk@gmail.com")
    resp = users.create_user(user_settings)
    return resp


def chloe_user_creation_security(user_type: UserTypes, users: UsersRoutes) -> Response:
    user_settings = create_user_settings(user_type, use_mail="_LKMWaHq8Kk@gmail.com")
    resp = users.create_user(user_settings)
    return resp


def create_user_settings(
    role: UserTypes,
    use_name: Optional[str] = None,
    use_mail: Optional[str] = None,
) -> UserSettings:
    set_role = None if role.value == "regular" else role.value
    user_name = use_name if use_name else secrets.token_urlsafe(8)
    mail = use_mail if use_mail else f"{secrets.token_urlsafe(8)}@gmail.com"
    data = UserSettings(
        userName=user_name,
        firstName="name is",
        lastName="Marshall",
        imageUrl="empty",
        email=mail,
        role=set_role,
        tenants=[{"_id": "639f26730f41887b3a376cda"}],
        isPasswordTemp=False,
    )
    return data
