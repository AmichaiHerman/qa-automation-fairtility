from enum import Enum


class UserTypes(Enum):
    AGENT = "agent"
    EMPLOYEE = "employee"
    REGULAR = "regular"
