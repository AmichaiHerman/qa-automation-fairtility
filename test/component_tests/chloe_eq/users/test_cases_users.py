from test.component_tests.chloe_eq.users.conftest import UserTypes
from test.shared_packages.test_data.test_users import LAB_CONNECTOR_1, ADMIN_CREDS

# test_user_data_retrieval
# test_onboarding_data_retrieval
TEST_CASES_USER_DATA_RETRIEVAL = [(LAB_CONNECTOR_1)]


# test_chloe_user_creation
TEST_CASES_USER_CREATION = [
    (ADMIN_CREDS, UserTypes.AGENT),
    (ADMIN_CREDS, UserTypes.EMPLOYEE),
    (ADMIN_CREDS, UserTypes.REGULAR),
]


# test_chloe_user_creation_with_existed_name
# test_chloe_user_creation_with_existed_mail
TEST_CASES_USER_CREATION_WITH = [(ADMIN_CREDS, UserTypes.AGENT)]


# test_chloe_user_creation_security ???? need no admin test cases i think
TEST_CASES_USER_CREATION_SECURITY = [(ADMIN_CREDS, UserTypes.AGENT)]
