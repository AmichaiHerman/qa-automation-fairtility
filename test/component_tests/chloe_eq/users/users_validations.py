from requests import Response

from test.shared_packages.chloe_backend.users.types.onboarding_response import (
    OnboardingResponse,
)
from test.shared_packages.chloe_backend.users.types.user_data_response import (
    UserDataResponse,
)


def validate_user_exist_same_name_message(resp: Response):
    assert resp.json()["message"] == "User exists with same username"


def validate_user_exist_same_email_message(resp: Response):
    assert resp.json()["message"] == "User exists with same email"


def validate_permissions_error_message(resp: Response):
    assert resp.json()["message"] == "You don`t have system admin permissions"


def validate_expected_data(
    resp: UserDataResponse or OnboardingResponse, expected_data: any
):
    assert resp == expected_data
