from test.shared_packages.chloe_backend.users.types.user_data_response import (
    UserDataResponse,
    Tenant,
    Settings,
)

import pytest

from test.shared_packages.chloe_backend.users.types.onboarding_response import (
    OnboardingResponse,
    ComponentTutorials,
    Treatments,
    WellCards,
    CompareWells,
    PerformanceTable,
    Default,
    Instruments,
    Wells,
    Default1,
    HelpCenterContent,
    Video,
    Manual,
    Faq,
)


@pytest.fixture
def expected_onboarding_data() -> OnboardingResponse:
    return OnboardingResponse(
        componentTutorials=ComponentTutorials(
            Treatments=Treatments(
                well_cards=WellCards(
                    title="Treatments page",
                    videoUrl="https://storage.googleapis.com/kidplus-help-ceneter/component-tutorials/treatment-page.mp4",
                ),
                compare_wells=CompareWells(
                    title="Compare wells",
                    videoUrl="https://storage.googleapis.com/kidplus-help-ceneter/component-tutorials/comparison.mp4",
                ),
                performance_table=PerformanceTable(
                    title="Performance table",
                    videoUrl="https://storage.googleapis.com/kidplus-help-ceneter/component-tutorials/list-view.mp4",
                ),
            ),
            Instruments=Instruments(
                default=Default(
                    title="Home page",
                    videoUrl="https://storage.googleapis.com/kidplus-help-ceneter/component-tutorials/home-page-tutorial.mp4",
                )
            ),
            Wells=Wells(
                default=Default1(
                    title="Focused Well tutorial",
                    videoUrl="https://storage.googleapis.com/kidplus-help-ceneter/component-tutorials/well-screen.mp4",
                )
            ),
        ),
        helpCenterContent=HelpCenterContent(
            videos=[
                Video(
                    title="Home page tutorial",
                    description="Step by step home page tutorial",
                    videoLength="1:18",
                    previewImageUrl="https://storage.googleapis.com/kidplus-help-ceneter/videos/home-page-tutorial-preview.jpg",
                    videoUrl="https://storage.googleapis.com/kidplus-help-ceneter/videos/home-page-tutorial.mp4",
                ),
                Video(
                    title="Treatments Page",
                    description="Treatment page tutorial",
                    videoLength="1:37",
                    previewImageUrl="https://storage.googleapis.com/kidplus-help-ceneter/videos/treatment-page-preview.jpg",
                    videoUrl="https://storage.googleapis.com/kidplus-help-ceneter/videos/treatment-page.mp4",
                ),
                Video(
                    title="Compare wells",
                    description="Compare wells",
                    videoLength="0:51",
                    previewImageUrl="https://storage.googleapis.com/kidplus-help-ceneter/videos/comparison-preview.jpg",
                    videoUrl="https://storage.googleapis.com/kidplus-help-ceneter/videos/comparison.mp4",
                ),
                Video(
                    title="Performance Table",
                    description="Performance Table tutorial",
                    videoLength="0:50",
                    previewImageUrl="https://storage.googleapis.com/kidplus-help-ceneter/videos/list-view-preview.jpg",
                    videoUrl="https://storage.googleapis.com/kidplus-help-ceneter/videos/list-view.mp4",
                ),
                Video(
                    title="Focused Well tutorial",
                    description="Focused Well tutorial",
                    videoLength="1:34",
                    previewImageUrl="https://storage.googleapis.com/kidplus-help-ceneter/videos/well-screen-preview.jpg",
                    videoUrl="https://storage.googleapis.com/kidplus-help-ceneter/videos/well-screen.mp4",
                ),
            ],
            manuals=[
                Manual(
                    category="Technotes",
                    title="KID+ AI based embryo ranking",
                    description="",
                    fileUrl="https://storage.googleapis.com/kidplus-help-ceneter/manuals/AI%20Features%20Tech%20Notes.pdf",
                ),
                Manual(
                    category="Technotes",
                    title="Automatic annotations",
                    description="",
                    fileUrl="https://storage.googleapis.com/kidplus-help-ceneter/manuals/Automatic%20Annotations.pdf",
                ),
                Manual(
                    category="Technotes",
                    title="Embryo Abnormalities",
                    description="",
                    fileUrl="https://storage.googleapis.com/kidplus-help-ceneter/manuals/Embryo%20Abnormalities.pdf",
                ),
                Manual(
                    category="Technotes",
                    title="Fairtility AI",
                    description="Clinical Decision Making Support System",
                    fileUrl="https://storage.googleapis.com/kidplus-help-ceneter/manuals/Fairtility%20AI%20-%20Guide.pdf",
                ),
                Manual(
                    category="Technotes",
                    title="KID+ walk through",
                    description="A screen by screen guide of our system",
                    fileUrl="https://storage.googleapis.com/kidplus-help-ceneter/manuals/KID%2B%20walk%20through%20.pdf",
                ),
            ],
            faqs=[
                Faq(
                    q="What is KID+?",
                    a="KID+, a decision support tool, uses artificial intelligence (AI) to assess embryo viability.<br/>We identify embryo morphological and morphokinetic features that predict implantation potential, including those invisible to the embryologist.<br/>KID+ ranks and annotates each embryo, assisting the embryologist with the hard task of selecting which embryo to transfer.",
                ),
                Faq(
                    q="How does it work?",
                    a="KID+ is a SAAS application.<br/>After a short set up, we automatically access the time lapse video for each patient.<br/>We run our AI analyses and score, which is continuous in time - vs.<br/>KID score which is singular in time.<br/>The blast and implantation prediction is a score from 0 – 10 where 10 denotes the highest quality embryo with the greatest implantation potential.<br/>The prediction and score updates itself as the embryo continues to develop.",
                ),
                Faq(
                    q="How will my patients benefit from KID+?",
                    a="We strive to present you with the best embryo for transfer.<br/>The main benefit to patients is better selection of the most suitable embryo for transfer the first time, which means a shorter time to pregnancy, reducing patient emotional and financial stress.<br/>Many patients want to be in “the know,” and with our system you can use the print option to provide your patients with detailed information and images of all the embryos.<br/>This can be a great tool to better connect and share the process with your patients.",
                ),
                Faq(
                    q="How will my clinic benefit from KID+?",
                    a="KID+ both standardizes and improves embryo assessment for clinics, with the potential to reduce time to pregnancy for patients.<br/>This in turn can improve IVF success rates and allow clinics to service more high-value first-cycle patients.<br/>KID+ is provided at no cost to clinics and there is no requirement for additional equipment besides a time lapse incubator.<br/>KID+ also allows clinics to market the use of cutting-edge AI technology to improve patient outcomes.",
                ),
                Faq(
                    q="Why should embryologists use KID+?",
                    a="With time-lapse videos that are connected to KID+, you have the best information possible about the entire embryo development history to decide which embryos to transfer or freeze for future use.<br/>As we are watching the embryo development around the clock, we are able to capture and analyze all the embryo development parameters that can be missed by the human eye, such as nuclear patterns.",
                ),
                Faq(
                    q="Does KID+ require time-lapse equipment?",
                    a="Yes, KID+ is a web-based application that easily connects to your timelapse incubator.<br/>Once it is set up we do all the work and do not require any involvement.<br/>Embryologists only need to login and use our special features.",
                ),
                Faq(
                    q="When should I use KID+?",
                    a="You can start using KID+ from the minute you put the slide into the incubator.<br/>We start analyzing the images and inform you with previously unavailable information.<br/>Images of embryos are taken every 20 minutes from the server and the analysis is performed and displayed next to each well.<br/>We rank the embryos for the best implantation chance which allows you to use it to drive smarter decisions on freezing, transferring or genetic testing.<br/>",
                ),
                Faq(
                    q="Is it the same as embryo grading?",
                    a="KID+ is not the same as standard embryo morphology grading.<br/>KID+ uses AI that has been trained on a huge dataset of tens of thousands of embryo videos and images where KID+ outcomes were known (that is why we named it KID+).<br/>When compared with traditional morphological grading methods, our AI is more objective and consistent, thus more accurate at predicting pregnancy outcome.<br/>",
                ),
                Faq(
                    q="Should KID+ be used if I am conducting PGT-A?",
                    a="Yes.<br/>KID+ assesses implantation potential based on morphology and morphokinetics of the embryos.<br/>Implantation potential is not the same as genetic integrity which is measured by PGT-A.<br/>KID+ should be used prior to embryo biopsy to select embryos with the highest implantation potential for PGT-A assessment.",
                ),
                Faq(
                    q="Is my data secure?",
                    a="Of course! KID+ is both HIPAA compliant and ISO27001 compliant.<br/>We localize and use servers in the relevant country to make sure data stays local.<br/>We meet the most strict data privacy requirements.<br/>We do not access your data and it is always encrypted in the application to the highest standards.",
                ),
                Faq(
                    q="Can we collaborate with fairtility on future projects?",
                    a="Yes, we want to improve our algorithms so just ping us and we can explore together.",
                ),
            ],
            documents=[],
        ),
    )


@pytest.fixture
def expected_user_data_retrieval() -> UserDataResponse:
    return UserDataResponse(
        idpId="5fd4743e-11cb-43f8-9c8c-49950f544b3c",
        firstName="deg",
        lastName="test",
        imageUrl="empty",
        email="deg@gmail.com",
        tenants=[
            Tenant(
                settings=Settings(
                    authorizedOrigins=[],
                    authEnabled=False,
                    isFDA=False,
                    blastScoreLow=20,
                    blastScoreHigh=70,
                    kidScoreLow=20,
                    kidScoreHigh=70,
                    instrumentSize=9,
                    slideSize=12,
                    availableMkEvents=[
                        "0C",
                        "t1",
                        "tPB2",
                        "tPNa",
                        "tPNf",
                        "t2",
                        "t3",
                        "t4",
                        "t5",
                        "t6",
                        "t7",
                        "t8",
                        "t9+",
                        "tSC",
                        "tM",
                        "tSB",
                        "tErB",
                        "tB",
                        "tEB",
                        "tHN",
                        "tHB",
                    ],
                    mainEntity="treatment",
                    isVRepro=False,
                ),
                name="deg-test",
            )
        ],
        favoriteItems=[],
    )
