from automation_sdk.atomic_types import UserCredentials
from automation_sdk.http_requests.status_codes import StatusCode
from test.component_tests.chloe_eq.users.test_cases_users import *
from test.component_tests.chloe_eq.users.expected_data_users import *
from test.component_tests.chloe_eq.users.users_functions import (
    chloe_user_creation,
    chloe_user_creation_with_existed_name,
    chloe_user_creation_with_existed_mail,
    chloe_user_creation_security,
)
from test.component_tests.chloe_eq.users.users_validations import (
    validate_user_exist_same_name_message,
    validate_user_exist_same_email_message,
    validate_permissions_error_message,
    validate_expected_data,
)
from test.shared_packages.chloe_backend.users.users_routes import UsersRoutes
from test.validations import is_status_code


@pytest.mark.users
@pytest.mark.parametrize("creds, user_type", TEST_CASES_USER_CREATION)
def test_chloe_user_creation(
    creds: UserCredentials, user_type: UserTypes, users: UsersRoutes
):
    resp = chloe_user_creation(user_type, users)
    assert is_status_code(resp.status_code, StatusCode.CREATED.value)


@pytest.mark.users
@pytest.mark.parametrize("creds, user_type", TEST_CASES_USER_CREATION_WITH)
def test_chloe_user_creation_with_existed_name(
    creds: UserCredentials, user_type: UserTypes, users: UsersRoutes
):
    resp = chloe_user_creation_with_existed_name(user_type, users)
    assert is_status_code(resp.status_code, StatusCode.CONFLICT.value)
    validate_user_exist_same_name_message(resp)


@pytest.mark.users
@pytest.mark.parametrize("creds, user_type", TEST_CASES_USER_CREATION_WITH)
def test_chloe_user_creation_with_existed_mail(
    creds: UserCredentials, user_type: UserTypes, users: UsersRoutes
):
    resp = chloe_user_creation_with_existed_mail(user_type, users)
    assert is_status_code(resp.status_code, StatusCode.CONFLICT.value)
    validate_user_exist_same_email_message(resp)


# NEED TO FIX - WHAT CREDS WILL GIVE 403 FORBBIDEN MESSAGE?
@pytest.mark.users
@pytest.mark.parametrize("creds, user_type", TEST_CASES_USER_CREATION_SECURITY)
def test_chloe_user_creation_security(
    creds: UserCredentials, user_type: UserTypes, users: UsersRoutes
):
    resp = chloe_user_creation_security(user_type, users)
    assert is_status_code(resp.status_code, StatusCode.FORBIDDEN.value)
    validate_permissions_error_message(resp)


@pytest.mark.parametrize("creds", TEST_CASES_USER_DATA_RETRIEVAL)
def test_user_data_retrieval(
    creds: UserCredentials,
    users: UsersRoutes,
    expected_user_data_retrieval: UserDataResponse,
):
    resp = users.get_user_data()
    validate_expected_data(resp, expected_user_data_retrieval)


@pytest.mark.parametrize("creds", TEST_CASES_USER_DATA_RETRIEVAL)
def test_onboarding_data_retrieval(
    creds: UserCredentials,
    users: UsersRoutes,
    expected_onboarding_data: OnboardingResponse,
):
    resp = users.get_onboarding_data()
    validate_expected_data(resp, expected_onboarding_data)
