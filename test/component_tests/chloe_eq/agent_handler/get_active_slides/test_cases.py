from test.shared_packages.test_data.test_tenants import (
    AGENT_HANDLER_TENANT_1,
)
from test.shared_packages.test_data.test_users import AGENT_HANDLER_2, AGENT_HANDLER_1

BASE_SCENARIO = [(AGENT_HANDLER_2)]


SECURITY = [(AGENT_HANDLER_1, AGENT_HANDLER_TENANT_1)]
