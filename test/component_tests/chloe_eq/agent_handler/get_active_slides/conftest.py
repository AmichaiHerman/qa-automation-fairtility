from typing import List

import pytest

from test.shared_packages.chloe_backend.agent_handler.types.active_slides import (
    ActiveSlides,
)


@pytest.fixture
def expected_active_slides() -> List[ActiveSlides]:
    return [
        ActiveSlides(
            esId="D2019.01.12_S01225_I0469_D",
            patientEsId="1024eb5f-d4dd-401a-957a-eb157e091fd9",
            wells=[],
            treatmentEsId="TIDX001",
            fertilizationDate="2019-01-11T13:40:00.000Z",
            firstImageHour=70452000,
        ),
        ActiveSlides(
            esId="D2018.11.06_S00013_I3169_P",
            patientEsId="5c2ae912-5fce-4f97-bc70-acc550bfca9a",
            wells=[],
            treatmentEsId="TIDX222",
            fertilizationDate="2018-11-05T15:45:00.000Z",
            firstImageHour=59940000,
        ),
    ]
