import pytest

from automation_sdk.http_requests.status_codes import StatusCode
from test.component_tests.chloe_eq.agent_handler.get_active_slides.test_cases import (
    BASE_SCENARIO,
    SECURITY,
)


@pytest.mark.agent_handler
@pytest.mark.parametrize("creds", BASE_SCENARIO)
def test_get_active_slides(creds, agent_handler, expected_active_slides):
    resp = agent_handler.get_active_slides_for_tenant()
    assert resp == expected_active_slides


@pytest.mark.skip("Until user is enabled")
@pytest.mark.agent_handler
@pytest.mark.parametrize("creds,tenant_id", SECURITY)
def test_get_active_slides_security(creds, tenant_id, agent_handler):
    resp = agent_handler.get_active_slides_for_tenant()
    assert resp.status_code == StatusCode.FORBIDDEN.value
    assert resp.json()["message"] == "You don`t have agent-user permissions"
