import pytest

from test.component_tests.chloe_eq.agent_handler.agent_handler_db.wells_col.mock_data import (
    mock_well_data,
)

DATA = mock_well_data()
WELL_DATA_TO_IMPORT = [(DATA.tenantId, DATA)]


@pytest.mark.agent_handler
@pytest.mark.parametrize("tenant_id, well_data", WELL_DATA_TO_IMPORT)
def test_import_well_data(tenant_id, well_data, agent_handler_db):
    is_import_successful = agent_handler_db.import_well_data(well_data)
    assert is_import_successful
    assert agent_handler_db.find_single_well(tenant_id) is not None
