import pytest

from test.component_tests.chloe_eq.agent_handler.agent_handler_db.wells_col.mock_data import (
    mock_well_data,
)

DATA = mock_well_data()
WELL_DATA_TO_IMPORT = [(DATA.tenantId, DATA)]


@pytest.mark.agent_handler
@pytest.mark.parametrize("tenant_id, well_data", WELL_DATA_TO_IMPORT)
def test_import_well_data(tenant_id, well_data, agent_handler_db):
    # import well with one run
    well_data.runs = [1]
    well_data.times = [2101000]
    is_import_successful = agent_handler_db.import_well_data(well_data)
    assert is_import_successful

    # update the well with the second run data
    updated_runs = [1, 2]
    updated_times = [2101000, 3301000]
    is_update_successful = agent_handler_db.update_well_runs(
        tenant_id, updated_runs, updated_times
    )
    assert is_update_successful
    updated_well = agent_handler_db.find_single_well(tenant_id)
    assert updated_well.runs == updated_runs
    assert updated_well.times == updated_times
