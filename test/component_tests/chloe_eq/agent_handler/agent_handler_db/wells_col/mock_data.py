from automation_sdk.mongo.mongo_orm.agent_handler.wells import Wells


def mock_well_data() -> Wells:
    return Wells(
        focals=[-5, -4, -3, -2, -1, 0, 1, 2, 3, 4, 5],
        runs=[1, 2],
        times=[2101000, 3301000],
        esId="1",
        tenantId="123",
        slideId="63c42562dbdb1aeb26faf45b",
        treatmentEsId="fiv 223011",
        slideEsId="273f75cf-90c9-11ed-8a05-c400ad4629c0",
        storagePath="63c42562dbdb1aeb26faf45b_1",
    )
