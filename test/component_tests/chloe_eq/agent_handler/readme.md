# Agent Handler Service Tests #

This package contains component tests for the Agent Handler service. 

## Coverage

The tests are validating the functionality of these API endpoints:


```POST /bulk-import```: Allows tenants to bulk import patients. Accepts a JSON payload containing patient information.

```GET /active-slides```: Allows tenants to get the active slides data.
