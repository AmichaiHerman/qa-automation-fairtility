from automation_sdk.mongo.mongo_orm.agent_handler.settings import Settings


def create_mock_settings(tenant_id: str) -> Settings:
    return Settings(
        invokePipeLineEveryNRun=10, pipelineDebounceInHours=0.5, tenantId=tenant_id
    )
