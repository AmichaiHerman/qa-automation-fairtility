import pytest

from automation_sdk.mongo.mongo_orm.agent_handler.settings import Settings
from test.component_tests.chloe_eq.agent_handler.clinic_settings.mock_settings import (
    create_mock_settings,
)

TENANTS_TO_TEST = [("54321"), ("12345")]


@pytest.mark.parametrize("tenant_id", TENANTS_TO_TEST)
def test_add_settings(agent_handler_db, tenant_id) -> None:
    mock_settings = create_mock_settings(tenant_id)
    agent_handler_db.add_clinic_settings(mock_settings)
    assert isinstance(agent_handler_db.find_clinic_settings(tenant_id), Settings)


@pytest.mark.skip("Until Bug Fix")
@pytest.mark.agent_handler
@pytest.mark.parametrize("tenant_id", TENANTS_TO_TEST)
def test_config_for_same_tenant_twice(agent_handler_db, tenant_id) -> None:
    print("TBD")
