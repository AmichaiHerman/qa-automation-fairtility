import pytest

from test.component_tests.chloe_eq.agent_handler.clinic_settings.mock_settings import (
    create_mock_settings,
)

TENANTS_TO_TEST = [
    ("12345", 1),
    ("12345", 5),
    ("12345", 10),
    ("12345", 100),
    ("12345", 1000),
]


@pytest.mark.agent_handler
@pytest.mark.parametrize("tenant_id, number_of_runs", TENANTS_TO_TEST)
def test_change_invoke_every(agent_handler_db, tenant_id, number_of_runs) -> None:
    mock_settings = create_mock_settings(tenant_id)
    agent_handler_db.add_clinic_settings(mock_settings)
    agent_handler_db.update_invoke_pipeline_every(tenant_id, number_of_runs)
    updated_clinic_settings = agent_handler_db.find_clinic_settings(tenant_id)
    assert updated_clinic_settings.invokePipeLineEveryNRun == number_of_runs
