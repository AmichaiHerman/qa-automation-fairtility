import pytest

from automation_sdk.http_requests.status_codes import StatusCode
from test.component_tests.chloe_eq.agent_handler.bulk_import.shared_logic import (
    bulk_import_payload,
)
from test.component_tests.chloe_eq.agent_handler.bulk_import.test_cases import (
    BASIC_SCENARIO,
)


@pytest.mark.agent_handler
@pytest.mark.parametrize("creds, tenant_id, vendor", BASIC_SCENARIO)
@pytest.mark.skip("Something is defected with the user - Until User Changes")
def test_bulk_import_agent(creds, tenant_id, vendor, agent_handler, agent_handler_db):
    resp = agent_handler.bulk_import_patients_from_agent(bulk_import_payload(vendor))
    assert resp.status_code == StatusCode.CREATED.value
    patients_list = agent_handler_db.find_patients_data_by_tenant(tenant_id)
    assert len(patients_list) == 2
