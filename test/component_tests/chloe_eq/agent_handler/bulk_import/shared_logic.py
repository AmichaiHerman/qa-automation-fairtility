from typing import List

from automation_sdk.mongo.mongo_orm.agent_handler.patientdatas import Patientdatas
from automation_sdk.mongo.repos.agent_handler_repo import AgentHandlerRepo
from test.integration_tests.data_import.utils.bulk_import_payloads import (
    regular_bulk_import_payload,
)
from test.shared_packages.atomic_types import Vendor
from test.shared_packages.chloe_backend.agent_handler.types.bulk_import_payload import (
    BulkImportPayload,
)


def bulk_import_payload(vendor: Vendor) -> BulkImportPayload:
    return regular_bulk_import_payload(vendor)


def create_patient_birthdate_list(data: List[Patientdatas]):
    return [patient.birthdates_list for patient in data]
