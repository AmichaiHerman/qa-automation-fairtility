import pytest

from automation_sdk.http_requests.status_codes import StatusCode
from test.component_tests.chloe_eq.agent_handler.bulk_import.shared_logic import (
    create_patient_birthdate_list,
    bulk_import_payload,
)

from test.component_tests.chloe_eq.agent_handler.bulk_import.test_cases import (
    BASIC_SCENARIO,
)


@pytest.mark.agent_handler
@pytest.mark.parametrize("creds, tenant_id, vendor", BASIC_SCENARIO)
def test_update_patient_birthdate(
    creds,
    tenant_id,
    vendor,
    agent_handler,
    agent_handler_db,
):
    """
    These Test Validates The Birthdate update mechanism.
    On the first bulk-import one of the birthdates is set to None.
    On the second bulk-import it gets updated
    """
    payload = bulk_import_payload(vendor)
    resp1 = agent_handler.bulk_import_patients_from_agent(bulk_import_payload(vendor))
    assert resp1.status_code == StatusCode.CREATED.value
    past_data = agent_handler_db.find_patients_data_by_tenant(tenant_id)
    past_birthdate_list = create_patient_birthdate_list(past_data)

    # change birthdate, then fire bulk-import again
    payload.__root__[0].birthDate = "1968-05-02T00:00:00.000Z"
    resp2 = agent_handler.bulk_import_patients_from_agent(payload)
    assert resp2.status_code == StatusCode.CREATED.value
    current_data = agent_handler_db.find_patients_data_by_tenant(tenant_id)
    current_birthdate_list = create_patient_birthdate_list(current_data)
    assert past_birthdate_list != current_birthdate_list
