from test.integration_tests.tenants import BULK_IMPORT_1
from test.integration_tests.users import DATA_IMPORT1
from test.shared_packages.atomic_types import Vendor
from test.shared_packages.test_data.test_tenants import (
    AGENT_HANDLER_TENANT_4,
    AGENT_HANDLER_TENANT_3,
    AGENT_HANDLER_TENANT_5,
)
from test.shared_packages.test_data.test_users import (
    AGENT_HANDLER_5,
    AGENT_HANDLER_3,
    AGENT_HANDLER_6,
    AGENT_HANDLER_4,
)

BASIC_SCENARIO = [
    (DATA_IMPORT1, BULK_IMPORT_1, Vendor.VITRO),
    (DATA_IMPORT1, BULK_IMPORT_1, Vendor.GERI),
    (DATA_IMPORT1, BULK_IMPORT_1, Vendor.MIRI),
]
