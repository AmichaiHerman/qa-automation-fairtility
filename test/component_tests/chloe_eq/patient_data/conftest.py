from typing import Dict, List
import pytest
from datetime import datetime, timezone, timedelta
from enum import Enum, auto
from test.shared_packages.chloe_backend.patient_data.types.requests_data.get_params import InstrumentGetParam
from test.shared_packages.chloe_backend.patient_data.types.requests_data.payloads import ChangeInstrumentName
import logging
from automation_sdk.mongo.repos.patient_data_repo import PatientDataRepo

from test.shared_packages.chloe_backend.patient_data.types.responses.instruments_response import (
    InstrumentsDataResponse, Treatment, Slide, BestWell, InferenceData1, Morphokinetic1, EarlyStageData)
from test.shared_packages.chloe_backend.patient_data.types.responses.patients_emr_response import (
    PatientsEMRResponse,
    EMRTreatment,
)
from test.shared_packages.chloe_backend.patient_data.types.responses.patients_response import (
    PatientDataResponse,
    Treatment,
    PatientsSlideResponse,
    Well,
    OocyteAgeItem,
)
from test.shared_packages.chloe_backend.patient_data.types.responses.emr_wells import (
    EMRWellsResponse,
    Instrument,
    InferenceData,
    Morphokinetic,
)
from test.shared_packages.chloe_backend.patient_data.types.responses.treatment_response import TreatmentDataResponse


logger = logging.getLogger(__name__)


@pytest.fixture
def setup_teardown_creating_patient_data_db(tenant_id: str):
    logging.info("Setting up the 'patient_data_db' test fixture...")
    repo = PatientDataRepo()
    yield repo
    logging.info("Tearing down the 'patient_data_db' test fixture...")
    repo.delete_tenant_data(tenant_id)


@pytest.fixture
def setup_teardown_creating_patient_data_db_list(tenants_list: List[str]):
    repo = PatientDataRepo()
    yield repo
    for tenant in tenants_list:
        repo.delete_tenant_data(tenant)


@pytest.fixture
def teardown_change_instrument_name(patient_data, instrument_id, old_name):
    yield
    resp = patient_data.instrument_route.change_instrument_name(
        params=InstrumentGetParam(instrument_id=instrument_id),
        payload=ChangeInstrumentName(name=old_name),
    )
    assert resp.name == old_name


@pytest.fixture(scope="session")
def teardown_update_fert_time_patient_data_repo(slide_esid: str):
    yield
    repo = PatientDataRepo()
    date_to_update = _get_date_1_day_early()
    repo.update_slide_fertilization_date(slide_esid, date_to_update)


def _get_date_1_day_early():
    current_date = datetime.now()
    previous_date = current_date - timedelta(days=1)
    previous_date_with_offset = previous_date.replace(tzinfo=timezone.utc)
    return previous_date_with_offset


class Expansion(Enum):
    Expansion1 = "1"
    Expansion2 = "2"
    Expansion3 = "3"
    Expansion4 = "4"
    Expansion5 = "5"
    Expansion6 = "6"
    Expansion7 = "col"


class Icm(Enum):
    icm1 = "A+"
    icm2 = "A"
    icm3 = "B+"
    icm4 = "B"
    icm5 = "B-"
    icm6 = "C+"
    icm7 = "C"
    icm8 = "C-"
    icm9 = "D"


class Te(Enum):
    te1 = "A+"
    te2 = "A"
    te3 = "A-"
    te4 = "B+"
    te5 = "B"
    te6 = "B-"
    te7 = "C+"
    te8 = "C"
    te9 = "C-"
    te10 = "D"


class MedicalDecision(Enum):
    PENDING = "PENDING"
    FROZEN = "FROZEN"
    THAWED = "THAWED"
    TRANSFERRED = "TRANSFERRED"
    DISCARDED = "DISCARDED"


class PgtaValues(Enum):
    EUPLOID_D3 = "euploid_d3"
    EUPLOID_D5 = "euploid_d5"
    EUPLOID_D6 = "euploid_d6"
    ANEUPLOID_D3 = "aneuploid_d3"
    ANEUPLOID_D5 = "aneuploid_d5"
    ANEUPLOID_D6 = "aneuploid_d6"
    MOSAIC_D3 = "mosaic_d3"
    MOSAIC_D5 = "mosaic_d5"
    MOSAIC_D6 = "mosaic_d6"


class DateFilterDontExist(Exception):
    ...


class FilterByDate(Enum):
    LAST_MONTH = auto()
    LAST_3_MONTHS = auto()
    LAST_SIX_MONTHS = auto()
    LAST_YEAR = auto()


@pytest.fixture
def expected_instruments_data() -> Dict[str, List[InstrumentsDataResponse]]:
    return {
        "vitamin-c": [
            InstrumentsDataResponse(
                instrumentSize=6,
                esId="3169",
                tenantId="62f5048237468866998a3918",
                type="VITRO",
                createdAt="2022-08-11T14:04:27.256Z",
                updatedAt="2022-08-11T14:04:27.256Z",
                slides=[
                    Slide(
                        esId="D2018.11.06_S00013_I3169",
                        tenantId="62f5048237468866998a3918",
                        treatment=Treatment(oocyteAge=None, patientAge=255),
                        patientId="62f50c6b46418c12e1c27b4e",
                        wellsCount=10,
                        hoursSinceFertilization=38.58,
                        slidePosition=1,
                        fertilizationDate="2018-11-05T15:45:00.000Z",
                        isEarlyStage=False,
                        slideDescriptionId=None,
                        bestWells=[
                            BestWell(
                                class_="well",
                                tenantId="62f5048237468866998a3918",
                                patientId="62f50c6b46418c12e1c27b4e",
                                treatmentId="62f50c6b46418c9652c27b4f",
                                esId="5",
                                slideId="62f50c6b46418c7ea7c27b51",
                                lastImageUrl="https://storage.googleapis.com/kidplus-bucket-dev/62f50c6edf280880c6c072f0_5/images/0/138918508.jpeg",
                                baseImageUrl="https://storage.googleapis.com/kidplus-bucket-dev/62f50c6edf280880c6c072f0_5/",
                                inferenceData=InferenceData1(
                                    blastScore=0.94920427,
                                    kidScore=0.87964123,
                                    morphokinetics=[
                                        Morphokinetic1(
                                            systemName="tPNa", value=16.654502868652344
                                        ),
                                        Morphokinetic1(
                                            systemName="tPNf", value=19.729129791259766
                                        ),
                                        Morphokinetic1(
                                            systemName="t2", value=21.94773292541504
                                        ),
                                        Morphokinetic1(
                                            systemName="t3", value=33.676002502441406
                                        ),
                                        Morphokinetic1(
                                            systemName="t4", value=34.7852897644043
                                        ),
                                    ],
                                ),
                            ),
                            BestWell(
                                class_="well",
                                tenantId="62f5048237468866998a3918",
                                patientId="62f50c6b46418c12e1c27b4e",
                                treatmentId="62f50c6b46418c9652c27b4f",
                                esId="1",
                                slideId="62f50c6b46418c7ea7c27b51",
                                lastImageUrl="https://storage.googleapis.com/kidplus-bucket-dev/62f50c6edf280880c6c072f0_1/images/0/138903507.jpeg",
                                baseImageUrl="https://storage.googleapis.com/kidplus-bucket-dev/62f50c6edf280880c6c072f0_1/",
                                inferenceData=InferenceData1(
                                    blastScore=0.9863208,
                                    kidScore=0.81681645,
                                    morphokinetics=[
                                        Morphokinetic1(
                                            systemName="tPNa", value=16.650331497192383
                                        ),
                                        Morphokinetic1(
                                            systemName="tPNf", value=23.845230102539062
                                        ),
                                        Morphokinetic1(
                                            systemName="t2", value=26.382286071777344
                                        ),
                                        Morphokinetic1(
                                            systemName="t3", value=37.79195022583008
                                        ),
                                        Morphokinetic1(
                                            systemName="t4", value=38.10890197753906
                                        ),
                                    ],
                                ),
                            ),
                            BestWell(
                                class_="well",
                                tenantId="62f5048237468866998a3918",
                                patientId="62f50c6b46418c12e1c27b4e",
                                treatmentId="62f50c6b46418c9652c27b4f",
                                esId="4",
                                slideId="62f50c6b46418c7ea7c27b51",
                                lastImageUrl="https://storage.googleapis.com/kidplus-bucket-dev/62f50c6edf280880c6c072f0_4/images/0/138914758.jpeg",
                                baseImageUrl="https://storage.googleapis.com/kidplus-bucket-dev/62f50c6edf280880c6c072f0_4/",
                                inferenceData=InferenceData1(
                                    blastScore=0.9941812,
                                    kidScore=0.7922907,
                                    morphokinetics=[
                                        Morphokinetic1(
                                            systemName="tPNa", value=16.653451919555664
                                        ),
                                        Morphokinetic1(
                                            systemName="tPNf", value=22.26365089416504
                                        ),
                                        Morphokinetic1(
                                            systemName="t2", value=25.117652893066406
                                        ),
                                        Morphokinetic1(
                                            systemName="t3", value=36.051963806152344
                                        ),
                                        Morphokinetic1(
                                            systemName="t4", value=36.21043014526367
                                        ),
                                    ],
                                ),
                            ),
                        ],
                        earlyStageData=None,
                    )
                ],
                class_="instrument",
            ),
            InstrumentsDataResponse(
                instrumentSize=6,
                esId="3235",
                tenantId="62f5048237468866998a3918",
                type="VITRO",
                createdAt="2022-08-11T14:04:28.190Z",
                updatedAt="2022-08-11T14:04:28.190Z",
                slides=[
                    Slide(
                        esId="D2019.12.03_S00058_I3235",
                        tenantId="62f5048237468866998a3918",
                        treatment=Treatment(oocyteAge=None, patientAge=268),
                        patientId="62f50c6c46418c8626c27b56",
                        wellsCount=16,
                        hoursSinceFertilization=10.94,
                        slidePosition=1,
                        fertilizationDate="2019-12-03T12:20:00.000Z",
                        isEarlyStage=True,
                        slideDescriptionId=None,
                        bestWells=None,
                        earlyStageData=EarlyStageData(
                            wellsInPNa=6, wellsInPNf=0, wellsIn2C=0
                        ),
                    ),
                    Slide(
                        esId="D2019.12.03_S00059_I3235",
                        tenantId="62f5048237468866998a3918",
                        treatment=Treatment(oocyteAge=None, patientAge=268),
                        patientId="62f50c6c46418c238ac27b5a",
                        wellsCount=16,
                        hoursSinceFertilization=14.45,
                        slidePosition=2,
                        fertilizationDate="2019-12-03T12:20:00.000Z",
                        isEarlyStage=True,
                        slideDescriptionId=None,
                        bestWells=None,
                        earlyStageData=EarlyStageData(
                            wellsInPNa=6, wellsInPNf=0, wellsIn2C=0
                        ),
                    ),
                    Slide(
                        esId="D2019.12.03_S00060_I3235",
                        tenantId="62f5048237468866998a3918",
                        treatment=Treatment(oocyteAge=None, patientAge=268),
                        patientId="62f50c6d46418cc3e4c27b5d",
                        wellsCount=16,
                        hoursSinceFertilization=23.19,
                        slidePosition=3,
                        fertilizationDate="2019-12-03T12:20:00.000Z",
                        isEarlyStage=True,
                        slideDescriptionId=None,
                        bestWells=None,
                        earlyStageData=EarlyStageData(
                            wellsInPNa=9, wellsInPNf=6, wellsIn2C=0
                        ),
                    ),
                ],
                class_="instrument",
            ),
            InstrumentsDataResponse(
                instrumentSize=6,
                esId="469",
                tenantId="62f5048237468866998a3918",
                type="VITRO",
                createdAt="2022-08-11T14:04:27.675Z",
                updatedAt="2022-08-11T14:04:27.675Z",
                slides=[],
                class_="instrument",
            ),
        ],
        "geri_Seinfeld": [
            InstrumentsDataResponse(
                instrumentSize=6,
                name="amit",
                esId="GERI00042",
                tenantId="62f25fdcd02512e1d4ec6b6b",
                type="GERI",
                createdAt="2022-08-30T08:58:08.518Z",
                updatedAt="2022-08-30T08:58:08.518Z",
                slides=[],
                class_="instrument",
            ),
            InstrumentsDataResponse(
                instrumentSize=6,
                name="amit",
                esId="GERI00430",
                tenantId="62f25fdcd02512e1d4ec6b6b",
                type="GERI",
                createdAt="2022-08-30T08:58:07.747Z",
                updatedAt="2022-08-30T08:58:07.747Z",
                slides=[
                    Slide(
                        esId="045dd470-25e5-11ed-a8c6-c400ad4629c0",
                        tenantId="62f25fdcd02512e1d4ec6b6b",
                        treatment=Treatment(oocyteAge=456, patientAge=511),
                        patientId="630dd12144d91c154b1fa592",
                        wellsCount=4,
                        hoursSinceFertilization=22.77,
                        slidePosition=6,
                        fertilizationDate="2022-08-29T11:10:00.000Z",
                        isEarlyStage=True,
                        slideDescriptionId=None,
                        bestWells=None,
                        earlyStageData=EarlyStageData(
                            wellsInPNa=4, wellsInPNf=0, wellsIn2C=0
                        ),
                    ),
                    Slide(
                        esId="eac82900-2460-11ed-8870-c400ad4629c0",
                        tenantId="62f25fdcd02512e1d4ec6b6b",
                        treatment=Treatment(oocyteAge=504, patientAge=511),
                        patientId="630dd11f44d91c113b1fa588",
                        wellsCount=12,
                        hoursSinceFertilization=98.9,
                        slidePosition=1,
                        fertilizationDate="2022-08-26T11:05:00.000Z",
                        isEarlyStage=False,
                        slideDescriptionId=None,
                        bestWells=[
                            BestWell(
                                class_="well",
                                tenantId="62f25fdcd02512e1d4ec6b6b",
                                patientId="630dd11f44d91c113b1fa588",
                                treatmentId="630dd11f44d91ca8fd1fa589",
                                esId="6",
                                slideId="630dd11f44d91c93291fa58c",
                                lastImageUrl="https://storage.googleapis.com/kidplus-bucket-dev/630dd121b7e1c515087c3fe5_6%2Fimages%2F0%2F356041000.png",
                                baseImageUrl="https://storage.googleapis.com/kidplus-bucket-dev/630dd121b7e1c515087c3fe5_6/",
                                inferenceData=InferenceData1(
                                    blastScore=1.0,
                                    kidScore=0.9963223,
                                    morphokinetics=[
                                        Morphokinetic1(
                                            systemName="tPNa", value=6.600277900695801
                                        ),
                                        Morphokinetic1(
                                            systemName="tPNf", value=23.43027687072754
                                        ),
                                        Morphokinetic1(
                                            systemName="t2", value=26.400278091430664
                                        ),
                                        Morphokinetic1(
                                            systemName="t3", value=36.96027755737305
                                        ),
                                        Morphokinetic1(
                                            systemName="t4", value=37.290279388427734
                                        ),
                                        Morphokinetic1(
                                            systemName="t5", value=49.1702766418457
                                        ),
                                        Morphokinetic1(
                                            systemName="t6", value=50.49027633666992
                                        ),
                                        Morphokinetic1(
                                            systemName="t7", value=51.81027603149414
                                        ),
                                        Morphokinetic1(
                                            systemName="t8", value=52.14027786254883
                                        ),
                                        Morphokinetic1(
                                            systemName="t9+", value=69.96028137207031
                                        ),
                                        Morphokinetic1(
                                            systemName="tM", value=78.90027618408203
                                        ),
                                        Morphokinetic1(
                                            systemName="tSB", value=95.23361206054688
                                        ),
                                    ],
                                ),
                            ),
                            BestWell(
                                class_="well",
                                tenantId="62f25fdcd02512e1d4ec6b6b",
                                patientId="630dd11f44d91c113b1fa588",
                                treatmentId="630dd11f44d91ca8fd1fa589",
                                esId="8",
                                slideId="630dd11f44d91c93291fa58c",
                                lastImageUrl="https://storage.googleapis.com/kidplus-bucket-dev/630dd121b7e1c515087c3fe5_8%2Fimages%2F0%2F356041000.png",
                                baseImageUrl="https://storage.googleapis.com/kidplus-bucket-dev/630dd121b7e1c515087c3fe5_8/",
                                inferenceData=InferenceData1(
                                    blastScore=1.0,
                                    kidScore=0.9901597,
                                    morphokinetics=[
                                        Morphokinetic1(
                                            systemName="tPNa", value=8.250277519226074
                                        ),
                                        Morphokinetic1(
                                            systemName="tPNf", value=23.100276947021484
                                        ),
                                        Morphokinetic1(
                                            systemName="t2", value=25.4102783203125
                                        ),
                                        Morphokinetic1(
                                            systemName="t3", value=36.30027770996094
                                        ),
                                        Morphokinetic1(
                                            systemName="t4", value=36.630279541015625
                                        ),
                                        Morphokinetic1(
                                            systemName="t5", value=38.940277099609375
                                        ),
                                        Morphokinetic1(
                                            systemName="t6", value=44.880279541015625
                                        ),
                                        Morphokinetic1(
                                            systemName="t7", value=46.530277252197266
                                        ),
                                        Morphokinetic1(
                                            systemName="t8", value=46.86027908325195
                                        ),
                                        Morphokinetic1(
                                            systemName="t9+", value=59.07027816772461
                                        ),
                                        Morphokinetic1(
                                            systemName="tM", value=82.90027618408203
                                        ),
                                        Morphokinetic1(
                                            systemName="tSB", value=94.56694793701172
                                        ),
                                        Morphokinetic1(
                                            systemName="tB", value=98.90027618408203
                                        ),
                                    ],
                                ),
                            ),
                            BestWell(
                                class_="well",
                                tenantId="62f25fdcd02512e1d4ec6b6b",
                                patientId="630dd11f44d91c113b1fa588",
                                treatmentId="630dd11f44d91ca8fd1fa589",
                                esId="12",
                                slideId="630dd11f44d91c93291fa58c",
                                lastImageUrl="https://storage.googleapis.com/kidplus-bucket-dev/630dd121b7e1c515087c3fe5_12%2Fimages%2F0%2F356041000.png",
                                baseImageUrl="https://storage.googleapis.com/kidplus-bucket-dev/630dd121b7e1c515087c3fe5_12/",
                                inferenceData=InferenceData1(
                                    blastScore=0.9934883,
                                    kidScore=0.89779395,
                                    morphokinetics=[
                                        Morphokinetic1(
                                            systemName="tPNa", value=7.9202775955200195
                                        ),
                                        Morphokinetic1(
                                            systemName="tPNf", value=25.740278244018555
                                        ),
                                        Morphokinetic1(
                                            systemName="t2", value=27.720277786254883
                                        ),
                                        Morphokinetic1(
                                            systemName="t3", value=42.24027633666992
                                        ),
                                        Morphokinetic1(
                                            systemName="t4", value=42.90027618408203
                                        ),
                                        Morphokinetic1(
                                            systemName="t5", value=55.440277099609375
                                        ),
                                        Morphokinetic1(
                                            systemName="t6", value=55.77027893066406
                                        ),
                                        Morphokinetic1(
                                            systemName="t7", value=56.100276947021484
                                        ),
                                        Morphokinetic1(
                                            systemName="t8", value=56.760276794433594
                                        ),
                                        Morphokinetic1(
                                            systemName="t9+", value=88.90027618408203
                                        ),
                                        Morphokinetic1(
                                            systemName="tM", value=98.90027618408203
                                        ),
                                    ],
                                ),
                            ),
                        ],
                        earlyStageData=None,
                    ),
                ],
                class_="instrument",
            ),
        ],
        "MiriAutomation": [
            InstrumentsDataResponse(
                instrumentSize=6,
                name="amit",
                esId="MTL-0017",
                tenantId="63f1e17661fb18f6c6572a93",
                type="MIRI",
                createdAt="2023-02-05T09:34:06.234Z",
                updatedAt="2023-02-05T09:34:06.234Z",
                slides=[
                    Slide(
                        esId="MTL-0017-18AC-B8E7",
                        tenantId="63f1e17661fb18f6c6572a93",
                        treatment=Treatment(oocyteAge=None, patientAge=637),
                        patientId="63e9f23aab7130815ae5534e",
                        wellsCount=10,
                        hoursSinceFertilization=49.33,
                        slidePosition=4,
                        fertilizationDate="2023-02-13T10:22:37.000Z",
                        isEarlyStage=False,
                        slideDescriptionId=None,
                        bestWells=[],
                        earlyStageData=None,
                    ),
                    Slide(
                        esId="MTL-0017-18AE-19A2",
                        tenantId="63f1e17661fb18f6c6572a93",
                        treatment=Treatment(
                            esId="3918", oocyteAge=None, patientAge=637
                        ),
                        patientId="63eb54d7ce0e74a2a88b2b4e",
                        wellsCount=14,
                        hoursSinceFertilization=24.33,
                        slidePosition=1,
                        fertilizationDate="2023-02-14T11:30:01.000Z",
                        isEarlyStage=True,
                        slideDescriptionId=None,
                        bestWells=None,
                        earlyStageData=EarlyStageData(
                            wellsInPNa=0, wellsInPNf=0, wellsIn2C=0
                        ),
                    ),
                ],
            ),
            InstrumentsDataResponse(
                instrumentSize=6,
                esId="MTL12-4708-1",
                tenantId="63f1e17661fb18f6c6572a93",
                type="MIRI",
                createdAt="2023-02-05T09:34:07.000Z",
                updatedAt="2023-02-05T09:34:07.000Z",
                slides=[],
            ),
            InstrumentsDataResponse(
                instrumentSize=6,
                esId="MTL12-4708-2",
                tenantId="63f1e17661fb18f6c6572a93",
                type="MIRI",
                createdAt="2023-02-05T09:34:08.070Z",
                updatedAt="2023-02-05T09:34:08.070Z",
                slides=[],
            ),
        ],
    }


@pytest.fixture
def expected_patients_data() -> Dict[str, List[PatientDataResponse]]:
    return {
        "vitamin-c": [
            PatientDataResponse(
                id="62f50c6b46418c12e1c27b4e",
                birthDate=None,
                tenantId="62f5048237468866998a3918",
                esId="ui-aut",
                status=None,
                fullName=None,
                bmi=None,
                govId=None,
                treatments=[
                    Treatment(
                        transfers=None,
                        oocyteAge=None,
                        id="62f50c6b46418c9652c27b4f",
                        startDate="2018-11-05T15:45:00.000Z",
                        endDate=None,
                        slides=[
                            PatientsSlideResponse(
                                id="62f50c6b46418c7ea7c27b51",
                                wells=[
                                    Well(esId="1", kidRank=2),
                                    Well(esId="2", kidRank=4),
                                    Well(esId="3", kidRank=5),
                                    Well(esId="4", kidRank=3),
                                    Well(esId="5", kidRank=1),
                                ],
                            )
                        ],
                    )
                ],
                createdAt="2022-08-11T14:04:27.167Z",
                updatedAt="2022-11-15T09:05:54.676Z",
            ),
            PatientDataResponse(
                id="62f50c6b46418c12e1c27b4e",
                birthDate=None,
                tenantId="62f5048237468866998a3918",
                esId="ui-aut-identifier",
                status=None,
                fullName=None,
                bmi=None,
                govId=None,
                treatments=[
                    Treatment(
                        transfers=None,
                        oocyteAge=None,
                        id="62f50c6b46418c9652c27b4f",
                        startDate="2018-11-05T15:45:00.000Z",
                        endDate=None,
                        slides=[
                            PatientsSlideResponse(
                                id="62f50c6b46418c7ea7c27b51",
                                wells=[
                                    Well(esId="1", kidRank=2),
                                    Well(esId="2", kidRank=4),
                                    Well(esId="3", kidRank=5),
                                    Well(esId="4", kidRank=3),
                                    Well(esId="5", kidRank=1),
                                ],
                            )
                        ],
                    )
                ],
                createdAt="2022-08-11T14:04:27.167Z",
                updatedAt="2022-11-15T09:05:54.676Z",
            ),
            PatientDataResponse(
                id="62f50c6b46418c12e1c27b4e",
                birthDate=None,
                tenantId="62f5048237468866998a3918",
                esId="ui-aut1",
                status=None,
                fullName=None,
                bmi=None,
                govId=None,
                treatments=[
                    Treatment(
                        transfers=None,
                        oocyteAge=None,
                        id="62f50c6b46418c9652c27b4f",
                        startDate="2018-11-05T15:45:00.000Z",
                        endDate=None,
                        slides=[
                            PatientsSlideResponse(
                                id="62f50c6b46418c7ea7c27b51",
                                wells=[
                                    Well(esId="1", kidRank=2),
                                    Well(esId="2", kidRank=4),
                                    Well(esId="3", kidRank=5),
                                    Well(esId="4", kidRank=3),
                                    Well(esId="5", kidRank=1),
                                ],
                            )
                        ],
                    )
                ],
                createdAt="2022-08-11T14:04:27.167Z",
                updatedAt="2022-11-15T09:05:54.676Z",
            ),
            PatientDataResponse(
                id="62f50c6b46418ca48dc27b52",
                birthDate=None,
                tenantId="62f5048237468866998a3918",
                esId="ui-aut2",
                status=None,
                fullName=None,
                bmi=None,
                govId=None,
                treatments=[
                    Treatment(
                        transfers=None,
                        oocyteAge=None,
                        id="62f50c6b46418ce727c27b53",
                        startDate="2019-01-11T13:39:59.999Z",
                        endDate=None,
                        slides=[],
                    )
                ],
                createdAt="2022-08-11T14:04:27.595Z",
                updatedAt="2022-11-15T09:05:54.940Z",
            ),
            PatientDataResponse(
                id="62f50c6c46418c8626c27b56",
                birthDate=None,
                tenantId="62f5048237468866998a3918",
                esId="ui-aut3",
                status=None,
                fullName=None,
                bmi=None,
                govId=None,
                treatments=[
                    Treatment(
                        transfers=None,
                        oocyteAge=None,
                        id="62f50c6c46418c078bc27b57",
                        startDate="2019-12-03T12:20:00.000Z",
                        endDate=None,
                        slides=[],
                    )
                ],
                createdAt="2022-08-11T14:04:28.105Z",
                updatedAt="2022-11-15T09:05:55.039Z",
            ),
            PatientDataResponse(
                id="62f50c6c46418c238ac27b5a",
                birthDate=None,
                tenantId="62f5048237468866998a3918",
                esId="ui-aut4",
                status=None,
                fullName=None,
                bmi=None,
                govId=None,
                treatments=[
                    Treatment(
                        transfers=None,
                        oocyteAge=None,
                        id="62f50c6c46418c47b3c27b5b",
                        startDate="2019-12-03T12:20:00.000Z",
                        endDate=None,
                        slides=[],
                    )
                ],
                createdAt="2022-08-11T14:04:28.499Z",
                updatedAt="2022-11-15T09:05:54.866Z",
            ),
            PatientDataResponse(
                id="62f50c6d46418cc3e4c27b5d",
                birthDate=None,
                tenantId="62f5048237468866998a3918",
                esId="ui-aut5",
                status=None,
                fullName=None,
                bmi=None,
                govId=None,
                treatments=[
                    Treatment(
                        transfers=None,
                        oocyteAge=None,
                        id="62f50c6d46418c0bb7c27b5e",
                        startDate="2019-12-03T12:20:00.000Z",
                        endDate=None,
                        slides=[],
                    )
                ],
                createdAt="2022-08-11T14:04:29.726Z",
                updatedAt="2022-11-15T09:05:54.796Z",
            ),
        ],
        "geri_Seinfeld": [
            PatientDataResponse(
                id="630dd12144d91c154b1fa592",
                birthDate=None,
                tenantId="62f25fdcd02512e1d4ec6b6b",
                esId="219474 blau",
                status=None,
                fullName=None,
                bmi=None,
                govId=None,
                treatments=[
                    Treatment(
                        transfers=None,
                        oocyteAge=OocyteAgeItem(
                            years=38,
                            months=0,
                            updatedAt="2022-08-30T08:58:09.199Z",
                            createdAt="2022-08-30T08:58:09.199Z",
                        ),
                        id="630dd12144d91c35441fa593",
                        startDate="2022-08-29T11:10:00.000Z",
                        endDate=None,
                        slides=[],
                    )
                ],
                createdAt="2022-08-30T08:58:09.199Z",
                updatedAt="2022-11-15T09:05:56.610Z",
            ),
            PatientDataResponse(
                id="630dd12044d91c5b211fa58d",
                birthDate=None,
                tenantId="62f25fdcd02512e1d4ec6b6b",
                esId="221282 morat",
                status=None,
                fullName=None,
                bmi=None,
                govId=None,
                treatments=[
                    Treatment(
                        transfers=None,
                        oocyteAge=OocyteAgeItem(
                            years=42,
                            months=0,
                            updatedAt="2022-08-30T08:58:08.447Z",
                            createdAt="2022-08-30T08:58:08.447Z",
                        ),
                        id="630dd12044d91ce5cd1fa58e",
                        startDate="2022-08-25T11:30:00.000Z",
                        endDate=None,
                        slides=[],
                    )
                ],
                createdAt="2022-08-30T08:58:08.447Z",
                updatedAt="2022-11-15T09:05:56.536Z",
            ),
            PatientDataResponse(
                id="630dffc644d91ccf151fd882",
                birthDate=None,
                tenantId="62f25fdcd02512e1d4ec6b6b",
                esId="221593 marron",
                status=None,
                fullName=None,
                bmi=None,
                govId=None,
                treatments=[
                    Treatment(
                        transfers=None,
                        oocyteAge=OocyteAgeItem(
                            years=42,
                            months=0,
                            updatedAt="2022-08-30T12:17:10.840Z",
                            createdAt="2022-08-30T12:17:10.840Z",
                        ),
                        id="630dffc644d91c17721fd883",
                        startDate="1970-01-01T00:00:00.000Z",
                        endDate=None,
                        slides=[],
                    )
                ],
                createdAt="2022-08-30T12:17:10.840Z",
                updatedAt="2022-11-15T09:05:57.312Z",
            ),
            PatientDataResponse(
                id="630dd11f44d91c113b1fa588",
                birthDate=None,
                tenantId="62f25fdcd02512e1d4ec6b6b",
                esId="222014 Roig",
                status=None,
                fullName=None,
                bmi=None,
                govId=None,
                treatments=[
                    Treatment(
                        transfers=None,
                        oocyteAge=OocyteAgeItem(
                            years=42,
                            months=0,
                            updatedAt="2022-08-30T08:58:07.614Z",
                            createdAt="2022-08-30T08:58:07.614Z",
                        ),
                        id="630dd11f44d91ca8fd1fa589",
                        startDate="2022-08-26T11:05:00.000Z",
                        endDate=None,
                        slides=[
                            PatientsSlideResponse(
                                id="630dd11f44d91c93291fa58c",
                                wells=[
                                    Well(esId="11", kidRank=4),
                                    Well(esId="12", kidRank=3),
                                    Well(esId="6", kidRank=1),
                                    Well(esId="8", kidRank=2),
                                ],
                            )
                        ],
                    )
                ],
                createdAt="2022-08-30T08:58:07.614Z",
                updatedAt="2022-11-15T09:05:56.462Z",
            ),
        ],
        "MiriAutomation": [
            PatientDataResponse(
                birthDate=None,
                tenantId="63f1e17661fb18f6c6572a93",
                esId="2644",
                status=None,
                fullName=None,
                bmi=None,
                govId=None,
                treatments=[
                    Treatment(
                        transfers=None,
                        oocyteAge=None,
                        startDate="2023-01-29T14:36:18.000Z",
                        endDate=None,
                        slides=[],
                        patientAge=None,
                    )
                ],
                createdAt="2023-02-05T09:34:06.003Z",
                updatedAt="2023-02-05T11:55:46.334Z",
            ),
            PatientDataResponse(
                birthDate=None,
                tenantId="63f1e17661fb18f6c6572a93",
                esId="2647",
                status=None,
                fullName=None,
                bmi=None,
                govId=None,
                treatments=[
                    Treatment(
                        transfers=None,
                        oocyteAge=None,
                        startDate="2023-02-02T14:22:47.000Z",
                        endDate=None,
                        slides=[],
                        patientAge=None,
                    )
                ],
                createdAt="2023-02-05T09:34:06.803Z",
                updatedAt="2023-02-05T09:39:24.968Z",
            ),
            PatientDataResponse(
                birthDate=None,
                tenantId="63f1e17661fb18f6c6572a93",
                esId="2648",
                status=None,
                fullName=None,
                bmi=None,
                govId=None,
                treatments=[
                    Treatment(
                        transfers=None,
                        oocyteAge=None,
                        startDate="2023-02-02T14:28:28.000Z",
                        endDate=None,
                        slides=[],
                        patientAge=None,
                    )
                ],
                createdAt="2023-02-05T09:34:07.383Z",
                updatedAt="2023-02-05T09:39:28.919Z",
            ),
            PatientDataResponse(
                birthDate=None,
                tenantId="63f1e17661fb18f6c6572a93",
                esId="2649",
                status=None,
                fullName=None,
                bmi=None,
                govId=None,
                treatments=[
                    Treatment(
                        transfers=None,
                        oocyteAge=None,
                        startDate="2023-02-02T14:30:39.000Z",
                        endDate=None,
                        slides=[],
                        patientAge=None,
                    )
                ],
                createdAt="2023-02-05T09:34:07.885Z",
                updatedAt="2023-02-05T09:39:21.083Z",
            ),
            PatientDataResponse(
                birthDate=None,
                tenantId="63f1e17661fb18f6c6572a93",
                esId="2650",
                status=None,
                fullName=None,
                bmi=None,
                govId=None,
                treatments=[
                    Treatment(
                        transfers=None,
                        oocyteAge=None,
                        startDate="2023-02-02T14:31:02.000Z",
                        endDate=None,
                        slides=[],
                        patientAge=None,
                    )
                ],
                createdAt="2023-02-05T09:34:08.410Z",
                updatedAt="2023-02-05T09:39:30.906Z",
            ),
            PatientDataResponse(
                birthDate=None,
                tenantId="63f1e17661fb18f6c6572a93",
                esId="2651",
                status=None,
                fullName=None,
                bmi=None,
                govId=None,
                treatments=[
                    Treatment(
                        transfers=None,
                        oocyteAge=None,
                        startDate="2023-02-02T14:36:41.000Z",
                        endDate=None,
                        slides=[],
                        patientAge=None,
                    )
                ],
                createdAt="2023-02-05T09:34:08.886Z",
                updatedAt="2023-02-05T09:39:54.955Z",
            ),
            PatientDataResponse(
                birthDate=None,
                tenantId="63f1e17661fb18f6c6572a93",
                esId="2652",
                status=None,
                fullName=None,
                bmi=None,
                govId=None,
                treatments=[
                    Treatment(
                        transfers=None,
                        oocyteAge=None,
                        startDate="2023-02-06T15:00:57.000Z",
                        endDate=None,
                        slides=[],
                        patientAge=None,
                    )
                ],
                createdAt="2023-02-06T13:06:05.478Z",
                updatedAt="2023-02-06T13:06:05.867Z",
            ),
            PatientDataResponse(
                birthDate=None,
                tenantId="63f1e17661fb18f6c6572a93",
                esId="2653",
                status=None,
                fullName=None,
                bmi=None,
                govId=None,
                treatments=[
                    Treatment(
                        transfers=None,
                        oocyteAge=None,
                        startDate="2023-02-06T15:01:56.000Z",
                        endDate=None,
                        slides=[],
                        patientAge=None,
                    )
                ],
                createdAt="2023-02-06T13:06:06.750Z",
                updatedAt="2023-02-06T13:06:06.827Z",
            ),
            PatientDataResponse(
                birthDate=None,
                tenantId="63f1e17661fb18f6c6572a93",
                esId="2654",
                status=None,
                fullName=None,
                bmi=None,
                govId=None,
                treatments=[
                    Treatment(
                        transfers=None,
                        oocyteAge=None,
                        startDate="2023-02-13T10:22:37.000Z",
                        endDate=None,
                        slides=[],
                        patientAge=None,
                    ),
                    Treatment(
                        transfers=None,
                        oocyteAge=None,
                        startDate="2023-02-13T10:16:52.000Z",
                        endDate=None,
                        slides=[],
                        patientAge=None,
                    ),
                ],
                createdAt="2023-02-13T08:18:02.389Z",
                updatedAt="2023-02-15T10:04:27.744Z",
            ),
            PatientDataResponse(
                birthDate=None,
                tenantId="63f1e17661fb18f6c6572a93",
                esId="2655",
                status=None,
                fullName=None,
                bmi=None,
                govId=None,
                treatments=[
                    Treatment(
                        transfers=None,
                        oocyteAge=None,
                        startDate="2023-02-13T10:30:05.000Z",
                        endDate=None,
                        slides=[],
                        patientAge=None,
                    )
                ],
                createdAt="2023-02-13T08:31:02.663Z",
                updatedAt="2023-02-13T08:31:02.760Z",
            ),
            PatientDataResponse(
                birthDate=None,
                tenantId="63f1e17661fb18f6c6572a93",
                esId="2656",
                status=None,
                fullName=None,
                bmi=None,
                govId=None,
                treatments=[
                    Treatment(
                        transfers=None,
                        oocyteAge=None,
                        startDate="2023-02-14T11:34:08.000Z",
                        endDate=None,
                        slides=[],
                        patientAge=None,
                    ),
                    Treatment(
                        transfers=None,
                        oocyteAge=None,
                        startDate="2023-02-14T11:30:01.000Z",
                        endDate=None,
                        slides=[],
                        patientAge=None,
                    ),
                ],
                createdAt="2023-02-14T09:31:03.103Z",
                updatedAt="2023-02-15T07:47:12.720Z",
            ),
        ],
    }


@pytest.fixture
def expected_emr_patients_data() -> Dict[str, List[PatientsEMRResponse]]:
    return {
        "vitamin-c": [
            PatientsEMRResponse(
                patientId="ui-aut1",
                treatments=[
                    EMRTreatment(name="1", startDate="2018-11-05T15:45:00.000Z")
                ],
            ),
            PatientsEMRResponse(
                patientId="ui-aut2",
                treatments=[
                    EMRTreatment(name="2", startDate="2019-01-11T13:39:59.999Z")
                ],
            ),
            PatientsEMRResponse(
                patientId="ui-aut3",
                treatments=[
                    EMRTreatment(name="3", startDate="2019-12-03T12:20:00.000Z")
                ],
            ),
            PatientsEMRResponse(
                patientId="ui-aut4",
                treatments=[
                    EMRTreatment(name="4", startDate="2019-12-03T12:20:00.000Z")
                ],
            ),
            PatientsEMRResponse(
                patientId="ui-aut5",
                treatments=[
                    EMRTreatment(name="5", startDate="2019-12-03T12:20:00.000Z")
                ],
            ),
        ],
        "geri_Seinfeld": [
            PatientsEMRResponse(
                patientId="219474 blau",
                treatments=[
                    EMRTreatment(
                        name="fiv 222327", startDate="2022-08-29T11:10:00.000Z"
                    )
                ],
            ),
            PatientsEMRResponse(
                patientId="221282 morat",
                treatments=[
                    EMRTreatment(
                        name="fiv 222323", startDate="2022-08-25T11:30:00.000Z"
                    )
                ],
            ),
            PatientsEMRResponse(
                patientId="221593 marron",
                treatments=[
                    EMRTreatment(
                        name="fiv 222329", startDate="1970-01-01T00:00:00.000Z"
                    )
                ],
            ),
            PatientsEMRResponse(
                patientId="222014 Roig",
                treatments=[
                    EMRTreatment(
                        name="fiv 222326", startDate="2022-08-26T11:05:00.000Z"
                    )
                ],
            ),
        ],
        "MiriAutomation": [
            PatientsEMRResponse(
                patientId="2644",
                treatments=[
                    EMRTreatment(name="3900", startDate="2023-01-29T14:36:18.000Z")
                ],
            ),
            PatientsEMRResponse(
                patientId="2647",
                treatments=[
                    EMRTreatment(name="3908", startDate="2023-02-02T14:22:47.000Z")
                ],
            ),
            PatientsEMRResponse(
                patientId="2648",
                treatments=[
                    EMRTreatment(name="3909", startDate="2023-02-02T14:28:28.000Z")
                ],
            ),
            PatientsEMRResponse(
                patientId="2649",
                treatments=[
                    EMRTreatment(name="3910", startDate="2023-02-02T14:30:39.000Z")
                ],
            ),
            PatientsEMRResponse(
                patientId="2650",
                treatments=[
                    EMRTreatment(name="3911", startDate="2023-02-02T14:31:02.000Z")
                ],
            ),
            PatientsEMRResponse(
                patientId="2651",
                treatments=[
                    EMRTreatment(name="3912", startDate="2023-02-02T14:36:41.000Z")
                ],
            ),
            PatientsEMRResponse(
                patientId="2652",
                treatments=[
                    EMRTreatment(name="3913", startDate="2023-02-06T15:00:57.000Z")
                ],
            ),
            PatientsEMRResponse(
                patientId="2653",
                treatments=[
                    EMRTreatment(name="3914", startDate="2023-02-06T15:01:56.000Z")
                ],
            ),
            PatientsEMRResponse(
                patientId="2654",
                treatments=[
                    EMRTreatment(name="3916", startDate="2023-02-13T10:22:37.000Z"),
                    EMRTreatment(name="3915", startDate="2023-02-13T10:16:52.000Z"),
                ],
            ),
            PatientsEMRResponse(
                patientId="2655",
                treatments=[
                    EMRTreatment(name="3917", startDate="2023-02-13T10:30:05.000Z")
                ],
            ),
            PatientsEMRResponse(
                patientId="2656",
                treatments=[
                    EMRTreatment(name="3920", startDate="2023-02-14T11:34:08.000Z"),
                    EMRTreatment(name="3918", startDate="2023-02-14T11:30:01.000Z"),
                ],
            ),
        ],
    }


@pytest.fixture
def expected_wells_data() -> Dict[str, List[EMRWellsResponse]]:
    return {
        "62f50c6b46418c9652c27b4f": [
            EMRWellsResponse(
                wellNumber="1",
                slideEsId="D2018.11.06_S00013_I3169",
                instrument=Instrument(type="VITRO"),
                status="GROWING",
                lastImageUrl="https://storage.googleapis.com/kidplus-bucket-dev/62f50c6edf280880c6c072f0_1/images/0/138903507.jpeg",
                inferenceData=InferenceData(
                    morphokinetics=[
                        Morphokinetic(event="tPNa", value=16.65),
                        Morphokinetic(event="tPNf", value=23.85),
                        Morphokinetic(event="t2", value=26.38),
                        Morphokinetic(event="t3", value=37.79),
                        Morphokinetic(event="t4", value=38.11),
                    ],
                    kidScore=0.81681645,
                    blastScore=0.9863208,
                    kidRank=2,
                    pnCount=2,
                    pnCountTime=22.102036666666667,
                    degeneratedOocyte=False,
                    duc1=False,
                    fragmentationTime=0,
                ),
                inferenceOverridingData={},
            ),
            EMRWellsResponse(
                wellNumber="10",
                slideEsId="D2018.11.06_S00013_I3169",
                instrument=Instrument(type="VITRO"),
                status="GROWING",
                lastImageUrl="https://storage.googleapis.com/kidplus-bucket-dev/62f50c6edf280880c6c072f0_10/images/0/138937227.jpeg",
                inferenceData=InferenceData(
                    morphokinetics=[],
                    kidScore=None,
                    blastScore=None,
                    kidRank=None,
                    pnCount=0,
                    pnCountTime=27.659363611111115,
                    degeneratedOocyte=False,
                    duc1=False,
                    fragmentationTime=0,
                ),
                inferenceOverridingData={},
            ),
            EMRWellsResponse(
                wellNumber="2",
                slideEsId="D2018.11.06_S00013_I3169",
                instrument=Instrument(type="VITRO"),
                status="GROWING",
                lastImageUrl="https://storage.googleapis.com/kidplus-bucket-dev/62f50c6edf280880c6c072f0_2/images/0/138907210.jpeg",
                inferenceData=InferenceData(
                    morphokinetics=[
                        Morphokinetic(event="tPNa", value=16.65),
                        Morphokinetic(event="tPNf", value=18.93),
                        Morphokinetic(event="t2", value=21.63),
                        Morphokinetic(event="t3", value=32.41),
                        Morphokinetic(event="t4", value=34.15),
                        Morphokinetic(event="t5", value=34.31),
                    ],
                    kidScore=0.76508254,
                    blastScore=0.9887322,
                    kidRank=4,
                    pnCount=2,
                    pnCountTime=17.8243525,
                    degeneratedOocyte=False,
                    duc1=False,
                    fragmentationTime=0,
                ),
                inferenceOverridingData={},
            ),
            EMRWellsResponse(
                wellNumber="3",
                slideEsId="D2018.11.06_S00013_I3169",
                instrument=Instrument(type="VITRO"),
                status="GROWING",
                lastImageUrl="https://storage.googleapis.com/kidplus-bucket-dev/62f50c6edf280880c6c072f0_3/images/0/138911024.jpeg",
                inferenceData=InferenceData(
                    morphokinetics=[
                        Morphokinetic(event="tPNa", value=16.65),
                        Morphokinetic(event="tPNf", value=19.89),
                        Morphokinetic(event="t2", value=22.42),
                        Morphokinetic(event="t3", value=35.1),
                        Morphokinetic(event="t4", value=35.26),
                    ],
                    kidScore=0.7426436,
                    blastScore=0.9663864,
                    kidRank=5,
                    pnCount=2,
                    pnCountTime=18.142370833333334,
                    degeneratedOocyte=False,
                    duc1=False,
                    fragmentationTime=0,
                ),
                inferenceOverridingData={},
            ),
            EMRWellsResponse(
                wellNumber="4",
                slideEsId="D2018.11.06_S00013_I3169",
                instrument=Instrument(type="VITRO"),
                status="GROWING",
                lastImageUrl="https://storage.googleapis.com/kidplus-bucket-dev/62f50c6edf280880c6c072f0_4/images/0/138914758.jpeg",
                inferenceData=InferenceData(
                    morphokinetics=[
                        Morphokinetic(event="tPNa", value=16.65),
                        Morphokinetic(event="tPNf", value=22.26),
                        Morphokinetic(event="t2", value=25.12),
                        Morphokinetic(event="t3", value=36.05),
                        Morphokinetic(event="t4", value=36.21),
                    ],
                    kidScore=0.7922907,
                    blastScore=0.9941812,
                    kidRank=3,
                    pnCount=2,
                    pnCountTime=19.569598055555556,
                    degeneratedOocyte=False,
                    duc1=False,
                    fragmentationTime=0,
                ),
                inferenceOverridingData={},
            ),
            EMRWellsResponse(
                wellNumber="5",
                slideEsId="D2018.11.06_S00013_I3169",
                instrument=Instrument(type="VITRO"),
                status="GROWING",
                lastImageUrl="https://storage.googleapis.com/kidplus-bucket-dev/62f50c6edf280880c6c072f0_5/images/0/138918508.jpeg",
                inferenceData=InferenceData(
                    morphokinetics=[
                        Morphokinetic(event="tPNa", value=16.65),
                        Morphokinetic(event="tPNf", value=19.73),
                        Morphokinetic(event="t2", value=21.95),
                        Morphokinetic(event="t3", value=33.68),
                        Morphokinetic(event="t4", value=34.79),
                    ],
                    kidScore=0.87964123,
                    blastScore=0.94920427,
                    kidRank=1,
                    pnCount=2,
                    pnCountTime=18.144449444444444,
                    degeneratedOocyte=False,
                    duc1=False,
                    fragmentationTime=0,
                ),
                inferenceOverridingData={},
            ),
            EMRWellsResponse(
                wellNumber="6",
                slideEsId="D2018.11.06_S00013_I3169",
                instrument=Instrument(type="VITRO"),
                status="GROWING",
                lastImageUrl="https://storage.googleapis.com/kidplus-bucket-dev/62f50c6edf280880c6c072f0_6/images/0/138922290.jpeg",
                inferenceData=InferenceData(
                    morphokinetics=[
                        Morphokinetic(event="tPNf", value=22.58),
                        Morphokinetic(event="t2", value=37.96),
                        Morphokinetic(event="t3", value=38.11),
                    ],
                    kidScore=None,
                    blastScore=None,
                    kidRank=None,
                    pnCount=2,
                    pnCountTime=19.571694722222222,
                    degeneratedOocyte=False,
                    duc1=True,
                    fragmentationTime=0,
                ),
                inferenceOverridingData={},
            ),
            EMRWellsResponse(
                wellNumber="7",
                slideEsId="D2018.11.06_S00013_I3169",
                instrument=Instrument(type="VITRO"),
                status="GROWING",
                lastImageUrl="https://storage.googleapis.com/kidplus-bucket-dev/62f50c6edf280880c6c072f0_7/images/0/138926056.jpeg",
                inferenceData=InferenceData(
                    morphokinetics=[
                        Morphokinetic(event="tPNf", value=21.0),
                        Morphokinetic(event="t2", value=36.06),
                        Morphokinetic(event="t3", value=36.37),
                    ],
                    kidScore=None,
                    blastScore=None,
                    kidRank=None,
                    pnCount=1,
                    pnCountTime=18.780403333333332,
                    degeneratedOocyte=False,
                    duc1=True,
                    fragmentationTime=0,
                ),
                inferenceOverridingData={},
            ),
            EMRWellsResponse(
                wellNumber="8",
                slideEsId="D2018.11.06_S00013_I3169",
                instrument=Instrument(type="VITRO"),
                status="GROWING",
                lastImageUrl="https://storage.googleapis.com/kidplus-bucket-dev/62f50c6edf280880c6c072f0_8/images/0/138929790.jpeg",
                inferenceData=InferenceData(
                    morphokinetics=[
                        Morphokinetic(event="tPNf", value=21.0),
                        Morphokinetic(event="t2", value=23.38),
                        Morphokinetic(event="t3", value=24.01),
                    ],
                    kidScore=None,
                    blastScore=None,
                    kidRank=None,
                    pnCount=1,
                    pnCountTime=18.78144916666667,
                    degeneratedOocyte=False,
                    duc1=True,
                    fragmentationTime=0,
                ),
                inferenceOverridingData={},
            ),
            EMRWellsResponse(
                wellNumber="9",
                slideEsId="D2018.11.06_S00013_I3169",
                instrument=Instrument(type="VITRO"),
                status="GROWING",
                lastImageUrl="https://storage.googleapis.com/kidplus-bucket-dev/62f50c6edf280880c6c072f0_9/images/0/138933493.jpeg",
                inferenceData=InferenceData(
                    morphokinetics=[
                        Morphokinetic(event="tPNf", value=25.28),
                        Morphokinetic(event="t2", value=27.82),
                        Morphokinetic(event="t3", value=29.56),
                    ],
                    kidScore=None,
                    blastScore=None,
                    kidRank=None,
                    pnCount=2,
                    pnCountTime=20.3671675,
                    degeneratedOocyte=False,
                    duc1=True,
                    fragmentationTime=0,
                ),
                inferenceOverridingData={},
            ),
        ],
        "62f50c6b46418ce727c27b53": [
            EMRWellsResponse(
                wellNumber="1",
                slideEsId="D2019.01.12_S01225_I0469",
                instrument=Instrument(type="VITRO"),
                status="GROWING",
                lastImageUrl="https://storage.googleapis.com/kidplus-bucket-dev/62f50c6edf2808cd35c072f3_1/images/0/93908940.jpeg",
                inferenceData=InferenceData(
                    morphokinetics=[
                        Morphokinetic(event="tPNf", value=22.08),
                        Morphokinetic(event="t2", value=24.92),
                    ],
                    kidScore=None,
                    blastScore=None,
                    kidRank=None,
                    pnCount=2,
                    pnCountTime=20.74549,
                    degeneratedOocyte=False,
                    duc1=False,
                    fragmentationTime=0,
                ),
                inferenceOverridingData={},
            ),
            EMRWellsResponse(
                wellNumber="2",
                slideEsId="D2019.01.12_S01225_I0469",
                instrument=Instrument(type="VITRO"),
                status="GROWING",
                lastImageUrl="https://storage.googleapis.com/kidplus-bucket-dev/62f50c6edf2808cd35c072f3_2/images/0/93917506.jpeg",
                inferenceData=InferenceData(
                    morphokinetics=[Morphokinetic(event="tPNf", value=25.75)],
                    kidScore=None,
                    blastScore=None,
                    kidRank=None,
                    pnCount=2,
                    pnCountTime=20.06895527777778,
                    degeneratedOocyte=False,
                    duc1=False,
                    fragmentationTime=0,
                ),
                inferenceOverridingData={},
            ),
            EMRWellsResponse(
                wellNumber="3",
                slideEsId="D2019.01.12_S01225_I0469",
                instrument=Instrument(type="VITRO"),
                status="GROWING",
                lastImageUrl="https://storage.googleapis.com/kidplus-bucket-dev/62f50c6edf2808cd35c072f3_3/images/0/93926120.jpeg",
                inferenceData=InferenceData(
                    morphokinetics=[Morphokinetic(event="tPNf", value=25.59)],
                    kidScore=None,
                    blastScore=None,
                    kidRank=None,
                    pnCount=2,
                    pnCountTime=22.5887475,
                    degeneratedOocyte=False,
                    duc1=False,
                    fragmentationTime=0,
                ),
                inferenceOverridingData={},
            ),
            EMRWellsResponse(
                wellNumber="4",
                slideEsId="D2019.01.12_S01225_I0469",
                instrument=Instrument(type="VITRO"),
                status="GROWING",
                lastImageUrl="https://storage.googleapis.com/kidplus-bucket-dev/62f50c6edf2808cd35c072f3_4/images/0/93934725.jpeg",
                inferenceData=InferenceData(
                    morphokinetics=[
                        Morphokinetic(event="tPNa", value=19.57),
                        Morphokinetic(event="tPNf", value=22.76),
                        Morphokinetic(event="t2", value=25.59),
                    ],
                    kidScore=None,
                    blastScore=None,
                    kidRank=None,
                    pnCount=2,
                    pnCountTime=20.075889722222218,
                    degeneratedOocyte=False,
                    duc1=False,
                    fragmentationTime=0,
                ),
                inferenceOverridingData={},
            ),
            EMRWellsResponse(
                wellNumber="5",
                slideEsId="D2019.01.12_S01225_I0469",
                instrument=Instrument(type="VITRO"),
                status="GROWING",
                lastImageUrl="https://storage.googleapis.com/kidplus-bucket-dev/62f50c6edf2808cd35c072f3_5/images/0/93943840.jpeg",
                inferenceData=InferenceData(
                    morphokinetics=[
                        Morphokinetic(event="tPNa", value=19.57),
                        Morphokinetic(event="tPNf", value=25.1),
                    ],
                    kidScore=None,
                    blastScore=None,
                    kidRank=None,
                    pnCount=2,
                    pnCountTime=22.260013055555557,
                    degeneratedOocyte=False,
                    duc1=False,
                    fragmentationTime=0,
                ),
                inferenceOverridingData={},
            ),
            EMRWellsResponse(
                wellNumber="6",
                slideEsId="D2019.01.12_S01225_I0469",
                instrument=Instrument(type="VITRO"),
                status="GROWING",
                lastImageUrl="https://storage.googleapis.com/kidplus-bucket-dev/62f50c6edf2808cd35c072f3_6/images/0/93952394.jpeg",
                inferenceData=InferenceData(
                    morphokinetics=[
                        Morphokinetic(event="tPNf", value=22.1),
                        Morphokinetic(event="t2", value=24.93),
                    ],
                    kidScore=None,
                    blastScore=None,
                    kidRank=None,
                    pnCount=2,
                    pnCountTime=20.24752722222222,
                    degeneratedOocyte=False,
                    duc1=False,
                    fragmentationTime=0,
                ),
                inferenceOverridingData={},
            ),
            EMRWellsResponse(
                wellNumber="7",
                slideEsId="D2019.01.12_S01225_I0469",
                instrument=Instrument(type="VITRO"),
                status="GROWING",
                lastImageUrl="https://storage.googleapis.com/kidplus-bucket-dev/62f50c6edf2808cd35c072f3_7/images/0/93961006.jpeg",
                inferenceData=InferenceData(
                    morphokinetics=[Morphokinetic(event="tPNf", value=23.27)],
                    kidScore=None,
                    blastScore=None,
                    kidRank=None,
                    pnCount=2,
                    pnCountTime=21.43116111111111,
                    degeneratedOocyte=False,
                    duc1=False,
                    fragmentationTime=0,
                ),
                inferenceOverridingData={},
            ),
            EMRWellsResponse(
                wellNumber="8",
                slideEsId="D2019.01.12_S01225_I0469",
                instrument=Instrument(type="VITRO"),
                status="GROWING",
                lastImageUrl="https://storage.googleapis.com/kidplus-bucket-dev/62f50c6edf2808cd35c072f3_8/images/0/93977400.jpeg",
                inferenceData=InferenceData(
                    morphokinetics=[
                        Morphokinetic(event="tPNf", value=21.6),
                        Morphokinetic(event="t2", value=24.1),
                    ],
                    kidScore=None,
                    blastScore=None,
                    kidRank=None,
                    pnCount=2,
                    pnCountTime=20.25232527777778,
                    degeneratedOocyte=False,
                    duc1=False,
                    fragmentationTime=0,
                ),
                inferenceOverridingData={},
            ),
        ],
        "630dd12144d91c35441fa593": [
            EMRWellsResponse(
                wellNumber="1",
                slideEsId="045dd470-25e5-11ed-a8c6-c400ad4629c0",
                instrument=Instrument(type="GERI"),
                status="GROWING",
                lastImageUrl="https://storage.googleapis.com/kidplus-bucket-dev/630dd121b7e1c54aa77c3fed_1%2Fimages%2F0%2F81973000.png",
                inferenceData=InferenceData(
                    morphokinetics=[],
                    kidScore=None,
                    blastScore=None,
                    kidRank=None,
                    pnCount=0,
                    pnCountTime=11.550277777777778,
                    degeneratedOocyte=False,
                    duc1=False,
                    fragmentationTime=0,
                ),
                inferenceOverridingData={},
            ),
            EMRWellsResponse(
                wellNumber="2",
                slideEsId="045dd470-25e5-11ed-a8c6-c400ad4629c0",
                instrument=Instrument(type="GERI"),
                status="GROWING",
                lastImageUrl="https://storage.googleapis.com/kidplus-bucket-dev/630dd121b7e1c54aa77c3fed_2%2Fimages%2F0%2F81973000.png",
                inferenceData=InferenceData(
                    morphokinetics=[Morphokinetic(event="tPNa", value=6.93)],
                    kidScore=None,
                    blastScore=None,
                    kidRank=None,
                    pnCount=1,
                    pnCountTime=17.160277777777775,
                    degeneratedOocyte=False,
                    duc1=False,
                    fragmentationTime=0,
                ),
                inferenceOverridingData={},
            ),
            EMRWellsResponse(
                wellNumber="3",
                slideEsId="045dd470-25e5-11ed-a8c6-c400ad4629c0",
                instrument=Instrument(type="GERI"),
                status="GROWING",
                lastImageUrl="https://storage.googleapis.com/kidplus-bucket-dev/630dd121b7e1c54aa77c3fed_3%2Fimages%2F0%2F81973000.png",
                inferenceData=InferenceData(
                    morphokinetics=[Morphokinetic(event="tPNa", value=11.88)],
                    kidScore=None,
                    blastScore=None,
                    kidRank=None,
                    pnCount=1,
                    pnCountTime=20.130277777777778,
                    degeneratedOocyte=False,
                    duc1=False,
                    fragmentationTime=0,
                ),
                inferenceOverridingData={},
            ),
            EMRWellsResponse(
                wellNumber="4",
                slideEsId="045dd470-25e5-11ed-a8c6-c400ad4629c0",
                instrument=Instrument(type="GERI"),
                status="GROWING",
                lastImageUrl="https://storage.googleapis.com/kidplus-bucket-dev/630dd121b7e1c54aa77c3fed_4%2Fimages%2F0%2F81973000.png",
                inferenceData=InferenceData(
                    morphokinetics=[Morphokinetic(event="tPNa", value=11.22)],
                    kidScore=None,
                    blastScore=None,
                    kidRank=None,
                    pnCount=2,
                    pnCountTime=20.790277777777778,
                    degeneratedOocyte=False,
                    duc1=False,
                    fragmentationTime=0,
                ),
                inferenceOverridingData={},
            ),
        ],
        "630dd12044d91ce5cd1fa58e": [
            EMRWellsResponse(
                wellNumber="1",
                slideEsId="60c100de-23a9-11ed-8fe5-408d5c16071c",
                instrument=Instrument(type="GERI"),
                status="GROWING",
                lastImageUrl="https://storage.googleapis.com/kidplus-bucket-dev/630dd121b7e1c52aee7c3fe9_1%2Fimages%2F0%2F81973000.png",
                inferenceData=InferenceData(
                    morphokinetics=[],
                    kidScore=None,
                    blastScore=None,
                    kidRank=None,
                    pnCount=0,
                    pnCountTime=11.550277777777778,
                    degeneratedOocyte=False,
                    duc1=False,
                    fragmentationTime=0,
                ),
                inferenceOverridingData={},
            ),
            EMRWellsResponse(
                wellNumber="2",
                slideEsId="60c100de-23a9-11ed-8fe5-408d5c16071c",
                instrument=Instrument(type="GERI"),
                status="GROWING",
                lastImageUrl="https://storage.googleapis.com/kidplus-bucket-dev/630dd121b7e1c52aee7c3fe9_2%2Fimages%2F0%2F81973000.png",
                inferenceData=InferenceData(
                    morphokinetics=[],
                    kidScore=None,
                    blastScore=None,
                    kidRank=None,
                    pnCount=0,
                    pnCountTime=11.550277777777778,
                    degeneratedOocyte=False,
                    duc1=False,
                    fragmentationTime=0,
                ),
                inferenceOverridingData={},
            ),
            EMRWellsResponse(
                wellNumber="3",
                slideEsId="60c100de-23a9-11ed-8fe5-408d5c16071c",
                instrument=Instrument(type="GERI"),
                status="GROWING",
                lastImageUrl="https://storage.googleapis.com/kidplus-bucket-dev/630dd121b7e1c52aee7c3fe9_3%2Fimages%2F0%2F81973000.png",
                inferenceData=InferenceData(
                    morphokinetics=[Morphokinetic(event="tPNa", value=9.24)],
                    kidScore=None,
                    blastScore=None,
                    kidRank=None,
                    pnCount=1,
                    pnCountTime=13.530277777777778,
                    degeneratedOocyte=False,
                    duc1=False,
                    fragmentationTime=0,
                ),
                inferenceOverridingData={},
            ),
            EMRWellsResponse(
                wellNumber="4",
                slideEsId="60c100de-23a9-11ed-8fe5-408d5c16071c",
                instrument=Instrument(type="GERI"),
                status="GROWING",
                lastImageUrl="https://storage.googleapis.com/kidplus-bucket-dev/630dd121b7e1c52aee7c3fe9_4%2Fimages%2F0%2F81973000.png",
                inferenceData=InferenceData(
                    morphokinetics=[],
                    kidScore=None,
                    blastScore=None,
                    kidRank=None,
                    pnCount=0,
                    pnCountTime=11.550277777777778,
                    degeneratedOocyte=False,
                    duc1=False,
                    fragmentationTime=0,
                ),
                inferenceOverridingData={},
            ),
            EMRWellsResponse(
                wellNumber="5",
                slideEsId="60c100de-23a9-11ed-8fe5-408d5c16071c",
                instrument=Instrument(type="GERI"),
                status="GROWING",
                lastImageUrl="https://storage.googleapis.com/kidplus-bucket-dev/630dd121b7e1c52aee7c3fe9_5%2Fimages%2F0%2F81973000.png",
                inferenceData=InferenceData(
                    morphokinetics=[],
                    kidScore=None,
                    blastScore=None,
                    kidRank=None,
                    pnCount=0,
                    pnCountTime=11.550277777777778,
                    degeneratedOocyte=False,
                    duc1=False,
                    fragmentationTime=0,
                ),
                inferenceOverridingData={},
            ),
            EMRWellsResponse(
                wellNumber="6",
                slideEsId="60c100de-23a9-11ed-8fe5-408d5c16071c",
                instrument=Instrument(type="GERI"),
                status="GROWING",
                lastImageUrl="https://storage.googleapis.com/kidplus-bucket-dev/630dd121b7e1c52aee7c3fe9_6%2Fimages%2F0%2F81973000.png",
                inferenceData=InferenceData(
                    morphokinetics=[Morphokinetic(event="tPNa", value=10.89)],
                    kidScore=None,
                    blastScore=None,
                    kidRank=None,
                    pnCount=2,
                    pnCountTime=20.130277777777778,
                    degeneratedOocyte=False,
                    duc1=False,
                    fragmentationTime=0,
                ),
                inferenceOverridingData={},
            ),
            EMRWellsResponse(
                wellNumber="7",
                slideEsId="60c100de-23a9-11ed-8fe5-408d5c16071c",
                instrument=Instrument(type="GERI"),
                status="GROWING",
                lastImageUrl="https://storage.googleapis.com/kidplus-bucket-dev/630dd121b7e1c52aee7c3fe9_7%2Fimages%2F0%2F81973000.png",
                inferenceData=InferenceData(
                    morphokinetics=[
                        Morphokinetic(event="tPNa", value=7.59),
                        Morphokinetic(event="tPNf", value=21.12),
                    ],
                    kidScore=None,
                    blastScore=None,
                    kidRank=None,
                    pnCount=2,
                    pnCountTime=16.50027777777778,
                    degeneratedOocyte=False,
                    duc1=False,
                    fragmentationTime=0,
                ),
                inferenceOverridingData={},
            ),
            EMRWellsResponse(
                wellNumber="8",
                slideEsId="60c100de-23a9-11ed-8fe5-408d5c16071c",
                instrument=Instrument(type="GERI"),
                status="GROWING",
                lastImageUrl="https://storage.googleapis.com/kidplus-bucket-dev/630dd121b7e1c52aee7c3fe9_8%2Fimages%2F0%2F81973000.png",
                inferenceData=InferenceData(
                    morphokinetics=[Morphokinetic(event="tPNa", value=11.88)],
                    kidScore=None,
                    blastScore=None,
                    kidRank=None,
                    pnCount=2,
                    pnCountTime=20.460277777777776,
                    degeneratedOocyte=False,
                    duc1=False,
                    fragmentationTime=0,
                ),
                inferenceOverridingData={},
            ),
            EMRWellsResponse(
                wellNumber="9",
                slideEsId="60c100de-23a9-11ed-8fe5-408d5c16071c",
                instrument=Instrument(type="GERI"),
                status="GROWING",
                lastImageUrl="https://storage.googleapis.com/kidplus-bucket-dev/630dd121b7e1c52aee7c3fe9_9%2Fimages%2F0%2F81973000.png",
                inferenceData=InferenceData(
                    morphokinetics=[],
                    kidScore=None,
                    blastScore=None,
                    kidRank=None,
                    pnCount=0,
                    pnCountTime=11.550277777777778,
                    degeneratedOocyte=False,
                    duc1=False,
                    fragmentationTime=0,
                ),
                inferenceOverridingData={},
            ),
        ],
        "63df780fb6d2f6506b1e4788": [
            EMRWellsResponse(
                wellNumber="14139",
                slideEsId="MTL12-4708-2-189E-72B7",
                instrument=Instrument(type="MIRI"),
                status="DISCARDED",
                lastImageUrl="https://storage.googleapis.com/kidplus-bucket-dev/63df7811d461faaf4f75b603_14139%2Fimages%2F0%2F85201000.jpeg",
                inferenceData=InferenceData(
                    morphokinetics=[
                        Morphokinetic(event="tPNa", value=0.0),
                        Morphokinetic(event="tPNf", value=7.67),
                        Morphokinetic(event="t2", value=8.0),
                    ],
                    kidScore=None,
                    blastScore=None,
                    kidRank=None,
                    pnCount=0,
                    pnCountTime=4.0,
                    degeneratedOocyte=False,
                    duc1=False,
                    fragmentationTime=0,
                ),
                inferenceOverridingData={
                    "pnCount": 2,
                    "duc1": False,
                    "degeneratedOocyte": False,
                },
            ),
            EMRWellsResponse(
                wellNumber="14140",
                slideEsId="MTL12-4708-2-189E-72B7",
                instrument=Instrument(type="MIRI"),
                lastImageUrl="https://storage.googleapis.com/kidplus-bucket-dev/63df7811d461faaf4f75b603_14140%2Fimages%2F0%2F85201000.jpeg",
                inferenceData=InferenceData(
                    morphokinetics=[],
                    kidScore=None,
                    blastScore=None,
                    kidRank=None,
                    pnCount=0,
                    pnCountTime=3.667,
                    degeneratedOocyte=False,
                    duc1=False,
                    fragmentationTime=0,
                ),
                inferenceOverridingData={},
            ),
            EMRWellsResponse(
                wellNumber="14141",
                slideEsId="MTL12-4708-2-189E-72B7",
                instrument=Instrument(type="MIRI"),
                lastImageUrl="https://storage.googleapis.com/kidplus-bucket-dev/63df7811d461faaf4f75b603_14141%2Fimages%2F0%2F85201000.jpeg",
                inferenceData=InferenceData(
                    morphokinetics=[],
                    kidScore=None,
                    blastScore=None,
                    kidRank=None,
                    pnCount=0,
                    pnCountTime=2.667,
                    degeneratedOocyte=False,
                    duc1=False,
                    fragmentationTime=0,
                ),
                inferenceOverridingData={},
            ),
            EMRWellsResponse(
                wellNumber="14147",
                slideEsId="MTL12-4708-2-189E-72B7",
                instrument=Instrument(type="MIRI"),
                lastImageUrl="https://storage.googleapis.com/kidplus-bucket-dev/63df7811d461faaf4f75b603_14147%2Fimages%2F0%2F85201000.jpeg",
                inferenceData=InferenceData(
                    morphokinetics=[],
                    kidScore=None,
                    blastScore=None,
                    kidRank=None,
                    pnCount=None,
                    pnCountTime=0.0,
                    degeneratedOocyte=False,
                    duc1=False,
                    fragmentationTime=0,
                ),
                inferenceOverridingData={},
            ),
            EMRWellsResponse(
                wellNumber="14142",
                slideEsId="MTL12-4708-2-189E-72B7",
                instrument=Instrument(type="MIRI"),
                lastImageUrl="https://storage.googleapis.com/kidplus-bucket-dev/63df7811d461faaf4f75b603_14142%2Fimages%2F0%2F85201000.jpeg",
                inferenceData=InferenceData(
                    morphokinetics=[],
                    kidScore=None,
                    blastScore=None,
                    kidRank=None,
                    pnCount=0,
                    pnCountTime=4.667,
                    degeneratedOocyte=False,
                    duc1=False,
                    fragmentationTime=0,
                ),
                inferenceOverridingData={},
            ),
            EMRWellsResponse(
                wellNumber="14148",
                slideEsId="MTL12-4708-2-189E-72B7",
                instrument=Instrument(type="MIRI"),
                lastImageUrl="https://storage.googleapis.com/kidplus-bucket-dev/63df7811d461faaf4f75b603_14148%2Fimages%2F0%2F85201000.jpeg",
                inferenceData=InferenceData(
                    morphokinetics=[],
                    kidScore=None,
                    blastScore=None,
                    kidRank=None,
                    pnCount=0,
                    pnCountTime=12.0,
                    degeneratedOocyte=False,
                    duc1=False,
                    fragmentationTime=0,
                ),
                inferenceOverridingData={},
            ),
            EMRWellsResponse(
                wellNumber="14146",
                slideEsId="MTL12-4708-2-189E-72B7",
                instrument=Instrument(type="MIRI"),
                lastImageUrl="https://storage.googleapis.com/kidplus-bucket-dev/63df7811d461faaf4f75b603_14146%2Fimages%2F0%2F85201000.jpeg",
                inferenceData=InferenceData(
                    morphokinetics=[],
                    kidScore=None,
                    blastScore=None,
                    kidRank=None,
                    pnCount=0,
                    pnCountTime=6.667,
                    degeneratedOocyte=False,
                    duc1=False,
                    fragmentationTime=0,
                ),
                inferenceOverridingData={},
            ),
            EMRWellsResponse(
                wellNumber="14143",
                slideEsId="MTL12-4708-2-189E-72B7",
                instrument=Instrument(type="MIRI"),
                lastImageUrl="https://storage.googleapis.com/kidplus-bucket-dev/63df7811d461faaf4f75b603_14143%2Fimages%2F0%2F85201000.jpeg",
                inferenceData=InferenceData(
                    morphokinetics=[],
                    kidScore=None,
                    blastScore=None,
                    kidRank=None,
                    pnCount=0,
                    pnCountTime=2.667,
                    degeneratedOocyte=False,
                    duc1=False,
                    fragmentationTime=0,
                ),
                inferenceOverridingData={},
            ),
            EMRWellsResponse(
                wellNumber="14145",
                slideEsId="MTL12-4708-2-189E-72B7",
                instrument=Instrument(type="MIRI"),
                lastImageUrl="https://storage.googleapis.com/kidplus-bucket-dev/63df7811d461faaf4f75b603_14145%2Fimages%2F0%2F85201000.jpeg",
                inferenceData=InferenceData(
                    morphokinetics=[],
                    kidScore=None,
                    blastScore=None,
                    kidRank=None,
                    pnCount=0,
                    pnCountTime=3.334,
                    degeneratedOocyte=False,
                    duc1=False,
                    fragmentationTime=0,
                ),
                inferenceOverridingData={},
            ),
            EMRWellsResponse(
                wellNumber="14144",
                slideEsId="MTL12-4708-2-189E-72B7",
                instrument=Instrument(type="MIRI"),
                lastImageUrl="https://storage.googleapis.com/kidplus-bucket-dev/63df7811d461faaf4f75b603_14144%2Fimages%2F0%2F85201000.jpeg",
                inferenceData=InferenceData(
                    morphokinetics=[],
                    kidScore=None,
                    blastScore=None,
                    kidRank=None,
                    pnCount=0,
                    pnCountTime=12.0,
                    degeneratedOocyte=False,
                    duc1=False,
                    fragmentationTime=0,
                ),
                inferenceOverridingData={},
            ),
        ],
    }


@pytest.fixture
def expected_patient_data() -> Dict[str, PatientDataResponse]:
    return {
        "62f50c6b46418c12e1c27b4e": PatientDataResponse(
            birthDate=None,
            tenantId="62f5048237468866998a3918",
            esId="ui-aut1",
            status=None,
            fullName=None,
            bmi=None,
            govId=None,
            treatments=[
                Treatment(
                    transfers=None,
                    oocyteAge=None,
                    esId="1",
                    startDate="2018-11-05T15:45:00.000Z",
                    endDate=None,
                    slides=[
                        PatientsSlideResponse(
                            id="62f50c6b46418c7ea7c27b51",
                            wells=[
                                Well(esId="1", kidRank=2),
                                Well(esId="2", kidRank=4),
                                Well(esId="3", kidRank=5),
                                Well(esId="4", kidRank=3),
                                Well(esId="5", kidRank=1),
                            ],
                        )
                    ],
                    patientAge=None,
                )
            ],
            createdAt="2022-08-11T14:04:27.167Z",
            updatedAt="2022-11-15T09:05:54.676Z",
        ),
        "62f50c6c46418c238ac27b5a": PatientDataResponse(
            birthDate=None,
            tenantId="62f5048237468866998a3918",
            esId="ui-aut4",
            status=None,
            fullName=None,
            bmi=None,
            govId=None,
            treatments=[
                Treatment(
                    transfers=None,
                    oocyteAge=None,
                    esId="4",
                    startDate="2019-12-03T12:20:00.000Z",
                    endDate=None,
                    slides=[],
                    patientAge=None,
                )
            ],
            createdAt="2022-08-11T14:04:28.499Z",
            updatedAt="2022-11-15T09:05:54.866Z",
        ),
        "62f50c6b46418ca48dc27b52": PatientDataResponse(
            birthDate=None,
            tenantId="62f5048237468866998a3918",
            esId="ui-aut2",
            status=None,
            fullName=None,
            bmi=None,
            govId=None,
            treatments=[
                Treatment(
                    transfers=None,
                    oocyteAge=None,
                    esId="2",
                    startDate="2019-01-11T13:39:59.999Z",
                    endDate=None,
                    slides=[],
                    patientAge=None,
                )
            ],
            createdAt="2022-08-11T14:04:27.595Z",
            updatedAt="2022-11-15T09:05:54.940Z",
        ),
        "630dd12044d91c5b211fa58d": PatientDataResponse(
            birthDate=None,
            tenantId="62f25fdcd02512e1d4ec6b6b",
            esId="221282 morat",
            status=None,
            fullName=None,
            bmi=None,
            govId=None,
            treatments=[
                Treatment(
                    transfers=None,
                    oocyteAge=OocyteAgeItem(
                        years=42,
                        months=0,
                        updatedAt="2022-08-30T08:58:08.447Z",
                        createdAt="2022-08-30T08:58:08.447Z",
                    ),
                    esId="fiv 222323",
                    startDate="2022-08-25T11:30:00.000Z",
                    endDate=None,
                    slides=[],
                    patientAge=None,
                )
            ],
            createdAt="2022-08-30T08:58:08.447Z",
            updatedAt="2022-11-15T09:05:56.536Z",
        ),
        "630dd12144d91c154b1fa592": PatientDataResponse(
            birthDate=None,
            tenantId="62f25fdcd02512e1d4ec6b6b",
            esId="219474 blau",
            status=None,
            fullName=None,
            bmi=None,
            govId=None,
            treatments=[
                Treatment(
                    transfers=None,
                    oocyteAge=OocyteAgeItem(
                        years=38,
                        months=0,
                        updatedAt="2022-08-30T08:58:09.199Z",
                        createdAt="2022-08-30T08:58:09.199Z",
                    ),
                    esId="fiv 222327",
                    startDate="2022-08-29T11:10:00.000Z",
                    endDate=None,
                    slides=[],
                    patientAge=None,
                )
            ],
            createdAt="2022-08-30T08:58:09.199Z",
            updatedAt="2022-11-15T09:05:56.610Z",
        ),
        "630dffc644d91ccf151fd882": PatientDataResponse(
            birthDate=None,
            tenantId="62f25fdcd02512e1d4ec6b6b",
            esId="221593 marron",
            status=None,
            fullName=None,
            bmi=None,
            govId=None,
            treatments=[
                Treatment(
                    transfers=None,
                    oocyteAge=OocyteAgeItem(
                        years=42,
                        months=0,
                        updatedAt="2022-08-30T12:17:10.840Z",
                        createdAt="2022-08-30T12:17:10.840Z",
                    ),
                    esId="fiv 222329",
                    startDate="1970-01-01T00:00:00.000Z",
                    endDate=None,
                    slides=[],
                    patientAge=None,
                )
            ],
            createdAt="2022-08-30T12:17:10.840Z",
            updatedAt="2022-11-15T09:05:57.312Z",
        ),
        "63b2c780a52aa892a6f5b9d1": PatientDataResponse(
            birthDate=None,
            tenantId="638f4ddd982c346d7e1af504",
            esId="1",
            status=None,
            fullName=None,
            bmi=None,
            govId=None,
            treatments=[
                Treatment(
                    transfers=None,
                    oocyteAge=OocyteAgeItem(
                        years=31,
                        months=11,
                        updatedAt="2023-01-03T08:19:27.341Z",
                        createdAt="2023-01-03T08:19:27.341Z",
                    ),
                    esId="1",
                    startDate="2022-02-21T00:00:00.000Z",
                    endDate=None,
                    slides=[
                        PatientsSlideResponse(
                            id="63b2c780a52aa8f184f5b9d7",
                            wells=[Well(esId="1", kidRank=1)],
                        )
                    ],
                    patientAge=None,
                )
            ],
            createdAt="2023-01-02T12:01:04.088Z",
            updatedAt="2023-01-03T16:48:10.854Z",
        ),
        "63df780fb6d2f69a961e4786": PatientDataResponse(
            birthDate=None,
            tenantId="63f1e17661fb18f6c6572a93",
            esId="2649",
            status=None,
            fullName=None,
            bmi=None,
            govId=None,
            treatments=[
                Treatment(
                    transfers=None,
                    oocyteAge=None,
                    esId="3910",
                    startDate="2023-02-02T14:30:39.000Z",
                    endDate=None,
                    slides=[],
                    patientAge=None,
                )
            ],
            createdAt="2023-02-05T09:34:07.885Z",
            updatedAt="2023-02-05T09:39:21.083Z",
        ),
    }


@pytest.fixture
def expected_patient_emr_data() -> Dict[str, PatientsEMRResponse]:
    return {
        "ui-aut1": PatientsEMRResponse(
            _id="62f50c6b46418c12e1c27b4e",
            patientId="ui-aut1",
            treatments=[EMRTreatment(_id="62f50c6b46418c9652c27b4f", name="1", startDate="2018-11-05T15:45:00.000Z")],
        ),
        "ui-aut4": PatientsEMRResponse(
            _id="62f50c6c46418c238ac27b5a",
            patientId="ui-aut4",
            treatments=[EMRTreatment(_id="62f50c6c46418c238ac27b5a", name="4", startDate="2019-12-03T12:20:00.000Z")],
        ),
        "ui-aut2": PatientsEMRResponse(
            _id="62f50c6b46418ce727c27b53",
            patientId="ui-aut2",
            treatments=[EMRTreatment(_id="62f50c6b46418ce727c27b53", name="2", startDate="2019-01-11T13:39:59.999Z")],
        ),
        "630dd12044d91c5b211fa58d": PatientsEMRResponse(
            patientId="221282 morat",
            treatments=[
                EMRTreatment(name="fiv 222323", startDate="2022-08-25T11:30:00.000Z")
            ],
        ),
        "630dd12144d91c154b1fa592": PatientsEMRResponse(
            patientId="219474 blau",
            treatments=[
                EMRTreatment(name="fiv 222327", startDate="2022-08-29T11:10:00.000Z")
            ],
        ),
        "630dffc644d91ccf151fd882": PatientsEMRResponse(
            patientId="221593 marron",
            treatments=[
                EMRTreatment(name="fiv 222329", startDate="1970-01-01T00:00:00.000Z")
            ],
        ),
        "63df780fb6d2f69a961e4786": PatientsEMRResponse(
            patientId="638109450348674032",
            treatments=[
                EMRTreatment(name="3910", startDate="2023-02-02T14:30:39.000Z")
            ],
        ),
    }


@pytest.fixture
def expected_treatment_data():
    return {
        # "62f50c6b46418c9652c27b4f": TreatmentDataResponse(
        #     availableFocals=[-75, -60, -45, -30, -15, 0, 15, 30, 45, 60, 75],
        #     transfers=None,
        #     oocyteAge=None,
        #     id="62f50c6b46418c9652c27b4f",
        #     startDate="2018-11-05T15:45:00.000Z",
        #     endDate=None,
        #     slides=[
        #         {
        #             "wells": [
        #                 {"esId": "1", "kidRank": 2},
        #                 {"esId": "2", "kidRank": 4},
        #                 {"esId": "3", "kidRank": 5},
        #                 {"esId": "4", "kidRank": 3},
        #                 {"esId": "5", "kidRank": 1},
        #             ],
        #             "id": "62f50c6b46418c7ea7c27b51",
        #         }
        #     ],
        #     patientAge=255,
        #     status="IN_PROGRESS",
        #     tenantId="62f5048237468866998a3918",
        #     patientId="62f50c6b46418c12e1c27b4e",
        #     instruments=[
        #         Instrument(
        #             id="62f50c6b46418c447ec27b50",
        #             esId="3169",
        #             slides=[
        #                 Slide(
        #                     tenantId="62f5048237468866998a3918",
        #                     id="62f50c6b46418c7ea7c27b51",
        #                     numberOfRuns=140,
        #                     availableMKEvents=["tPNa", "tPNf", "t2", "t3", "t4", "t5"],
        #                     videoLength=14.0,
        #                     firstRunTime=59941193,
        #                     esId="D2018.11.06_S00013_I3169",
        #                     delayBetweenCycles=10,
        #                     fertilizationTime=None,
        #                     hoursSinceFertilization=38,
        #                     lastImageHour=138903507,
        #                     instrumentId="62f50c6b46418c447ec27b50",
        #                 )
        #             ],
        #         )
        #     ],
        # ),
        # "62f50c6b46418ce727c27b53": TreatmentDataResponse(
        #     availableFocals=[-45, -30, -15, 0, 15, 30, 45],
        #     transfers=None,
        #     oocyteAge=None,
        #     startDate="2019-01-11T13:39:59.999Z",
        #     endDate=None,
        #     slides=[],
        #     patientAge=257,
        #     status="INACTIVE",
        #     tenantId="62f5048237468866998a3918",
        #     patientId="62f50c6b46418ca48dc27b52",
        #     instruments=[
        #         Instrument(
        #             esId="469",
        #             slides=[
        #                 Slide(
        #                     tenantId="62f5048237468866998a3918",
        #                     numberOfRuns=40,
        #                     availableMKEvents=["tPNf", "t2", "tPNa"],
        #                     videoLength=4.0,
        #                     firstRunTime=70437125,
        #                     esId="D2019.01.12_S01225_I0469",
        #                     delayBetweenCycles=10,
        #                     fertilizationTime=None,
        #                     hoursSinceFertilization=26,
        #                     lastImageHour=93908940,
        #                     instrumentId="62f50c6b46418cf10fc27b54",
        #                 )
        #             ],
        #         )
        #     ],
        # ),
        # "62f50c6c46418c078bc27b57": TreatmentDataResponse(
        #     availableFocals=[-75, -60, -45, -30, -15, 0, 15, 30, 45, 60, 75],
        #     transfers=None,
        #     oocyteAge=None,
        #     id="62f50c6c46418c078bc27b57",
        #     startDate="2019-12-03T12:20:00.000Z",
        #     endDate=None,
        #     slides=[],
        #     patientAge=268,
        #     status="IN_PROGRESS",
        #     tenantId="62f5048237468866998a3918",
        #     patientId="62f50c6c46418c8626c27b56",
        #     instruments=[
        #         Instrument(
        #             id="62f50c6c46418c6204c27b58",
        #             esId="3235",
        #             slides=[
        #                 Slide(
        #                     tenantId="62f5048237468866998a3918",
        #                     id="62f50c6c46418c28d2c27b59",
        #                     numberOfRuns=60,
        #                     availableMKEvents=["tPNa"],
        #                     videoLength=6.0,
        #                     firstRunTime=2583635,
        #                     esId="D2019.12.03_S00058_I3235",
        #                     delayBetweenCycles=11,
        #                     fertilizationTime=None,
        #                     hoursSinceFertilization=10,
        #                     lastImageHour=39380404,
        #                     instrumentId="62f50c6c46418c6204c27b58",
        #                 )
        #             ],
        #         )
        #     ],
        # ),
        # "630dd11f44d91ca8fd1fa589": TreatmentDataResponse(
        #     availableFocals=[0],
        #     transfers=None,
        #     oocyteAge=504,
        #     id="630dd11f44d91ca8fd1fa589",
        #     startDate="2022-08-26T11:05:00.000Z",
        #     endDate=None,
        #     slides=[
        #         {
        #             "wells": [
        #                 {"esId": "11", "kidRank": 4},
        #                 {"esId": "12", "kidRank": 3},
        #                 {"esId": "6", "kidRank": 1},
        #                 {"esId": "8", "kidRank": 2},
        #             ],
        #             "id": "630dd11f44d91c93291fa58c",
        #         }
        #     ],
        #     patientAge=511,
        #     status="IN_PROGRESS",
        #     tenantId="62f25fdcd02512e1d4ec6b6b",
        #     patientId="630dd11f44d91c113b1fa588",
        #     instruments=[
        #         Instrument(
        #             id="630dd11f44d91c52b61fa58b",
        #             esId="GERI00430",
        #             slides=[
        #                 Slide(
        #                     tenantId="62f25fdcd02512e1d4ec6b6b",
        #                     id="630dd11f44d91c93291fa58c",
        #                     numberOfRuns=300,
        #                     availableMKEvents=[
        #                         "tPNa",
        #                         "tPNf",
        #                         "t2",
        #                         "t3",
        #                         "t4",
        #                         "t5",
        #                         "t6",
        #                         "t7",
        #                         "t8",
        #                         "t9+",
        #                         "tM",
        #                         "tSB",
        #                     ],
        #                     videoLength=30.0,
        #                     firstRunTime=1000,
        #                     esId="eac82900-2460-11ed-8870-c400ad4629c0",
        #                     delayBetweenCycles=20,
        #                     fertilizationTime=None,
        #                     hoursSinceFertilization=98,
        #                     lastImageHour=356041000,
        #                     instrumentId="630dd11f44d91c52b61fa58b",
        #                 )
        #             ],
        #         )
        #     ],
        # ),
        # "630dd12144d91c35441fa593": TreatmentDataResponse(
        #     availableFocals=[0],
        #     transfers=None,
        #     oocyteAge=456,
        #     id="630dd12144d91c35441fa593",
        #     startDate="2022-08-29T11:10:00.000Z",
        #     endDate=None,
        #     slides=[],
        #     patientAge=511,
        #     status="IN_PROGRESS",
        #     tenantId="62f25fdcd02512e1d4ec6b6b",
        #     patientId="630dd12144d91c154b1fa592",
        #     instruments=[
        #         Instrument(
        #             id="630dd11f44d91c52b61fa58b",
        #             esId="GERI00430",
        #             slides=[
        #                 Slide(
        #                     tenantId="62f25fdcd02512e1d4ec6b6b",
        #                     id="630dd12144d91c96e61fa595",
        #                     numberOfRuns=70,
        #                     availableMKEvents=["tPNa"],
        #                     videoLength=7.0,
        #                     firstRunTime=1000,
        #                     esId="045dd470-25e5-11ed-a8c6-c400ad4629c0",
        #                     delayBetweenCycles=20,
        #                     fertilizationTime=None,
        #                     hoursSinceFertilization=22,
        #                     lastImageHour=81973000,
        #                     instrumentId="630dd11f44d91c52b61fa58b",
        #                 )
        #             ],
        #         )
        #     ],
        # ),
        # "630dd12044d91ce5cd1fa58e": TreatmentDataResponse(
        #     availableFocals=[0],
        #     transfers=None,
        #     oocyteAge=504,
        #     startDate="2022-08-25T11:30:00.000Z",
        #     endDate=None,
        #     slides=[],
        #     patientAge=511,
        #     status="INACTIVE",
        #     tenantId="62f25fdcd02512e1d4ec6b6b",
        #     patientId="630dd12044d91c5b211fa58d",
        #     instruments=[
        #         Instrument(
        #             esId="GERI00042",
        #             slides=[
        #                 Slide(
        #                     tenantId="62f25fdcd02512e1d4ec6b6b",
        #                     numberOfRuns=70,
        #                     availableMKEvents=["tPNa", "tPNf"],
        #                     videoLength=7.0,
        #                     firstRunTime=1000,
        #                     esId="60c100de-23a9-11ed-8fe5-408d5c16071c",
        #                     delayBetweenCycles=20,
        #                     fertilizationTime=None,
        #                     hoursSinceFertilization=22,
        #                     lastImageHour=81973000,
        #                     instrumentId="630dd12044d91c84df1fa590",
        #                 )
        #             ],
        #         )
        #     ],
        # ),
        # "63b2c780a52aa828ddf5b9d3": TreatmentDataResponse(
        #     availableFocals=[-3, -2, -1, 0, 1, 2, 3],
        #     transfers=None,
        #     oocyteAge=383,
        #     startDate="2022-02-21T00:00:00.000Z",
        #     endDate=None,
        #     slides=[
        #         {
        #             "wells": [{"esId": "1", "kidRank": 1}],
        #             "id": "63b2c780a52aa8f184f5b9d7",
        #         }
        #     ],
        #     patientAge=383,
        #     status="IN_PROGRESS",
        #     tenantId="638f4ddd982c346d7e1af504",
        #     patientId="63b2c780a52aa892a6f5b9d1",
        #     instruments=[
        #         Instrument(
        #             esId="DEMO",
        #             slides=[
        #                 Slide(
        #                     tenantId="638f4ddd982c346d7e1af504",
        #                     numberOfRuns=164,
        #                     availableMKEvents=[
        #                         "tPNa",
        #                         "tPNf",
        #                         "t2",
        #                         "t3",
        #                         "t4",
        #                         "t5",
        #                         "t6",
        #                         "t7",
        #                     ],
        #                     videoLength=16.4,
        #                     firstRunTime=1000,
        #                     esId="MTL-DEMO-0D89-3624-[142]",
        #                     delayBetweenCycles=20,
        #                     fertilizationTime=None,
        #                     hoursSinceFertilization=54,
        #                     lastImageHour=195601000,
        #                     instrumentId="63b2c780a52aa814a5f5b9d6",
        #                 )
        #             ],
        #         )
        #     ],
        # ),
        "63df780fb6d2f6506b1e4788": TreatmentDataResponse(
            availableFocals=[-2, -1, 0, 1, 2],
            transfers=None,
            oocyteAge=None,
            startDate="2023-02-02T14:30:39.000Z",
            endDate=None,
            slides=[],
            patientAge=637,
            status="INACTIVE",
            tenantId="63f1e17661fb18f6c6572a93",
            patientId="63df780fb6d2f69a961e4786",
            instruments=[
                Instrument(
                    esId="MTL12-4708-2",
                    slides=[
                        Slide(
                            tenantId="63f1e17661fb18f6c6572a93",
                            numberOfRuns=72,
                            availableMKEvents=[],
                            videoLength=7.2,
                            firstRunTime=1000,
                            esId="MTL12-4708-2-189E-72B7",
                            delayBetweenCycles=20,
                            fertilizationTime=None,
                            hoursSinceFertilization=23,
                            lastImageHour=85201000,
                            instrumentId="63df7810b6d2f6fdcf1e478b",
                        )
                    ],
                )
            ],
        ),
    }
