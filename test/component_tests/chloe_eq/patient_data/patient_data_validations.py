from typing import Any, Optional

from test.shared_packages.chloe_backend.patient_data.types.responses.emr_wells import EMRWellsCompare


def validate_emr_wells_compare(input_resp: list, expected_resp: list) -> bool:
    return EMRWellsCompare().compare(input_resp, expected_resp)


def validate_object_is_not_none(abj) -> None:
    assert abj is not None


def validate_object_is_none(abj) -> None:
    assert abj is None


def validate_list_is_empty(l: list) -> None:
    assert len(l) == 0


def validate_patients_exists(patient_data_db_list: Optional[Any], tenants_list: Optional[list]) -> None:
    for tenant in tenants_list:
        assert patient_data_db_list.find_patients(tenant) is not None
    return


def validate_patient_exists(patient_data_db, tenant_id: str) -> None:
    assert patient_data_db.find_patients(tenant_id) is not None
