from automation_sdk.mongo.mongo_orm.patient_data.patient import (
    Patients,
    Treatment,
    ProducerTreatment,
)


def base_patients_payload(tenant_id: str):
    return Patients(
        birthDate=None,
        tenantId=tenant_id,
        esId="ESDBL1610007016_43830.5155541551",
        identifier1="NA",
        treatments=[Treatment(startDate={"$date": {"$numberLong": "1577795008842"}})],
        producerTreatments=[
            ProducerTreatment(
                id={"$oid": "637355d4f9a52521d1e0457a"},
                oocyteAge=None,
                instrumentType="VITRO",
                startDate={"$date": {"$numberLong": "1577795008842"}},
                slides=None,
            )
        ],
    )
