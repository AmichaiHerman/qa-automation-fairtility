from datetime import date, datetime
from typing import Optional, Any, Union

from requests import Response

from test.component_tests.chloe_eq.patient_data.conftest import DateFilterDontExist, FilterByDate
from test.component_tests.chloe_eq.patient_data.patient_data_test_scenarios import WELL_TO_CHANGE
from test.shared_packages.chloe_backend.patient_data.patient_data_routes import PatientDataRoutes
from test.shared_packages.chloe_backend.patient_data.types.requests_data.get_params import GetTreatmentQueryParams, \
    GetWellQueryParams, GetPatientQueryParams, InstrumentGetParam, GetFinishedSlidesQueryParams
from test.shared_packages.chloe_backend.patient_data.types.requests_data.payloads import ChangeAgesPayload, \
    ChangeLanguageAndColorPayload, ChangeMedicalDecision, ChangePGTA, ChangeInstrumentName, PostSharedDataLink, DEG, \
    DucPayload, DUC, PnCountPayload, PnCountChange
from test.shared_packages.chloe_backend.patient_data.types.responses.instruments_response import InstrumentsDataResponse
from test.shared_packages.chloe_backend.patient_data.types.responses.treatment_response import TreatmentDataResponse
from test.shared_packages.chloe_backend.patient_data.types.responses.wells_response import WellData
from test.shared_packages.chloe_backend.tenants.tenants_routes import TenantsRoutes


def change_age_of(treatment_id: str, patient_data, patient_age: Optional[int] = None, oocyte_age: Optional[int] = None
                  ) -> TreatmentDataResponse:
    patient_data.treatment_route.change_ages(treatment_id,
                                             ChangeAgesPayload(patientAge=patient_age, oocyteAge=oocyte_age))
    return get_one_treatment(treatment_id, patient_data)


def change_language_and_color(
    tenant_id: str, lan: str, color: str, tenants: TenantsRoutes
) -> (str, str):
    resp = tenants.change_language_and_color(
        tenantId=tenant_id,
        payload=ChangeLanguageAndColorPayload(language=lan, color=color),
    )
    return resp.language, resp.color


def changing_medical_decision(well_id: str, medical_decision: str, patient_data) -> Any:
    resp = patient_data.wells_route.change_medical_decision(
        GetWellQueryParams(well_id=well_id), ChangeMedicalDecision(status=medical_decision),
    )
    return resp


def change_pgta(pgta_value, patient_data) -> WellData:
    resp = patient_data.wells_route.change_pgta(
        params=GetWellQueryParams(well_id=WELL_TO_CHANGE),
        payload=ChangePGTA(pgta=pgta_value.value),
    )
    return resp


def get_all_instruments(patient_data) -> Response:
    resp = patient_data.instrument_route.get_all_instruments()
    return resp


def get_past_csv(start_date: str, end_date: str, patient_data) -> str:
    resp = patient_data.get_past_csv(start_date, end_date)
    return resp.text


def get_all_patients(patient_data):
    resp = patient_data.patients_route.get_all_patients()
    return resp


def get_all_emr_patients(patient_data,):
    resp = patient_data.get_all_emr_patients()
    return resp


def get_all_wells_emr(treatment_id: str, patient_data, expected_wells_data: Optional[Any]) -> Any:
    resp = patient_data.get_all_emr_wells(
        GetTreatmentQueryParams(treatment_id=treatment_id)
    )
    if expected_wells_data:
        expected_res = expected_wells_data[treatment_id]
        return resp, expected_res
    return resp


def get_one_patient(patient_id, patient_data):
    resp = patient_data.patients_route.get_one_patient(
        GetPatientQueryParams(patient_id=patient_id)
    )
    return resp


def get_patients_emr_by_patient_id(patient_id, patient_data):
    resp = patient_data.get_emr_patients_by_id(patient_id)
    #     GetPatientQueryParams(patient_id=patient_id)
    # )
    if resp:
        return resp[0]
    return resp


def change_instrument_name(patient_data, instrument_id, new_name) -> Union[Response, InstrumentsDataResponse]:
    resp = patient_data.instrument_route.change_instrument_name(
        params=InstrumentGetParam(instrument_id=instrument_id),
        payload=ChangeInstrumentName(name=new_name),
    )
    return resp


def get_one_treatment(treatment_id: str, patient_data):
    resp = patient_data.treatment_route.get_one_treatment(treatment_id)
    return resp


def post_shared_data(patient_data):
    resp = patient_data.patients_route.post_shared_data(
        payload=PostSharedDataLink(
            treatmentId="61ed4f705474aa78edcaee26",
            tenantId="60b6466c422c2b418642d959",
            birthYear="1985",
            email="email@fairtility.com",
        )
    )
    return resp


def create_payload_deg(status) -> DEG:
    return DEG(degeneratedOocyte=status)


def create_payload_dug(status) -> DucPayload:
    return DucPayload(duc=DUC(duc1=status))


def create_payload_pn(desired_pn: int) -> PnCountPayload:
    """Creates The Payload for the change PN request"""
    return PnCountPayload(
        pnCount=PnCountChange(value=desired_pn, time=17.160277777777775)
    )


def inference_override_request(
    patient_data: PatientDataRoutes, payload: Union[DEG, DucPayload, PnCountPayload], well_id: str
) -> Union[WellData, Response]:
    return patient_data.wells_route.activate_inference_overriding(
        params=GetWellQueryParams(well_id=well_id), payload=payload
    )


def set_date_param(date_filter: FilterByDate) -> GetFinishedSlidesQueryParams:
    """A method that creates the dates query params for the test given user input (the date range to filter by)"""
    today = datetime.now().date()
    return GetFinishedSlidesQueryParams(
        start_date=str(_set_start_date(today, date_filter)), end_date=str(today)
    )


def _set_start_date(today: date, date_filter: FilterByDate) -> date:
    """Find the start date for the test by user input"""
    if date_filter == FilterByDate.LAST_MONTH:
        return _find_x_months_ago(today, months_back=1)
    elif date_filter == FilterByDate.LAST_3_MONTHS:
        return _find_x_months_ago(today, months_back=3)
    elif date_filter == FilterByDate.LAST_SIX_MONTHS:
        return _find_x_months_ago(today, months_back=6)
    elif date_filter == FilterByDate.LAST_YEAR:
        return today.replace(year=today.year - 1)
    else:
        raise DateFilterDontExist("The Date Filter Provided don't exist")


def _find_x_months_ago(today: date, months_back: int):
    end_date_in_int = int(today.month - months_back)

    if (
        end_date_in_int > 0
    ):  # which means x months ago was on the same year within today.
        return today.replace(month=end_date_in_int)
    else:
        # map between int result to month result
        end_date_mapping = {
            0: 12,
            -1: 11,
            -2: 10,
            -3: 9,
            -4: 8,
            -5: 7,
            -6: 6,
            -7: 5,
            -8: 4,
            -9: 3,
            -10: 2,
            -11: 1,
        }
        end_date_in_month = end_date_mapping[end_date_in_int]
        return today.replace(month=end_date_in_month, year=today.year - 1)
