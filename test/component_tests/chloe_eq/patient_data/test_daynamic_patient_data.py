import pytest
from automation_sdk.http_requests.status_codes import StatusCode
from test.component_tests.chloe_eq.patient_data.input_data_patient_data import base_patients_payload
from test.component_tests.chloe_eq.patient_data import patient_data_functions
from test.component_tests.chloe_eq.patient_data import patient_data_validations
from test.component_tests.chloe_eq.patient_data.patient_data_test_scenarios import *
from test.shared_packages.chloe_backend.patient_data.types.requests_data.get_params import GetFinishedSlidesQueryParams
from test.shared_packages.chloe_backend.patient_data.types.requests_data.payloads import (ChangeGardnerPayload,
                                                                                          ClinicGardner)
from test import validations


@pytest.mark.patient_data
@pytest.mark.parametrize("creds, well_id, expansion_value, icm_value, te_value",
                         TEST_CASES_CHANGE_GARDNER, scope="session")
def test_change_gardner(
        creds, well_id, expansion_value, icm_value, te_value, patient_data
):
    resp = patient_data.wells_route.change_gardner(
        well_id=well_id,
        payload=ChangeGardnerPayload(
            clinicGardner=ClinicGardner(
                expansion=expansion_value.value, icm=icm_value.value, te=te_value.value
            )
        ),
    )

    # assert (resp.clinicGardner.expansion, expansion_value)
    # assert (resp.clinicGardner.icm, icm_value)
    # assert (resp.clinicGardner.te, te_value)

    assert validations.is_str_eq(resp.clinicGardner.expansion, expansion_value.value)
    assert validations.is_str_eq(resp.clinicGardner.icm, icm_value.value)
    assert validations.is_str_eq(resp.clinicGardner.te, te_value.value)


@pytest.mark.patient_data
@pytest.mark.parametrize("creds, treatment_id, oocyte_age, patient_age",
                         CHANGE_BOTH_AGES_TEST_CASES_VALID)
def test_change_both_ages(creds, patient_data, treatment_id, oocyte_age, patient_age):
    resp = patient_data_functions.change_age_of(treatment_id, patient_data, patient_age, oocyte_age)
    assert validations.is_int_eq(resp.patientAge, patient_age)
    assert validations.is_int_eq(resp.oocyteAge, oocyte_age)


@pytest.mark.patient_data
@pytest.mark.parametrize("creds, treatment_id, patient_age",
                         CHANGE_PATIENT_AGE_TEST_CASES_VALID)
def test_change_patient_age(creds, patient_data, treatment_id: str, patient_age):
    resp = patient_data_functions.change_age_of(treatment_id, patient_data, patient_age, oocyte_age=None)
    assert validations.is_int_eq(resp.patientAge, patient_age)


@pytest.mark.patient_data
@pytest.mark.parametrize("creds, treatment_id, oocyte_age",
                         CHANGE_OOCYTE_AGE_TEST_CASES_VALID)
def test_change_oocyte_age(creds, patient_data, treatment_id, oocyte_age):
    resp = patient_data_functions.change_age_of(treatment_id, patient_data, oocyte_age=oocyte_age)
    assert validations.is_int_eq(resp.oocyteAge, oocyte_age)


#  ↓↓↓↓ Negative Tests ↓↓↓↓
@pytest.mark.patient_data
@pytest.mark.parametrize("creds, treatment_id, oocyte_age, patient_age",
                         CHANGE_BOTH_AGES_TEST_CASES_INVALID)
def test_change_both_ages_invalid(creds, patient_data, treatment_id, oocyte_age, patient_age):
    resp = patient_data_functions.change_age_of(treatment_id, patient_data, patient_age, oocyte_age)
    assert resp.oocyteAge != oocyte_age
    assert resp.patientAge != patient_age

    # validate_int_is_not_eq(resp.oocyteAge, oocyte_age)
    # validate_int_is_not_eq(resp.patientAge, patient_age)


@pytest.mark.patient_data
@pytest.mark.parametrize("creds, treatment_id, oocyte_age",
                         CHANGE_OOCYTE_AGE_TEST_CASES_INVALID)
def test_change_oocyte_age_invalid(creds, treatment_id, oocyte_age, patient_data):
    resp = patient_data_functions.change_age_of(treatment_id=treatment_id, oocyte_age=oocyte_age,
                                                patient_data=patient_data, patient_age=None)
    assert not validations.is_int_eq(resp.oocyteAge, oocyte_age)
    # validate_int_is_not_eq(resp.oocyteAge, oocyte_age)


@pytest.mark.patient_data
@pytest.mark.parametrize("creds, treatment_id, patient_age",
                         CHANGE_PATIENT_AGE_TEST_CASES_INVALID)
def test_change_patient_age_invalid(creds, patient_data, treatment_id: str, patient_age):
    resp = patient_data_functions.change_age_of(treatment_id, patient_data, patient_age)
    assert not validations.is_int_eq(resp.patientAge, patient_age)
    # validate_int_is_not_eq(resp.patientAge, patient_age)


@pytest.mark.patient_data
@pytest.mark.parametrize(
    "creds, tenant_id, old_lan, new_lan,old_color, new_color", TEST_CASES_CHANGE_LANGUAGE
)
def test_change_language(
        creds, tenant_id, old_lan, new_lan, old_color, new_color, tenants):
    resp_language, resp_color = patient_data_functions.change_language_and_color(tenant_id, new_lan, new_color, tenants)
    # assert resp_language == new_lan
    # assert resp_color == new_color
    assert validations.is_str_eq(resp_language, new_lan)
    assert validations.is_str_eq(resp_color, new_color)
    # return to the old color
    resp_language, resp_color = patient_data_functions.change_language_and_color(tenant_id, old_lan, old_color, tenants)
    # assert resp_language == old_lan
    # assert resp_color == old_color
    assert validations.is_str_eq(resp_language, old_lan)
    assert validations.is_str_eq(resp_color, old_color)


@pytest.mark.patient_data
@pytest.mark.parametrize("creds, well_id, medical_decision", CHANGE_MEDICAL_DECISION)
def test_changing_medical_decision(creds, well_id, medical_decision, patient_data):
    resp = patient_data_functions.changing_medical_decision(well_id, medical_decision.value, patient_data)
    assert validations.is_str_eq(resp.status, medical_decision.value)


@pytest.mark.patient_data
@pytest.mark.parametrize("creds, well_id, medical_decision",
                         CHANGE_MEDICAL_DECISION_SECURITY)
def test_changing_medical_decision_security(
    creds, medical_decision, well_id, patient_data
):
    resp = patient_data_functions.changing_medical_decision(well_id, medical_decision.value, patient_data)
    assert validations.is_int_eq(resp.status_code, StatusCode.FORBIDDEN.value)


@pytest.mark.patient_data
@pytest.mark.parametrize("creds, pgta_value", CHANGE_PGTA)
def test_change_pgta(creds, pgta_value, patient_data):
    resp = patient_data_functions.change_pgta(pgta_value, patient_data)
    assert validations.is_str_eq(resp.pgta, pgta_value)


@pytest.mark.patient_data
@pytest.mark.parametrize("creds", GET_ALL_CREDS)
def test_get_all_instruments(creds, patient_data, expected_instruments_data):
    resp = patient_data_functions.get_all_instruments(patient_data)
    assert resp == expected_instruments_data[creds.user_name]


@pytest.mark.patient_data
@pytest.mark.parametrize("creds, start_date, end_date", PAST_CSV)
def test_get_past_csv(creds, start_date, end_date, patient_data):
    resp_text = patient_data.get_past_csv(start_date, end_date)
    # resp_text.text
    assert resp_text.text.startswith("https://storage.googleapis.com/kidplus-bucket-misc-dev/csv")
    assert resp_text.text.endswith(f"treatments-{start_date}-{end_date}.csv")
    # validations.is_str_starts_with(resp_text, "https://storage.googleapis.com/kidplus-bucket-misc-dev/csv")
    # validate_str_ends_with(resp_text, f"treatments-{start_date}-{end_date}.csv")


@pytest.mark.patient_data
@pytest.mark.parametrize("creds", GET_ALL_CREDS)
def test_get_all_patients(creds, patient_data, expected_patients_data):
    resp = patient_data.patients_route.get_all_patients()
    # resp = patient_data_functions.get_all_patients(patient_data)
    assert validations.check_list_equality(resp, expected_patients_data[creds.user_name])
    # validations.is_jsons_eq(resp, expected_patients_data[creds.user_name])


@pytest.mark.patient_data
@pytest.mark.parametrize("creds", GET_ALL_CREDS)
def test_get_all_emr_patients(creds, patient_data, expected_emr_patients_data):
    resp = patient_data.get_all_emr_patients()
    assert validations.check_list_equality(resp, expected_emr_patients_data[creds.user_name])


@pytest.mark.patient_data
@pytest.mark.parametrize("creds, treatment_id", ALL_WELLS_EMR)
def test_get_all_wells_emr(creds, treatment_id, patient_data, expected_wells_data):
    resp = patient_data.get_all_emr_wells(treatment_id)
    assert validations.is_deserialization_succeeded(resp), \
        "The resp is of ValidationError, fail in the deserialization 😧\nPlease check the DTO is match to the response"
    assert patient_data_validations.validate_emr_wells_compare(resp, expected_wells_data[treatment_id])


@pytest.mark.patient_data
@pytest.mark.parametrize("creds, treatment_id", ALL_WELLS_EMR_SECURITY)
@pytest.mark.xfail  # Security Bug -  Response Status Code should be 403
def test_get_all_wells_emr_security(creds, treatment_id, patient_data, expected_wells_data):
    resp = patient_data_functions.get_all_wells_emr(treatment_id, patient_data, expected_wells_data)
    assert validations.is_status_code(resp.status_code, StatusCode.FORBIDDEN)
    # assert resp.status_code != StatusCode.FORBIDDEN.value


@pytest.mark.patient_data
@pytest.mark.parametrize("creds, patient_id", ONE_PATIENT_TEST_CASES)
def test_get_patients_emr_by_patient_id(
    creds, patient_id, patient_data, expected_patient_emr_data
):
    resp = patient_data.get_emr_patients_by_id(patient_id)
    # resp = patient_data_functions.get_patients_emr_by_patient_id(patient_id, patient_data)
    assert resp[0] == expected_patient_emr_data[creds.user_name]
    assert validations.is_jsons_eq(resp, expected_patient_emr_data[patient_id])


@pytest.mark.patient_data
@pytest.mark.parametrize("creds, patient_id", NON_EXISTING_PATIENT_TEST_CASES)
def test_get_non_existing_patients_emr_by_patient_id(
    creds, patient_id, patient_data, expected_patient_emr_data
):
    resp = patient_data_functions.get_patients_emr_by_patient_id(patient_id, patient_data)
    assert len(resp) == 0
    # validate_list_is_empty(resp)


@pytest.mark.patient_data
@pytest.mark.parametrize("creds, patient_id", ONE_PATIENT_SECURITY_TEST_CASES)
def test_get_patients_emr_security_by_patient_id(creds, patient_id, patient_data):
    resp = patient_data_functions.get_patients_emr_by_patient_id(patient_id, patient_data)
    assert len(resp) == 0
    # validate_list_is_empty(resp)


@pytest.mark.patient_data
@pytest.mark.parametrize("creds, patient_id", ONE_PATIENT_TEST_CASES)
def test_get_one_patient(creds, patient_id: str, patient_data, expected_patient_data):
    resp = patient_data.patients_route.get_one_patient(patient_id)
    assert validations.is_jsons_eq(resp, expected_patient_data[patient_id])


@pytest.mark.patient_data
@pytest.mark.parametrize("creds, patient_id", ONE_PATIENT_SECURITY_TEST_CASES)
def test_get_one_patient_security(creds, patient_id, patient_data):
    resp = patient_data.patients_route.get_one_patient(patient_id)
    # validations.is_status_code(resp.status_code, StatusCode.FORBIDDEN.value)
    assert resp.status_code == StatusCode.FORBIDDEN.value


@pytest.mark.patient_data
@pytest.mark.parametrize("creds, instrument_id, old_name, new_name",
                         CHANGE_INSTRUMENT_NAME_TEST_CASES)
def test_change_instrument_name(
    patient_data, teardown_change_instrument_name, creds, instrument_id, old_name, new_name
):
    resp = patient_data_functions.change_instrument_name(patient_data, instrument_id, new_name)
    assert validations.is_str_eq(resp.name, new_name)
    assert not validations.is_str_eq(resp.name, old_name)
    # validate_str_is_not_eq(resp.name, old_name)


@pytest.mark.skip("Until bug fix - https://fairtility.atlassian.net/browse/KID-4725")
@pytest.mark.patient_data
@pytest.mark.parametrize("creds, instrument_id, old_name, new_name",
                         CHANGE_INSTRUMENT_NAME_SECURITY_CASE)
def test_change_instrument_name_security(
    patient_data, creds, instrument_id, old_name, new_name
):
    resp = patient_data_functions.change_instrument_name(patient_data, instrument_id, new_name)
    assert validations.is_status_code(resp.status_code, StatusCode.FORBIDDEN.value)


@pytest.mark.patient_data
@pytest.mark.parametrize("creds, treatment_id", ONE_TREATMENT_TEST_CASES)
def test_get_one_treatment(creds, treatment_id: str, patient_data, expected_treatment_data):
    resp = patient_data_functions.get_one_treatment(treatment_id, patient_data)
    assert validations.is_jsons_eq(resp, expected_treatment_data[treatment_id])


@pytest.mark.patient_data
@pytest.mark.parametrize("creds, treatment_id", ONE_TREATMENT_SECURITY_TEST_CASES)
@pytest.mark.xfail  # status code should be 403 , not 500
def test_get_one_treatment_security(creds, treatment_id, patient_data):
    resp = patient_data_functions.get_one_treatment(treatment_id, patient_data)
    assert validations.is_status_code(resp.status_code, StatusCode.FORBIDDEN.value)


@pytest.mark.patient_data
@pytest.mark.parametrize("creds", POST_SHARE_DATA_TEST_CASES)
def test_post_shared_data(creds, patient_data):
    resp = patient_data_functions.post_shared_data(patient_data)
    assert resp.cipher is not None
    assert resp.appUrl is not None


@pytest.mark.patient_data
@pytest.mark.parametrize("creds, treatment_es_id, identifier1", GET_TREATMENT_LINK_TEST_CASES)
def test_get_treatment_link(creds, treatment_es_id, identifier1, patient_data):
    resp = patient_data.get_treatment_link(treatment_es_id, identifier1)
    assert validations.is_str_starts_with(resp.text, BASE_URL_PATIENT_DATA_LINK)


@pytest.mark.patient_data
@pytest.mark.parametrize("creds, well_id, desired_deg_value", CHANGE_DEG_STATUS_TEST_CASES)
def test_changing_deg_status(patient_data, creds, well_id, desired_deg_value):
    desired_deg_payload = patient_data_functions.create_payload_deg(desired_deg_value)
    response = patient_data_functions.inference_override_request(patient_data, desired_deg_payload, well_id)
    deg_status = response.inferenceOverridingData.degeneratedOocyte
    assert validations.is_bool_equal(deg_status, desired_deg_payload.degeneratedOocyte)


@pytest.mark.patient_data
@pytest.mark.parametrize("creds, well_id, desired_deg_value",
                         CHANGE_DEG_STATUS_SECURITY_TEST_CASES)
def test_changing_deg_status_security(patient_data, creds, well_id, desired_deg_value):
    desired_deg_payload = patient_data_functions.create_payload_deg(desired_deg_value)
    resp = patient_data_functions.inference_override_request(patient_data, desired_deg_payload, well_id)
    assert validations.is_status_code(resp.status_code, StatusCode.FORBIDDEN.value)


@pytest.mark.patient_data
@pytest.mark.parametrize("creds, well_id, desired_duc_value", CHANGE_DUG_STATUS_TEST_CASES)
def test_changing_duc_status(patient_data, creds, well_id, desired_duc_value):
    desired_dug_payload = patient_data_functions.create_payload_dug(desired_duc_value)
    response = patient_data_functions.inference_override_request(
        patient_data, desired_dug_payload, well_id
    )
    duc_status = response.inferenceOverridingData.duc.duc1
    assert validations.is_bool_equal(duc_status, desired_dug_payload.duc.duc1)


@pytest.mark.patient_data
@pytest.mark.parametrize("creds, well_id, desired_duc_value",
                         CHANGE_DUG_STATUS_SECURITY_TEST_CASES)
def test_changing_duc_status_security(patient_data, creds, well_id, desired_duc_value):
    desired_dug_payload = patient_data_functions.create_payload_dug(desired_duc_value)
    resp = patient_data_functions.inference_override_request(
        patient_data, desired_dug_payload, well_id
    )
    assert validations.is_status_code(resp.status_code, StatusCode.FORBIDDEN.value)


@pytest.mark.patient_data
@pytest.mark.parametrize("creds, well_id, desired_pn", CHANGE_PN_COUNT_TEST_CASES)
def test_change_pn_count(patient_data, creds, well_id, desired_pn):
    desired_pn_payload = patient_data_functions.create_payload_pn(desired_pn)
    response = patient_data_functions.inference_override_request(
        patient_data, desired_pn_payload, well_id
    )
    pn_count_data = response.inferenceOverridingData.pnCount
    assert validations.is_int_eq(pn_count_data.value, desired_pn_payload.pnCount.value)


@pytest.mark.patient_data
@pytest.mark.parametrize("creds, well_id, desired_pn", CHANGE_PN_COUNT_SECURITY_TEST_CASES)
def test_changing_pn_count_security(patient_data, creds, well_id, desired_pn):
    desired_pn_payload = patient_data_functions.create_payload_pn(desired_pn)
    resp = patient_data_functions.inference_override_request(
        patient_data, desired_pn_payload, well_id
    )
    assert validations.is_status_code(resp.status_code, StatusCode.FORBIDDEN.value)


@pytest.mark.patient_data
@pytest.mark.parametrize("tenants_list", ADD_MANY_PATIENTS_TEST_CASES)
def test_add_many_patients(setup_teardown_creating_patient_data_db_list, tenants_list: list) -> None:
    mock_patients = [base_patients_payload(tenant_id) for tenant_id in tenants_list]
    setup_teardown_creating_patient_data_db_list.add_many_patients(mock_patients)
    assert patient_data_validations.validate_patients_exists(setup_teardown_creating_patient_data_db_list, tenants_list)


@pytest.mark.agent_handler
@pytest.mark.parametrize("tenant_id", ADD_PATIENTS_TEST_CASES)
def test_add_patient(tenant_id, setup_teardown_creating_patient_data_db) -> None:
    mock_patient = base_patients_payload(tenant_id)
    setup_teardown_creating_patient_data_db.add_patient(mock_patient)
    assert patient_data_validations.validate_patient_exists(setup_teardown_creating_patient_data_db, tenant_id)


@pytest.mark.patient_data
@pytest.mark.parametrize("creds, date_filter, slide_esid",
                         FIND_FINISHED_SLIDES_BY_DATE_TEST_CASES, scope="session")
def test_find_finished_slides_by_date(
    patient_data, creds, date_filter, slide_esid, teardown_update_fert_time_patient_data_repo
):
    query_object = patient_data_functions.set_date_param(date_filter)
    resp = patient_data.past_treatments_routes.get_finished_slides(params=query_object)
    assert validations.is_obj_in_list(slide_esid, resp.slides_esid_list)


@pytest.mark.patient_data
@pytest.mark.parametrize("creds, esid, expected_amount_of_slides", FIND_FINISHED_SLIDES_BY_ESID_TEST_CASES)
def test_find_finished_slides_by_esid(
    patient_data, creds, esid, expected_amount_of_slides
):
    query_object = GetFinishedSlidesQueryParams(esid=esid)
    resp = patient_data.past_treatments_routes.get_finished_slides(params=query_object)
    assert validations.is_int_eq(len(resp.slides_esid_list), expected_amount_of_slides)


@pytest.mark.patient_data
@pytest.mark.parametrize("creds, page, expected_amount_of_slides",
                         FIND_FINISHED_SLIDES_BY_PAGE_TEST_CASES)
def test_find_finished_slides_by_page(patient_data, creds, page, expected_amount_of_slides):
    query_object = GetFinishedSlidesQueryParams(page=page)
    resp = patient_data.past_treatments_routes.get_finished_slides(params=query_object)
    assert validations.is_int_eq(len(resp.slides_esid_list), expected_amount_of_slides)


# todo don't forget to manually configure the fertilization date every 3 months!
# in the test preparation - configured manually 1 slide for each date range, pls don't touch past slides user
@pytest.mark.patient_data
@pytest.mark.parametrize("creds, page_size", FIND_FINISHED_SLIDES_BY_PAGE_SIZE_TEST_CASES)
def test_find_finished_slides_by_page_size(patient_data, creds, page_size):
    query_object = GetFinishedSlidesQueryParams(page_size=page_size)
    resp = patient_data.past_treatments_routes.get_finished_slides(params=query_object)
    assert validations.is_int_eq(len(resp.slideCards), page_size)
