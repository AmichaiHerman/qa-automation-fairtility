# Patient Data Service Tests #

This package contains component tests for the Patient Data service.

## Coverage

The tests are validating the functionality of these API endpoints:

```GET /instrunents```: GET All instruments for tenant

```GET /patients```: GET All patients for tenant

```GET /emr/v1/patients```: GET All EMR patients for tenant

```GET /emr/v1/wells```: GET All EMR wells

```GET /patient/{patientId}```: GET One Patient of a tenant

```GET /emr/v1/patient/{patientId}```: GET One Patient of a tenant

```GET /treatment/{treatmentId}```: GET One Treatment for treatment

```GET /instrunents```: GET All instruments for tenant

```PATCH /infernce_overriding```: Changes manualy an inference result value

```PATCH /wells```: Changes medical decision for a well
