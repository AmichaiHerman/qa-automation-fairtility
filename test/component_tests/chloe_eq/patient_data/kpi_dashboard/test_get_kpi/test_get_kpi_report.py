import pytest

from test.shared_packages.test_data.test_users import PATIENT_DATA_VITRO2

TEST_CASES = [(PATIENT_DATA_VITRO2)]


@pytest.mark.tenants
@pytest.mark.parametrize("creds", TEST_CASES)
def test_get_kpi(creds, patient_data, expected_kpi_dashboard_data):
    resp = patient_data.reports_route.get_kpi_report()
    assert resp == expected_kpi_dashboard_data[creds.user_name]
