import pytest

from test.component_tests.chloe_eq.patient_data.kpi_dashboard.infra.kpi_calculator import (
    KpiCalculator,
)
from test.component_tests.chloe_eq.patient_data.kpi_dashboard.infra.kpis_repr.cleavage_rate import (
    CleavageRate,
)
from test.component_tests.chloe_eq.patient_data.kpi_dashboard.infra.kpis_repr.day2_dev_rate import (
    Day2DevRate,
)

from test.component_tests.chloe_eq.patient_data.kpi_dashboard.infra.kpis_repr.day3_dev_rate import (
    Day3DevRate,
)
from test.component_tests.chloe_eq.patient_data.kpi_dashboard.infra.kpis_repr.day5_blast_rate import (
    Day5BlastRate,
)
from test.component_tests.chloe_eq.patient_data.kpi_dashboard.infra.kpis_repr.failed_icsi_treatments import (
    FailedICSITreatments,
)
from test.component_tests.chloe_eq.patient_data.kpi_dashboard.infra.kpis_repr.icsi_degeneration_rate import (
    ICSIDegenerationRate,
)
from test.component_tests.chloe_eq.patient_data.kpi_dashboard.infra.kpis_repr.icsi_normall_ferilization_rate import (
    ICSINormalFertilizationRate,
)
from test.component_tests.chloe_eq.patient_data.kpi_dashboard.infra.kpis_repr.one_pn_rate import (
    OnePNRate,
)
from test.component_tests.chloe_eq.patient_data.kpi_dashboard.infra.kpis_repr.overall_blast_rate import (
    OverallBlastRate,
)
from test.shared_packages.chloe_backend.patient_data.types.requests_data.get_params import (
    GetKPIReportsQueryParams,
)
from test.shared_packages.chloe_backend.patient_data.types.responses.kpi_report_response import (
    KPIReportResponse,
)
from test.shared_packages.test_data.test_users import MIRI_KPI

TEST_USERS = [
    (MIRI_KPI, OnePNRate),
    (MIRI_KPI, ICSINormalFertilizationRate),
    (MIRI_KPI, OverallBlastRate),
    (MIRI_KPI, ICSIDegenerationRate),
    (MIRI_KPI, Day5BlastRate),
    (MIRI_KPI, Day2DevRate),
    (MIRI_KPI, Day3DevRate),
    (MIRI_KPI, CleavageRate),
    (MIRI_KPI, FailedICSITreatments),
]


@pytest.mark.parametrize("creds, kpi", TEST_USERS)
@pytest.mark.kpi
def test_kpi_calculations_without_filters(
    creds, tenant_id, kpi, patient_data, wells_of_tenant
):
    resp = patient_data.get_kpi_reports(GetKPIReportsQueryParams())
    actual_kpi_data = KPIReportResponse.fetch_kpi_data(resp, kpi.name)
    kpi_calculator = KpiCalculator(tenant_id, kpi, wells_of_tenant)
    expected_kpi_data = kpi_calculator.fetch_kpi_calculation_result()
    assert actual_kpi_data.total == expected_kpi_data.total
    assert actual_kpi_data.passed == expected_kpi_data.passed
    assert actual_kpi_data.ratio == expected_kpi_data.ratio
