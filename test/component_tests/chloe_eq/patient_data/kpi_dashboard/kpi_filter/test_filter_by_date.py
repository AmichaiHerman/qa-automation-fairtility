import pytest

from test.shared_packages.chloe_backend.patient_data.patient_data_routes import validate_year_in_kpi_report
from test.shared_packages.test_data.test_users import PATIENT_DATA_VITRO2

INVALID_YEAR = 8000
MAX_YEAR = 2100
VALID_YEAR = 2019

TEST_CASES = [
    (
        PATIENT_DATA_VITRO2,
        f"01-10-{str(VALID_YEAR)}",
        f"12-11-{str(VALID_YEAR)}",
    )
]
INVALID_TEST_CASES = [
    (
        PATIENT_DATA_VITRO2,
        f"01-10-{str(INVALID_YEAR)}",
        f"12-11-{str(INVALID_YEAR)}",
    )
]


@pytest.mark.patient_data
@pytest.mark.parametrize("creds,start_date,end_date", TEST_CASES)
def test_filter_by_date(creds, patient_data, start_date, end_date):
    resp = patient_data.reports_route.get_kpi_report_filter_by_date(
        start_date=start_date, end_date=end_date
    )
    validate_year_in_kpi_report(resp, VALID_YEAR)


# ↓ ↓ ↓ NEGATIVE TESTS ↓ ↓ ↓
# @pytest.mark.patient_data
# @pytest.skip("Until Bug Fix")
# @pytest.mark.parametrize("creds, start_date,end_date", INVALID_TEST_CASES)
# def test_filter_by_date_invalid_year(creds, patient_data, start_date, end_date):
#    resp = patient_data.reports_route.get_kpi_report_filter_by_date(
#        start_date=start_date, end_date=end_date
#   )
#    validate_year_in_kpi_report(resp, MAX_YEAR)
