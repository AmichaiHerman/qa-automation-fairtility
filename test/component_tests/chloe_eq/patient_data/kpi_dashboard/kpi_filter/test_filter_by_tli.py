import pytest

from test.shared_packages.test_data.test_users import PATIENT_DATA_VITRO2

TEST_CASES = [
    (PATIENT_DATA_VITRO2, "VITRO"),
    # (PATIENT_DATA_VITRO2, "MIRI"),
    # (PATIENT_DATA_VITRO2, "GERI")
]


@pytest.mark.patient_data
@pytest.mark.parametrize("creds, instrument_type", TEST_CASES)
def test_filter_by_tli(creds, instrument_type, patient_data, expected_kpi_report):
    resp = patient_data.reports_route.get_kpi_report_filter_by_tli(instrument_type)
    for kpi_response in resp:
        if kpi_response.missingDataWells is not None:
            for missing_data_well in kpi_response.missingDataWells:
                assert missing_data_well.instrumentType == instrument_type
