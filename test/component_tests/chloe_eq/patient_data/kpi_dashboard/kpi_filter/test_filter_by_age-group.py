import pytest
from requests import Response

from automation_sdk.http_requests.status_codes import StatusCode
from test.component_tests.chloe_eq.patient_data.kpi_dashboard.kpi_filter.conftest import (
    expected_kpi_report_filter_by_age_group_id,
)
from test.shared_packages.test_data.test_users import PATIENT_DATA_VITRO2


TEST_CASES = [(PATIENT_DATA_VITRO2, "649adb73c274b5002e35b942")]


@pytest.mark.patient_data
@pytest.mark.parametrize("creds, age_group_id", TEST_CASES)
def test_get_kpi_filter_by_age_group(
    creds, patient_data, expected_kpi_report_filter_by_age_group_id, age_group_id: str
):
    resp = patient_data.reports_route.get_kpi_report_filter_by_age_group(age_group_id)
    assert resp == expected_kpi_report_filter_by_age_group_id[creds.user_name]
