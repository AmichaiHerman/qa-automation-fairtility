import math
from abc import ABC, abstractmethod
from typing import List

from automation_sdk.mongo.mongo_orm.patient_data.wells import PDWells
from test.component_tests.chloe_eq.patient_data.kpi_dashboard.infra.kpis_repr.expected_kpis import (
    ExpectedKpiCalc,
)


class KPI(ABC):
    """An Abstract Class That Represents KPI'S on KPI Dashboard"""

    def __init__(self, wells: List[PDWells]):
        self.__wells = wells

    @classmethod
    @property
    def name(self) -> str:
        return "placeholder name for KPI's"

    @property
    def calculation_result(self) -> ExpectedKpiCalc:
        return ExpectedKpiCalc(
            passed=self.nominator,
            total=self.denominator,
            ratio=math.floor(self.nominator / self.denominator * 100),
        )

    @property
    @abstractmethod
    def nominator(self) -> int:
        ...

    @property
    @abstractmethod
    def denominator(self) -> int:
        ...
