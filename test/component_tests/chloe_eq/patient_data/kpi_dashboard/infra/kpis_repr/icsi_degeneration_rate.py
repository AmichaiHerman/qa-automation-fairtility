from typing import List

from automation_sdk.mongo.mongo_orm.patient_data.wells import PDWells
from test.component_tests.chloe_eq.patient_data.kpi_dashboard.infra.kpis_repr.common_denominators import (
    CommonDenominators,
)
from test.component_tests.chloe_eq.patient_data.kpi_dashboard.infra.kpis_repr.kpi import (
    KPI,
)


class ICSIDegenerationRate(KPI):
    def __init__(self, wells: List[PDWells]):
        super().__init__(wells)
        self.__denominator_filtered_embryos = CommonDenominators.amount_of_ICSI_embryos(
            wells
        )

    @classmethod
    @property
    def name(self) -> str:
        return "icsi_damage_rate"

    @property
    def nominator(self) -> int:
        regular_deg = [
            embryo for embryo in self.__denominator_filtered_embryos if embryo.is_DEG
        ]
        return len(regular_deg)

    @property
    def denominator(self) -> int:
        return len(self.__denominator_filtered_embryos)
