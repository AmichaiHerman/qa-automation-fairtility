from typing import List, Dict

from automation_sdk.mongo.mongo_orm.patient_data.wells import PDWells
from automation_sdk.mongo.repos.patient_data_repo import PatientDataRepo
from test.component_tests.chloe_eq.patient_data.kpi_dashboard.infra.kpis_repr.kpi import (
    KPI,
)


class FailedICSITreatments(KPI):
    def __init__(self, wells: List[PDWells]):
        super().__init__(wells)
        self.__treatments_with_0_pns = [
            embryo.treatmentId
            for embryo in wells
            if embryo.is_ICSI and embryo.is_fertilization_failed
        ]
        self.__treatment_to_status = self.__treatment_to_fert_status()
        self.__patient_data_repo = PatientDataRepo()

    @classmethod
    @property
    def name(self) -> str:
        return "failed_icsi_treatments_rate"

    @property
    def nominator(self) -> int:
        failed_treatments = []
        for treatment, pns in self.__treatment_to_status.items():
            if all(pn == 0 for pn in pns):
                failed_treatments.append(treatment)

        return len(failed_treatments)

    @property
    def denominator(self) -> int:
        return len(self.__treatment_to_status)

    def __treatment_to_fert_status(self) -> Dict[str, List[int]]:
        treatments_to_fert_status = {}
        patient_data_repo = PatientDataRepo()
        print(list(set(self.__treatments_with_0_pns)))
        for treatment in list(set(self.__treatments_with_0_pns)):
            wells: List[PDWells] = patient_data_repo.find_wells_by_treatment(treatment)
            wells_pns = [well.inferenceData.pnCount.value for well in wells]
            treatments_to_fert_status.update({treatment: wells_pns})

        return treatments_to_fert_status
