from pydantic import BaseModel


class ExpectedKpiCalc(BaseModel):
    passed: int
    total: int
    ratio: int
