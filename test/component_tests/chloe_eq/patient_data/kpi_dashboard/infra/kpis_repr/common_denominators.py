from automation_sdk.mongo.mongo_orm.patient_data.wells import PDWells
from typing import List


class CommonDenominators:
    """A Util For handling common denominators calculations on KPI dashboard"""

    @staticmethod
    def amount_of_ICSI_embryos(wells: List[PDWells]) -> List[PDWells]:
        return [embryo for embryo in wells if embryo.is_ICSI]

    @staticmethod
    def amount_of_normally_fertilized_embryos(wells: List[PDWells]) -> List[PDWells]:
        regular_2pn_embryos = [
            embryo for embryo in wells if embryo.is_normally_fertilized_by_34h
        ]
        return regular_2pn_embryos
