from typing import List

from automation_sdk.mongo.mongo_orm.patient_data.wells import PDWells
from automation_sdk.mongo.repos.patient_data_repo import PatientDataRepo
from test.component_tests.chloe_eq.patient_data.kpi_dashboard.infra.kpis_repr.expected_kpis import (
    ExpectedKpiCalc,
)
from test.component_tests.chloe_eq.patient_data.kpi_dashboard.infra.kpis_repr.kpi import (
    KPI,
)


class KpiCalculator:
    """A Module that mimics the kpis calculations process"""

    def __init__(self, tenant_id: str, kpi: KPI, wells: List[PDWells]):
        self.__tenant_id = tenant_id
        self.__patient_data_repo = PatientDataRepo()
        self.__fetched_wells = wells
        self.__kpi: KPI = kpi(self.__fetched_wells)

    def fetch_kpi_calculation_result(self) -> ExpectedKpiCalc:
        return self.__kpi.calculation_result
