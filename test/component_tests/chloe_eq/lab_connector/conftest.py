import pytest

from test.component_tests.chloe_eq.lab_connector.connector_creation.connector_factory import (
    ConnectorFactory,
)


@pytest.fixture
def connector_factory():
    return ConnectorFactory()
