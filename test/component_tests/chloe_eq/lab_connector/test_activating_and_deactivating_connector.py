import pytest

from automation_sdk.mongo.repos.lab_connector_repo import LabConnectorRepo

TENANTS = [("60ae41155a582290103ee8db")]


@pytest.mark.lab_connector
@pytest.mark.parametrize("tenant_id", TENANTS)
def test_activating_and_deactivating_connector(tenant_id):
    repo = LabConnectorRepo()
    repo.activate_connector(tenant_id)
    assert repo.is_connector_active(tenant_id)
    repo.deactivate_connector(tenant_id)
    assert repo.is_connector_active(tenant_id) is False
