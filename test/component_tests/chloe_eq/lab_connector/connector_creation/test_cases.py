from test.shared_packages.atomic_types import Vendor
from test.shared_packages.test_data.test_tenants import (
    LAB_CONNECTOR_TENANT_1,
    LAB_CONNECTOR_TENANT_3,
    LAB_CONNECTOR_TENANT_2,
    MULTI_TLI_1,
    MULTI_TLI_2,
    MULTI_TLI_3,
    MULTI_TLI_4,
    MULTI_TLI_5,
    MULTI_TLI_6,
    MULTI_TLI_7,
)
from test.shared_packages.test_data.test_users import ADMIN_CREDS, LAB_CONNECTOR_1

BASE_SCENARIO = [
    (ADMIN_CREDS, Vendor.VITRO, LAB_CONNECTOR_TENANT_1),
    (ADMIN_CREDS, Vendor.GERI, LAB_CONNECTOR_TENANT_2),
    (ADMIN_CREDS, Vendor.MIRI, LAB_CONNECTOR_TENANT_3),
]

SECURITY = [(LAB_CONNECTOR_1, Vendor.VITRO, LAB_CONNECTOR_TENANT_1)]

MULTI_TLI = [
    (ADMIN_CREDS, Vendor.VITRO, Vendor.VITRO, MULTI_TLI_1),
    (ADMIN_CREDS, Vendor.VITRO, Vendor.GERI, MULTI_TLI_2),
    (ADMIN_CREDS, Vendor.VITRO, Vendor.MIRI, MULTI_TLI_3),
    (ADMIN_CREDS, Vendor.GERI, Vendor.GERI, MULTI_TLI_4),
    (ADMIN_CREDS, Vendor.GERI, Vendor.MIRI, MULTI_TLI_5),
    (ADMIN_CREDS, Vendor.MIRI, Vendor.MIRI, MULTI_TLI_6),
]

CONFLICTED_CONNECTOR_CREATION = [(ADMIN_CREDS, MULTI_TLI_7)]
