import uuid
import secrets
from typing import Dict, Optional

from requests import Response

from test.shared_packages.chloe_backend.lab_connector.lab_connector_routes import (
    LabConnectorRoutes,
)
from test.shared_packages.chloe_backend.lab_connector.types.create_connector_request import (
    ConnectorCreationRequest,
    LabApi,
)


class ConnectorFactory:
    def create_connector(
        self,
        vendor: str,
        tenant_id: str,
        lab_connector_service: LabConnectorRoutes,
        use_name: Optional[str] = None,
        use_url: Optional[str] = None,
    ) -> Response:
        payload = self.__create_connector_payload(vendor, tenant_id, use_name, use_url)
        resp = lab_connector_service.create_new_connector(payload)
        return resp

    def __create_connector_payload(
        self,
        vendor: str,
        tenant_id: str,
        use_name: Optional[str],
        use_url: Optional[str],
    ) -> ConnectorCreationRequest:
        api_creds = self.__vendor_to_creds(vendor)
        connector_name = use_name if use_name else str(uuid.uuid4())
        target_url = use_url if use_url else self.__generate_mock_url()
        return ConnectorCreationRequest(
            timeZone="GMT",
            isActive=False,
            tenantId=tenant_id,
            labApi=LabApi(
                url=target_url,
                user=api_creds["user"],
                password=api_creds["password"],
                version="1.3",
                batchSize=2,
            ),
            labStorageSettings=None,
            focals=[0],
            name=connector_name,
            connectorType=vendor,
        )

    def __vendor_to_creds(self, vendor: str) -> Dict[str, str]:
        if vendor == "VITRO":
            return {"user": "vitro", "password": "vitro"}
        elif vendor == "GERI":
            return {"user": "geri", "password": "geri"}
        elif vendor == "MIRI":
            return {"user": "miri", "password": "miri"}

    def __generate_mock_url(self) -> str:
        return f"https://{secrets.token_urlsafe(8)}.com"
