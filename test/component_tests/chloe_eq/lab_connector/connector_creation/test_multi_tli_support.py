import pytest

from automation_sdk.http_requests.status_codes import StatusCode
from test.component_tests.chloe_eq.lab_connector.connector_creation.connector_factory import (
    ConnectorFactory,
)
from test.component_tests.chloe_eq.lab_connector.connector_creation.test_cases import (
    MULTI_TLI,
    CONFLICTED_CONNECTOR_CREATION,
)
from test.shared_packages.atomic_types import Vendor


@pytest.mark.lab_connector
@pytest.mark.parametrize("creds, vendor_1, vendor_2, tenant_id", MULTI_TLI)
def test_multi_tli_support(
    creds, vendor_1, vendor_2, tenant_id, lab_connector, lab_connector_db
):
    first_response = ConnectorFactory().create_connector(
        vendor_1.value, tenant_id, lab_connector
    )
    assert first_response.status_code == StatusCode.CREATED.value
    second_response = ConnectorFactory().create_connector(
        vendor_2.value, tenant_id, lab_connector
    )
    assert second_response.status_code == StatusCode.CREATED.value
    assert len(lab_connector_db.find_connectors(tenant_id)) == 2


@pytest.mark.lab_connector
@pytest.mark.parametrize("creds, tenant_id", CONFLICTED_CONNECTOR_CREATION)
def test_create_two_connectors_with_same_name(
    creds, tenant_id, lab_connector, lab_connector_db
):
    connector_name = "FairAI"
    first_response = ConnectorFactory().create_connector(
        Vendor.VITRO.value, tenant_id, lab_connector, use_name=connector_name
    )
    assert first_response.status_code == StatusCode.CREATED.value
    second_response = ConnectorFactory().create_connector(
        Vendor.GERI.value, tenant_id, lab_connector, use_name=connector_name
    )
    assert second_response.status_code == StatusCode.CONFLICT.value
    assert second_response.json()["message"] == "Connector name already exists"


@pytest.mark.lab_connector
@pytest.mark.parametrize("creds, tenant_id", CONFLICTED_CONNECTOR_CREATION)
def test_create_two_connectors_with_same_url(
    creds, tenant_id, lab_connector, lab_connector_db
):
    connector_url = "https://fairAI.com:8443"
    first_response = ConnectorFactory().create_connector(
        Vendor.VITRO.value, tenant_id, lab_connector, use_url=connector_url
    )
    assert first_response.status_code == StatusCode.CREATED.value
    second_response = ConnectorFactory().create_connector(
        Vendor.GERI.value, tenant_id, lab_connector, use_url=connector_url
    )
    assert second_response.status_code == StatusCode.CONFLICT.value
    assert second_response.json()["message"] == "URL already exists for this tenant"
