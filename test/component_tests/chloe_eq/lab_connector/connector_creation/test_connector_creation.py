import pytest

from automation_sdk.http_requests.status_codes import StatusCode
from test.component_tests.chloe_eq.lab_connector.connector_creation.test_cases import (
    BASE_SCENARIO,
    SECURITY,
)


@pytest.mark.lab_connector
@pytest.mark.parametrize("creds, vendor, tenant_id", BASE_SCENARIO)
def test_connector_successful_creation(
    creds, vendor, tenant_id, lab_connector, lab_connector_db, connector_factory
):
    resp = connector_factory.create_connector(vendor.value, tenant_id, lab_connector)
    assert resp.status_code == StatusCode.CREATED.value
    assert lab_connector_db.find_connector(tenant_id) is not None


@pytest.mark.lab_connector
@pytest.mark.parametrize("creds, vendor, tenant_id", SECURITY)
def test_connector_creation_security(
    creds, vendor, tenant_id, lab_connector, connector_factory
):
    resp = connector_factory.create_connector(vendor.value, tenant_id, lab_connector)
    assert resp.status_code == StatusCode.FORBIDDEN.value
    assert resp.json()["message"] == "You don`t have system admin permissions"
