import pytest

from automation_sdk.mongo.repos.lab_connector_repo import LabConnectorRepo

TENANTS = [("60ae41155a582290103ee8db")]


@pytest.mark.lab_connector
@pytest.mark.parametrize("tenant_id", TENANTS)
def test_get_connector(tenant_id):
    repo = LabConnectorRepo()
    assert repo.find_connector(tenant_id) is not None
