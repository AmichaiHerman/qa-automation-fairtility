# Lab Connector Service Tests #

This package contains component tests for the Lab Connector service. 

## Coverage

The tests are validating the functionality of these API endpoints:


```POST /lab-sync```: Creating New Connector

```POST connector-manager/lab-sync```: Triggering Connector Manager Service.

```POST /lab-sync/sync```: Triggers Lab Sync


