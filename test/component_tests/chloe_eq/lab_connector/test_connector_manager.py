import pytest

from automation_sdk.http_requests.status_codes import StatusCode
from test.shared_packages.test_data.test_users import LAB_CONNECTOR_1

USER_CREDENTIALS = [LAB_CONNECTOR_1]


# todo from security perspective, only admin user should perfrom this operatrion.


@pytest.mark.lab_connector
@pytest.mark.parametrize("creds", USER_CREDENTIALS)
def test_connector_manager_triggering(creds, lab_connector):
    response = lab_connector.trigger_lab_sync()
    # assert lab-connector has been triggered
    assert response.status_code == StatusCode.CREATED.value
