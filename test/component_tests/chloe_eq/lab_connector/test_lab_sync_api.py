import pytest

from automation_sdk.http_requests.status_codes import StatusCode
from automation_sdk.mongo.repos.lab_connector_repo import LabConnectorRepo
from test.shared_packages.chloe_backend.lab_connector.types.lab_sync_request import (
    SyncType,
    LabSyncRequest,
)
from test.shared_packages.test_data.test_tenants import LAB_CONNECTOR_TENANT_4
from test.shared_packages.test_data.test_users import LAB_CONNECTOR_2

TEST_CASES = [
    (
        LAB_CONNECTOR_2,
        "63a44b4ede7cafe1b62bce7d",
        LAB_CONNECTOR_TENANT_4,
        SyncType.FULL_SYNC,
    )
]


def create_payload(connector_id: str, sync_type: SyncType) -> LabSyncRequest:
    return LabSyncRequest(connectorId=connector_id, syncType=sync_type)


@pytest.fixture
def lab_connector_db(tenant_id):
    lab_connector_db = LabConnectorRepo()
    lab_connector_db.activate_connector(tenant_id)
    yield lab_connector_db
    lab_connector_db.deactivate_connector(tenant_id)


# @pytest.mark.lab_connector
@pytest.mark.parametrize("creds, connector_id, tenant_id, sync_type", TEST_CASES)
def test_triggering_lab_sync(
    creds, connector_id, tenant_id, sync_type, lab_connector, lab_connector_db
):
    payload = create_payload(connector_id, sync_type.value)
    resp = lab_connector.perform_lab_sync(payload)
    # assert lab sync has been triggered
    assert resp.status_code == StatusCode.CREATED.value
    assert lab_connector_db.is_connector_running(tenant_id)
