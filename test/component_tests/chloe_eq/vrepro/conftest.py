import pytest

from automation_sdk.mongo.repos.vrepro_repo import VreproRepo


@pytest.fixture(autouse=True)
def vrepro_db(tenant_id):
    repo = VreproRepo()
    yield repo
    repo.delete_tenant_data(tenant_id)
