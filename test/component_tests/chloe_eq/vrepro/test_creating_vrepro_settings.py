import pytest

from automation_sdk.mongo.repos.vrepro_repo import VreproRepo

TEST_CASES = [("123456")]


@pytest.fixture(autouse=True)
def vrepro_db(tenant_id):
    repo = VreproRepo()
    yield repo
    repo.delete_settings(tenant_id)


@pytest.mark.vrepro
@pytest.mark.parametrize("tenant_id", TEST_CASES)
def test_creating_vrepro_settings(tenant_id, vrepro_db):
    vrepro_db.create_vrepro_settings(tenant_id)
    assert vrepro_db.search_vrepro_settings(tenant_id) is not None
