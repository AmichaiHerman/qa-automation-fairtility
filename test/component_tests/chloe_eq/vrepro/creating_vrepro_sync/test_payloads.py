from typing import Dict


from test.shared_packages.chloe_backend.vrepro.types.sync_payload import (
    VreproSyncPayload,
    Patient,
    Instrument,
    Treatment,
    ReqSlide,
)


def expected_payloads() -> Dict[str, VreproSyncPayload]:
    return {
        "vrepro-testings": VreproSyncPayload(
            patient=Patient(esId="0000545", id="63f76077fe8cda028b555006"),
            tenantId="63f4872aaf0fc24bd0922922",
            instrument=Instrument(id="63f76077fe8cda8cde55500b", esId="11"),
            treatment=Treatment(id="63f76077fe8cdacf61555008", esId="Algorithm"),
            slide=ReqSlide(
                id="63f76077fe8cda3b0455500c",
                esId="S0000",
                wells=[
                    "63f7627dfe8cdad78455500d",
                    "63f7627dfe8cda8a4c55502a",
                    "63f7627dfe8cda8c9b555047",
                    "63f7627dfe8cda8931555064",
                    "63f7627dfe8cda4542555081",
                    "63f7627dfe8cda04e355509e",
                    "63f7627dfe8cdada015550bb",
                    "63f7627dfe8cdabbbe5550d8",
                ],
            ),
        )
    }
