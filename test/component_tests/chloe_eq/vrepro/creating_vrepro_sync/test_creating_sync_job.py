import pytest

from automation_sdk.http_requests.status_codes import StatusCode
from test.component_tests.chloe_eq.vrepro.creating_vrepro_sync.test_payloads import (
    expected_payloads,
)
from test.shared_packages.test_data.test_users import (
    VREPRO_1,
    PATIENT_DATA_VITRO,
    VREPRO_2,
)

TEST_USERS = [
    (VREPRO_1, "63f4872aaf0fc24bd0922922"),
]


@pytest.mark.vrepro
@pytest.mark.parametrize("creds, tenant_id", TEST_USERS)
def test_creating_vrepro_sync_job(creds, tenant_id, vrepro):
    resp = vrepro.export_patient_data(payload=expected_payloads()[creds.user_name])
    assert resp.status_code == StatusCode.CREATED.value


TEST_USERS = [(VREPRO_1, "63f4872aaf0fc24bd0922922")]


@pytest.mark.vrepro
@pytest.mark.parametrize("creds, tenant_id", TEST_USERS)
def test_creating_vrepro_sync_job_twice(creds, tenant_id, vrepro):
    # fire first request
    vrepro.export_patient_data(payload=expected_payloads()[creds.user_name])

    # fire second request, expect 400

    resp = vrepro.export_patient_data(payload=expected_payloads()[creds.user_name])

    assert resp.status_code == StatusCode.BAD_REQUEST.value
    assert "Job is already running" in resp.json()["message"]


SECURITY_CASE = [
    (VREPRO_2, "63f4872aaf0fc24bd0922922"),
    (PATIENT_DATA_VITRO, "63f4872aaf0fc24bd0922922"),
]


@pytest.mark.vrepro
@pytest.mark.parametrize("creds,tenant_id", SECURITY_CASE)
def test_creating_vrepro_sync_job_security(creds, tenant_id, vrepro):
    # fire first request

    resp = vrepro.export_patient_data(
        payload=expected_payloads()[
            "vrepro-testings"
        ]  # sending payload of a different vrepro user.
    )
    assert resp.status_code == StatusCode.FORBIDDEN.value
