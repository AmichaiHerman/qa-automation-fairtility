import pytest

from automation_sdk.mongo.repos.tenants_repo import TenantsRepo


@pytest.fixture
def tenants_db_vrepo(tenant_id):
    repo = TenantsRepo()
    yield repo
    repo.change_vrepro_status(tenant_id, status=False)


TEST_CASES = [("63f635a869f69b1740cd6871")]


@pytest.mark.vrepro
@pytest.mark.parametrize("tenant_id", TEST_CASES)
def test_enabling_vrepro(tenant_id, tenants_db_vrepo):
    tenants_db_vrepo.change_vrepro_status(tenant_id, True)
    assert tenants_db_vrepo.check_vrepro_status(tenant_id)
