# Fairtility's Component Testing Framework #

This package contains the complete component testing framework, designed to test every microservice separately.

## Coverage ##

According to Dec 2022, The Framework Covers The Following Services:
```agent-handler```,```imaging```,```patient-data```,```lab connector```,```tenants```,```users```.

## Testing Domains ##

* ```service-logic```: These tests cover the basic functionality of the service, such as the ability to bulk import
  patients.
* ```security```: These tests verify that the service has appropriate security measures in place, such as preventing
  unauthorized access

## Usage ##

to run the tests locally, please make sure that:

* Dependencies Installed. use ```pip install -r requirements.txt```
* VPN is turned on (Test is Connecting to mongo DB and GCP)
* Gcloud CLI is installed. for more details please visit https://cloud.google.com/sdk/docs/install


* To Run All The Tests Together, use ```pytest -s -v```
* To Run Only The service logic tests, use ```pytest -m service_logic -s -v```
* To Run Only The service logic tests, use ```pytest -m service_logic -s -v```


