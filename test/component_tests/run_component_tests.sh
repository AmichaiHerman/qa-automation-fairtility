#!/bin/bash

echo "this is the PYTHONPATH $PYTHONPATH"
# fetch secrets
 if [ -z "$1" ]; then
    echo "Error: Please provide env for testing"
    exit 1
    fi

  ENV=$1

  echo "Trying to pull secrets"

  SECRET=$(gcloud secrets versions access latest --secret="automation-$ENV")

  echo "Trying to create env variables"

  SECRET=$(echo $SECRET | jq -r 'to_entries[] | .key + "=" + .value')

  for line in $SECRET; do
    export "$line"
    done
# Run the tests for the service
python3 -m pytest -s -v --env=$ENV --allure-project=" component-nightly" --alluredir=results --reruns 3 --reruns-delay 2