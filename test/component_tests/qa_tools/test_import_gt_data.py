import pytest

from automation_sdk.mongo.mongo_orm.qa_repo.embryo_gt_data import EmbryoGTData
from automation_sdk.mongo.repos.qa_repo import QARepo

TEST_URL = [("bla")]


@pytest.mark.parametrize("well_url", TEST_URL)
@pytest.mark.qa_toolbox
def test_embryo_gt_import(well_url, qa_repo):
    mock_data = EmbryoGTData(
        well_url=well_url,
        storage_path="t",
        well_es_id="1",
        vendor="bla",
        expected_pn=1,
        actual_pn=2,
        pn_identification_difficulty_level="g",
        expected_duc=True,
        actual_duc=True,
        expected_deg=True,
        actual_deg=True,
        with_abnormalities=True,
    )
    res = qa_repo.import_embryo_gt_data(mock_data)
    assert res is True
    assert qa_repo.find_by_well_url(mock_data.well_url) is not None


@pytest.mark.parametrize("well_url", TEST_URL)
@pytest.mark.qa_toolbox
def test_violating_embryo_gt_index(well_url, qa_repo):
    mock_data = EmbryoGTData(
        well_url=well_url,
        storage_path="t",
        well_es_id="1",
        vendor="bla",
        expected_pn=1,
        actual_pn=2,
        pn_identification_difficulty_level="g",
        expected_duc=True,
        actual_duc=True,
        expected_deg=True,
        actual_deg=True,
        with_abnormalities=True,
    )

    # first import
    res = qa_repo.import_embryo_gt_data(mock_data)
    assert res is True
    assert qa_repo.find_by_well_url(mock_data.well_url) is not None

    # second import
    res = qa_repo.import_embryo_gt_data(mock_data, ignore_excpetions=True)
    assert "E11000 duplicate key error collection" in res
