# AGENT HANDLER COMPONENT TESTS
AGENT_HANDLER_TENANT_1 = "6391c0eb757216c8ccae39cd"
AGENT_HANDLER_TENANT_2 = "639f111b0f4188db4e376cd8"
AGENT_HANDLER_TENANT_3 = "639f26730f41887b3a376cda"
AGENT_HANDLER_TENANT_4 = "639f08160f4188fd29376cd7"
AGENT_HANDLER_TENANT_5 = "63a20e16a74d8f9f5dc6653f"

# LAB CONNECTOR COMPONENT TESTS
LAB_CONNECTOR_TENANT_1 = "6398b269801184041bc2846e"
LAB_CONNECTOR_TENANT_2 = "6398b2798011847242c2846f"
LAB_CONNECTOR_TENANT_3 = "6398b288801184715ac28470"
LAB_CONNECTOR_TENANT_4 = "6399ddba9ba5e57765974bf0"
MULTI_TLI_1 = "6399cf729ba5e57614974be9"
MULTI_TLI_2 = "6399cf879ba5e552a7974bea"
MULTI_TLI_3 = "6399cf969ba5e5ce9f974beb"
MULTI_TLI_4 = "6399cfc69ba5e5394f974bec"
MULTI_TLI_5 = "6399cfd39ba5e528e6974bed"
MULTI_TLI_6 = "6399d0389ba5e50bcd974bee"
MULTI_TLI_7 = "6399d9c89ba5e58114974bef"

# PATIENT DATA COMPONENT TESTING
PATIENT_DATA_1 = "62f50c6b46418c9652c27b4f"
PATIENT_DATA_2 = "62f50c6b46418ce727c27b53"
PATIENT_DATA_3 = "630dd12144d91c35441fa593"
PATIENT_DATA_4 = "630dd12044d91ce5cd1fa58e"
