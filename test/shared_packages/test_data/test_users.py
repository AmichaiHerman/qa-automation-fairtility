import os

from test.shared_packages.atomic_types import UserCredentials

# ADMIN USER

# ADMIN_CREDS = UserCredentials(
#     user_name=os.getenv("KEYCLOAK_ADMIN_USER"),
#     password=os.getenv("KEYCLOAK_ADMIN_PASSWORD"),
# )

# Agent Handler Component Tests

AGENT_HANDLER_1 = UserCredentials(
    user_name="agent-handler-test-2", password="mJBW{0Yr|w"
)
AGENT_HANDLER_2 = UserCredentials(
    user_name="agent-handler-test-3", password="Agent1234!@#$"
)
AGENT_HANDLER_3 = UserCredentials(
    user_name="agent-handler-test-1", password="O3hBiJj-~<"
)
AGENT_HANDLER_4 = UserCredentials(
    user_name="agent-handler-test-4", password="9+,Tw.meHV"
)
AGENT_HANDLER_5 = UserCredentials(
    user_name="bulks_imports_tests", password="NX]KK;x2D$"
)
AGENT_HANDLER_6 = UserCredentials(
    user_name="agent-handler-multi-tli", password="wox7x.|hTo"
)

# Imaging Component Tests

IMAGING_1 = UserCredentials(user_name="imaging_test", password="Imaging123@!@#$")

# Lab Connector Component Tests

LAB_CONNECTOR_1 = UserCredentials(user_name="deg-test", password="DegDegDeg123@")
LAB_CONNECTOR_2 = UserCredentials(user_name="labsync2", password="""2z-xVDoVml""")

# Patient Data Component Tests

PATIENT_DATA_VITRO = UserCredentials(user_name="pastSlides", password="Qw123456!1")
PATIENT_DATA_GERI = UserCredentials(user_name="geri_Seinfeld", password="cR70x_(_")
PATIENT_DATA_MIRI = UserCredentials(user_name="MiriAutomation", password="%A>IzV3+-+-@")
PATIENT_DATA_VITRO2 = UserCredentials(user_name="vitro-fc2", password="VitroFc2!@#")
PATIENT_DATA_GERI2 = UserCredentials(user_name="tom-and-geri", password="m8^CM8xk:8897")
PATIENT_DATA_PAST_SLIDES: UserCredentials = UserCredentials(user_name="pastSlides", password="Qw123456!")

# vRepro Component Tests

VREPRO_1 = UserCredentials(
    user_name="vrepro-testings", password="VreprosTestosSantos123!@##"
)
VREPRO_UPDATE = UserCredentials(
    user_name="vrepro-update", password="VV1@!#@$UPDATEaaaa"
)
VREPRO_2 = UserCredentials(user_name="vrepro-2", password="Vrepros!@#$$1111")
VREPRO_GERI = UserCredentials(user_name="crop-geri", password="CropiCrop1234@!@3")
VREPRO_ADVANCED_DAYS_1 = UserCredentials(
    user_name="vrepro-day-three", password="Vreporday3@#$$$%%$%"
)
VREPRO_ADVANCED_DAYS_2 = UserCredentials(
    user_name="vrepro-advanced-days", password="Vrepro-advanced-days1234@11!"
)


# KPI Dashboard
MIRI_KPI = UserCredentials(user_name="miri-regev", password="(G/T.:sW6$2")


INVALID_USERNAME = UserCredentials(
    user_name="vitamin", password=":S+q!~R9if!@"
)
INVALID_PASSWORD = UserCredentials(
    user_name="vitamin-c", password="mJBW{0Yr|w"
)

AI_PN_MODEL = UserCredentials(user_name="AI-QA-Automation", password='Q`j6[tZFp"')
