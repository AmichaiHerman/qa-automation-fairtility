from abc import ABC, abstractmethod


class MKEvents(ABC):
    BASE_PATH = "inferenceData.morphokinetics"

    @property
    @abstractmethod
    def name(self) -> str:
        ...

    @property
    @abstractmethod
    def mongo_path(self) -> str:
        ...


class T2(MKEvents):
    @property
    def name(self) -> str:
        return "t2"

    @property
    def mongo_path(self) -> str:
        return MKEvents.BASE_PATH + "[2]"


class T3(MKEvents):
    @property
    def name(self) -> str:
        return "t3"

    @property
    def mongo_path(self) -> str:
        return MKEvents.BASE_PATH + "[3]"


class T4(MKEvents):
    @property
    def name(self) -> str:
        return "t4"

    @property
    def mongo_path(self) -> str:
        return MKEvents.BASE_PATH + "[4]"


class T5(MKEvents):
    @property
    def name(self) -> str:
        return "t5"

    @property
    def mongo_path(self) -> str:
        return MKEvents.BASE_PATH + "[5]"
