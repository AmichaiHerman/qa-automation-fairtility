import pytest

from test.shared_packages.atomic_types import UserCredentials
from test.shared_packages.chloe_backend.agent_handler.agent_handler_routes import AgentHandlerRoutes
from test.shared_packages.chloe_backend.imaging.imaging_routes import ImagingRoutes
from test.shared_packages.chloe_backend.imaging_crop.imaging_crop_routes import ImagingCropRoutes
from test.shared_packages.chloe_backend.lab_connector.lab_connector_routes import LabConnectorRoutes
from test.shared_packages.chloe_backend.notes.notes_routes import NotesRoutes
from test.shared_packages.chloe_backend.patient_data.patient_data_routes import PatientDataRoutes
from test.shared_packages.chloe_backend.slide_revision_manager.srm_routes import SlideRevisionManagerRoutes
from test.shared_packages.chloe_backend.tenants.tenants_routes import TenantsRoutes
from test.shared_packages.chloe_backend.users.users_routes import UsersRoutes
from test.shared_packages.chloe_backend.vrepro.vrepro_routes import VreproRoutes


@pytest.fixture
def agent_handler(creds: UserCredentials) -> AgentHandlerRoutes:
    """Setup Agent-Handler Service API Connection"""
    return AgentHandlerRoutes(creds)
    # return setup_service_routes(AgentHandlerRoutes, creds)


@pytest.fixture
def imaging(creds: UserCredentials) -> ImagingRoutes:
    """Setup Imaging Service API Connection"""
    return ImagingRoutes(creds)
    # return setup_service_routes(ImagingRoutes, creds)


@pytest.fixture
def imaging_crop(creds: UserCredentials) -> ImagingCropRoutes:
    """Setup Imaging Crop Service API Connection"""
    return ImagingCropRoutes(creds)
    # return setup_service_routes(ImagingCropRoutes, creds)


@pytest.fixture
def notes(creds: UserCredentials) -> NotesRoutes:
    """Setup Notes Service API Connection"""
    return NotesRoutes(creds)
    # return setup_service_routes(NotesRoutes, creds)


@pytest.fixture
def slide_revision_manager(creds: UserCredentials) -> SlideRevisionManagerRoutes:
    """Setup Slide Revision Manager Service API Connection"""
    return SlideRevisionManagerRoutes(creds)
    # return setup_service_routes(SlideRevisionManagerRoutes, creds)


@pytest.fixture
def vrepro(creds: UserCredentials) -> VreproRoutes:
    """Setup Vrepro Service API Connection"""
    return VreproRoutes(creds)
    # return setup_service_routes(VreproRoutes, creds)


@pytest.fixture
def lab_connector(creds: UserCredentials) -> LabConnectorRoutes:
    """Setup Lab Connector Service API Connection"""
    return LabConnectorRoutes(creds)
    # return setup_service_routes(LabConnectorRoutes, creds)


@pytest.fixture
def patient_data(creds: UserCredentials) -> PatientDataRoutes:
    """Setup Patient Data Service API Connection"""

    return PatientDataRoutes(creds)
    # return setup_service_routes(PatientDataRoutes, creds)


@pytest.fixture
def tenants(creds: UserCredentials) -> TenantsRoutes:
    """Setup Tenant Service API Connection"""
    return TenantsRoutes(creds)
    # return setup_service_routes(TenantsRoutes, creds)


@pytest.fixture
def users(creds: UserCredentials) -> UsersRoutes:
    """Setup Users Service API Connection"""
    return UsersRoutes(creds)
    # return setup_service_routes(UsersRoutes, creds)
