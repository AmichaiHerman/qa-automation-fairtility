import pytest

from automation_sdk.mongo.repos.agent_handler_repo import AgentHandlerRepo
from automation_sdk.mongo.repos.lab_connector_repo import LabConnectorRepo
from automation_sdk.mongo.repos.patient_data_repo import PatientDataRepo
from automation_sdk.mongo.repos.qa_repo import QARepo
from automation_sdk.mongo.repos.tenants_repo import TenantsRepo


# Don't add it patient-data DB fixture unless you are 100% SURE it will not delete important test users.


@pytest.fixture
def qa_repo(well_url: str) -> QARepo:
    repo = QARepo()
    yield repo
    repo.delete_by_well_url(well_url)


@pytest.fixture
def agent_handler_db(tenant_id_param: str) -> AgentHandlerRepo:
    repo = AgentHandlerRepo()
    yield repo
    repo.delete_tenant_data(tenant_id_param)


@pytest.fixture
def lab_connector_db(tenant_id_param: str) -> LabConnectorRepo:
    repo = LabConnectorRepo()
    yield repo
    repo.delete_tenant_data(tenant_id_param)


