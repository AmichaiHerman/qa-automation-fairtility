from typing import Any

import pytest
import logging


logger = logging.getLogger(__name__)


def set_marker(request, marker_name: str, marker_value: Any):
    logger.info("Setting new marker")
    setattr(request.node, marker_name, marker_value)


def get_marker(request, marker_name: str):
    logger.info("Getting new marker")
    marker_value = getattr(request.node, marker_name, None)
    if marker_value:
        return marker_value
    logger.error("Marker Was Not Found")
    return None
