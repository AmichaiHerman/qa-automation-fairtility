import pytest

from test.shared_packages.reporting.abstract_reporter import AbstractReporter


class TestRailReporter(AbstractReporter):
    @staticmethod
    def detect_testrail_markers_within_gherkin(items):
        for item in items:
            test_cases = []
            for marker in item.iter_markers():
                if marker.name.startswith("testrail_"):
                    test_case = marker.name.split("_")[1]
                    if test_case not in test_cases:
                        test_cases.append(test_case)
            if test_cases:
                item.add_marker(pytest.mark.testrail(ids=tuple(test_cases)))
