import datetime
import os, requests, json, base64

from test.shared_packages.reporting.abstract_reporter import AbstractReporter


class AllureReporter(AbstractReporter):
    def __init__(self, results_directory: str, project_id: str):
        self.project_id = project_id
        self.results_directory = results_directory
        self.allure_server = "http://vitro-simulator:5050/"

    def make_report(self):
        self.__handle_allure_data()
        self.__clean_previous_results()
        self.__send_results_to_allure_container()
        self.__generate_new_allure_report()

    def __handle_allure_data(self):
        print(f"results length: {len(os.listdir(self.results_directory))}")
        for file in os.listdir(self.results_directory):
            path_to_file = self.results_directory + "/" + file
            with open(path_to_file, "r", encoding="utf-8") as f:
                if "png" in path_to_file or "txt" in path_to_file:
                    continue
                else:
                    data = json.load(f)
                    if "parameters" in data:
                        del data["parameters"]
                        with open(path_to_file, "w", encoding="utf-8") as f:
                            f.write(json.dumps(data))

    def __clean_previous_results(self):
        ssl_verification = True
        headers = {"Content-type": "application/json"}
        url = (
            self.allure_server
            + "/allure-docker-service/clean-results?project_id="
            + self.project_id
        )
        response = requests.get(url=url, headers=headers, verify=ssl_verification)
        self.__handle_response(response, "Clean Results")

    def __send_results_to_allure_container(self):
        print("RESULTS DIRECTORY PATH: " + self.results_directory)

        files = os.listdir(self.results_directory)

        results = []

        for file in files:
            result = {}
            file_path = self.results_directory + "/" + file
            if os.path.isfile(file_path):
                try:
                    with open(file_path, "rb") as f:
                        content = f.read()
                        if content.strip():
                            b64_content = base64.b64encode(content)
                            result["file_name"] = file
                            result["content_base64"] = b64_content.decode("UTF-8")
                            results.append(result)
                        else:
                            print("Empty File skipped: " + file_path)
                finally:
                    f.close()
            else:
                print("Directory skipped: " + file_path)

        headers = {"Content-type": "application/json"}
        request_body = {"results": results}

        json_request_body = json.dumps(request_body)
        ssl_verification = True
        print("------------------SEND-RESULTS------------------")
        response = requests.post(
            self.allure_server
            + "/allure-docker-service/send-results?project_id="
            + self.project_id,
            headers=headers,
            data=json_request_body,
            verify=ssl_verification,
        )

        self.__handle_response(response, "Post new data")

    def __generate_new_allure_report(self):
        ssl_verification = True
        headers = {"Content-type": "application/json"}
        url = (
            self.allure_server
            + "/allure-docker-service/generate-report?project_id="
            + self.project_id
            + f"&execution_name={datetime.date}"
        )
        response = requests.get(url=url, headers=headers, verify=ssl_verification)
        self.__handle_response(response, "generate new report")

    def __handle_response(self, response, request: str):
        json_response_body = json.loads(response.content)
        json_prettier_response_body = json.dumps(
            json_response_body, indent=4, sort_keys=True
        )

        if response.status_code != 200:
            print("STATUS CODE:")
            print(response.status_code)
            print("RESPONSE:")
            print(json_prettier_response_body)
        else:
            print(f"Request to {request} to has been successful")
