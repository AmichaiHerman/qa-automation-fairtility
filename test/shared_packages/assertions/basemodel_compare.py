from typing import Protocol

from pydantic import BaseModel

from test.shared_packages.chloe_backend.patient_data.types.responses.treatment_response import (
    TreatmentDataResponse,
    Instrument,
    Slide,
)

from test.shared_packages.chloe_backend.patient_data.types.responses.treatment_response import (
    TreatmentDataResponse,
)


class BaseModelCompare(Protocol):
    """A protocol For Comparing 2 Pydantic Base Models"""

    def compare(self, object_a: BaseModel, object_b: BaseModel) -> bool:
        """A method for comparing lists from some BaseModel Objects"""
        pass

    def __sort(self):
        """A method for sorting the list of a certain model for some parameter"""
        pass
