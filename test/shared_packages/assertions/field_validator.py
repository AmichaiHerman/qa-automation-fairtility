from typing import Any

# todo for future use


def validate_eq(expected: Any, actual: Any) -> None:
    assert expected == actual, f"expected: {expected}, actual: {actual}"
