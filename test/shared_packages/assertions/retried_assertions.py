""" A functional util for handling flaky assertions """
from retry import retry


def assert_with_retry(
    assertion_func, exception_type=AssertionError, tries=2, delay=1.0
):
    @retry(exception_type, tries=tries, delay=delay)
    def execute_assertion():
        assertion_func()

    execute_assertion()
