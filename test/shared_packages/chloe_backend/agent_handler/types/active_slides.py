from __future__ import annotations

from typing import List

from pydantic import BaseModel


class ActiveSlides(BaseModel):
    esId: str
    patientEsId: str
    wells: List
    treatmentEsId: str
    fertilizationDate: str
    firstImageHour: int
