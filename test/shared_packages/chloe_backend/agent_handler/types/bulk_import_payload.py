from __future__ import annotations

from typing import Any, List, Optional

from pydantic import BaseModel


class Slide(BaseModel):
    esId: str
    wells: List[str]
    fertilizationDate: str
    firstImageHour: int
    slidePosition: int
    instrumentEsId: str
    slideDescriptionId: Any


class OocyteAge(BaseModel):
    years: int
    months: int


class Treatment(BaseModel):
    slides: List[Slide]
    esId: str
    created: str
    oocyteAge: Optional[OocyteAge]


class BulkImport(BaseModel):
    esId: str
    identifier1: str
    birthDate: Optional[str]
    treatments: List[Treatment]
    instrumentType: str


class BulkImportPayload(BaseModel):
    __root__: List[BulkImport]
