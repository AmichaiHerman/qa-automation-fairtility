from typing import List, Union

from requests import Response

from automation_sdk.http_requests.request_util import make_request
from automation_sdk.http_requests.http_client import HttpClient as http
from automation_sdk.http_requests.service_routes import ServiceRoutes
from automation_sdk.atomic_types import UserCredentials
from test.shared_packages.chloe_backend.agent_handler.types.active_slides import (
    ActiveSlides,
)
from test.shared_packages.chloe_backend.agent_handler.types.bulk_import_payload import (
    BulkImportPayload,
)


class AgentHandlerRoutes(ServiceRoutes):
    """
    An interface for connecting with Chloe's Agent Handler Service
    """

    def __init__(self, creds: UserCredentials):
        super().__init__(creds)
        self._service_prefix = self._base_url + "agent-handler/"
        self._patients_url = self._service_prefix + "patients/"
        self._slides_url = self._service_prefix + "slides/"

    @make_request(action="Perform Health Check")
    def check_health(self) -> Response:
        return http.get(url=self._patients_url + "healthz", headers=self._token)

    @make_request(action="Bulk Import Patients From Agent")
    def bulk_import_patients_from_agent(self, payload: BulkImportPayload) -> Response:
        return http.post(
            url=self._patients_url + "agent/bulk-import",
            payload=payload,
            headers=self._token,
            cut_pydantic_root=True,
        )

    @make_request(action="Get Active Slides", dto=ActiveSlides)
    def get_active_slides_for_tenant(self) -> Union[List[ActiveSlides], Response]:
        return http.get(url=self._slides_url + "active-slides", headers=self._token)
