from __future__ import annotations

from typing import List

from chloe.shared.types.request_data import RequestPayload


class CreateTimelapsePayload(RequestPayload):
    slideId: str
    wellEsId: str
    runNumber: int
    lastImageHour: int
    lastImageUrl: str
    instrumentType: str
    rootPath: str
    focals: List[int]
    imagesFormat: str
