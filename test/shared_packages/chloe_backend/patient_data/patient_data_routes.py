from typing import List, Union

from requests import Response

from automation_sdk.http_requests.http_client import HttpClient
from automation_sdk.http_requests.request_util import make_request
from automation_sdk.http_requests.service_routes import ServiceRoutes
from test.shared_packages.atomic_types import UserCredentials
from test.shared_packages.chloe_backend.patient_data.instrument_routes import (
    InstrumentRoutes,
)
from test.shared_packages.chloe_backend.patient_data.past_treatments_routes import (
    PastTreatmentsRoutes,
)
from test.shared_packages.chloe_backend.patient_data.patient_routes import (
    PatientsRoutes,
)
from test.shared_packages.chloe_backend.patient_data.report_routes import ReportRoutes
from test.shared_packages.chloe_backend.patient_data.treatments_routes import (
    TreatmentRoutes,
)
from test.shared_packages.chloe_backend.patient_data.types.requests_data.get_params import (
    GetPatientQueryParams,
    GetKPIReportsQueryParams,
    GetWellQueryParams, TreatmentNameParam,
)

from test.shared_packages.chloe_backend.patient_data.types.responses.emr_wells import EMRWellsResponse
from test.shared_packages.chloe_backend.patient_data.types.responses.kpi_dashboard_response import KpiDashboardResponse
from test.shared_packages.chloe_backend.patient_data.types.responses.kpi_report_response import KPIReportResponse
from test.shared_packages.chloe_backend.patient_data.types.responses.patients_emr_response import PatientsEMRResponse

from test.shared_packages.chloe_backend.patient_data.wells_routes import WellsRoutes


class PatientDataRoutes(ServiceRoutes):
    """
    An interface for connecting with Chloe's Patient-Data Service
    """

    def __init__(self, creds: UserCredentials):
        super().__init__(creds)
        self.emr_route = self._base_url + "v1/emr/"
        self.emr_patients_route = self.emr_route + "patients"
        self.emr_treatments_route = self.emr_route + "treatments"
        self.emr_treatments_link = self.emr_route + "treatments/link"
        self.emr_wells_route = self.emr_route + "wells"
        self.notes_route = self._base_url + "notes/"
        self.past_csv = self._base_url + "treatments/past/csv"
        self.treatment_link = self._base_url + "treatments/link"
        self.reports_route = ReportRoutes(creds, self._base_url)
        self.patients_route = PatientsRoutes(creds, self._base_url)
        self.treatment_route = TreatmentRoutes(creds, self._base_url)
        self.instrument_route = InstrumentRoutes(creds, self._base_url)
        self.wells_route = WellsRoutes(creds, self._base_url)
        self.past_treatments_routes = PastTreatmentsRoutes(creds, self._base_url)

    @make_request(action="Check Health")
    def check_health(self) -> Response:
        return HttpClient.get(url=self._base_url + "/healthz", headers=self._token)

    @make_request(action="Get treatment link")
    def get_treatment_link(self, treatment_es_id: str, identifier1: str):
        return HttpClient.get(
            url=f"{self.treatment_link}?treatmentName={treatment_es_id}&patientIdentifier1={identifier1}",
            headers=self._token,
        )

    @make_request(action="Get past csv")
    def get_past_csv(self, start_date: str, end_date: str) -> Response:
        return HttpClient.get(
            url=f"{self.past_csv}?start={start_date}&end={end_date}",
            headers=self._token,
        )

    @make_request(action="GET all EMR Patients", dto=PatientsEMRResponse)
    def get_all_emr_patients(self) -> Union[Response, List[PatientsEMRResponse]]:
        return HttpClient.get(url=self.emr_patients_route, headers=self._token)
        # return HttpClient.get(url=self.emr_patients_route, headers=self._token)

    @make_request(action="GET EMR Patients by patient id", dto=PatientsEMRResponse)
    def get_emr_patients_by_id(
            self, patient_id: str
            # self, params: GetPatientQueryParams
    ) -> Union[Response, PatientsEMRResponse]:
        return HttpClient.get(
            url=f"{self.emr_patients_route}?patientId={patient_id}", headers=self._token
            # url = self.emr_patients_route + "?patientId=" + params.patient_id, headers = self._token
        )

    @make_request(action="GET all EMR Wells", dto=EMRWellsResponse)
    def get_all_emr_wells(self, treatment_id: str) -> Union[Response, List[EMRWellsResponse]]:
        return HttpClient.get(self.emr_wells_route + "/?treatmentId=" + treatment_id, self._token)

    @make_request(action="GET one EMR Well")
    def get_one_emr_well(self, params: GetWellQueryParams) -> Union[Response, Response]:
        return HttpClient.get(self.emr_wells_route + "/" + params.well_id, self._token)

    @make_request(action="GET one EMR Well")
    def get_treatment_link(self, params: [TreatmentNameParam, GetPatientQueryParams]) -> Union[Response, Response]:
        return HttpClient.get(
            url=self.emr_treatments_link + "?treatmentName=" + params.treatment_name + "&patientIdentifier1=" + params.
            patient_id, headers=self._token
        )

    @make_request(action="GET KPI Reports", dto=KPIReportResponse)
    def get_kpi_reports(
            self, params: GetKPIReportsQueryParams
    ) -> Union[Response, List[KPIReportResponse]]:
        return HttpClient.get(self.reports_route + params.__str__(), headers=self._token)


# ↓↓↓ Methods ↓↓↓
def validate_year_in_kpi_report(resp: KpiDashboardResponse, year: int):
    for kpi_resp in resp:
        if kpi_resp.missingDataWells:
            for missing_data in kpi_resp.missingDataWells:
                year_string = missing_data.slideEsId[1:]
                dot_index = year_string.index(".")
                year_number = int(year_string[:dot_index])
                assert year_number == year
