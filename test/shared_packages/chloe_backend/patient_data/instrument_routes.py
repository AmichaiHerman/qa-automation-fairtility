import http
from typing import Union, List

from requests import Response
from automation_sdk.http_requests.http_client import HttpClient as http
from automation_sdk.http_requests.request_util import make_request
from automation_sdk.http_requests.service_routes import ServiceRoutes
from test.shared_packages.atomic_types import UserCredentials
from test.shared_packages.chloe_backend.patient_data.types.requests_data.get_params import (
    InstrumentGetParam,
)
from test.shared_packages.chloe_backend.patient_data.types.requests_data.payloads import (
    ChangeInstrumentName,
)
from test.shared_packages.chloe_backend.patient_data.types.responses.instruments_response import (
    InstrumentsDataResponse,
)


class InstrumentRoutes(ServiceRoutes):

    def __init__(self, creds: UserCredentials, base_url: str):
        super().__init__(creds)
        self.__instrument_base_url = base_url + "instruments"

    @make_request(action="GET all instruments", dto=InstrumentsDataResponse)
    def get_all_instruments(self) -> Union[Response, List[InstrumentsDataResponse]]:
        return http.get(url=self.__instrument_base_url, headers=self._token)

    @make_request(action="Change Instrument Name", dto=InstrumentsDataResponse)
    def change_instrument_name(
        self, params: InstrumentGetParam, payload: ChangeInstrumentName
    ) -> Union[Response, InstrumentsDataResponse]:
        return http.patch(
            url=self.__instrument_base_url + "/" + params.instrument_id,
            payload=payload,
            headers=self._token,
        )
