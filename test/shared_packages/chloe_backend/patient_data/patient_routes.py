from typing import List, Union
from requests import Response

from automation_sdk.http_requests.request_util import make_request
from automation_sdk.http_requests.service_routes import ServiceRoutes
from test.shared_packages.atomic_types import UserCredentials
from test.shared_packages.chloe_backend.patient_data.types.requests_data.payloads import PostSharedDataLink
from test.shared_packages.chloe_backend.patient_data.types.responses.patients_response import (
    SharedDataLinkResp,
    PatientDataResponse,
)
from automation_sdk.http_requests.http_client import HttpClient


class PatientsRoutes(ServiceRoutes):

    def __init__(self, creds: UserCredentials, base_url: str):
        super().__init__(creds)
        self.__patients_route_url = base_url + "patients"

    @make_request(action="Perform Health Check")
    def check_health(self) -> Response:
        return HttpClient.get(url=self.__patients_route_url + "/healthz", headers=self._token)

    @make_request(action="GET all patients", dto=PatientDataResponse)
    def get_all_patients(self) -> Union[Response, List[PatientDataResponse]]:
        return HttpClient.get(url=self.__patients_route_url, headers=self._token)

    @make_request(action="Post shared data link", dto=SharedDataLinkResp)
    def post_shared_data(
        self, payload: PostSharedDataLink
    ) -> Union[Response, SharedDataLinkResp]:
        return HttpClient.post(
            url=f"{self.__patients_route_url}/63df780db6d2f6a9bd1e4772/share-link",
            payload=payload,
            headers=self._token,
        )

    @make_request(action="GET one patient", dto=PatientDataResponse)
    def get_one_patient(self, patient_id: str) -> Union[Response, PatientDataResponse]:
        print(self.__patients_route_url + "/" + patient_id)
        print(self._token)
        return HttpClient.get(self.__patients_route_url + "/" + patient_id, self._token)
