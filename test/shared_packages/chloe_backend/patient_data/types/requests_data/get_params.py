from typing import Optional, List

from chloe.shared.types.request_data import QueryParams


class GetPatientQueryParams(QueryParams):
    patient_id: str


class GetTreatmentQueryParams(QueryParams):
    treatment_id: str


class GetWellQueryParams(QueryParams):
    well_id: str


class GetLanguageQueryParams(QueryParams):
    language: str


class InstrumentGetParam(QueryParams):
    instrument_id: str


class TreatmentNameParam(QueryParams):
    treatment_name: str


class GetFinishedSlidesQueryParams(QueryParams):
    page: Optional[int] = 1
    page_size: Optional[int] = 51
    start_date: Optional[str] = None
    end_date: Optional[str] = None
    esid: Optional[str] = None

    def __str__(self) -> str:
        base_query_string: str = "?"

        # assign query params by assignment
        if self.page is not None:
            base_query_string += f"page={self.page}&"
        if self.page_size is not None:
            base_query_string += f"pageSize={self.page_size}&"
        if self.start_date is not None:
            base_query_string += f"start={self.start_date}&"
        if self.end_date is not None:
            base_query_string += f"end={self.end_date}&"
        if self.esid is not None:
            base_query_string += f"_search={self.esid}&"

        # remove the final & from the string
        return base_query_string[:-1]


class GetKPIReportsQueryParams(QueryParams):
    start_date: Optional[str] = None
    end_date: Optional[str] = None
    instrument_types: Optional[str] = None
    instrument_ids: Optional[List[str]] = None
    age_groups_ids: Optional[List[str]] = None

    def __str__(self) -> str:
        base_query_string = "?"

        # assign query params by assignment
        if self.start_date is not None:
            base_query_string += f"start={self.start_date}&"
        if self.end_date is not None:
            base_query_string += f"end={self.end_date}&"
        if self.instrument_types is not None:
            base_query_string += f"instrumentTypes={self.instrument_types}&"
        if self.instrument_ids is not None:
            base_query_string += f"instrumentIds={self.instrument_ids}&"
        if self.age_groups_ids is not None:
            base_query_string += f"ageGroupIds={self.age_groups_ids}&"

        # remove the final & from the string
        return base_query_string[:-1]
