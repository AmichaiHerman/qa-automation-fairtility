from typing import Any, Optional

from chloe.shared.types.request_data import RequestPayload
from test.shared_packages.chloe_backend.patient_data.types.responses.kpi_dashboard_response import (
    MissingDataWell,
)


class ChangePatientAge(RequestPayload):
    patientAge: int


class ChangeOocyteAge(RequestPayload):
    oocyteAge: int


class InferenceOverriding(RequestPayload):
    pass


class PnCountChange(InferenceOverriding):
    value: int
    time: str


class DUC(InferenceOverriding):
    duc1: bool


class DEG(InferenceOverriding):
    degeneratedOocyte: bool


class PnCountPayload(InferenceOverriding):
    pnCount: PnCountChange


class DucPayload(InferenceOverriding):
    duc: DUC


class ChangeMedicalDecision(RequestPayload):
    status: str


class ChangeInstrumentName(RequestPayload):
    name: str


class ChangePGTA(RequestPayload):
    pgta: str


class ClinicGardner(RequestPayload):
    expansion: str
    icm: str
    te: str


class ChangeGardnerPayload(RequestPayload):
    clinicGardner: ClinicGardner


class PostNote(RequestPayload):
    authorId: str
    content: str
    entityName: str
    relatedDocId: str
    tenantId: str


class PostSharedDataLink(RequestPayload):
    treatmentId: str
    tenantId: str
    birthYear: int
    email: str


class ChangeAgesPayload(RequestPayload):
    oocyteAge: Optional[int]
    patientAge: Optional[int]


class ChangeLanguageAndColorPayload(RequestPayload):
    language: str
    color: str


class AddAgeGroup(RequestPayload):
    min: int
    max: int
