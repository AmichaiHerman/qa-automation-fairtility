from __future__ import annotations

from typing import Optional

from pydantic import BaseModel, Field


class KpiAgeGroupsAdded(BaseModel):
    max: Optional[int]
    age_group_id: str = Field(..., alias="_id")
    min: int
