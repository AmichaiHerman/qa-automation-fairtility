from __future__ import annotations

from typing import Any, List, Optional

from pydantic import BaseModel


class Treatment(BaseModel):
    _id: str
    oocyteAge: Any
    patientAge: int


class Confidence(BaseModel):
    blastScoreTP: float
    kidScoreTP: float
    kidScoreProb: float


class PnCount(BaseModel):
    value: int
    time: str


class Morphokinetic(BaseModel):
    _id: str
    systemName: str
    value: float


class Duc(BaseModel):
    duc1: bool
    duc2: bool
    duc3: bool


class InferenceData(BaseModel):
    blastScore: float
    kidScore: float
    mkScore: float
    confidence: Confidence
    pnCount: PnCount
    fragmentationTime: Any
    degeneratedOocyteTime: Any
    morphokinetics: List[Morphokinetic]
    isValidT: bool
    duc: Duc
    updatedAt: str
    createdAt: str


class InferenceOverridingData(BaseModel):
    createdAt: str
    updatedAt: str


class InferenceAdminOverridingData(BaseModel):
    pnCount: Any
    createdAt: str
    updatedAt: str


class HighItem(BaseModel):
    focal: int
    videoUrl: str
    videoLength: int


class LowItem(BaseModel):
    focal: int
    videoUrl: str
    videoLength: int


class Videos(BaseModel):
    high: List[HighItem]
    low: List[LowItem]
    fps: int


class _Well(BaseModel):
    _id: str
    inferenceData: InferenceData
    inferenceOverridingData: InferenceOverridingData
    inferenceAdminOverridingData: InferenceAdminOverridingData
    status: str
    like: Any
    pgta: Any
    clinicGardner: Any
    tenantId: str
    treatmentId: str
    patientId: str
    slideId: str
    esId: str
    lastImageUrl: str
    videos: Videos
    createdAt: str
    updatedAt: str
    __v: int


class Morphokinetic1(BaseModel):
    _id: str
    systemName: str
    value: float


class InferenceData1(BaseModel):
    blastScore: float
    kidScore: float
    morphokinetics: List[Morphokinetic1]


class BestWell(BaseModel):
    _id: str
    tenantId: str
    patientId: str
    treatmentId: str
    esId: str
    slideId: str
    lastImageUrl: str
    baseImageUrl: str
    _well: _Well
    inferenceData: InferenceData1


class EarlyStageData(BaseModel):
    wellsInPNa: int
    wellsInPNf: int
    wellsIn2C: int


class Slide(BaseModel):
    esId: str
    _id: str
    tenantId: str
    treatment: Treatment
    patientId: str
    wellsCount: int
    hoursSinceFertilization: float
    slidePosition: int
    fertilizationDate: str
    isEarlyStage: bool
    slideDescriptionId: Any
    bestWells: Optional[List[BestWell]] = None
    earlyStageData: Optional[EarlyStageData] = None


class InstrumentsDataResponse(BaseModel):
    _id: str
    instrumentSize: Optional[int]
    name: Optional[str] = None
    esId: Optional[str]
    tenantId: Optional[str]
    type: Optional[str]
    __v: int
    slides: Optional[List[Slide]]
