from __future__ import annotations

from typing import Any, List, Optional


from chloe.shared.types.abstract_response import HttpResponse


class Slide(HttpResponse):
    tenantId: str
    numberOfRuns: int
    videoLength: float
    firstRunTime: int
    esId: str
    delayBetweenCycles: int
    fertilizationTime: Optional[str]
    hoursSinceFertilization: int
    lastImageHour: int
    instrumentId: str
    # wells: List[Optional[WellData]]


class Instrument(HttpResponse):
    esId: str
    slides: List[Slide]


class TreatmentDataResponse(HttpResponse):
    availableFocals: List[int]
    transfers: Optional[List]
    oocyteAge: Optional[int]
    startDate: str
    endDate: Any
    slides: List
    patientAge: int
    status: str
    tenantId: str
    patientId: str
    instruments: List[Instrument]
