from typing import List, Optional, Any

from pydantic import Field

from chloe.shared.types.abstract_response import HttpResponse


class HighItem(HttpResponse):
    focal: int
    videoUrl: str
    videoLength: float


class LowItem(HttpResponse):
    focal: int
    videoUrl: str
    videoLength: float


class Videos(HttpResponse):
    high: List[HighItem]
    low: List[LowItem]
    fps: Optional[int]


class PnCount(HttpResponse):
    value: int
    time: Optional[float]


class Duc(HttpResponse):
    duc1: bool


class InferenceOverridingData(HttpResponse):
    createdAt: str
    updatedAt: str
    pnCount: Optional[PnCount]
    duc: Optional[Duc]
    degeneratedOocyte: Optional[bool]


class Run(HttpResponse):
    time: float


class Confidence(HttpResponse):
    blastScoreTP: Optional[float]
    kidScoreTP: Optional[float]
    kidScoreProb: Optional[float]


class RangeItem(HttpResponse):
    time: float
    color: str


class Morphokinetic(HttpResponse):
    id: str = Field(..., alias="_id")
    systemName: str
    value: float
    delta: float
    range: List[RangeItem]


class Segmentation(HttpResponse):
    pn: Optional[List[List[List[float]]]] = None


class InferenceData(HttpResponse):
    blastScore: Any
    blastScoreOverTime: Any
    kidScore: Any
    kidScoreOverTime: Any
    mkScore: float
    confidence: Confidence
    pnCount: PnCount
    fragmentationTime: Any
    degeneratedOocyteTime: Any
    morphokinetics: List[Morphokinetic]
    isValidT: bool
    duc: Duc
    predictMorphokinetics: List
    segmentations: Optional[List[Segmentation]]
    updatedAt: str
    createdAt: str
    degeneratedOocyte: bool
