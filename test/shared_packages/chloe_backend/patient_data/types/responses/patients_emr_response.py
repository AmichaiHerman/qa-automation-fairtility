from typing import List, Optional

from pydantic import BaseModel


class EMRTreatment(BaseModel):
    _id: str
    name: str
    startDate: str
    instrumentType: Optional[list[str]] = None


class PatientsEMRResponse(BaseModel):
    _id: str
    patientId: Optional[str] = None
    esId: Optional[str] = None
    identifier1: Optional[str] = None
    treatments: Optional[List[EMRTreatment]]=None
