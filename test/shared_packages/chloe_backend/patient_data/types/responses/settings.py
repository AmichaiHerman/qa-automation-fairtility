from __future__ import annotations
from typing import List
from pydantic import BaseModel


class Settings(BaseModel):
    authorizedOrigins: List
    authEnabled: bool
    isFDA: bool
    blastScoreLow: int
    blastScoreHigh: int
    kidScoreLow: int
    kidScoreHigh: int
    instrumentSize: int
    slideSize: int
    availableMkEvents: List[str]
    mainEntity: str
    isVRepro: bool


class SettingsModel(BaseModel):
    settings: Settings
    language: str
    color: str
    _id: str
    name: str
    createdAt: str
    updatedAt: str
    __v: int


class KpiSettings(BaseModel):
    _id: str
    kpiName: str
    kpiType: str
    benchmark: int
    competency: int
    benchmarkCondition: str
    competencyCondition: str
    favourite: bool
