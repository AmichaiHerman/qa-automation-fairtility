from __future__ import annotations

from typing import Any, List, Optional

from pydantic import BaseModel


class Metadata(BaseModel):
    page: int
    pageSize: int
    totalCount: int
    pageCount: int


class Treatment(BaseModel):
    _id: str
    oocyteAge: Any


class EarlyStageData(BaseModel):
    wellsInPNa: int
    wellsInPNf: int
    wellsIn2C: int


class SlideCard(BaseModel):
    esId: str
    _id: str
    tenantId: str
    treatment: Treatment
    patientId: str
    wellsCount: int
    hoursSinceFertilization: int
    slidePosition: int
    fertilizationDate: str
    isEarlyStage: bool
    slideDescriptionId: Any
    earlyStageData: Optional[EarlyStageData]


class FinishedSlidesSearchResult(BaseModel):
    metadata: Metadata
    slideCards: List[SlideCard]

    @property
    def slides_esid_list(self) -> List[str]:
        return [slide.esId for slide in self.slideCards]
