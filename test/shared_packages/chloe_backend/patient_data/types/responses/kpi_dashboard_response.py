from __future__ import annotations

from typing import List, Optional

from pydantic import BaseModel


class MissingDataWell(BaseModel):
    id: str
    esId: str
    instrumentType: str
    treatmentEsId: str
    treatmentId: str
    patientId: str
    slideEsId: str
    slidePosition: int
    missingData: str


class KpiDashboardResponse(BaseModel):
    name: Optional[str]
    passed: Optional[int]
    total: Optional[int]
    ratio: Optional[int]
    missingDataWells: Optional[List[MissingDataWell]] = None


class Model(BaseModel):
    __root__: List[KpiDashboardResponse]
