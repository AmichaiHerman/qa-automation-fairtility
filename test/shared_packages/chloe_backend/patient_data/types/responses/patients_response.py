from __future__ import annotations

from typing import Any, List, Optional

from chloe.shared.types.abstract_response import HttpResponse
from pydantic import BaseModel


class OocyteAgeItem(HttpResponse):
    years: int
    months: int
    _id: Optional[str] = None
    updatedAt: Optional[str] = None
    createdAt: Optional[str] = None


class Well(HttpResponse):
    esId: str
    kidRank: int


class PatientsSlideResponse(HttpResponse):
    id: str
    wells: List[Well]

    @property
    def number_of_wells(self):
        return len(self.wells)


class Treatment(HttpResponse):
    transfers: Optional[List]
    oocyteAge: Optional[Optional[OocyteAgeItem]] = None
    startDate: Optional[str] = None
    endDate: Optional[Any] = None
    slides: List[PatientsSlideResponse]
    patientAge: Optional[str]


class PatientDataResponse(BaseModel):
    birthDate: Optional[str]
    tenantId: str
    esId: str
    status: Optional[str] = None
    fullName: Optional[str] = None
    bmi: Optional[str] = None
    govId: Optional[str] = None
    treatments: List[Treatment]
    __v: int


class SharedDataLinkResp(HttpResponse):
    cipher: str
    appUrl: str
