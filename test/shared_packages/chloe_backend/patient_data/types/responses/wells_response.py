from __future__ import annotations

from typing import Any, List, Optional

from pydantic import Field


from chloe.shared.types.abstract_response import HttpResponse
from test.shared_packages.chloe_backend.patient_data.types.requests_data.payloads import (
    ClinicGardner,
)

from test.shared_packages.chloe_backend.patient_data.types.responses.shared_dtos import (
    Run,
    InferenceData,
    InferenceOverridingData,
    Videos,
)


class Insight(HttpResponse):
    _id: str = Field(alias="_id")
    contentLanguageKey: str
    atHour: Any
    contentParamValues: Any


class WellData(HttpResponse):
    _id: str = Field(alias="_id")
    tenantId: str
    patientId: str
    treatmentId: str
    esId: str
    slideId: str
    lastImageUrl: str
    baseImageUrl: str
    videoFramesTime: List[int]
    videos: Videos
    inferenceOverridingData: InferenceOverridingData
    status: Any
    runs: List[Run]
    inferenceData: InferenceData
    pgta: Any
    clinicGardner: Optional[ClinicGardner]
    insights: Optional[List[Insight]]
