from __future__ import annotations

from typing import List, Optional

from pydantic import BaseModel


class KPIReportResponse(BaseModel):
    name: str
    passed: int
    total: int
    ratio: int
    missingDataWells: Optional[List] = None

    @staticmethod
    def fetch_kpi_data(
        response: List[KPIReportResponse], kpi_name: str
    ) -> KPIReportResponse:
        filtered_responses = filter(
            lambda response: response.name == kpi_name, response
        )
        for response in filtered_responses:
            return response  # Return the first matching response
        raise Exception(f"KPI Report with name {kpi_name} not found")
