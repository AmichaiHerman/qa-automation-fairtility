from __future__ import annotations

from typing import Any, Dict, List, Optional

from pydantic import BaseModel


class Instrument(BaseModel):

    type: Optional[str]
    id: Optional[str] = None
    name: Optional[str] = None


class Morphokinetic(BaseModel):
    event: str
    value: float


class InferenceData(BaseModel):
    morphokinetics: List[Morphokinetic]
    kidScore: Optional[float]
    blastScore: Optional[float]
    kidRank: Optional[int] = None
    pnCount: Optional[int]
    pnCountTime: float
    degeneratedOocyte: bool
    duc1: bool
    fragmentationTime: int


class EMRWellsResponse(BaseModel):
    wellNumber: str
    _id: str
    slideEsId: str
    instrument: Instrument
    status: Optional[str]
    lastImageUrl: str
    inferenceData: InferenceData
    inferenceOverridingData: Dict[str, Any]


class EMRWellsCompare:
    """Implements BaseModelCompare protocol"""

    def compare(self, input_resp: List[EMRWellsResponse], expected_resp: List[EMRWellsResponse]) -> bool:
        # n_input_resp = self.__sort(input_resp)
        # n_expected_resp = self.__sort(expected_resp)
        #
        # if len(n_input_resp) != len(n_expected_resp):
        #     return False
        #
        # for i in range(len(n_input_resp)):
        #     if not n_input_resp[i] == n_expected_resp[i]:
        #         return False
        # return True
        return self.__sort(input_resp) == self.__sort(expected_resp)

    @staticmethod
    def __sort(obj: List[EMRWellsResponse]) -> List[EMRWellsResponse]:
        return sorted(obj, key=lambda emr_well: emr_well.wellNumber)
