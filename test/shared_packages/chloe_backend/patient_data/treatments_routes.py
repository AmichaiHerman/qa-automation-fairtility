import http
from typing import Union, List
from automation_sdk.http_requests.service_routes import ServiceRoutes
from test.shared_packages.atomic_types import UserCredentials
from requests import Response

from automation_sdk.http_requests.request_util import make_request
from test.shared_packages.chloe_backend.patient_data.types.requests_data.payloads import ChangeAgesPayload
from test.shared_packages.chloe_backend.patient_data.types.responses.treatment_response import (
    TreatmentDataResponse,
)
from automation_sdk.http_requests.http_client import HttpClient as http


class TreatmentRoutes(ServiceRoutes):
    def __init__(self, creds: UserCredentials, base_url: str):
        super().__init__(creds)
        self.__treatment_base_url = base_url + "treatments/"

    @make_request(action="Change ages", dto=TreatmentDataResponse)
    def change_ages_no_func(
            self, treatment_id: str, payload: ChangeAgesPayload
    ) -> Union[Response, List[TreatmentDataResponse]]:
        headers = {"token": self._token}
        return http.patch(self.__treatment_base_url + treatment_id, payload, headers)

    @make_request(action="GET one treatment", dto=TreatmentDataResponse)
    def get_one_treatment(self, treatment_id: str) -> Union[Response, TreatmentDataResponse]:
        return http.get(url=self.__single_treatment_url(treatment_id), headers=self._token)

    @make_request(action="PATCH ages", dto=TreatmentDataResponse)
    def change_ages(self, treatment_id: str, payload: ChangeAgesPayload):
            # self, params: Union[GetTreatmentQueryParams, ChangePatientAge, ChangeOocyteAge], payload: ChangeAgesPayload
        return http.patch(self.__single_treatment_url(treatment_id),  payload, self._token)

    def __single_treatment_url(self, treatment_id: str) -> str:
        return self.__treatment_base_url + treatment_id
