from typing import Union

from requests import Response
from automation_sdk.http_requests.service_routes import ServiceRoutes
from test.shared_packages.atomic_types import UserCredentials
from automation_sdk.http_requests.request_util import make_request
from test.shared_packages.chloe_backend.patient_data.types.requests_data.get_params import GetFinishedSlidesQueryParams
from test.shared_packages.chloe_backend.patient_data.types.responses.finished_slides import FinishedSlidesSearchResult
from automation_sdk.http_requests.http_client import HttpClient


class PastTreatmentsRoutes(ServiceRoutes):

    def __init__(self, creds: UserCredentials, base_url: str):
        super().__init__(creds)
        self.__past_treatments_base_url = base_url + "/slides"

    @make_request(
        action="GET finished treatments by filter", dto=FinishedSlidesSearchResult
    )
    def get_finished_slides(self, params: GetFinishedSlidesQueryParams) -> Union[Response, FinishedSlidesSearchResult]:
        url_with_filter = self.__past_treatments_base_url + params.__str__()
        return HttpClient.get(url_with_filter, self._token)
