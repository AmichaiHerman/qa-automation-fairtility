from typing import List, Union, Dict
from requests import Response
from automation_sdk.http_requests.http_client import HttpClient
from automation_sdk.http_requests.request_util import make_request
from automation_sdk.http_requests.service_routes import ServiceRoutes
from test.shared_packages.atomic_types import UserCredentials
from test.shared_packages.chloe_backend.patient_data.types.responses.kpi_dashboard_response import (
    KpiDashboardResponse,
)


class ReportRoutes(ServiceRoutes):
    def __init__(self, creds: UserCredentials, base_url: str):
        super().__init__(creds)
        self.__report_routes_url = base_url + "reports"

    @make_request(action="Get kpi_dashboard", dto=KpiDashboardResponse)
    def get_kpi_report(self) -> Union[Response, List[KpiDashboardResponse]]:
        return HttpClient.get(url=self.__report_routes_url, headers=self._token)

    @make_request(action="Get KPI Report Filtered By Date", dto=KpiDashboardResponse)
    def get_kpi_report_filter_by_date(
        self, start_date: str, end_date: str
    ) -> Union[Response, List[KpiDashboardResponse]]:
        return HttpClient.get(
            url=self.__report_routes_url + f"?start={start_date}&end={end_date}",
            headers=self._token,
        )

    @make_request(
        action="Get KPI Report Filtered By Age Group", dto=KpiDashboardResponse
    )
    def get_kpi_report_filter_by_age_group(self, age_group_id: str) -> Union[Response, List[KpiDashboardResponse]]:
        return HttpClient.get(self.__report_routes_url + f"?ageGroupIds={age_group_id}", self._token)

    @make_request(action="Get KPI Report Filtered By TLI", dto=KpiDashboardResponse)
    def get_kpi_report_filter_by_tli(self, instrument_type: str) -> Union[Response, List[KpiDashboardResponse]]:
        return HttpClient.get(self.__report_routes_url + f"?instrumentTypes={instrument_type}", self._token)
