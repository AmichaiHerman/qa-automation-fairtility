from typing import Union

from requests import Response
from automation_sdk.http_requests.service_routes import ServiceRoutes
from automation_sdk.http_requests.request_util import make_request
from test.shared_packages.atomic_types import UserCredentials
from test.shared_packages.chloe_backend.patient_data.types.requests_data.get_params import GetWellQueryParams

from test.shared_packages.chloe_backend.patient_data.types.requests_data.payloads import (
    InferenceOverriding,
    ChangeMedicalDecision,
    ChangeGardnerPayload,
    ChangePGTA,
)
from test.shared_packages.chloe_backend.patient_data.types.responses.wells_response import (
    WellData,
)
from automation_sdk.http_requests.http_client import HttpClient as http


class WellsRoutes(ServiceRoutes):

    def __init__(self, creds: UserCredentials, base_url: str):
        super().__init__(creds)
        self.__wells_base_url = base_url + "wells"

    @make_request(action="PATCH Inference Overriding", dto=WellData)
    def activate_inference_overriding(
        self, params: GetWellQueryParams, payload: InferenceOverriding
    ) -> Union[Response, WellData]:
        return http.patch(
            url=self.__wells_base_url + "/" + params.well_id + "/inference-overriding",
            payload=payload,
            headers=self._token,
        )

    @make_request(action="Change medical decision", dto=WellData)
    def change_medical_decision(
        self, params: GetWellQueryParams, payload: ChangeMedicalDecision
    ) -> Union[Response, WellData]:
        return http.patch(
            url=self.__single_well_url(params), payload=payload, headers=self._token
        )

    @make_request(action="GET one well", dto=WellData)
    def get_one_well(self, params: GetWellQueryParams) -> Union[Response, WellData]:
        return http.get(url=self.__single_well_url(params), headers=self._token)

    @make_request(action="Change gardner", dto=WellData)
    def change_gardner(self, well_id: str, payload: ChangeGardnerPayload) -> Union[Response, WellData]:
        print(self.__wells_base_url + f"/{well_id}")
        return http.patch(
            url=self.__wells_base_url + f"/{well_id}",
            payload=payload,
            headers=self._token,
        )

    @make_request(action="Change pgta", dto=WellData)
    def change_pgta(
        self, params: GetWellQueryParams, payload: ChangePGTA
    ) -> Union[Response, WellData]:
        return http.patch(
            url=self.__single_well_url(params), payload=payload, headers=self._token
        )

    def __single_well_url(self, params: GetWellQueryParams) -> str:
        return self.__wells_base_url + "/" + params.well_id
