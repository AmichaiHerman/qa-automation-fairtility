from __future__ import annotations

from typing import List

from pydantic import BaseModel, Field


class Settings(BaseModel):
    authorizedOrigins: List
    authEnabled: bool
    isFDA: bool
    blastScoreLow: int
    blastScoreHigh: int
    kidScoreLow: int
    kidScoreHigh: int
    instrumentSize: int
    slideSize: int
    availableMkEvents: List[str]
    mainEntity: str
    isVRepro: bool


class TenantsResponse(BaseModel):
    settings: Settings
    language: str
    tenant_id: str = Field(..., alias="_id")
    name: str
    createdAt: str
    updatedAt: str
    __v: int
