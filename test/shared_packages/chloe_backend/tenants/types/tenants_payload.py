from pydantic import BaseModel


class TenantPayloadSettings(BaseModel):
    blastScoreLow: int = 20
    blastScoreHigh: int = 70
    kidScoreLow: int = 20
    kidScoreHigh: int = 70
    instrumentSize: int = 9
    slideSize: int = 12


class TenantsPayload(BaseModel):
    name: str
    settings: TenantPayloadSettings
