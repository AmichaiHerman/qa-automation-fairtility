from typing import Union, List, Dict

from requests import Response

from automation_sdk.http_requests.request_util import make_request
from automation_sdk.http_requests.service_routes import ServiceRoutes
from automation_sdk.mongo.mongo_orm.agent_handler.settings import Settings
from test.shared_packages.atomic_types import UserCredentials
from test.shared_packages.chloe_backend.patient_data.types.requests_data.payloads import (
    ChangeLanguageAndColorPayload,
    AddAgeGroup,
)
from test.shared_packages.chloe_backend.patient_data.types.responses.Kpi_age_groups_resp import (
    KpiAgeGroupsAdded,
)
from test.shared_packages.chloe_backend.patient_data.types.responses.kpi_dashboard_response import (
    KpiDashboardResponse,
)
from test.shared_packages.chloe_backend.patient_data.types.responses.settings import (
    SettingsModel,
    KpiSettings,
)

from test.shared_packages.chloe_backend.tenants.types.tenant_response import (
    TenantsResponse,
)
from automation_sdk.http_requests.http_client import HttpClient as http
from test.shared_packages.chloe_backend.tenants.types.tenants_payload import (
    TenantsPayload,
)


class TenantsRoutes(ServiceRoutes):
    """
    An interface for connecting with Chloe's Tenants Service
    """

    def __init__(self, creds: UserCredentials):
        super().__init__(creds)
        self._base_url = self._base_url + "/tenants"

    @make_request(action="Perform health check")
    def check_health(self) -> Response:
        return http.get(url=self._base_url + "/healthz", headers=self._token)

    @make_request(action="Create Tenant", dto=TenantsResponse)
    def create_tenant(
        self, payload: TenantsPayload
    ) -> Union[TenantsResponse, Response]:
        return http.post(url=self._base_url, payload=payload, headers=self._token)

    @make_request(action="Update Tenant", dto=TenantsResponse)
    def update_tenant(
            self, tenant_id: str, payload: TenantsPayload
    ) -> Union[TenantsResponse, Response]:
        return http.post(url=self._base_url + f"/{tenant_id}", payload=payload, headers=self._token)

    @make_request(action="Change language and color", dto=SettingsModel)
    def change_language_and_color(
        self, tenantId: str, payload: ChangeLanguageAndColorPayload
    ) -> Union[Response, SettingsModel]:
        return http.patch(
            url=f"{self._base_url}/{tenantId}", payload=payload, headers=self._token
        )

    @make_request(action="Get KPI Settings", dto=KpiSettings)
    def get_kpi_settings(self) -> Union[Response, List[KpiSettings]]:
        return http.get(url=self._base_url + "/kpi-settings", headers=self._token)

    @make_request(action="Add KPI age group", dto=KpiAgeGroupsAdded)
    def post_kpi_age_group(self, payload: AddAgeGroup) -> Response:
        return http.post(
            url=self._base_url + "/age-groups", payload=payload, headers=self._token
        )

    @make_request(action="Delete KPI age group")
    def delete_kpi_age_group(self, age_group_id: str):
        return http.delete(
            url=self._base_url + "/age-groups/" + age_group_id, headers=self._token
        )

    @make_request(action="Get all age-groups", dto=KpiAgeGroupsAdded)
    def get_kpi_age_groups(self) -> Union[Response, List[KpiAgeGroupsAdded]]:
        return http.get(url=self._base_url + "/age-groups", headers=self._token)
