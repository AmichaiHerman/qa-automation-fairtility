from requests import Response

from automation_sdk.http_requests.request_util import make_request
from automation_sdk.http_requests.service_routes import ServiceRoutes
from test.shared_packages.atomic_types import UserCredentials
from test.shared_packages.chloe_backend.lab_connector.types.create_connector_request import (
    ConnectorCreationRequest,
)
from test.shared_packages.chloe_backend.lab_connector.types.lab_sync_request import (
    LabSyncRequest,
)
from automation_sdk.http_requests.http_client import HttpClient as http


class LabConnectorRoutes(ServiceRoutes):
    """
    An interface for connecting with Chloe's Lab Connector Service
    """

    def __init__(self, creds: UserCredentials):
        super().__init__(creds)
        self._lab_sync_url = self._base_url + "lab-sync"
        self._connector_manager_url = self._base_url + "connector-manager"
        self._connectors_url = self._base_url + "connectors"

    @make_request(action="Perform health check")
    def check_health(self) -> Response:
        return http.get(
            url=self._connector_manager_url + "/healthz", headers=self._token
        )

    # todo change this route to be the new route
    @make_request(action="Creating New Connector")
    def create_new_connector(self, payload: ConnectorCreationRequest) -> Response:
        return http.post(url=self._connectors_url, payload=payload, headers=self._token)

    @make_request(action="Triggering Connector Manager")
    def trigger_lab_sync(self) -> Response:
        return http.post(
            url=self._connector_manager_url + "/lab-sync",
            payload=None,
            headers=self._token,
        )

    @make_request(action="Triggering Lab Sync")
    def perform_lab_sync(self, payload: LabSyncRequest) -> Response:
        return http.post(
            url=self._lab_sync_url + "/sync", payload=payload, headers=self._token
        )
