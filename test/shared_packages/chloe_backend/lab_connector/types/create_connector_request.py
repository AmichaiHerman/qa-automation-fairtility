from __future__ import annotations

from typing import List, Optional

from pydantic import BaseModel


class LabStorageSettings(BaseModel):
    storage_type: str


class LabApi(BaseModel):
    url: str
    user: str
    password: str
    version: str
    batchSize: int


class ConnectorCreationRequest(BaseModel):
    timeZone: str
    isActive: bool
    tenantId: str
    labApi: LabApi
    labStorageSettings: Optional[LabStorageSettings]
    focals: List[int]
    name: str
    connectorType: str
