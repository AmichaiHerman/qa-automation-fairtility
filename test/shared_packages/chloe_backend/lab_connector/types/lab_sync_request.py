from enum import Enum

from pydantic import BaseModel


class SyncType(Enum):
    FULL_SYNC = "full"
    SLIDES_SYNC = "slides"


class LabSyncRequest(BaseModel):
    connectorId: str
    syncType: str
