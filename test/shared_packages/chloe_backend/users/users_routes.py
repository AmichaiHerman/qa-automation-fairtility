from requests import Response

from automation_sdk.http_requests.request_util import make_request
from automation_sdk.http_requests.service_routes import ServiceRoutes
from test.shared_packages.atomic_types import UserCredentials
from test.shared_packages.chloe_backend.users.types.onboarding_response import (
    OnboardingResponse,
)
from test.shared_packages.chloe_backend.users.types.user_data_response import (
    UserDataResponse,
)
from test.shared_packages.chloe_backend.users.types.user_settings import UserSettings
from automation_sdk.http_requests.http_client import HttpClient as http


class UsersRoutes(ServiceRoutes):
    """
    An interface for connecting with Chloe's Users Service
    """

    def __init__(self, creds: UserCredentials):
        super().__init__(creds)
        self._users_prefix = self._base_url + "/users"
        self._user_data_url = self._users_prefix + "/me/user-data/"
        self._onboarding_url = self._users_prefix + "/onboarding"

    @make_request(action="Perform health check")
    def check_health(self) -> Response:
        return http.get(url=self._users_prefix + "/healthz", headers=self._token)

    @make_request(action="Create User")
    def create_user(self, payload: UserSettings) -> Response:
        return http.post(url=self._users_prefix, payload=payload, headers=self._token)

    @make_request(action="GET User Data", dto=UserDataResponse)
    def get_user_data(self) -> UserDataResponse:
        return http.get(url=self._user_data_url, headers=self._token)

    @make_request(action="GET Onboarding Data", dto=OnboardingResponse)
    def get_onboarding_data(self) -> OnboardingResponse:
        return http.get(url=self._onboarding_url, headers=self._token)
