from __future__ import annotations

from typing import List, Dict, Optional

from pydantic import BaseModel


class UserSettings(BaseModel):
    userName: str
    firstName: str
    lastName: str
    imageUrl: str
    email: str
    role: Optional[str]
    tenants: List[Dict]
    isPasswordTemp: bool
