from __future__ import annotations

from typing import List

from pydantic import BaseModel


class WellCards(BaseModel):
    title: str
    videoUrl: str


class CompareWells(BaseModel):
    title: str
    videoUrl: str


class PerformanceTable(BaseModel):
    title: str
    videoUrl: str


class Treatments(BaseModel):
    well_cards: WellCards
    compare_wells: CompareWells
    performance_table: PerformanceTable


class Default(BaseModel):
    title: str
    videoUrl: str


class Instruments(BaseModel):
    default: Default


class Default1(BaseModel):
    title: str
    videoUrl: str


class Wells(BaseModel):
    default: Default1


class ComponentTutorials(BaseModel):
    Treatments: Treatments
    Instruments: Instruments
    Wells: Wells


class Video(BaseModel):
    _id: int
    title: str
    description: str
    videoLength: str
    previewImageUrl: str
    videoUrl: str


class Manual(BaseModel):
    _id: int
    category: str
    title: str
    description: str
    fileUrl: str


class Faq(BaseModel):
    _id: int
    q: str
    a: str


class HelpCenterContent(BaseModel):
    videos: List[Video]
    manuals: List[Manual]
    faqs: List[Faq]
    documents: List


class OnboardingResponse(BaseModel):
    componentTutorials: ComponentTutorials
    helpCenterContent: HelpCenterContent
