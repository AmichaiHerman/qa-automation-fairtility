from __future__ import annotations

from typing import List

from pydantic import BaseModel


class Settings(BaseModel):
    authorizedOrigins: List
    authEnabled: bool
    isFDA: bool
    blastScoreLow: int
    blastScoreHigh: int
    kidScoreLow: int
    kidScoreHigh: int
    instrumentSize: int
    slideSize: int
    availableMkEvents: List[str]
    mainEntity: str
    isVRepro: bool


class Tenant(BaseModel):
    settings: Settings
    name: str


class UserDataResponse(BaseModel):
    idpId: str
    firstName: str
    lastName: str
    imageUrl: str
    email: str
    tenants: List[Tenant]
    __v: int
    favoriteItems: List
