from requests import Response

from automation_sdk.atomic_types import UserCredentials
from automation_sdk.http_requests.http_client import HttpClient as http
from automation_sdk.http_requests.request_util import make_request
from automation_sdk.http_requests.service_routes import ServiceRoutes


class SlideRevisionManagerRoutes(ServiceRoutes):
    def __init__(self, creds: UserCredentials):
        super().__init__(creds)
        self.__service_prefix = self._base_url + "slide-revision-manager"

    @make_request(action="Perform health check")
    def check_health(self) -> Response:
        return http.get(url=self.__service_prefix + "/healthz", headers=self._token)
