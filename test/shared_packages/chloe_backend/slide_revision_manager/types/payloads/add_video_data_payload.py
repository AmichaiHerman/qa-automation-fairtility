from __future__ import annotations

from typing import List

from pydantic import BaseModel


class HighItem(BaseModel):
    focal: int
    videoUrl: str
    videoLength: float


class LowItem(BaseModel):
    focal: int
    videoUrl: str
    videoLength: float


class Videos(BaseModel):
    high: List[HighItem]
    low: List[LowItem]
    fps: int


class VideoDataPayload(BaseModel):
    esId: str
    runNumber: int
    lastImageUrl: str
    lastImageHour: int
    videos: Videos
    videoFramesTime: List[int]
