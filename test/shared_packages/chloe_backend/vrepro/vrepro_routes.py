from typing import Union

from requests import Response

from automation_sdk.atomic_types import UserCredentials
from automation_sdk.http_requests.http_client import HttpClient as http
from automation_sdk.http_requests.request_util import make_request
from automation_sdk.http_requests.service_routes import ServiceRoutes
from test.shared_packages.chloe_backend.vrepro.types.sync_payload import (
    VreproSyncPayload,
)
from test.shared_packages.chloe_backend.vrepro.types.topic_handler_payload import (
    TopicHandlerPayload,
)


class VreproRoutes(ServiceRoutes):
    def __init__(self, creds: UserCredentials):
        super().__init__(creds)
        self.__service_prefix = self._base_url + "vrepro"
        self.__sync_patients_url = self.__service_prefix + "/patients"
        self.__topic_handler_url = self.__service_prefix + "/vrepro_topic_handler"

    @make_request(action="Perform health check")
    def check_health(self) -> Response:
        return http.get(url=self.__service_prefix + "/healthz", headers=self._token)

    @make_request(action="Export patient data to vRepro")
    def export_patient_data(self, payload: VreproSyncPayload) -> Response:
        return http.post(
            url=self.__sync_patients_url, payload=payload, headers=self._token
        )

    @make_request(action="Invoke Topic Handler")
    def invoke_topic_handler(self, payload: TopicHandlerPayload) -> Response:
        return http.post(
            url=self.__topic_handler_url, payload=payload, headers=self._token
        )
