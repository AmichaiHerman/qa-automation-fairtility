from __future__ import annotations

from typing import List, Optional, Any

from pydantic import BaseModel

from chloe.shared.types.request_data import RequestPayload


class Patient(BaseModel):
    esId: str
    id: Optional[str]


class Instrument(BaseModel):
    id: str
    esId: str


class Treatment(BaseModel):
    id: str


class ReqSlide(BaseModel):
    id: str
    esId: str
    wells: List[str]


class VreproSyncPayload(RequestPayload):
    patient: Patient
    tenantId: str
    instrument: Instrument
    treatment: Treatment
    slide: ReqSlide
