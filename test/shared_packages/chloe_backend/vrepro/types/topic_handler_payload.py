from __future__ import annotations

from pydantic import BaseModel


class Message(BaseModel):
    data: str
    messageId: str
    message_id: str
    publishTime: str
    publish_time: str


class TopicHandlerPayload(BaseModel):
    message: Message
    subscription: str
