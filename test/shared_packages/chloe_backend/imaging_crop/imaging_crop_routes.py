from requests import Response

from automation_sdk.http_requests.request_util import make_request
from automation_sdk.http_requests.service_routes import ServiceRoutes
from automation_sdk.http_requests.http_client import HttpClient as http
from test.shared_packages.atomic_types import UserCredentials
from test.shared_packages.chloe_backend.imaging.types.requests_data import (
    CreateTimelapsePayload,
)


class ImagingCropRoutes(ServiceRoutes):
    """
    An interface for connecting with Chloe's Imaging Service
    """

    def __init__(self, creds: UserCredentials):
        super().__init__(creds)
        self._service_prefix = self._base_url + "/imaging-crop"
        self._timelapse_route = self._service_prefix + "/video"

    @make_request(action="Perform Health Check")
    def check_health(self) -> Response:
        return http.get(url=self._service_prefix + "/healthz", headers=self._token)

    @make_request(action="Create Timelapse")
    def create_timelapse(self, payload: CreateTimelapsePayload) -> Response:
        return http.post(
            url=self._timelapse_route, payload=payload, headers=self._token
        )
