from typing import List, Union

from requests import Response

from automation_sdk.atomic_types import UserCredentials
from automation_sdk.http_requests.http_client import HttpClient as http
from automation_sdk.http_requests.request_util import make_request
from automation_sdk.http_requests.service_routes import ServiceRoutes

from test.shared_packages.chloe_backend.patient_data.types.requests_data.payloads import (
    PostNote,
)


class NotesRoutes(ServiceRoutes):
    def __init__(self, creds: UserCredentials):
        super().__init__(creds)
        self.__service_prefix = self._base_url + "notes"

    @make_request(action="Perform health check")
    def check_health(self) -> Response:
        return http.get(url=self.__service_prefix + "/healthz", headers=self._token)

    @make_request(action="Add a note")
    def post_note(self, payload: PostNote) -> Response:
        return http.post(
            url=self.__service_prefix, payload=payload, headers=self._token
        )

    @make_request(action="Get notes", dto=PostNote)
    def get_notes(self, well_id: str) -> Union[Response, PostNote]:
        return http.get(
            url=self.__service_prefix + "?objectId=" + well_id,
            headers=self._token,
        )

    @make_request(action="Delete note")
    def delete_note(self, well_id: str) -> Union[Response, PostNote]:
        return http.delete(
            url=self.__service_prefix + "/" + well_id, headers=self._token
        )
