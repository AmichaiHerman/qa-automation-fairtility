from pydantic import BaseModel


class Author(BaseModel):
    _id: str
    firstName: str
    lastName: str
    imageUrl: str
    email: str


class NotesDataResponse(BaseModel):
    _id: str
    authorId: str
    content: str
    entityName: str
    relatedDocId: str
    tenantId: str
    createdAt: str
    updatedAt: str
    __v: int
    author: Author
