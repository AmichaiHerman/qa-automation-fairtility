from __future__ import annotations

from typing import List

from pydantic import BaseModel, Field


class FivLabLinItem(BaseModel):
    fiv_lab: int
    fiv_lab_ref: str = Field(..., alias="fiv_lab.ref")
    id: int
    num_ovo: int
    zi_pro_num: int
    estado_dia1_name: str = Field(..., alias="estado_dia1.name")
    pre2_bla_num: int
    pre2_frag_name: str = Field(..., alias="pre2_frag.name")
    pre2_div_dir_name: str = Field(..., alias="pre2_div_dir.name")
    pre3_bla_num: int
    pre3_mor_name: str = Field(..., alias="pre3_mor.name")
    pre3_frag_name: str = Field(..., alias="pre3_frag.name")
    pre4_bla_num: int
    pre4_gra_exp_name: str = Field(..., alias="pre4_gra_exp.name")
    pre5_gra_exp_name: str = Field(..., alias="pre5_gra_exp.name")
    destino_name: str = Field(..., alias="destino.name")
    pre6_gra_exp_name: str = Field(..., alias="pre6_gra_exp.name")
    estado_dia1: str
    dgp_diag_1: str
    dgp_diag_1_name: str = Field(..., alias="dgp_diag_1.name")
    fiv_lab_del_id: str = Field(..., alias="fiv_lab.del.id")


class VreproEmbryoResponse(BaseModel):
    count: int
    total_count: int
    fiv_lab_lin: List[FivLabLinItem]
