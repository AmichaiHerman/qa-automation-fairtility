import os
from typing import Union

from requests import Response

from automation_sdk.http_requests.http_client import HttpClient as http
from automation_sdk.http_requests.request_util import make_request
from test.shared_packages.third_party_intgreations.vrepro.vrepro_embryo_response import (
    VreproEmbryoResponse,
)


class VreproIntegration:
    """A client used for integrating with the vrepro test server"""

    def __init__(self):
        self._base_url = (
            os.getenv("VREPRO_BASE_URL") + "/fairtility/VRepro_Datos_dat/v1/"
        )
        self._ivf_lab_data = self._base_url + "fiv_lab_lin/"

    # todo maybe add retries
    @make_request(
        action="Check vRepro Embryo Monitoring Data", dto=VreproEmbryoResponse
    )
    def check_embryo_monitoring_data(
        self, embryo_id
    ) -> Union[Response, VreproEmbryoResponse]:
        print(self.__create_embryo_monitoring_url(vrepro_embryo_id=embryo_id))
        return http.get(
            url=self.__create_embryo_monitoring_url(vrepro_embryo_id=embryo_id),
            headers=None,
        )

    def __create_embryo_monitoring_url(self, vrepro_embryo_id: int) -> str:
        return (
            self._ivf_lab_data
            + f"""{vrepro_embryo_id}?api_key={os.getenv("VREPRO_API_KEY")}"""
        )
