from test.shared_packages.chloe_backend.imaging.types.requests_data import (
    CreateTimelapsePayload,
)
from test.shared_packages.payload_builders.timelapse_creation_builder import (
    timelapse_creation_request_payload,
)


def vitro_timelapse_request(run_number: int) -> CreateTimelapsePayload:
    return _generic_timelapse_request(
        run_number=run_number,
        vendor_type="VITRO",
        root_path="6422ffc708db53ba97e46403_1",
        last_image_url="https://storage.googleapis.com/kidplus-bucket-dev/6422ffc708db53ba97e46403_2%2Fimages%2F0%2F244728000.jpeg"
    )


def miri_timelapse_request(run_number: int) -> CreateTimelapsePayload:
    return _generic_timelapse_request(
        run_number=run_number,
        vendor_type="MIRI",
        root_path="64242578d8854f2b5acf6142_11",
        last_image_url="https://storage.googleapis.com/kidplus-bucket-dev/64242578d8854f2b5acf6142_11%2Fcropped_images%2F0%2F148420000.jpeg",
    )


def geri_timelapse_request(run_number: int) -> CreateTimelapsePayload:
    return _generic_timelapse_request(
        run_number=run_number,
        vendor_type="GERI",
        root_path="63d25418e3e6385efcf10999_2",
        last_image_url="https://storage.googleapis.com/kidplus-bucket-dev/63d25418e3e6385efcf10999_2/cropped_images/0/495601000.jpeg",
    )


def _generic_timelapse_request(
    run_number: int, vendor_type: str, root_path: str, last_image_url: str
) -> CreateTimelapsePayload:
    return timelapse_creation_request_payload(
        slide_id="123",
        well_es_id="1",
        run_number=run_number,
        last_image_hour=114138000,
        last_image_url=last_image_url,
        root_path=root_path,
        focals=[0, -3, -2, -1, 1, 2, 3],
        images_format="jpeg",
        instrument_type=vendor_type
    )
