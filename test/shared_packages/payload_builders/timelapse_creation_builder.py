from typing import List

from test.shared_packages.atomic_types import Vendor
from test.shared_packages.chloe_backend.imaging.types.requests_data import (
    CreateTimelapsePayload,
)


def timelapse_creation_request_payload(
    slide_id: str,
    well_es_id: str,
    run_number: str,
    last_image_hour: int,
    last_image_url: str,
    root_path: str,
    instrument_type: Vendor,
    focals: List[int],
    images_format: str = "jpeg",
) -> CreateTimelapsePayload:
    return CreateTimelapsePayload(
        slideId=slide_id,
        wellEsId=well_es_id,
        runNumber=run_number,
        lastImageHour=last_image_hour,
        lastImageUrl=last_image_url,
        rootPath=root_path,
        instrumentType=instrument_type,
        focals=focals,
        imagesFormat=images_format,
    )
