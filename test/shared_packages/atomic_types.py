from enum import Enum
from pydantic import BaseModel


class UserCredentials(BaseModel):
    user_name: str
    password: str


class Vendor(Enum):
    VITRO = "VITRO"
    GERI = "GERI"
    MIRI = "MIRI"
