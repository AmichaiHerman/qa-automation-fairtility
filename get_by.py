from typing import Callable


def add(x: int, y: int) -> int:
    return x + y


# get by value -> 1
# get by name -> bring the fucking pointer (pointer in memory of the object 1)


def add_2(x: int, y: int, adding_func: Callable) -> int:
    return adding_func(x, y)


print(add_2(1, 3, add))
